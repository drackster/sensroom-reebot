﻿using System;
using Xamarin.Forms;
using System.IO;

namespace ProjectX.Models
{
    public class XImageSource
    {
        public long AssetId { get; set; }
        public long ImageId { get; set; }
        public Image Img { get; set; }
    }
}
