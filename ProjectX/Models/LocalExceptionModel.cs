﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.Models
{
    public class LocalExceptionModel
    {
        [PrimaryKey]
        public string Id { get; set; }
        public DateTime CreatedUTC { get; set; }
        public string Message { get; set; }
        public string InnerMessage { get; set; }
        public string ViewName { get; set; }
        public bool IsSynced { get; set; }
    }
}
