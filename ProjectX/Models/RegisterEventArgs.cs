﻿using System;
using ProjectX.ViewModels;

namespace ProjectX.Models
{
    public class RegisterEventArgs : EventArgs
    {
        private RegisterViewModel _model { get; set; }
        public RegisterEventArgs(RegisterViewModel model)
        {
            _model = model;
        }
        public RegisterViewModel Model { get { return _model; } }
  
    }
}
