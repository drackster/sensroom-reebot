﻿#region Copyright Syncfusion Inc. 2001 - 2017
// Copyright Syncfusion Inc. 2001 - 2017. All rights reserved.
// Use of this code is subject to the terms of our license.
// A copy of the current license can be obtained at any time by e-mailing
// licensing@syncfusion.com. Any infringement will be prosecuted under
// applicable laws. 
#endregion
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Syncfusion.SfSchedule.XForms;

namespace ProjectX.Pages.Asset
{
    public class ScheduleViewModel
    {
        public ObservableCollection<Meeting> ListOfMeeting { get; set; }
        private List<Color> color_collection;
        public ScheduleViewModel(List<InvitationViewModel> orders)
        {
            ListOfMeeting = new ObservableCollection<Meeting>();
            InitializeColors();

            int c = 0;
            foreach(var o in orders)
            {
                Meeting meeting = new Meeting();
                meeting.OrderId = o.OrderId;
                meeting.From = o.EventDateTime.Value;
                meeting.To = o.EventEnd.Value;
                meeting.CustomerName = o.CustomerName;// + " -" + o.StatusName;
                meeting.Notes = string.IsNullOrEmpty(o.Comments) ? "" : o.Comments;
                meeting.Location = "";// o.Address1;
                meeting.color = color_collection[c];
                ListOfMeeting.Add(meeting);

                c++;
                if (c == color_collection.Count)
                    c = 0;//reset
            }
        }
        private void InitializeColors()
        {
            color_collection = new List<Color>();
            color_collection.Add(Color.FromHex("#000000"));
            color_collection.Add(Color.FromHex("#9b00b2"));
            color_collection.Add(Color.FromHex("#008e72"));
            color_collection.Add(Color.FromHex("#1153fc"));
            color_collection.Add(Color.FromHex("#000000"));
            color_collection.Add(Color.FromHex("#000000"));
        }
    }
    public class Meeting
    {
        public string CustomerName { get; set; }
        public long OrderId { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public Color color { get; set; }
        public string Notes { get; set; }
        public string Location { get; set; }
    }
}



