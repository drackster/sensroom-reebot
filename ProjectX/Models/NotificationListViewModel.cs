﻿//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using ProjectX.ViewModels;

//namespace ProjectX.Models
//{
//    public class NotificationListViewModel
//    {
//        public ObservableCollection<NotificationViewModel> items { get; set; }
//        private bool _isRefreshing = false;
//        public bool IsRefreshing
//        {
//            get { return _isRefreshing; }
//            set
//            {
//                _isRefreshing = value;
//                OnPropertyChanged(nameof(IsRefreshing));
//            }
//        }
//        public ICommand RefreshCommand
//        {
//            get
//            {
//                return new Command(async () =>
//                {
//                    IsRefreshing = true;

//                    await RefreshData();

//                    IsRefreshing = false;
//                });
//            }
//        }
//    }
//}
