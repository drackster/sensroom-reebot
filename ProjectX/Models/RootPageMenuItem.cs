﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;

namespace ProjectX.Pages
{

    public class RootPageMenuItem
    {
        public RootPageMenuItem()
        {
            //TargetType = typeof(RootPageDetail);
        }
        public int Id { get; set; }
        public MessageSender MSender { get; set; }
        public string Title { get; set; }
        public Type TargetType { get; set; }
        public string Icon { get; set; }
        public bool ShowCircle { get { return Badge > 0; } }
        public int Badge { get; set; }
       
    }
}