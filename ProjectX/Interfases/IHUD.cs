﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.Interfases
{
    public enum MaskType
    {
        None = 1,
        Clear,
        Black,
        Gradient,
        Red
    }
    /// <summary>
    /// this is the interface for displaying native Message Overlays, Boxes, Toast, etc.
    /// </summary>
    public interface IHUD
    {
        void Show(string message, MaskType maskType = MaskType.Black, int progress = -1);
        void Dismiss();
        void ShowToast(string message, bool showToastCentered = true, double timeoutMs = 1000);
        void ShowToast(string message, MaskType maskType, bool showToastCentered = true, double timeoutMs = 1000);
        void SetStatus(string status);
        void ShowSuccessWithStatus(string status, double timeoutMs = 1000);
        void ShowErrorWithStatus(string status, double timeoutMs = 1000);
    }
}
