﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.Interfases
{
    public interface IKeyboardNotifications
    {
        event EventHandler KeyboardShowing;
        void StartListening();
        void StopListening();
    }
    public class KeyboardHeightEventArgs : EventArgs
    {
        private double _height;
        public KeyboardHeightEventArgs(double height)
        {
            _height = height;
        }
        public double Height { get { return _height; } }
    }
}
