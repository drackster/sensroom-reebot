﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.Interfases
{
    public interface INotificationHelper
    {
        void StartAuthorization();

        event EventHandler<DeviceTokenEventArgs> DeviceTokenChanged;
        void SetIconBadgeNumber(int num);
    }
    public class DeviceTokenEventArgs : EventArgs
    {
        private string deviceToken;
        private string errorMsg;
        public DeviceTokenEventArgs(string Token, string ErrorMsg)
        {
            this.deviceToken = Token;
            this.errorMsg = ErrorMsg;
        }
        public string DeviceToken { get { return deviceToken; } }
        public string ErrorMsg { get { return errorMsg; } }
    }
}
