﻿using System;
using System.Threading.Tasks;

namespace ProjectX.Interfases
{
    public interface IConektaHelper
    {
        Task<string> TokenizeCard(string cardNumber, string name, string cvc, int year, int month);
    }
}
