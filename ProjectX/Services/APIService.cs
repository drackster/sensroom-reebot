﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using ProjectX.Pages.Templates;
using System.IO;
using ProjectX.Models;
using System.Collections.ObjectModel;
using System.Threading;

namespace ProjectX.Services
{
    public class APIService
    {
        public string BaseUrl = "http://luxx-dev.azurewebsites.net/api/";

       //public string BaseUrl = "http://10.26.38.22:59607/api/";


        #region Private Members
        private HttpClient _client { get; set; }
#endregion

#region Public Properties
        public UserToken SessionToken { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }


        public string FormatImgUrl(string imageId)
        {
            return App.apiService.BaseUrl + "asset/getimage/" + imageId;
        }
        public string FormatImgUrl(long AssetId)
        {
            return App.apiService.BaseUrl + "asset/getassetcoverimage/" + AssetId.ToString();
        }
        private FormUrlEncodedContent Credentials
        {
            get
            {
                return new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("userName", UserName),
                    new KeyValuePair<string, string>("password", UserPassword)
                });
            }
        }
        private FormUrlEncodedContent RegisterCredentials
        {
            get
            {
                return new FormUrlEncodedContent(new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("email", UserName),
                    new KeyValuePair<string, string>("password", UserPassword),
                    new KeyValuePair<string, string>("confirmPassword", UserPassword)
                });
            }
        }

        #endregion

        #region version
        public async Task<bool> GetVersion(string ver)
        {
            bool verision = false;
            HttpClient client = new HttpClient(); //Anonymous
            var Response = await client.GetAsync(BaseUrl + "version/getversion/" + ver);
            ProcessResponse(Response);
            await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
            {
                verision = JsonConvert.DeserializeObject<bool>(readTask.Result);
            });
            return verision;
        }

        public async Task<bool> GetFiltroPais(string Clave)
        {
            bool verision = false;
            HttpClient client = new HttpClient(); //Anonymous
            var Response = await client.GetAsync(BaseUrl + "version/getfiltropais/" + Clave);
            ProcessResponse(Response);
            await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
            {
                verision = JsonConvert.DeserializeObject<bool>(readTask.Result);
            });
            return verision;
        }


        public async Task<GetLeyendas> GetLeyenda(int id)
        {
            GetLeyendas leyenda = new GetLeyendas();
            HttpClient client = new HttpClient();
            var Response = await client.GetAsync(BaseUrl + "version/getleyenda/" + id);
            ProcessResponse(Response);
            await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
            {
                leyenda = JsonConvert.DeserializeObject<GetLeyendas>(readTask.Result);
            });
            return leyenda;
        }

        #endregion

        #region Security
        public async Task<RegisterViewModel> ValidateCode(RegisterViewModel model)
        {
            //StartAnimation();
            try
            {
                HttpClient client = new HttpClient(); //Anonymous
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "user/validatecode/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    model = JsonConvert.DeserializeObject<RegisterViewModel>(readTask.Result);
                });
                return model;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
           
        }
        public async Task<RegisterViewModel> ValidateUserName(RegisterViewModel model)
        {
            //StartAnimation();
            try
            {
                HttpClient client = new HttpClient(); //Anonymous
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "user/validateusername/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    model = JsonConvert.DeserializeObject<RegisterViewModel>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
            return model;
        }
        public async Task<RegisterViewModel> Register(RegisterViewModel model)
        {
            //StartAnimation();
            try
            {
                var json = JsonConvert.SerializeObject(model);

                HttpClient client = new HttpClient(); //Anonymous
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "user/register/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    model = JsonConvert.DeserializeObject<RegisterViewModel>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
            return model;
        }
        public async Task<RegisterViewModel> Login(RegisterViewModel model)
        {
            //StartAnimation();
            try
            {
                HttpClient client = new HttpClient(); //Anonymous
                var json = JsonConvert.SerializeObject(model);
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "user/login/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    model = JsonConvert.DeserializeObject<RegisterViewModel>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
            return model;
        }
        //public async Task<RegisterViewModel> GetRequestStatus(RegisterViewModel model)
        //{
        //    StartAnimation();
        //    try
        //    {
        //        HttpClient client = new HttpClient(); //Anonymous
        //        HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
        //        var Response = await client.PostAsync(BaseUrl + "user/getrequeststatus/", contentPost);
        //        ProcessResponse(Response);
        //        await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
        //        {
        //            model = JsonConvert.DeserializeObject<RegisterViewModel>(readTask.Result);
        //        });
        //        return model;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(ex);
        //        throw (ex);
        //    }
        //    finally
        //    {
        //        StopAnimation();
        //    }

        //}
        public async Task<bool> UpdateRequestStatus(RegisterViewModel model)
        {
            //StartAnimation();
            try
            {
                bool result = false;
                HttpClient client = new HttpClient(); //Anonymous
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "user/updaterequeststatus/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
        }

        public async Task<List<RegisterViewModel>> GetActivationRequest()
        {
            //StartAnimation();
            try
            {
                List<RegisterViewModel> result = new List<RegisterViewModel>();
                HttpClient client = new HttpClient(); //Anonymous
                var Response = await client.GetAsync(BaseUrl + "user/getactivationrequest");
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<RegisterViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            finally
            {
                //StopAnimation();
            }
        }
        public async Task<UserToken> GetAuthorizationToken()
        {
            HttpClient _client = new HttpClient();
            //_client.Timeout = new TimeSpan(10000);
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            UserToken token = new UserToken();
            try
            {
                var requestTask = await _client.PostAsync(BaseUrl.Replace("api/", "") + "token", Credentials);
                if (requestTask.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    await requestTask.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                    {
                        token = JsonConvert.DeserializeObject<UserToken>(readTask.Result);
                    });
                    token.Authenticated = true;
                }
                //token.ReasonCode = requestTask.StatusCode;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }
            return token;
        }
        private async Task<HttpClient> Client(bool IsStream=false)
        {
            if (SessionToken == null)
            {
                SessionToken = new UserToken();
            }

            if ((UserName == null || UserPassword == null) && SessionToken.UserName != null)
            {
                UserName = SessionToken.UserName;
                UserPassword = SessionToken.Password;
            }
            if (SessionToken == null || !SessionToken.Authenticated)// || !Lava.Shared.Helpers.Util.IsTokenValid(LavaToken))
            {
                if ((UserName == null || UserPassword == null) && SessionToken.UserName != null)
                {
                    UserName = SessionToken.UserName;
                    UserPassword = SessionToken.Password;
                }
                SessionToken = await GetAuthorizationToken();
            }
            _client = new HttpClient();

            if (IsStream)
            {
                //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/octet-stream"));
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("image/png"));
                //_client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("image/jpeg"));
            }
            else
            {
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            _client.DefaultRequestHeaders.Add("Authorization", SessionToken.TokenType + " " + SessionToken.AccessToken);
            _client.MaxResponseContentBufferSize = 1024 * 1024 * 10;
            _client.Timeout = new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs);
            return _client;
        }

#endregion

#region Shared
        private void ProcessResponse(HttpResponseMessage Response)
        {
            string errorMsg = string.Empty;
            if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                return;
            if (Response.StatusCode == System.Net.HttpStatusCode.InternalServerError)
                errorMsg = Response.Content.ReadAsStringAsync().Result;
            else
                errorMsg = ((System.Net.HttpStatusCode)Response.StatusCode).ToString();
            Exception ex = new Exception(errorMsg);
            throw (ex);
        }
        public void LogException(Exception ex)
        {
            //TODO
            System.Diagnostics.Debug.WriteLine("Error:" + ex.Message);
            throw (ex);
        }
#endregion

#region Progress Animation
        //public async Task StartAnimation()
        //{
        //    if (App.NavBar == null) return;
        //    var prog = App.NavBar.NavProgessBar;
        //    prog.Progress = .2;
        //    prog.IsEnabled = prog.IsVisible = true;
        //    prog.ProgressTo(1, 500, Xamarin.Forms.Easing.Linear);
        //}
        //public void StopAnimation()
        //{
        //    if (App.NavBar == null) return;
        //    var prog = App.NavBar.NavProgessBar;
        //    prog.Progress = 0;
        //    prog.IsEnabled = prog.IsVisible = false;
        //}

#endregion

#region Asset
        public async Task<AssetViewModel> GetAsset(CancellationToken cts)
        {
            try
            {
                AssetViewModel result = new AssetViewModel();
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/getasset",cts);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<AssetViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<bool> SaveAsset(AssetViewModel model)
        {
            try
            {
                //quitar las imagenes clonando
                model = ViewModels.Helper.CloneAsset(model);
                var json = JsonConvert.SerializeObject(model);
                bool result = new bool();
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/saveasset/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }
        public async Task<bool> UpdateAssetLocation(AssetLocationModel model)
        {
            try
            {
                bool result = new bool();
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/updateassetlocation/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }

        public async void assteOffline(long asstId)
        {
            try
            {
                bool result = new bool();
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/assetoffline/" + asstId);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<bool> assteOnlineValidate(long asstId)
        {
            try
            {
                bool result = new bool();
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/assetonlineValidate/" + asstId);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
                
            }
        }

        #endregion

        #region Images
        public async Task<AssetImageViewModel> SaveImage(byte[] bytes)
        {
            try
            {
                AssetImageViewModel result = null;
                HttpClient client = await Client(true);

                var imageStream = new ByteArrayContent(bytes);
                var Response = await client.PostAsync(BaseUrl + "asset/saveimage/", imageStream);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<AssetImageViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }

        //public async Task<byte[]> GetImage(long Id)
        //{
        //    AssetImageViewModel model = null;
        //    try
        //    {
        //        HttpClient client = await Client(true);
        //        //var Response = await client.GetByteArrayAsync(BaseUrl + "asset/getimage/" + Id.ToString());
        //        var Response = await client.GetAsync(BaseUrl + "asset/getimage/" + Id.ToString());
        //        ProcessResponse(Response);
        //        await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
        //        {
        //            model = JsonConvert.DeserializeObject<AssetImageViewModel>(readTask.Result);
        //        });
        //        //return Response;
        //        return model.ImgSource;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(ex);
        //        throw (ex);
        //    }
        //}
        //public async Task<List<AssetImageViewModel>> GetImageList()
        //{
        //    try
        //    {
        //        List<AssetImageViewModel> result = new List<AssetImageViewModel>();
        //        HttpClient client = await Client();
        //        var Response = await client.GetAsync(BaseUrl + "asset/getimagelist", HttpCompletionOption.ResponseHeadersRead);
        //        ProcessResponse(Response);
        //        await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
        //        {
        //            result = JsonConvert.DeserializeObject<List<AssetImageViewModel>>(readTask.Result);
        //        });
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(ex);
        //        throw (ex);
        //    }
        //}
        public async Task<bool> DeleteImage(string Id)
        {
            try
            {
                bool result = false;
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/deleteimage/" + Id, HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        //public async Task<byte[]> GetCoverImage(long AssetId)
        //{
        //    AssetImageViewModel model = null;
        //    try
        //    {
        //        HttpClient client = await Client(true);
        //        //var Response = await client.GetByteArrayAsync(BaseUrl + "asset/getimage/" + Id.ToString());
        //        var Response = await client.GetAsync(BaseUrl + "asset/getcoverimage/" + AssetId.ToString());
        //        ProcessResponse(Response);
        //        await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
        //        {
        //            model = JsonConvert.DeserializeObject<AssetImageViewModel>(readTask.Result);
        //        });
        //        //return Response;
        //        return model.ImgSource;
        //    }
        //    catch (Exception ex)
        //    {
        //        LogException(ex);
        //        throw (ex);
        //    }
        //}
        public async Task<bool> RearangeImages(List<AssetImageViewModel> model)
        {
            try
            {
                bool result = new bool();
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/rearangeimages/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }

        public async Task<bool> SetImageProfile(string Id)
        {
            try
            {
                bool result = false;
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/setprofileimage/" + Id, HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
#endregion

#region Categories and Services
        public async Task<ASettingsViewModel> GetSettings()
        {
            try
            {
                
                HttpClient client = await Client();
                ASettingsViewModel result = new ASettingsViewModel();
                var Response = await client.GetAsync(BaseUrl + "asset/getsettings");
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<ASettingsViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<bool> SaveSettings(ASettingsViewModel model)
        {
            try
            {
                bool result = false;
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/savesettings/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<List<CalendarTimeViewModel>> GetCalendarTimes()
        {
            try
            {
                HttpClient client = await Client();
                List <CalendarTimeViewModel> result = new List<CalendarTimeViewModel>();
                var Response = await client.GetAsync(BaseUrl + "asset/getcalendartimes");
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<CalendarTimeViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

#endregion

#region Calendar
        public async Task<CalendarViewModel> SaveCalendar(CalendarViewModel model)
        {
            try
            {
                CalendarViewModel result = new CalendarViewModel();
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/savecalendar/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<CalendarViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }
        public async Task<List<CalendarViewModel>> GetCalendar()
        { 
            try
            {
                HttpClient client = await Client();
                List<CalendarViewModel> result = new List<CalendarViewModel>();
                var Response = await client.GetAsync(BaseUrl + "asset/getcalendar");
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<CalendarViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }        
        }
        public async Task<CalendarViewModel> UpdateCalendar(CalendarViewModel model)
        {
            try
            {
                CalendarViewModel result = new CalendarViewModel();
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/updatecalendar/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<CalendarViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }
        public async Task<bool> DeleteCalendar(CalendarViewModel model)
        {
            try
            {
                bool result = false;
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/deletecalendar/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }


        }

#endregion

#region Banking
        public async Task<List<ABankViewModel>> GetBankList()
        {
            try
            {
                List<ABankViewModel> result = new List<ABankViewModel>();
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/getbanklist", HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<ABankViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
#endregion

#region Notifications
        public async Task<List<NotificationViewModel>> GetNotifications(NotificationRequestModel model)
        {
            try
            {
                HttpClient client = await Client();
                List<NotificationViewModel> result = new List<NotificationViewModel>();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/getnotifications/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<NotificationViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<int> GetNotificationCount(NotificationRequestModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);
                HttpClient client = await Client();
                int result = 0;
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/getnotificationcount/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<int>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task UpdateNotificationStatus(NotificationUpdateModel model)
        {
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/updatenotificationstatus/", contentPost);
                ProcessResponse(Response);
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<NotificationViewModel> SendNotification(NotificationViewModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);

                HttpClient client = await Client();
                NotificationViewModel result = new NotificationViewModel();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/sendnotification/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<NotificationViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<NotificationViewModel> SendNotificationAdmin(NotificationViewModel model)
        {
            try
            {
                var json = JsonConvert.SerializeObject(model);

                HttpClient client = await Client();
                NotificationViewModel result = new NotificationViewModel();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/sendnotificationadmin/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<NotificationViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        #endregion

        #region Customer/Asset
        public async Task<AssetListViewModel> GetAssetList(SearchModel model)
        {
            try
            {
                AssetListViewModel result = new AssetListViewModel();
                HttpClient client = await Client();
                var json = JsonConvert.SerializeObject(model);
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/getassetlist/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<AssetListViewModel>(readTask.Result);
                });
                return result;

            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

#endregion

#region Orders
        public async Task<List<OrderViewModel>> GetOrders(OrderSearchModel model, CancellationToken cts)
        {
            try
            {

                HttpClient client = await Client();
                List<OrderViewModel> result = new List<OrderViewModel>();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/getorders/", contentPost, cts);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<OrderViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<InvitationViewModel> SaveOrder(OrderViewModel model)
        {
            var json = JsonConvert.SerializeObject(model);
            InvitationViewModel result = new InvitationViewModel();
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/saveorder/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<InvitationViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<InvitationViewModel> SaveOrderOxxo(OrderViewModel model)
        {
            var json = JsonConvert.SerializeObject(model);
            InvitationViewModel result = new InvitationViewModel();
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/saveorderoxxo/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<InvitationViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<CustomerPaymentViewModel> GetTDCData(int id)
        {         
            try
            {               
                HttpClient client = await Client();
                CustomerPaymentViewModel result = new CustomerPaymentViewModel();
                var Response = await client.GetAsync(BaseUrl + "order/getTDCData/" + id, HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<CustomerPaymentViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<List<MetodosPagoVM>> getMediosDePago()
        {
            List<MetodosPagoVM> result = new List<MetodosPagoVM>();
            try
            {
                HttpClient client = await Client();
                
                var Response = await client.GetAsync(BaseUrl + "payment/getpaymentslist/", HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<MetodosPagoVM>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<InvitationViewModel> SaveOrderAndPayment(PaymentChargeModel model)
        {
            InvitationViewModel result = new InvitationViewModel();
            try
            {
                var json = JsonConvert.SerializeObject(model);
                HttpClient client = await Client();
                //Lo extendemos a 60 segundos porque CONEKTA puede tardar mucho tiempo en procesar una TC
                client.Timeout = new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs*2);

                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/saveorderandpayment/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<InvitationViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<OrderStatusViewModel> GetOrderStatus(long OrderId)
        {
            try
            {
               
                HttpClient client = await Client();
                OrderStatusViewModel result = new OrderStatusViewModel();
                var Response = await client.GetAsync(BaseUrl + "order/getorderstatus/" + OrderId.ToString(), HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<OrderStatusViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<bool> UpdateOrderStatus(OrderViewModel model)
        {
            bool result = false;
            try
            {
                HttpClient client = await Client();
                var json = JsonConvert.SerializeObject(model);
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/updateorderstatus/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<AScheduleViewModel> GetAppointments(NotificationRequestModel model, CancellationToken cts)
        {
            try
            {
                HttpClient client = await Client();
                AScheduleViewModel result = new AScheduleViewModel();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "order/getappointments/", contentPost, cts);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<AScheduleViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<bool> SaveCustomerCredit(CustomerCredit model)
        {

            bool result = false;
            try
            {
                var json = JsonConvert.SerializeObject(model);
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "customer/savecustomercredit/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<bool> UpdateCustomerCredit(string model)
        {

            bool result = false;
            try
            {
                var json = JsonConvert.SerializeObject(model);
                HttpClient client = await Client();
               
                var Response = await client.GetAsync(BaseUrl + "customer/updatecustomercredit/" + model);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<bool> VerificaPagoOxxo(string id)
        {            
            try
            {
                HttpClient client = await Client();

                var Response = await client.GetAsync(BaseUrl + "order/verificaPagoOxxo/" + id);//
                ProcessResponse(Response);

                if (Response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            //verificaOxxo
        }

        //http://luxx-dev.azurewebsites.net/api/order/verificaPagoOxxo/ord_2ifYq8QUiefdf2k47
        //http://luxx-dev.azurewebsites.net/api/order/verificaPagoOxxo/




        #endregion

        #region Reviews
        public async Task<List<ReviewViewModel>> GetAssetReviews(long AssetId)
        {
            try
            {
                HttpClient client = await Client();
                List<ReviewViewModel> result = new List<ReviewViewModel>();
                var Response = await client.GetAsync(BaseUrl + "asset/getassetreviews/" + AssetId.ToString(), HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<ReviewViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<long> AddAssetReview(ReviewViewModel model)
        {
            try
            {
                long newReviewId = 0;
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/addassetreview/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    newReviewId = JsonConvert.DeserializeObject<long>(readTask.Result);
                });
                return newReviewId;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<List<ReviewViewModel>> GetCustomerReviews(long CustomerId)
        {
            try
            {
                HttpClient client = await Client();
                List<ReviewViewModel> result = new List<ReviewViewModel>();
                var Response = await client.GetAsync(BaseUrl + "asset/getcustomerreviews/" + CustomerId.ToString(), HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<ReviewViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<long> AddCustomerReview(ReviewViewModel model)
        {
            try
            {
                long newReviewId = 0;
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/addcustomerreview/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    newReviewId = JsonConvert.DeserializeObject<long>(readTask.Result);
                });
                return newReviewId;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

#endregion

#region Customer
        public async Task<XSettingsViewModel> GetXSettings()
        {
            try
            {
                HttpClient client = await Client();
                XSettingsViewModel result = new XSettingsViewModel();
                var Response = await client.GetAsync(BaseUrl + "customer/getxsettings");
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<XSettingsViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

#endregion

#region Payments
        public async Task<List<CustomerPaymentViewModel>> GetCustomerPayments()
        {
            List<CustomerPaymentViewModel> result = null;

            try
            {
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "customerpayment/getpayments/", HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<CustomerPaymentViewModel>>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            return result;
        }
        public async Task<CustomerPaymentViewModel> AddCustomerPayment(CustomerPaymentViewModel model)
        {
            CustomerPaymentViewModel result = new CustomerPaymentViewModel();
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "customer/addpayment", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<CustomerPaymentViewModel>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<int> UpdateCustomerPayment(CustomerPaymentViewModel model)
        {
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "customerpayment/updatepayment/", contentPost);
                ProcessResponse(Response);
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            return 1;
        }
        public async Task<bool> DeleteCustomerPayment(CustomerPaymentViewModel model)
        {
            bool result=false;
            try
            {
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "customerpayment/deletepayment/" + model.Id.ToString());
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<bool>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            return result;
        }
        public async Task<string> CreateCharge(ChargeViewModel model)
        {
            string result = null;
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "api/customerpayment/createcharge/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<string>(readTask.Result);
                });
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
            return result;
        }
#endregion

#region Balance
        public async Task<List<BalanceViewModel>> GetBalance()
        {
            try
            {
                List<BalanceViewModel> result = new List<BalanceViewModel>();
                HttpClient client = await Client();
                var Response = await client.GetAsync(BaseUrl + "asset/getbalance", HttpCompletionOption.ResponseHeadersRead);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<BalanceViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }
        public async Task<List<OrderViewModel>> GetTransactions(TransactionWeek model)
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            try
            {
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/gettransactions", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    result = JsonConvert.DeserializeObject<List<OrderViewModel>>(readTask.Result);
                });
                return result;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }

        public async Task<CustomerCredit> GetCustomCredit(string name)
        {
            CustomerCredit result = new CustomerCredit();
            HttpClient client = await Client();
            //HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
            var Response = await client.GetAsync(BaseUrl + "order/getcustomercredit/" + name);
            ProcessResponse(Response);
            await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
            {
                result = JsonConvert.DeserializeObject<CustomerCredit>(readTask.Result);
            });
            return result;     
        }

        #endregion

        #region Push Notifications
        public async Task<long> AddDeviceToken(DeviceTokenViewModel model)
        {
            try
            {
                long newTokenId = 0;
                HttpClient client = await Client();
                HttpContent contentPost = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, "application/json");
                var Response = await client.PostAsync(BaseUrl + "asset/adddevicetoken/", contentPost);
                ProcessResponse(Response);
                await Response.Content.ReadAsStringAsync().ContinueWith((readTask) =>
                {
                    newTokenId = JsonConvert.DeserializeObject<long>(readTask.Result);
                });
                return newTokenId;
            }
            catch (Exception ex)
            {
                LogException(ex);
                throw (ex);
            }
        }


#endregion

    }
}
