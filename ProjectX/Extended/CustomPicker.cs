﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xamarin.Forms;

namespace ProjectX.Extended
{
    public class CustomPicker : Picker
    {
        public bool IgnoreCenterText { get; set; }
    }
}
