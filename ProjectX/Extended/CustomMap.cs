﻿using Xamarin.Forms.Maps;

namespace ProjectX.Extended
{
    public class CustomMap : Map
    {
        public CustomCircle Circle { get; set; }
        public CustomMap()
        {
            Circle = new CustomCircle()
            {
                Position = new Position(25.663695,-100.3689),
                Radius = 1000
            };
        }
    }
    public class CustomCircle
    {
        public Position Position { get; set; }

        public double Radius { get; set; }
    }
}

