﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class NotificationBGConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var statusId = value as Int32?;
            if (statusId.HasValue && statusId != 3)
                return Color.LightGray;
            return Color.White;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
