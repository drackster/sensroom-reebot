﻿//using System;
//using System.Globalization;
//using Xamarin.Forms;
//using System.Collections.Generic;
//using ProjectX.ViewModels;
//using System.IO;

//namespace ProjectX.Converters
//{
//    public class SnapperImageConverter : IValueConverter
//    {
//        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
//        {
//            try
//            {
//                var model = value as OrderViewModel;
//                if (model != null)
//                {
//                    object snap = null;
//                    Device.BeginInvokeOnMainThread(() =>
//                    {
//                        snap = App.Snapper.GetSnapImageAsync(model.CLatitude, 
//                            model.CLongitude,
//                          App.ScreenWidth, 150);
//                    });
//                    if(snap!=null)
//                        return ImageSource.FromStream(() => (Stream)snap);
//                }
//            }
//            catch (Exception ex)
//            {
//                //ignore
//            }
//            return "loadingmsg.png";
//        }
//        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
//        {
//            return null;
//        }
//    }
//}
