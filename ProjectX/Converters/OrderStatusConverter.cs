﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class OrderStatusConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            OrderStatus status = (OrderStatus)value;
            return ViewModels.Helper.OrderStatusConverter(status);            
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
