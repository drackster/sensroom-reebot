﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class AppointmentConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var invite = value as InvitationViewModel;
            if (invite == null) return string.Empty;
            return Helpers.DTUtil.FormatAppointment(invite);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
