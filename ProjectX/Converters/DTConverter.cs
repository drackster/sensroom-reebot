﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class DTConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null) return string.Empty;
            var dt = (DateTime)value;
            return Helpers.DTUtil.ConvertDT(dt.ToLocalTime(),false,false,true,true);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
