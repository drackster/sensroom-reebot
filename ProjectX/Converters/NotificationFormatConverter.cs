﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class NotificationFormatConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            NotificationViewModel n = value as NotificationViewModel;
            if(n!=null)
            {
                if (n.MType != MessageType.AssetMessage && n.MType != MessageType.CustomerMessage)
                    n.MessageText = Helpers.NotificationHelper.FormatNotificationMessage(n);
                return n.MessageText;
            }
            return string.Empty;
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
