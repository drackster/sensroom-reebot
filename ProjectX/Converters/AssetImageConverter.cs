﻿using System;
using System.Globalization;
using Xamarin.Forms;
using System.Collections.Generic;
using ProjectX.ViewModels;
using System.IO;

namespace ProjectX.Converters
{
    public class AssetImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //var model = value as AssetImageViewModel;
            //if(model!=null && model.ImgSource != null)
            //{
            //    Stream stream = new MemoryStream(model.ImgSource);
            //    model.IsLoaded = true;
            //    return ImageSource.FromStream(() =>
            //    {
            //        return stream;
            //    });
            //}
            if(value!=null && value.ToString()!=string.Empty)
            {
                return App.apiService.BaseUrl + "asset/getimage/" + value.ToString();
            }
            return ImageSource.FromFile("loadingmsg.png");
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
