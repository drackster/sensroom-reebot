﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;

namespace ProjectX.Helpers
{
    public class MessageStrings
    {
        public static string LoadingMsg { get { return "Obteniendo datos..."; } }
        public static string SavingMsg { get { return "Salvando..."; } }
        public static string DeletingMsg { get { return "Eliminando..."; } }
        public static string AceptingMsg { get { return "Aceptando Invitación..."; } }
        public static string RejectingMsg { get { return "Rechazando Invitación..."; } }
        public static string InvalidHost { get { return "* Favor de ingresar un anfitrión válido"; } }
        public static string InvalidField { get { return "* Favor de ingresar un valor válido"; } }
        public static string StrNext { get { return "Sig."; } }
        public static string ImageCaptureError { get { return "No se pudo accesar al carrete de imagenes"; } }
        public static string ImageSyncError { get { return "No se pudo salvar la imagen"; } }
        public static string ModelScheduleError { get { return "La chica no cuenta con el tiempo de servicio solicitado"; } }
        public static string GeneralErrorTitle { get { return "Lo sentimos"; } }
        public static string GeneralError { get { return "Ha sucedido un error."; } }
        public static string GeneralConfirmationTitle { get { return "Confirmación"; } }
        public static string DeleteConfirmation { get { return "¿Seguro de eliminar?"; } }
        public static string PleaseWait { get { return "Por favor espere..."; } }
        public static string OfflineMode { get { return "OFFLINE"; } }
        public static string OnlineMode { get { return "ONLINE"; } }
        public static string Version { get { return "¡Buenas noticias, hay una nueva versión disponible!"; } }

        public static string PaymentMode0 { get { return "Confirma con el 20%, paypal ó en tienda, y el resto en efectivo antes de iniciar el servicio."; } }
        public static string PaymentMode1 { get { return "Paga el 100% del total. En caso de cancelación, se te devolverá el monto íntegro."; } }
        public static string UserTypeAsset { get { return "Usuarios para recibir invitaciones a eventos y cobrar por ello."; } }
        public static string UserTypeCustomer { get { return "Es el tipo de usuarios para buscar personas e invitarlas a eventos."; } }
        public static string PendingConfirmation { get { return "EN ESPERA"; } }
        public static string ConfirmInvitation { get { return "ACEPTAR INVITACION"; } }
        public static string ConfirmedInvitation { get { return "ACEPTADA"; } }
        public static string CanceledInvitation { get { return "CANCELADA *"; } }
        public static string RejectedInvitation { get { return "RECHAZADA"; } }
        public static string ExpiredInvitation { get { return "EXPIRADA"; } }
        public static string CommentsSample { get { return "ejemplo: casa roja, no sirve el timbre, tocar puerta"; } }
        public static string InitChat { get { return "INICIAR CHAT"; } }
        public static string LocationError { get { return "No se pudo obtener tu ubicación"; } }
        public static string GetMonthName(int MonthNum)
        {
            string[] monthNames = new string[] { "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
                  "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" };

            return monthNames[MonthNum];
        }
        public static string Requesting { get { return "Solicitando pedido..."; } }

        public static string GetInvitationStatus(OrderStatus status)
        {
            switch(status)
            {
                case OrderStatus.Pending:return App.MSender == MessageSender.Asset ? ConfirmInvitation : PendingConfirmation;
                case OrderStatus.Confirmed:return ConfirmedInvitation;
                case OrderStatus.Rejected:return RejectedInvitation;
                case OrderStatus.Canceled:return CanceledInvitation;
            }
            return "N/A";
        }
        public static string FormatAddress(AddressViewModel CurrentAddress, bool IsShort=false)
        {
            //if (CurrentAddress.Thoroughfare == null || CurrentAddress.SubThoroughfare == null)
            //{
            //    if (IsShort) return CurrentAddress.A1;
            //    return CurrentAddress.A;
            //}
            //else
            //{
                string address = string.Empty;
                address += CurrentAddress.Thoroughfare == null ? "" : CurrentAddress.Thoroughfare + " ";
                address += CurrentAddress.SubThoroughfare == null ? "" : CurrentAddress.SubThoroughfare + " ";

                if (IsShort)
                {
                    return address;
                }
                address = string.Empty;
                address += CurrentAddress.Locality == null ? "" : CurrentAddress.Locality + " ";
                address += CurrentAddress.SubLocality == null ? "" : CurrentAddress.SubLocality + " ";

                address += CurrentAddress.PostalCode == null ? "" : CurrentAddress.PostalCode + " ";
                address += CurrentAddress.CountryCode == null ? "" : CurrentAddress.CountryCode + " ";
                return address;
            //}


        }
    }
}
