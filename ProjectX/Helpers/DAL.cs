﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xamarin.Forms;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using ProjectX.ViewModels;
using ProjectX.Interfases;

namespace ProjectX.Helpers
{
    public class LDAL
    {
        static object locker = new object();

        SQLiteAsyncConnection database;
        /// <summary>
        /// Initializes a new instance of the <see cref="Tasky.DL.TaskDatabase"/> TaskDatabase. 
        /// if the database doesn't exist, it will create the database and all the tables.
        /// </summary>
        /// <param name='path'>
        /// Path.
        /// </param>
        public LDAL()
        {
            database = new SQLiteAsyncConnection(DependencyService.Get<IFileHelper>().GetLocalFilePath("projectx.db3"));
        }

        #region Initialize Tables
        public async Task InitDeviceTokenTable()
        {
            await database.CreateTableAsync<CustomerCredit>();
            await database.CreateTableAsync<DeviceTokenViewModel>();
        }
        public async Task InitializeTables()
        {
            await database.CreateTableAsync<UserViewModel>();
            await database.CreateTableAsync<RegisterViewModel>();
            await database.CreateTableAsync<AssetViewModel>();
            await database.CreateTableAsync<CalendarViewModel>();
            await database.CreateTableAsync<CalendarDetailViewModel>();
            await database.CreateTableAsync<SearchModel>();
            await database.CreateTableAsync<AssetImageViewModel>();
            await database.CreateTableAsync<DeviceTokenViewModel>();
            await database.CreateTableAsync<Models.LocalExceptionModel>();
            await database.CreateTableAsync<CustomerCredit>();
           
        }

        #endregion

        public async Task SaveCustomerCredit(CustomerCredit model)
        {
            try
            {
                await database.InsertAsync(model);
            }
            catch (Exception ex)
            {

            }
             
        }

        public async Task DeleteCustomerCredit()
        {

            var cr = await database.Table<CustomerCredit>().FirstOrDefaultAsync();
            if (cr != null)
            {
                try
                {
                    await database.DeleteAsync(cr);
                }
                catch(Exception ex)
                {

                }
                
            }
           // await database.InsertAsync(model);
        }

        public async Task<CustomerCredit> getCustomerCredit()
        {
            return await database.Table<CustomerCredit>().FirstOrDefaultAsync();            
        }

        #region Register (register model is shared)
        public async Task SaveRegister(RegisterViewModel model)
        {
            var cust = await database.Table<RegisterViewModel>().FirstOrDefaultAsync();
            if (cust != null)
            {
                await UpdateRegister(model);
            }
            else
            {
                await database.InsertAsync(model);
            }
        }
        public async Task<RegisterViewModel> GetRegister()
        {
            return await database.Table<RegisterViewModel>().FirstOrDefaultAsync();
        }
        public async Task UpdateRegister(RegisterViewModel model)
        {
            await database.UpdateAsync(model);
        }
        public async Task DeleteRegister()
        {
            var reg = await GetRegister();
            await database.DeleteAsync(reg);
        }

       

        #endregion

        #region Users
        public async Task SaveUser(UserViewModel user)
        {
            var u = await database.Table<UserViewModel>().FirstOrDefaultAsync();
            if (u != null)
            {
                await UpdateUser(user);
            }
            else
            {
                await database.InsertAsync(user);
            }
        }
        public async Task<UserViewModel> GetUser()
        {
            return await database.Table<UserViewModel>().FirstOrDefaultAsync();
        }
        public async Task UpdateUser(UserViewModel user)
        {
            await database.UpdateAsync(user);
        }
        public async Task DeleteUser()
        {
            var user = await GetUser();
            await database.DeleteAsync(user);
        }

        #endregion

        #region Assets
        public async Task SaveAsset(AssetViewModel asset)
        {
            var a = await database.Table<AssetViewModel>().FirstOrDefaultAsync();
            if (a != null)
            {
                await UpdateAsset(asset);
            }
            else
            {
                try
                {
                    await database.InsertAsync(asset);
                }
                catch (Exception e)
                { }
                
            }
        }
        public async Task<AssetViewModel> GetAsset()
        {
            return await database.Table<AssetViewModel>().FirstOrDefaultAsync();
        }
        public async Task UpdateAsset(AssetViewModel asset)
        {
            try
            {
                await database.UpdateAsync(asset);
            }
            catch(Exception e)
            {

            }
        }
        public async Task DeleteAsset()
        {
            var asset = await GetAsset();
            await database.DeleteAsync(asset);
        }

        #endregion

        #region Assets Images
        public async Task SaveAssetImages(List<AssetImageViewModel> images)
        {
            foreach(var img in images)
            {
                var i = await database.Table<AssetImageViewModel>().Where(x => x.Id == img.Id).FirstOrDefaultAsync();
                if (i != null)
                {
                    await UpdateAssetImage(img);
                }
                else
                {
                    await database.InsertAsync(img);
                }
            }
        }
        public async Task<List<AssetImageViewModel>> GetAssetImages()
        {
            return await database.Table<AssetImageViewModel>().ToListAsync();
        }
        public async Task UpdateAssetImage(AssetImageViewModel asset)
        {
            await database.UpdateAsync(asset);
        }
        public async Task DeleteAssetImage(string ImageId)
        {
            var img = await database.Table<AssetImageViewModel>().Where(x => x.Id == ImageId).FirstOrDefaultAsync();
            if(img!=null)
                await database.DeleteAsync(img);
        }
        public async Task DeleteAssetImages()
        {
            var imgs = await database.Table<AssetImageViewModel>().ToListAsync();
            foreach(var img in imgs)
                await database.DeleteAsync(img);
        }

        #endregion

        #region Asset Calendar
        public async Task SaveCalendar(List<CalendarViewModel> cals)
        {
            foreach (var cal in cals)
            {
                var i = await database.Table<CalendarViewModel>().Where(x => x.Id == cal.Id).FirstOrDefaultAsync();
                if (i != null)
                {
                    await UpdateCalendar(cal);
                    await UpdateCalendarDetail(cal.Detail);
                }
                else
                {
                    await database.InsertAsync(cal);
                    await SaveCalendarDetails(cal.Detail);
                }
            }
        }
        public async Task<List<CalendarViewModel>> GetCalendars()
        {
            var cals = await database.Table<CalendarViewModel>().ToListAsync();
            foreach (var cal in cals)
                cal.Detail = await GetCalendarDetails(cal.Id);
            return cals;
        }
        public async Task UpdateCalendar(CalendarViewModel cal)
        {
            await database.UpdateAsync(cal);
            await SaveCalendarDetails(cal.Detail);
        }
        public async Task DeleteCalendar(long CalendarId)
        {
            var cal = await database.Table<CalendarViewModel>().Where(x => x.Id == CalendarId).FirstOrDefaultAsync();
            if (cal != null)
            {
                foreach(var det in cal.Detail)
                    await DeleteCalendarDetail(det.Id);
                await database.DeleteAsync(cal);
            }
        }
        public async Task DeleteCalendars()
        {
            var cals = await database.Table<CalendarViewModel>().ToListAsync();
            foreach (var cal in cals)
            {
                foreach (var det in cal.Detail)
                    await database.DeleteAsync(det);
                await database.DeleteAsync(cal);
            }
        }

        //CALENDAR DETAILS
        public async Task savetimedisapper(TimeLogout time)
        {

        }
        public async Task SaveCalendarDetails(List<CalendarDetailViewModel> cals)
        {
            foreach (var cal in cals)
            {
                var i = await database.Table<CalendarDetailViewModel>().Where(x => x.Id == cal.Id).FirstOrDefaultAsync();
                if (i != null)
                {
                    await UpdateCalendarDetail(cal);
                }
                else
                {
                    await database.InsertAsync(cal);
                }
            }
        }
        public async Task<List<CalendarDetailViewModel>> GetCalendarDetails(long CalendarId)
        {
            var test = await database.Table<CalendarDetailViewModel>().ToListAsync();
            return await database.Table<CalendarDetailViewModel>()
                .Where(x => x.CalendarId == CalendarId).ToListAsync();
        }
        public async Task UpdateCalendarDetail(CalendarDetailViewModel CalendarDetail)
        {
            await database.UpdateAsync(CalendarDetail);
        }
        public async Task UpdateCalendarDetail(List<CalendarDetailViewModel> CalendarDetails)
        {
            foreach(var cal in CalendarDetails)
                await database.UpdateAsync(cal);
        }
        public async Task DeleteCalendarDetail(long CalendarDetailId)
        {
            var cal = await database.Table<CalendarDetailViewModel>().Where(x => x.Id == CalendarDetailId).FirstOrDefaultAsync();
            if (cal != null)
                await database.DeleteAsync(cal);
        }
        public async Task DeleteCalendarDetails()
        {
            var cals = await database.Table<CalendarDetailViewModel>().ToListAsync();
            foreach (var cal in cals)
                await database.DeleteAsync(cals);
        }

        #endregion

        #region SearchModel
        public async Task SaveSearchModel(SearchModel model)
        {
            try
            {
                var s = await database.Table<SearchModel>().FirstOrDefaultAsync();
                if (s != null)
                {
                    await UpdateSearchModel(model);
                }
                else
                {
                    await database.InsertAsync(model);
                }
            }
            catch
            { }
            
        }
        public async Task<SearchModel> GetSearchModel()
        {
            return await database.Table<SearchModel>().FirstOrDefaultAsync();
        }
        public async Task UpdateSearchModel(SearchModel user)
        {
            await database.UpdateAsync(user);
        }
        public async Task DeleteSearchModel()
        {
            var s = await GetSearchModel();
            await database.DeleteAsync(s);
        }
        #endregion

        #region DeviceToken
        public async Task SaveDeviceToken(DeviceTokenViewModel model)
        {
            var token = await database.Table<DeviceTokenViewModel>().FirstOrDefaultAsync();
            if (token != null)
            {
                await UpdateDeviceToken(model);
            }
            else
            {
                await database.InsertAsync(model);
            }
        }
        public async Task<DeviceTokenViewModel> GetDeviceToken()
        {
            return await database.Table<DeviceTokenViewModel>().FirstOrDefaultAsync();
        }
        public async Task UpdateDeviceToken(DeviceTokenViewModel model)
        {
            await database.UpdateAsync(model);
        }
        public async Task DeleteDeviceToken(DeviceTokenViewModel model)
        {
            var token = await database.Table<DeviceTokenViewModel>().FirstOrDefaultAsync();
            if (token != null)
                await database.DeleteAsync(token);
        }
        #endregion

        #region Log
        public async Task LogException(Exception ex, string viewName = null)
        {
            if (ex != null && ex.Message != null)
            {
                System.Diagnostics.Debug.WriteLine("PXERROR----> " + ex.Message);
                int x = 0;
                if (x != 0)//test
                {
                    Models.LocalExceptionModel model = new Models.LocalExceptionModel()
                    {
                        Id = System.Guid.NewGuid().ToString(),
                        CreatedUTC =DateTime.UtcNow,
                        //UserId = UserId,
                        ViewName = viewName,
                        Message = ex.Message,
                        InnerMessage = ex.InnerException == null ? null : ex.InnerException.Message
                    };
                    await database.InsertAsync(model);
                }
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("PXERROR----> unknown");
            }
        }

        #endregion


    }

}