﻿using Xamarin.Forms;
using ProjectX.Interfases;

namespace ProjectX.Helpers
{
    public static class KeyboardListenerHelper
    {
        private static IKeyboardNotifications _instance;
        public static IKeyboardNotifications Instance
        {
            get
            {
                //return DependencyService.Get<IKeyboardNotifications>();
                //Bug al parecer no esta funcionando con Singleton, mejor regresamos una instancia siempre
                return _instance ?? (_instance = DependencyService.Get<IKeyboardNotifications>());
            }
        }
    }
}
