﻿using System;
using ProjectX.ViewModels;

namespace ProjectX.Helpers
{
    public class NotificationHelper
    {
        public static string FormatNotificationMessage(NotificationViewModel model)
        {
            switch (model.MType)
            {
                //case MessageType.AssetMessage: return model.MessageText;
                //case MessageType.CustomerMessage: return model.MessageText;
                case MessageType.AuthRequest:
                    return string.Format("Tienes una solicitud de {0} para conectar", model.SenderName);
                case MessageType.Invitation:
                    string msg = string.Empty;
                    if (App.MSender == MessageSender.Asset)
                        msg = string.Format("Has recibido una invitación de {0} por {1}", model.SenderName, model.TierName);
                    else
                        msg = string.Format("Has invitado a {0} por {1}",model.SenderName, model.TierName);

                    if (model.EventDateTime.HasValue)
                        msg += " para " + string.Format("{0:dd MMMM}", model.EventDateTime.Value.ToLocalTime()) +
                               " " + string.Format("{0:hh:mm tt}", model.EventDateTime.Value.ToLocalTime());
                    return msg;
            }
            return string.Empty;
       }
    }
}
