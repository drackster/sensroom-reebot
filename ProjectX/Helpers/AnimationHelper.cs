﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ProjectX.Helpers
{
    /// <summary>
    /// Animation helper by Victor Arce
    /// </summary>
    public class AnimationHelper
    {
        /// <summary>
        /// Scales an UI Element with animation
        /// </summary>
        /// <param name="uiElement">The element to animate</param>
        /// <param name="InOrOut">True if scalingup to show</param>
        /// <param name="duration">optional time in ms</param>
        public static async Task ViewScale(View uiElement, bool InOrOut, uint duration = 200, Easing easing = null)
        {
            if (InOrOut)
            {
                if (easing == null) easing = Easing.SinIn;
                await uiElement.ScaleTo(1, duration, easing);
            }
            else
            {
                if (easing == null) easing = Easing.SinOut;
                await uiElement.ScaleTo(.1, duration, easing);
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="InOrOut">Moving in to show, Moving out to hide</param>
        /// <param name="UpOrDown">If the UI View will move up or down in order to hide</param>
        /// <param name="ExtraMile">In case that the UI View is not in the Begin or End of the MainView, specify</param>
        /// <param name="duration"></param>
        /// <param name="easing"></param>
        /// <returns></returns>
        public static async Task ViewMove(View uiElement, bool InOrOut, bool UpOrDown, double ScreenHeight, double ExtraMile = 0, uint duration = 150, Easing easing = null)
        {
            double y = 0;
            //double fixHeight = 0;

            //if (InOrOut && !UpOrDown && (uiElement.Height == ScreenHeight || uiElement.Height / 2 != ScreenHeight))
            //{
            //    InOrOut = !InOrOut;
            //}
            //else if (InOrOut && UpOrDown && (uiElement.Height * 2) > (ScreenHeight - uiElement.Height))
            //{
            //    return;
            //}
            //else if (!InOrOut && !UpOrDown && uiElement.Height == ScreenHeight)
            //{
            //    return; //Do nothing
            //}


            if (!UpOrDown)
                y = InOrOut ? uiElement.Y - uiElement.Height - ExtraMile : uiElement.Y + uiElement.Height + ExtraMile;
            else
                y = InOrOut ? uiElement.Y + uiElement.Height + ExtraMile : uiElement.Y - uiElement.Height - ExtraMile;

            await uiElement.LayoutTo(new Rectangle(new Point(uiElement.X, y), new Size(uiElement.Width, uiElement.Height)), duration, easing);
        }
        public static async Task ViewMoveReset(View uiElement, bool UpOrDown, double ScreenHeight, double ExtraMile = 0, uint duration = 150, Easing easing = null)
        {
            double y = 0;
            if (!UpOrDown)
                y = ScreenHeight - ((uiElement.Height * 2) + ExtraMile);
            else
                y = ExtraMile;

            await uiElement.LayoutTo(new Rectangle(new Point(uiElement.X, y), new Size(uiElement.Width, uiElement.Height)), duration, easing);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="InOrOut"></param>
        /// <param name="duration"></param>
        /// <param name="easing"></param>
        /// <returns></returns>
        public static async Task ViewFade(View uiElement, bool InOrOut, uint duration = 150, Easing easing = null)
        {
            if (easing == null && InOrOut) easing = Easing.SinIn;
            else if (easing == null && !InOrOut) easing = Easing.SinOut;
            await uiElement.FadeTo(InOrOut ? 1 : 0, duration, easing);
        }


    }
}
