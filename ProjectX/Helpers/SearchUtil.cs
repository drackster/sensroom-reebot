﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ProjectX.ViewModels;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms;

namespace ProjectX.Helpers
{
    public class SearchUtil
    {
        public static AddressViewModel ParseAddressViewModel(Address ca)
        {
            return new AddressViewModel
            {
                CountryCode = ca.CountryCode,
                FeatureName = ca.FeatureName,
                Latitude = ca.Latitude,
                Longitude = ca.Longitude,
                Locality = ca.Locality,
                PostalCode = ca.PostalCode,
                SubLocality = ca.SubLocality,
                SubThoroughfare = ca.SubThoroughfare,
                Thoroughfare = ca.Thoroughfare
            };
        }
        public static string[] GetSplittedPrediction(string Location, string Separator)
        {
            string[] result = new string[3];
            int i = Location.IndexOf(Separator);
            if (i >= 0)
            {
                result[0] = Location.Substring(0, i);
                Location = Location.Substring(i + 1);

                i = Location.IndexOf(Separator);
                if (i >= 0)
                {
                    result[1] = Location.Substring(0, i);
                    result[2] = Location.Substring(i + 1);
                }
                else
                {
                    result[1] = Location;
                    result[2] = string.Empty;
                }
                return result;
            }

            result[0] = Location;
            result[1] = string.Empty;
            result[2] = string.Empty;
            return result;

        }
        public static double[] GetScreenCoordinates(VisualElement view)
        {
            // A view's default X- and Y-coordinates are LOCAL with respect to the boundaries of its parent,
            // and NOT with respect to the screen. This method calculates the SCREEN coordinates of a view.
            // The coordinates returned refer to the top left corner of the view.

            // Initialize with the view's "local" coordinates with respect to its parent
            double screenCoordinateX = view.X;
            double screenCoordinateY = view.Y;

            // Get the view's parent (if it has one...)
            if (view.Parent.GetType() != typeof(App))
            {
                VisualElement parent = (VisualElement)view.Parent;


                // Loop through all parents
                while (parent != null)
                {
                    // Add in the coordinates of the parent with respect to ITS parent
                    screenCoordinateX += parent.X;
                    screenCoordinateY += parent.Y;

                    // If the parent of this parent isn't the app itself, get the parent's parent.
                    if (parent.Parent.GetType() == typeof(App))
                        parent = null;
                    else
                        parent = (VisualElement)parent.Parent;
                }
            }

            // Return the final coordinates...which are the global SCREEN coordinates of the view
            return new double[] { screenCoordinateX, screenCoordinateY };
        }
    }
}
