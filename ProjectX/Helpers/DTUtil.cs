﻿using System;
using ProjectX.ViewModels;

namespace ProjectX.Helpers
{
    public class DTUtil
    {
        public static string ConvertDT(DateTime DT, bool FriendlyDay, bool FriendlyMonth,
                                            bool IncludeYear, bool IncludeTime)
        {
            string result = string.Empty;
            result += FriendlyDay ? Util.GetDayAsString(DT.DayOfWeek).Substring(0, 3) + " " + string.Format("{0:dd}", DT) : string.Format("{0:dd}", DT);
            result += "/";
            result += FriendlyMonth ? Util.GetMonthAsString(DT.Month).Substring(0, 3) : string.Format("{0:MM}", DT);

            if(IncludeYear)
                result += "/" + string.Format("{0:yy}", DT);
            if (IncludeTime)
                result += " " + string.Format("{0:HH:mm}", DT);

            return result;
        }

        /// <summary>
        /// Buscar el tiempo futuro más cercano, 
        /// ejemplo: si son las 4:09pm, debe de regresar 4:15pm
        /// //Tambien recibe el numero de minutos enfrente para poder reservar
        /// </summary>
        /// <returns></returns>
        public static DateTime FindNearestRSVPTime(int mins, bool ignoreFirstMins=false)
        {
            var dt =DateTime.UtcNow;

            if(!ignoreFirstMins)
                dt = dt.AddMinutes(mins);
            while((dt.Minute % 15) != 0)
            {
                dt = dt.AddMinutes(1);
            };
            dt = ViewModels.Util.StripMinSec(dt);
            return dt;

        }
        public static string FormatAppointment(InvitationViewModel model)
        {
            string d = string.Empty;
            DateTime dt =DateTime.UtcNow.Date;
            DateTime ft = model.EventDateTime.Value.Date;
            if (ft == dt) d = "Hoy";
            else if (ft == dt.AddDays(1)) d = "Mañana";
            else d = ConvertDT(ft, false, true, false, false);

            return d + " " + string.Format("{0:HH:mm}", model.EventDateTime.Value);// + "-" +
                   //string.Format("{0:HH:mm}", model.EventEnd.Value);
        }

    }
}
