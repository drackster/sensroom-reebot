﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Services;
using ProjectX.Pages;
using ProjectX.ViewModels;
using ProjectX.Helpers;
using ProjectX.Pages.Templates;
using ProjectX.Interfases;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;
using System.Diagnostics;
using AdvancedTimer.Forms.Plugin.Abstractions;
using TK.CustomMap.Api;
using TK.CustomMap.Api.Google;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace ProjectX
{
    public partial class App : Application
    {
        #region Consts
        //TODO GLOBAL CONFIG
        public static int CancelationTokenTimeoutSecs { get { return 30; } }
        public static int DeviceTimerTimeOutSeconds { get { return 10; } }
        public static int LocationTimeOutSeconds { get { return 15; } }
        public static int ScheduleFutureDays { get { return 10; } }
        public static int ScheduleFutureMinutes { get { return 120; } }
        public static int ImageCacheDays { get { return 1; } }
        private static string sqliteDBProperty { get { return "sqlitetablesInit"; } }
        
        #endregion

        #region Private Members
        public static double ScreenWidth;
        public static double ScreenHeight;
        //public static Size ScreenSize;     
        private static APIService _apiService;
        private static Calculator _calculator;
        private static RootPage _rootPage;
        private static MessageSender _msender;
        private static RegisterViewModel _userViewModel;
        private static LDAL _database;
        private static IHUD hud;
        private static IAudio audio;
        private static IAdvancedTimer advcTimer;
        //private static ISnapper snapper;
        #endregion

        #region Public
        public static bool sqliteDBTablesInit { get; set; }
        public static List<CalendarTimeViewModel> CalendarTimes { get; set; }
        public static IAdvancedTimer AdvcTimer
        {
            get
            {
                if (advcTimer == null)
                    advcTimer = DependencyService.Get<IAdvancedTimer>();
                return advcTimer;
            }
        }
        public static bool AdvcTimerRunning { get; set; }

        public static INotificationHelper notificationHelper;
        public static List<ABankViewModel> banks { get; set; }
        public static AScheduleViewModel scheduleViewModel { get; set; }
        public static ListView MenuList { get; set; }
        public static Calculator calculator
        {
            get
            {
                if (_calculator == null)
                    _calculator = new Calculator();
                return _calculator;
            }
        }
        public static RootPage rootPage
        {
            get
            {
                if (_rootPage == null)
                    _rootPage = new RootPage();
                return _rootPage;
            }
        }
        public static APIService apiService
        {
            get
            {
                if (_apiService == null)
                    _apiService = new APIService();
                return _apiService;
            }
        }
        public static MessageSender MSender
        {
            get
            {
                return _msender;
            }
            set
            {
                _msender = value;
            }
        }
        public static RegisterViewModel RegisterVM
        {
            get
            {
                if (_userViewModel == null)
                    _userViewModel = new RegisterViewModel() { IsActivated = false };
                return _userViewModel;
            }
            set
            {
                _userViewModel = value;
            }
        }
        public static AssetViewModel AssetVM
        {
            get
            {
                return RegisterVM == null ? null : RegisterVM.AssetVM;
            }
        }
        public static UserViewModel UserVM
        {
            get { return RegisterVM == null ? null : RegisterVM.UserVM; }
        }
        public static XSettingsViewModel SettingsVM { get; set; }
        public static ASettingsViewModel ASettings { get; set; }
        public static LDAL Database
        {
            get
            {
                if (_database == null)
                    _database = new LDAL();
                return _database;
            }
        }
        public static CustomNavBar NavBar { get; set;}
        public static IHUD HUD
        {
            get
            {
                if (hud == null)
                {
                    hud = DependencyService.Get<IHUD>();
                }
                return hud;
            }
        }

        //public static ISnapper Snapper
        //{
        //    get
        //    {
        //        if(snapper ==null)
        //        {
        //            snapper = DependencyService.Get<ISnapper>();
        //        }
        //        return snapper;
        //    }
        //}
        public static IAudio Audio
        {
            get
            {
                if (audio == null)
                {
                    audio = DependencyService.Get<IAudio>();
                }
                return audio;
            }
        }
        public static int RootStep { get; set; }
        #endregion
        public App()
        {
            InitializeComponent();

            if(Device.RuntimePlatform == Device.Android)
            { 
                GmsPlace.Init("AIzaSyDmNAB_aCZKPrMGKmYp-g1xCiwvMdqAD3g");
                //GmsDirection.Init("YOUR API KEY");
            }
            
            RootStep = 0;
            MSender = MessageSender.None;
            SetMainPage();

        }
        public static void SetMainPage()
        {
            Current.MainPage = rootPage;
            //Current.MainPage = new BlankPage();
        }

        #region Methods
        public static void SetBadgeCount(string title, int badge)
        {
            //Badge count
            if (title != null)
            {
                var menu = App.MenuList.ItemsSource as ObservableCollection<RootPageMenuItem>;
                if (menu != null)
                {
                    var i = menu.Where(x => x.Title == title).FirstOrDefault();
                    i.Badge = badge;
                    App.MenuList.ItemsSource = null;
                    App.MenuList.ItemsSource = menu;
                }
            }
            if (notificationHelper == null)
                notificationHelper = DependencyService.Get<INotificationHelper>();
            if(notificationHelper!=null)
                notificationHelper.SetIconBadgeNumber(badge);
        }
        public static async Task CheckBadge()
        {
            try
            {
                System.Diagnostics.Debug.WriteLine("Checking badge...");
                var requestModel = new NotificationRequestModel()
                {
                    SType = App.MSender,
                    MType = MessageType.All
                };//traer todo

                if (App.MSender == MessageSender.Customer)
                    requestModel.CustomerId = App.UserVM.Id;
                else
                    requestModel.AssetId = App.AssetVM.Id;

                var unread = await App.apiService.GetNotificationCount(requestModel);

                if (App.MSender == MessageSender.Customer)
                    App.SetBadgeCount("Buzón", unread);
                else
                    App.SetBadgeCount("Mensajes", unread);
            }
            catch
            {
                //ignore
            }
        }
        public static async void UpdateDeviceToken(string deviceToken)
        {
            if (!string.IsNullOrEmpty(deviceToken))
            {
                //No podemos enviarlo a la nube porque puede que aun no sepamos a quien pertenece
                //Generamos la BD en caso de que no haya sido creada
                await Database.InitDeviceTokenTable();
                //var ct = await Database.GetDeviceToken();
                //save or update
                await Database.SaveDeviceToken(new DeviceTokenViewModel
                {
                    Id = 1,
                    DeviceId = DeviceType.Google,
                    Token = deviceToken,
                    UpdatedOn =DateTime.UtcNow
                });
            }
        }

        #endregion

        #region Events

        protected override void OnStart()
        {
            base.OnStart();
            if (Application.Current.Properties.ContainsKey(sqliteDBProperty))
                sqliteDBTablesInit = (bool)Application.Current.Properties[sqliteDBProperty];
            System.Diagnostics.Debug.WriteLine("start:sqliteDBTablesInit:" + sqliteDBTablesInit.ToString());

            //ShowLogin();
        }
        protected override void OnResume()
        {
            base.OnResume();
            if (Application.Current.Properties.ContainsKey(sqliteDBProperty))
                sqliteDBTablesInit = (bool)Application.Current.Properties[sqliteDBProperty];
            System.Diagnostics.Debug.WriteLine("resume;sqliteDBTablesInit:"+sqliteDBTablesInit.ToString());
            //ShowLogin();
        }
        private bool loginIsVisible;
        private void ShowLogin()
        {
            if (!Application.Current.Properties.ContainsKey("resume"))
                Application.Current.Properties.Add("resume", true);
            if (!loginIsVisible)
            {
                loginIsVisible = true;
                var login = new Pages.Common.CLogin2();
                login.LoginComplete += Login_LoginComplete;
                Application.Current.MainPage.Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, false);
            }
        }

        private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        {
            if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            {
                Application.Current.Properties.Remove("resume");
            }
            var inicio = new RootPage();

            inicio.OpenMainPage();           
            await Application.Current.MainPage.Navigation.PopModalAsync(true);
            loginIsVisible = false;
        }
        protected override void OnSleep()
        {
            base.OnSleep();
            
            if (!Application.Current.Properties.ContainsKey(sqliteDBProperty))
                Application.Current.Properties.Add(sqliteDBProperty, sqliteDBTablesInit);
            else
                Application.Current.Properties[sqliteDBProperty] = sqliteDBTablesInit;
            System.Diagnostics.Debug.WriteLine("sleep:sqliteDBTablesInit:" + sqliteDBTablesInit.ToString());

            //Desconectarse cuando no esta en uso
            TKNativePlacesApi.Instance.DisconnectAndRelease();

        }

        #endregion
    }
}
