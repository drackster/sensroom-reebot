﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Pages.Customer;
using ProjectX.ViewModels;
using ProjectX.Pages.Asset;
using ProjectX.Pages.Common;
using ProjectX.Models;

using System.IO;
using System.Collections.ObjectModel;

namespace ProjectX.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootPage : MasterDetailPage
    {
        #region Properties
        private bool IsLoaded;
        private bool IsMainPageLoaded;
        private CSelector selector;
        private CPermissions permissionsPage;
        #endregion

        #region Pages

        private AMain amain;
        private XList xlist;
        public AMain _AMain
        {
            get
            {
                if (amain == null)
                    amain = new AMain();
                return amain;
            }
        }
        public XList _XList
        {
            get
            {
                if (xlist == null)
                    xlist = new XList();
                return xlist;
            }
        }

        #endregion

        #region cTor
        public RootPage()
        {
            InitializeComponent();

            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            MasterPage.TCYTapped += MasterPage_TCYTapped;

            #region Subscriptions
            MessagingCenter.Subscribe<CLogin, bool>(this, "showmainfromlogin", (sender, args) =>
            {
                Detail = new NavigationPage(new XList()) { BarTextColor = Color.White };
            });
            MessagingCenter.Subscribe<Calculator, bool>(this, "showcalculator", (sender, args) =>
            {
                Detail = new NavigationPage(new XList()) { BarTextColor = Color.White };
            });
            MessagingCenter.Subscribe<AInvitation, bool>(this, "invitationcontinue", (sender, args) =>
            {
                Detail = new NavigationPage(new XList()) { BarTextColor = Color.White };
            });
            MessagingCenter.Subscribe<MessageList, bool>(this, "chatcontinue", (sender, args) =>
            {
                Detail = new NavigationPage(new XList()) { BarTextColor = Color.White };
            });
            #endregion

            Detail = new NavigationPage(new BlankPage()) { BarTextColor = Color.White, BackgroundColor = Color.FromHex("364F76") };

        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            IsGestureEnabled = false;

            //Cargar informacion local
            if (!IsLoaded || App.RegisterVM == null)
            {
                if (!App.sqliteDBTablesInit)
                {
                    await App.Database.InitializeTables();
                    App.sqliteDBTablesInit = true;
                }
                if (!App.RegisterVM.IsActivated)
                {
                    await App.Database.InitializeTables();
                    App.sqliteDBTablesInit = true;
                    App.RegisterVM = await App.Database.GetRegister();
                }
                    
                if (App.RegisterVM != null && App.RegisterVM.IsActivated)
                {
                    //Establecer variables de API
                    App.apiService.UserName = App.RegisterVM.UserName;
                    App.apiService.UserPassword = App.RegisterVM.Password;
                    App.MSender = App.RegisterVM.Sender;
                    if (App.MSender == MessageSender.Customer)
                    {
                        var u = await App.Database.GetUser();
                        u.SearchFilters = await App.Database.GetSearchModel();
                        App.RegisterVM.UserVM = u;
                    }
                    else if (App.MSender == MessageSender.Asset)
                    {
                        App.RegisterVM.AssetVM = await App.Database.GetAsset();
                        App.RegisterVM.AssetVM.Images = await App.Database.GetAssetImages();
                        App.RegisterVM.AssetVM.Calendars = await App.Database.GetCalendars();
                    }
                }             
            }
            LoadMainViews();
        }

        public async void LoadMainViews()
        {
            UserViewModel user = await App.Database.GetUser();
            if (App.RegisterVM != null && App.RegisterVM.IsActivated) //la cuenta ya esta activa
            {
                await OpenMainPage();
            }
            //Aqui entra al selector AKA login
            else if ((App.RegisterVM == null || !App.RegisterVM.IsActivated) && user == null)
            {
                IsMainPageLoaded = false;//logout
                if (App.RootStep == 0)
                {
                    selector = new CSelector();
                    selector.SelectedTapped += Selector_SelectedTapped;
                    await Navigation.PushModalAsync(new NavigationPage(selector) { BarTextColor = Color.White }, true);
                }
            }
            else
            {
                var login = new Common.CLogin2();
                login.LoginComplete += Login_LoginComplete;
                await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            }

            IsLoaded = true;
        }
        #endregion

        #region Registration/Login/Logout Events
        public async void Selector_SelectedTapped(object sender, EventArgs e)
        {
            //if (App.RegisterVM.ActivationStatus > 0) return;
            if (App.RootStep == 99) return;
            if (App.MSender != ViewModels.MessageSender.None && App.MSender != ViewModels.MessageSender.Grap)
            {
                await Navigation.PopModalAsync(true);
                var request = new CRequest();
                request.RequestCompleted += Request_RequestCompleted;
                await Navigation.PushModalAsync(new NavigationPage(request) { BarTextColor = Color.White }, true);
            }
            else if (App.MSender == ViewModels.MessageSender.None)
            {
                await Navigation.PopModalAsync(true);
                //Login
                var login = new CLogin();
                login.LoginComplete += Login_LoginComplete;
                await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            }
            else
            {
                await Navigation.PopModalAsync(true);
                //Login
                var login = new CLogin2();
                login.LoginComplete += Login_LoginComplete;
                await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            }
        }
        private async void Request_RequestCompleted(object sender, RegisterEventArgs e)
        {
            App.RegisterVM = e.Model;
            if (e.Model.XSettingsVM != null)
                App.SettingsVM = e.Model.XSettingsVM;

            await Navigation.PopModalAsync(true);
        }
        public async void Login_LoginComplete(object sender, RegisterEventArgs e)
        {
            if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            {
                Application.Current.Properties.Remove("resume");
            }
            if (Device.RuntimePlatform == Device.Android)
            {
                await OpenMainPage();
            }
            
            await Navigation.PopModalAsync(true);
        }
        public async void Logout_LogoutComplete(object sender, EventArgs e)
        {
            IsLoaded = false;
            IsMainPageLoaded = false;
            IsPresented = false;
            //App.apiService.UserName = 
                App.apiService.UserPassword = null;
            App.apiService.SessionToken = null;
            
            Detail = new BlankPage();

            //Reset Pages
            xlist = null;
            amain = null;

            await Navigation.PopModalAsync(true);
        }
        #endregion

        #region Menu Events
        private void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as RootPageMenuItem;
            if (item == null)
                return;

            Page page = null;

            //AMain
            if (item.Id == 4)
            {
                page = _AMain;
            }
            else if (item.Id == 0)
            {
                page = _XList;
            }
            else
            {
                page = (Page)Activator.CreateInstance(item.TargetType);
            }

            page.Title = item.Title;

            Color textColor = Color.White;
            //calculadora o main o login
            //if (item.Id == 4) textColor = Color.Black;

            if (item.Id == 10)//logout
            {
                var logout = new Logout();
                logout.LogoutComplete += Logout_LogoutComplete;
                this.Navigation.PushModalAsync(logout, true);
            }
            else
            {
                Detail = new NavigationPage(page) { BarTextColor = textColor };
                IsPresented = false;
            }

            MasterPage.ListView.SelectedItem = null;
        }
        private async void MasterPage_TCYTapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new XTYC(true), true);
        }
        #endregion

        #region Methods
        public  async Task OpenMainPage()
        {
            if (IsBusy || IsMainPageLoaded) return;
            IsBusy = true;
            try
            {
                //checar permisos primero
                if (!App.RegisterVM.Permissions)
                {
                    permissionsPage = new CPermissions();
                    var permissionsCheck = await permissionsPage.Init();
                    if (!permissionsCheck)
                    {
                        //await Navigation.PushModalAsync(permissionsPage, true);
                        //return;
                    }
                }

                //reconstruir el menu
                IsMainPageLoaded = true;
                MasterPage.ListView.ItemSelected -= ListView_ItemSelected;
                MasterPage.TCYTapped -= MasterPage_TCYTapped;
                //MasterPage.RepopulateMenu();
                this.Master = MasterPage = new RootPageMaster();
                MasterPage.ListView.ItemSelected += ListView_ItemSelected;
                MasterPage.TCYTapped += MasterPage_TCYTapped;
                App.MenuList = MasterPage.ListView;
                
                if (App.MSender == ViewModels.MessageSender.Customer)
                {
                    Detail = new NavigationPage(_XList) { BarTextColor = Color.White };
                }
                else if (App.MSender == ViewModels.MessageSender.Asset)
                {
                    if (!App.AssetVM.IsInvalid)
                    {
                        Detail = new NavigationPage(_AMain) { BarTextColor = Color.White };
                    }
                    else
                    {
                        Detail = new NavigationPage(new Asset.AConfiguration()) { BarTextColor = Color.White };
                        App.AssetVM.MainLoaded = true;
                    }
                    //Establecer imagen
                    if (!string.IsNullOrEmpty(App.AssetVM.CoverImage))
                    {
                        MasterPage.ProfileImage.Source = App.apiService.FormatImgUrl(App.AssetVM.CoverImage.ToString());
                    }

                }
                IsPresented = false;
                await App.CheckBadge();
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
                await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, 
                    Helpers.MessageStrings.GeneralError, "Ok");
             
            }
            finally
            {
                IsBusy = false;
            }
        }

        #endregion



    }
}