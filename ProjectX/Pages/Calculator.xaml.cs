﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ProjectX.Pages
{
    public partial class Calculator : ContentPage
    {
        public event EventHandler<EventArgs> PasswordEntered;
        int currentState = 1;
        string mathOperator;
        double firstNumber, secondNumber;

        public Calculator()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            OnClear(this, null);

            //ESTO ES UNA PRUEBA IGNORAR
        }

        void OnSelectNumber(object sender, EventArgs e)
        {
            Button button = (Button)sender;
            string pressed = button.Text;

            if (this.resultText.Text == "0" || currentState < 0)
            {
                this.resultText.Text = "";
                if (currentState < 0)
                    currentState *= -1;
            }

            this.resultText.Text += pressed;

            double number;
            if (double.TryParse(this.resultText.Text, out number))
            {
                this.resultText.Text = number.ToString("N0");
                if (currentState == 1)
                {
                    firstNumber = number;

                    //TODO
                    if (firstNumber == 5555)
                    {
                        //ok clave ingresada, descargar y 
                        //if (PasswordEntered != null)
                        //    PasswordEntered.Invoke(this, new EventArgs());

                        MessagingCenter.Send(this, "showcalculator", true);


                    }


                }
                else
                {
                    secondNumber = number;
                }
            }
        }

        void OnSelectOperator(object sender, EventArgs e)
        {
            currentState = -2;
            Button button = (Button)sender;
            string pressed = button.Text;
            mathOperator = pressed;
        }

        void OnClear(object sender, EventArgs e)
        {
            firstNumber = 0;
            secondNumber = 0;
            currentState = 1;
            this.resultText.Text = "0";
        }

        void OnCalculate(object sender, EventArgs e)
        {
            if (currentState == 2)
            {
                var result = Helpers.SimpleCalculator.Calculate(firstNumber, secondNumber, mathOperator);

                this.resultText.Text = result.ToString();
                firstNumber = result;
                currentState = -1;
            }
        }
    }
}
