﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XOrderDetail : ContentPage
    {
        private OrderViewModel model;
        public XOrderDetail(OrderViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = this.model = model;
            customNavBar.SetTitle("Detalle de Pedido");
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            //var chat = App.apiService.GetChatList().Where(x => x.SenderName == model.Asset.Name).FirstOrDefault();
            //if(chat!=null)
            //Navigation.PushAsync(new Common.MessageList(chat), true);
        }
    }
}