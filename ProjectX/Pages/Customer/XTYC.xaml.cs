﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XTYC : ContentPage
    {
        public XTYC(bool isModal=false)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Términos y Condiciones");
            if(isModal)
                customNavBar.SetIconText("X");
        }
    }
}