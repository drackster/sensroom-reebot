﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Asset;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XConfirmation : ContentPage
    {
        #region Properties
        private AssetViewModel model;
        private ACategoryTierViewModel selectedTier;
        private int tierIndex;
        //private bool isLoaded;
        private AddressViewModel CurrentAddress;
        #endregion

        #region cTor
        public XConfirmation(AssetViewModel model, AddressViewModel currentAddress)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = this.model = model;
            //selectedHours = model.MinHours;
            customNavBar.SetTitle("Confirmación");

            CurrentAddress = currentAddress;
            //por default elegir el primer tier
            tierIndex = 0;
            SetHours();
            SetAddress();

            txtComments.WidthRequest = App.ScreenWidth;
            txtComments.Text = MessageStrings.CommentsSample;
            txtComments.TextChanged += TxtComments_TextChanged;

            if (App.UserVM.SearchFilters.orderType == OrderType.ASAP)
                txtWhen.Text = "Ahora";
            else///calendar
            {
                var selectedDate = App.UserVM.SearchFilters.SelectedDateTime;
                txtWhen.Text = string.Format("{0:dddd dd MMMM  HH:mm}", selectedDate);
            }
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        #endregion

        #region Events
        private void BackButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }
        private void TxtComments_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtComments.Text == null ? 0 : txtComments.Text.Length;
            if (ccount > 99)
                txtComments.Text = e.OldTextValue;
            lblCommentsCount.Text = ccount.ToString();
        }
        //private void MinusGestureRecognizer_Tapped(object sender, EventArgs e)
        //{
        //    if (tierIndex > 0)
        //    {
        //        tierIndex--;
        //        SetHours();
        //    }
        //}
        //private void AddGestureRecognizer_Tapped(object sender, EventArgs e)
        //{
        //    if (tierIndex < model.Tiers.Count - 1)
        //    {
        //        tierIndex++;
        //        SetHours();
        //    }
        //}
        private async void Request_Clicked(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            bool IsError = false;
            //App.HUD.Show(MessageStrings.Requesting, Interfases.MaskType.Black);
            try
            {
                DateTime? requestedDateTime = App.UserVM.SearchFilters.orderType == OrderType.ASAP ? null :
                                              App.UserVM.SearchFilters.SelectedDateTime;

                if (CurrentAddress.A1 == null)
                    CurrentAddress.Parse();
                
                var newOrderVM = new OrderViewModel
                {
                    AssetId = model.Id,
                    Asset = this.model,
                    AddressId = -1, //agrega 
                    Address1 = CurrentAddress.A1,
                    Address2 = CurrentAddress.A2,
                    Address3 = CurrentAddress.A3,
                    Comments = string.IsNullOrEmpty(txtComments.Text) || txtComments.Text == MessageStrings.CommentsSample ? null : txtComments.Text,
                    CategoryTier = selectedTier,
                    
                    CLatitude = CurrentAddress.Latitude,
                    CLongitude = CurrentAddress.Longitude,
                    ALatitude = model.Latitude.Value,
                    ALongitude = model.Longitude.Value,

                    DownPayment = 0,
                    TotalCost = selectedTier.Cost,
                    RequestType = App.UserVM.SearchFilters.orderType,
                    EventDateTime = requestedDateTime
                };
                //await Navigation.PushAsync(new XPayments(newOrderVM), true);
            }
            catch
            {
                IsError = true;
            }
            finally
            {
                IsBusy = false;
                //App.HUD.Dismiss();
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
            }
        }
        private void txtComments_Focused(object sender, FocusEventArgs e)
        {
            if(txtComments.Text == MessageStrings.CommentsSample)
            {
                txtComments.TextColor = Color.Black;
                txtComments.Text = string.Empty;
            }
        }

        #endregion

        #region Methods
        private void SetHours()
        {
            //selectedTier = model.Tiers[tierIndex];
            int selectedTime = App.UserVM.SearchFilters.Hours;
            selectedTier = model.Tiers.Where(x => x.NumHours == selectedTime).FirstOrDefault();
            lblTierPrice.Text = selectedTier.Name;
            //lblTierPrice.Text = selectedTime.ToString() + " horas x " + 
            //    string.Format("{0:C0}", selectedTier.Cost);
            //lblHours.Text = selectedTier.Name;
            lblTotal.Text = string.Format("{0:C2}", selectedTier.Cost) + " MXN";
        }
        private void SetAddress()
        {
            txtAddress.Text = MessageStrings.FormatAddress(CurrentAddress);
        }

        #endregion

    }
}