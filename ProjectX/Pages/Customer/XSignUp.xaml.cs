﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XSignUp : ContentPage
    {
        private int StepNum;
        //private bool isLoaded;
        public XSignUp()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Validar Invitación");
            customNavBar.SetModalMode();
            StepNum = 1;


            txtInvitationCode.LineEntryField.Completed += LineEntryField_Completed;

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        private void LineEntryField_Completed(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtInvitationCode.LineEntryField.Text))
            {
                txtInvitationCode.ErrorValue = "* Favor de ingresar in código válido";
            }
            else
            {
                txtInvitationCode.ErrorValue = string.Empty;
                lblCheck.IsVisible = true;
                Transition();
            }
        }

        private void btnNext_Clicked(object sender, EventArgs e)
        {
            Transition();
        }
        private void Transition()
        {
            if (StepNum == 1)
            {
                StepNum++;
                customNavBar.SetTitle("Crear Cuenta");
                lblUserName.IsVisible = lblNewUser.IsVisible = lblCheck.IsVisible = lblHelp2.IsVisible = true;
                lblHelp.IsVisible = txtInvitationCode.LineEntryField.IsVisible = lblCode.IsVisible = false;
                btnNext.IsVisible = true;
                btnNext.Text = Helpers.MessageStrings.StrNext;
            }
            else if (StepNum == 2)
            {
                StepNum++;
                customNavBar.SetTitle("Generar Contraseña");
                lblUserName.IsVisible = lblNewUser.IsVisible = lblCheck.IsVisible = false;
                lblPasswordText.IsVisible = lblPassword.IsVisible = lblConfirm.IsVisible = true;
                btnNext.Text = "Terminar";
            }
            else if (StepNum == 3)
            {
                Navigation.PopToRootAsync(true);
               //MessagingCenter.Send(this, "showconfig", true);
            }
        }
    }
}