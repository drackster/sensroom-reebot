﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using PayPal.Forms.Abstractions;
using ProjectX.ViewModels;
using ProjectX.Helpers;
using System.Text.RegularExpressions;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardConfirmation : ContentPage
    {
        private OrderViewModel Order;
        private CustomerPaymentViewModel Payment;
        private Card card;
        //private bool loginIsVisible;
        public CardConfirmation(Card card, CustomerPaymentViewModel payment, OrderViewModel order)
        {
            InitializeComponent();
            this.card = card;
            Order = order;
            Payment = payment;
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Pago");
            Init();
            GetTDCData();
        }
        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected async override void OnAppearing()
        {
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
        }
        private void Confirmed_Clicked(object sender, EventArgs e)
        {
           
            ConfirmaPago();
            
        }
        private void Init()
        {
            switch (card.CardType)
            {
                case CreditCardType.Visa: lblCardIcon.Text = "\uf1f0"; break;
                case CreditCardType.Mastercard: lblCardIcon.Text = "\uf1f1"; break;
                case CreditCardType.Amex: lblCardIcon.Text = "\uf1f3"; break;
                case CreditCardType.Discover: lblCardIcon.Text = "\uf1f2"; break;
                default: lblCardIcon.Text = "\uf09d"; break;
            }

            lblCardNumber.Text += card.CardType.ToString().ToLower() == "amex" ? card.CardNumber.Substring(12, 3) :
                    card.CardNumber.Substring(12, 4);

            double GTotalAPagar = double.Parse(Order.Precio.ToString()) * 0.2;
            lblGrandTotal.Text = string.Format("{0:C2}", GTotalAPagar);
            //lblGrandTotal.Text = Order.DownPayment.HasValue ? string.Format("{0:C2}", Order.DownPayment.Value)
            //                        : string.Format("{0:C2}", Order.TotalCost);
        }

        private async void ConfirmaPago()
        {
            if (!ValidaDatos())
            {
                return;
            }

            bool assetValidateOnline = await App.apiService.assteOnlineValidate(Order.AssetId);

            if (!assetValidateOnline)
            {
                await DisplayAlert("¡Ups!", Order.Asset.Name.ToString()+ " ya no está disponible", "Ok");
                return;
            }
            

            if (IsBusy) return;
            IsBusy = true;
            bool IsError = false;
            App.HUD.Show(MessageStrings.Requesting, Interfases.MaskType.Black);
            try
            {
                //Tratar de realizar el pago y crear la orden en una sola llamada
                PaymentChargeModel model = new PaymentChargeModel
                {
                    
                    Order = this.Order,
                    Payment = this.Payment
                };
                //model.Payment.NameOnCard = EnNombre.Text + " " + EnApellidos.Text;

                Regex.Replace(EnNombre.Text, @"[0-9\-]", string.Empty).Trim();
                Regex.Replace(EnApellidos.Text, @"[0-9\-]", string.Empty).Trim();
               
                model.Payment.Email = EnMail.Text;
                model.Payment.Telefono = EnTel.Text;
                model.Payment.Nombre = EnNombre.Text;
                model.Payment.Apellidos = EnApellidos.Text;

                var newInvite = await App.apiService.SaveOrderAndPayment(model);
                if (newInvite.ChargeModel.ChargeStatus == OrderStatus.PreChargeSucess)
                {
                    App.HUD.Dismiss();
                    App.HUD.ShowSuccessWithStatus("Orden Generada", 2500);
                    App.apiService.assteOffline(Order.AssetId);
                    //decimal monto = Order.Precio;
                    //double porcentaje = double.Parse(monto.ToString()) * 0.2;
                    //monto = decimal.Parse(porcentaje.ToString());

                    
                    //CustomerCredit SaveCredito = new CustomerCredit();
                    //SaveCredito.UserId = Order.CustomerId.ToString();
                    //SaveCredito.NoOrden = newInvite.OrderId.ToString();
                    //SaveCredito.Monto = decimal.Parse(newInvite.DownPayment.ToString());
                    //SaveCredito.Credito = true;
                    //await App.apiService.SaveCustomerCredit(SaveCredito);

                    await Navigation.PushAsync(new Asset.AInvitation(newInvite), true);
                }
                else
                {
                    await DisplayAlert(newInvite.ChargeModel.ErrorCode,
                        newInvite.ChargeModel.ErrorMessage, "Ok");
                    await Navigation.PopAsync(true);
                }
            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                IsError = true;
            }
            finally
            {
                IsBusy = false;
                App.HUD.Dismiss();
                App.Audio.Vibrate();
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
            }
        }

        private async void GetTDCData()
        {
            CustomerPaymentViewModel DatosPago;
            int id = int.Parse(App.UserVM.Id.ToString());
            DatosPago = await App.apiService.GetTDCData(id);

            if (DatosPago != null)
            {
                if (DatosPago.Nombre == string.Empty || DatosPago.Nombre == null ||
                    DatosPago.Apellidos == string.Empty || DatosPago.Apellidos == null ||
                    DatosPago.Email == string.Empty || DatosPago.Email == null ||
                    DatosPago.Telefono == string.Empty || DatosPago.Telefono == null)
                {
                    slDatosClienteTDC.IsEnabled = true; ;
                    return;
                }
                else
                {
                    EnNombre.Text = DatosPago.Nombre;
                    EnApellidos.Text = DatosPago.Apellidos;
                    EnTel.Text = DatosPago.Telefono;
                    EnMail.Text = DatosPago.Email;
                    slDatosClienteTDC.IsEnabled = false;
                }
                
            }
            else
            {
                slDatosClienteTDC.IsEnabled = true; ;
                return;
            }
        }

        private  bool ValidaDatos()
        {
            if (EnNombre.Text == string.Empty || EnApellidos.Text == string.Empty || EnMail.Text == string.Empty || EnTel.Text == string.Empty)
            {

                DisplayAlert("", "Por favor complete los datos requeridos para preocesar el pago.", "OK");
                return false;
            }
            else
            {
                return true;
            }
            
        }
    }
}