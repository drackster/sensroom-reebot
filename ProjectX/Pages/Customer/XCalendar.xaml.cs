﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    public partial class XCalendar : ContentPage
    {
        public XCalendar()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Elige fecha");

            customNavBar.SetEditMode("Aplicar");
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;

            calendar.Locale = new System.Globalization.CultureInfo("es-MX");
        }

        private void CustomNavBar_EditButtonTapped(object sender, Templates.EditModeArgs e)
        {
            Navigation.PopAsync(true);
        }
    }
}