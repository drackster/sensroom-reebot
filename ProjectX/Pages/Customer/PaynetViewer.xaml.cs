﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PaynetViewer : ContentPage
    {
        public PaynetViewer(InvitationViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            NavigationPage.SetBackButtonTitle(this, "");
            this.Title = "Pago en tienda";
            //ToolbarItem toolbarItem = new ToolbarItem("Cerrar", "", CloseView, ToolbarItemOrder.Default);
            //ToolbarItems.Add(toolbarItem);

            //#if DEBUG
            //string url = "https://sandbox-dashboard.openpay.mx/paynet-pdf/";
            //#else
            //            string url = "https://dashboard.openpay.mx/paynet-pdf/";
            //#endif

            //url += model.ChargeModel.MerchantId + "/" + model.ChargeModel.Reference;
            //paynetWebView.Source = url;
            //paynetWebView.zoom

        }
        //private async void CloseView()
        //{
        //    await Navigation.PopModalAsync(true);
        //}
    }
}