﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Asset;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XMain : ContentPage
    {
        private SearchModel searchModel;
        private bool IsLoaded;
        public XMain()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            App.SetNavigationTextColor(Color.Black);

            searchModel = new SearchModel()
            {
                DistanceRadius = 5,
                MaxPrice = 2000,
                Service = new AServiceViewModel() { Id = 1, Name = "H-M" }
            };

            this.BindingContext = searchModel;
            
            //ToolbarItem item = new ToolbarItem("filtrar", "icono_filtros.png", OnFilterTapped, ToolbarItemOrder.Default);
            //ToolbarItems.Add(item);
            lstAssets.ItemsSource = App.apiService.GetAssets();
            lstAssets.ItemSelected += LstAssets_ItemSelected;
        }

        #region Events

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        private void LstAssets_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selected = e.SelectedItem as AssetViewModel;
            if(selected!=null)
            {
                Navigation.PushAsync(new ADetail(selected), true);
                lstAssets.SelectedItem = null;
            }
        }
        private void OnFilterTapped()
        {
            Navigation.PushAsync(new XFilters(), true);
        }
        public void OnHamburgerIconTapped(Object sender, EventArgs e)
        {
            Element current = this;
            while (current.Parent != null)
            {
                current = current.Parent;
                if (current.GetType().Name == "RootPage")
                {
                    break;
                }
            }

            var master = current as MasterDetailPage;

            if (master != null)
            {
                master.IsPresented = true;
            }
        }
        public void OnGPSIconTapped(Object sender, EventArgs e)
        {
            DisplayAlert("Alert", "GPS Icon Tapped", "Ok");
        }
        private void SchedulerGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            DisplayAlert("Alert", "DateTime Picker", "Ok");
        }
        #endregion


    }
}