﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    public partial class XConfig : ContentPage
    {
        public XConfig()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Configuración");
            customNavBar.SetHamburguerMode();
            switchCalc.IsToggled = true;

            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            customNavBar.SetEditMode("Listo", "");
        }
        private void CustomNavBar_EditButtonTapped(object sender, Templates.EditModeArgs e)
        {
            MessagingCenter.Send(this, "showmain", true);
        }
    }
}