﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XReceipt : ContentPage
    {
        private InvitationViewModel Model;
        public XReceipt(InvitationViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Pago en Oxxo");
            Model = model;


            long referenceId = 0;
            if (model.ChargeModel != null && model.ChargeModel.Reference.HasValue)
            {
                referenceId = model.ChargeModel.Reference.Value;
            }
            else if (model.OxxoRef.HasValue)
            {
                referenceId = model.OxxoRef.Value;
            }
            
                                
            txtReference.Text = String.Format("{0:0000-0000-0000-00}", referenceId);
            txtAmount.Text = string.Format("{0:C2}", model.DownPayment.HasValue ? model.DownPayment : model.Cost);
            txtExpires.Text = Helpers.DTUtil.ConvertDT(model.ExpiresOn.ToLocalTime(), true, true, false, true);
        }


    }
}