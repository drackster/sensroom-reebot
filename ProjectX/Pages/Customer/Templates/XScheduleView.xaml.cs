﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Extended;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XScheduleView : ContentView
    {
        public Button CloseButton { get { return btnClose; } }
        public DateTime CDT;
        public CustomDateTimePicker DTPicker { get { return pckDate; } }
        public XScheduleView()
        {
            InitializeComponent();
            DateTime dt = DateTime.Now;
            pckDate.MinimumDate = dt;
            pckDate.MaximumDate = dt.AddDays(App.ScheduleFutureDays);//todo
            pckDate.DTChanged += PckDate_DTChanged;
            pckDate.IsVisible = false;
        }
        private void PckDate_DTChanged(object sender, Extended.DateTimePickerArgs e)
        {
            CDT = e.DT;
        }

    }
}