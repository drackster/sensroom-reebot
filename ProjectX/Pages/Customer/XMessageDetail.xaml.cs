﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XMessageDetail : ContentPage
    {
        public XMessageDetail(int MessageType)
        {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Detalle");
            customNavBar.SetHamburguerMode();
            Mensaje(MessageType);
        }


        private void Mensaje(int type)
        {
            switch (type)
            {
                case 1:
                    byte[] utf8bytes = Encoding.UTF8.GetBytes("No te preocupes la diversión sigue.\n\nAhora cuentas con un crédito para invitar a alguien más a tú evento.\n\nNo importa si el costo es distinto.");
                    string r = Encoding.UTF8.GetString(utf8bytes, 0, utf8bytes.Length);
                    lbMesDetail.Text = r;
                    break;
            }
                
        }


    }
}