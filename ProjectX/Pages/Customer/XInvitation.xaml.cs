﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XInvitation : ContentPage
    {
        private string code;
        public XInvitation()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Invitar amig@");
            customNavBar.SetHamburguerMode();
            code = txtCode.Text = App.RegisterVM.UserName;
        }
        private async void btnNext_Clicked(object sender, EventArgs e)
        {
            try
            {
                //Navigation.PushModalAsync(new XConfig(), true);
                await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage()
                {
                    Text = "Entra a 'http://luxxapp.com/' , descargar el app y registrate con el código de invitación: " +"'"+ code+"'",
                    Title = "Invitación de " + "'"+code+"'"
                }, 
                new Plugin.Share.Abstractions.ShareOptions()
                {
                    ChooserTitle = "Invitación de " + "'"+code+ "'"
                });
            }
            catch { }
        }

        protected override bool OnBackButtonPressed()
        {

            return true;
        }
    }
}