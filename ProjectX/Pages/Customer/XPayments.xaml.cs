﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using ProjectX.Helpers;
using System.Diagnostics;
using PayPal.Forms;
using PayPal.Forms.Abstractions;
//using PayPal.Forms.Abstractions.Enum;
using SegmentedControl.FormsPlugin.Abstractions;
using ProjectX.Interfases;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XPayments : ContentPage
    {
        #region Properties
        private OrderViewModel Order;
        private CustomerCredit CustomerCred;
        private AddressViewModel CurrentAddress;
        private AssetViewModel model;
        private ACategoryTierViewModel selectedTier;
        //private int tierIndex;
        private bool AddressChanged;
        //private bool loginIsVisible;
        private IConektaHelper conektaTokenizer;
        #endregion

        #region DependecyService
        public IConektaHelper ConektaTokenizer
        {
            get
            {
                if (conektaTokenizer == null)
                    conektaTokenizer = DependencyService.Get<IConektaHelper>();
                return conektaTokenizer;
            }
        }
        #endregion

        #region cTor
        public XPayments(AssetViewModel model, AddressViewModel currentAddress, CustomerCredit creditoCliente)
        {
            InitializeComponent();
            customNavBar.SetTitle("Confirmación y Pago");
            NavigationPage.SetHasNavigationBar(this, false);

            this.model = model;
            this.Order = new OrderViewModel();
            CurrentAddress = currentAddress;
            //tierIndex = 0;
           // txtAddress.IsVisible = false;
            BindOrder();
            //SegControl1.ValueChanged += SegControl1_ValueChanged;
            txtAddress.TextChanged += TxtAddress_TextChanged;
            CustomerCred = creditoCliente;
            if (creditoCliente.Credito)
            {
                slMetodoPago.IsVisible = false;
                slOpcionesPago.IsVisible = false;
                slBtnInvitar.IsVisible = true;
                Order.DownPayment = 0;
            }
            else
            {
                slMetodoPago.IsVisible = true;
                slOpcionesPago.IsVisible = true;
                slBtnInvitar.IsVisible = false;
                Order.DownPayment = model.Precio * .2M;
            }
            SetHours();
            SetLabels();
           // GetMediosPago();
            //Order.DownPayment = selectedTier.Cost * .2M;
        }
        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected override void OnAppearing()
        {
            GetMediosPago();
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
        }

        #endregion

        #region Events
        private void TxtAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            AddressChanged = true;
        }
        private void TxtComments_TextChanged(object sender, TextChangedEventArgs e)
        {
        //    int ccount = txtComments.Text == null ? 0 : txtComments.Text.Length;
        //    if (ccount > 99)
        //        txtComments.Text = e.OldTextValue;
        //    lblCommentsCount.Text = ccount.ToString();
        //}
        //private void txtComments_Focused(object sender, FocusEventArgs e)
        //{
        //    lblCommentsCount.IsVisible = true;
        //    if (txtComments.Text == MessageStrings.CommentsSample)
        //    {
        //        txtComments.TextColor = Color.Black;
        //        txtComments.Text = string.Empty;
        //    }
        }
        private void SegControl1_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            //if (selectedTier == ((ACategoryTierViewModel)null))
            //    return;
            //if (SegControl1.SelectedSegment == 1)
            //{
            //    SegmentText.Text = Helpers.MessageStrings.PaymentMode1;
            //    //slCash.IsVisible = false;
            //    //lblGrandTotal.Text = string.Format("{0:C2}", selectedTier.Cost);
            //    Order.DownPayment = selectedTier.Cost;
            //}
            //else
            //{
            //    //Para pago en tienda, el monto max es de 9,999.99 //todo
            //    //slCash.IsVisible = true;
            //    SegmentText.Text = Helpers.MessageStrings.PaymentMode0;
            //    Order.DownPayment = selectedTier.Cost * .2M;
            //    //lblGrandTotal.Text = string.Format("{0:C2}", Order.DownPayment);
            //}
        }
        private async void Card_Tapped(object sender, EventArgs e)
        {
            //await Navigation.PushAsync(new XFormularioTDC());
            //return;
            if (IsBusy) return;
            if (await ValidateAddress())
            {
                IsBusy = true;
                bool IsError = false;
                CustomerPaymentViewModel newCard = new CustomerPaymentViewModel();
                try
                {
                    Card card = null;
                    //Optional parameter CardIOLogo("PayPal", "CardIO" or "None") for ScanCard method by default "PayPal" is used
                    var result = await CrossPayPalManager.Current.ScanCard(CardIOLogo.None);

                    if (result.Status == PayPalStatus.Cancelled)
                    {
                        Debug.WriteLine("Cancelled");
                    }
                    else if (result.Status == PayPalStatus.Successful)
                    {
                        if (result.Card != null)
                        {
                            Debug.WriteLine($"CardNumber: {result.Card.CardNumber}, CardType: {result.Card.CardType.ToString()}, Cvv: {result.Card.Cvv}, ExpiryMonth: {result.Card.ExpiryMonth}");
                            Debug.WriteLine($"ExpiryYear: {result.Card.ExpiryYear}, PostalCode: {result.Card.PostalCode}, RedactedCardNumber: {result.Card.RedactedCardNumber}, Scaned: {result.Card.Scaned}");
                            card = result.Card;
                            //await Navigation.PushAsync(new CardConfirmation(card, Order), true);
                        }

                        //if (result.Card.CardImage != null)
                        //{
                        //    //CardImage.Source = result.Card.CardImage;//Imagen
                        //}
                    }

                    if (card != null)
                    {
                        //App.HUD.Show("Validando tarjeta...", Interfases.MaskType.Black);
                        //UNA VES CAPTURADA LA TARJETA, GENERAMOS EL TOKEN DE CONEKTA
                        //utilizando el nombre del cliente sin digitos ni caracteres especiales
                        string customerName = ViewModels.Util.GetCustomerName(App.UserVM.UserName);
                        var cToken = await ConektaTokenizer.TokenizeCard(card.CardNumber, customerName, card.Cvv, card.ExpiryYear, card.ExpiryMonth);

                        //OPENPAY * YA NO SE UTILIZA
                        //Obtener los ultimos 4 digitos, 
                        //jamas debemos de enviar el numero completo por internet
                        var cctype = card.CardType.ToString();
                        string last4digits = cctype.ToLower() == "amex" ? card.CardNumber.Substring(12, 3) : card.CardNumber.Substring(12, 4);

                        var paymentModel = new CustomerPaymentViewModel
                        {
                            NameOnCard = customerName,
                            CreditCardNumber = last4digits,
                            ProviderId = cToken,
                            ExpirationDate = new DateTime(card.ExpiryYear, card.ExpiryMonth, 1),
                            CreditCardType = card.CardType.ToString(),
                            CVC = string.Empty
                        };
                        
                        BindOrder();
                        Order.paymentMethod = PaymentMethod.OpenPay;
                        //Order.PaymentMethodId = newCard.Id; Ahora este Id lo generamos al mismo tiempo de generar el cobro
                        await Navigation.PushAsync(new CardConfirmation(card, paymentModel, Order), true);
                    }
                }
                catch (Exception ex)
                {
                    IsError = true;
                    await App.Database.LogException(ex);
                }
                finally
                {
                    IsBusy = false;
                    //App.HUD.Dismiss();
                }

                if (IsError)
                {
                    if (string.IsNullOrEmpty(newCard.ProviderId))
                        await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                    else
                        await DisplayAlert(MessageStrings.GeneralErrorTitle, newCard.ProviderId, "Ok");
                }
            }
        }
        private async void Paypal_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            if (await ValidateAddress())
            {
                IsBusy = true;
                string ErrorMsg = null;
                try
                {
                    BindOrder();
                    var result = await CrossPayPalManager.Current.Buy(
                        new PayPalItem("Orden de servicio PX", (Order.Cost - Order.Tax), "MXN"), Order.Tax);
                    if (result.Status == PayPalStatus.Cancelled)
                    {
                        Debug.WriteLine("Cancelled");
                    }
                    else if (result.Status == PayPalStatus.Error)
                    {
                        Debug.WriteLine(result.ErrorMessage);
                        ErrorMsg = result.ErrorMessage;
                    }
                    else if (result.Status == PayPalStatus.Successful)
                    {
                        App.HUD.Show(MessageStrings.PleaseWait, Interfases.MaskType.Black);
                        Debug.WriteLine(result.ServerResponse.Response.Id);
                        Order.paymentMethod = PaymentMethod.Paypal;
                        Order.PaypalTransId = result.ServerResponse.Response.Id;
                        Order.PaymentMethodId = null; //reset
                        var newInvite = await App.apiService.SaveOrder(Order);
                        App.HUD.Dismiss();
                        App.HUD.ShowSuccessWithStatus("Orden Generada", 2500);
                        await Navigation.PushAsync(new Asset.AInvitation(newInvite), true);
                    }
                }
                catch (Exception ex)
                {
                    await App.Database.LogException(ex);
                    ErrorMsg = MessageStrings.GeneralError;
                }
                finally
                {
                    IsBusy = false;
                    App.HUD.Dismiss();
                    App.Audio.Vibrate();
                }

                if (ErrorMsg != null)
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, ErrorMsg, "Ok");
                }
            }
        }
        private async void Paynet_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            if (await ValidateAddress())
            {
                IsBusy = true;
                bool IsError = false;
                bool response = false;
                try
                {
                    BindOrder();
                    response = await DisplayAlert(MessageStrings.GeneralConfirmationTitle,
                        "¿Generar orden de pago en Oxxo por "
                        + string.Format("{0:C2}", Order.Cost) + " mxn ?", "Sí", "No");

                    if (response)
                    {
                        App.HUD.Show(MessageStrings.PleaseWait, Interfases.MaskType.Black);

                        Order.paymentMethod = PaymentMethod.Paynet;
                        Order.PaymentMethodId = null; //forzar a que no sea tarjeta
                        var newInvite = await App.apiService.SaveOrderOxxo(Order);
                        if (newInvite.ChargeModel.ChargeStatus == OrderStatus.PreChargeSucess)
                        {
                            App.HUD.Dismiss();
                            App.HUD.ShowSuccessWithStatus("Orden Generada", 2500);
                            await Navigation.PushAsync(new XReceipt(newInvite));
                            await Navigation.PushAsync(new Asset.AInvitation(newInvite, false, true), true);
                        }
                        else
                        {
                            await DisplayAlert(newInvite.ChargeModel.ErrorCode,
                                newInvite.ChargeModel.ErrorMessage, "Ok");
                            //await Navigation.PopAsync(true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    await App.Database.LogException(ex);
                    IsError = true;
                }
                finally
                {
                    IsBusy = false;
                    App.HUD.Dismiss();
                    if (response)
                        App.Audio.Vibrate();
                }

                if (IsError)
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                }
            }
        }

        #endregion

        #region Methods
        private void SetHours()
        {
            //selectedTier = model.Tiers[tierIndex];
            int selectedTime = App.UserVM.SearchFilters.Hours < 1 || App.UserVM.SearchFilters.Hours > 3 ? 1 : App.UserVM.SearchFilters.Hours;
            selectedTier = model.Tiers.Where(x => x.NumHours == selectedTime).FirstOrDefault();
            //lblTierPrice.Text = selectedTier.Name;
            //lblTierPrice.Text = selectedTime.ToString() + " horas x " + 
            //    string.Format("{0:C0}", selectedTier.Cost);
            if (selectedTier == ((ACategoryTierViewModel)null))
            {
                selectedTier = model.Tiers.FirstOrDefault();
            }
            //lblHours.Text = selectedTier.Name;
            //lblTotal.Text = string.Format("{0:C2}", selectedTier.Cost) + " MXN";
        }
        private async Task<bool> ValidateAddress()
        {
            // se comentó esta validación mientras de da razón por la cual pide la direccion para hacer el pago.

            //if(string.IsNullOrEmpty(txtAddress.Text))
            //{
            //    await DisplayAlert(MessageStrings.GeneralErrorTitle, "Favor de ingresar una dirección válida para continuar", "Ok");
            //    return false;
            //}
            return true;
        }
        private void BindOrder()
        {
            DateTime? requestedDateTime = App.UserVM.SearchFilters.orderType == OrderType.ASAP ? 
                                          DTUtil.FindNearestRSVPTime(App.ScheduleFutureMinutes,true) :                                          
                                          App.UserVM.SearchFilters.SelectedDateTime;
            //ViewModels.Util.StripMinSec(DateTime.Now)
            //Si el cliente modificó la direccion manualmente
            //Entonces no hay forma de dividirlo en 3 partes tan facilmente,
            //Por lo tanto optamos por enviarla en un solo string
            if (AddressChanged)
            {
                CurrentAddress.A1 = txtAddress.Text;
                CurrentAddress.A2 = CurrentAddress.A3 = null;
            }
            else if (CurrentAddress.A1 == null)
                CurrentAddress.Parse();

            this.Order = new OrderViewModel
            {
                AssetId = model.Id,
                Asset = this.model,
                AddressId = -1, //agrega 
                Address1 = CurrentAddress.A1,
                Address2 = CurrentAddress.A2,
                Address3 = CurrentAddress.A3,
                //    Comments = string.IsNullOrEmpty(txtComments.Text) || txtComments.Text == MessageStrings.CommentsSample ? null : txtComments.Text,
                CategoryTier = selectedTier,
                Precio = model.Precio,
                CLatitude = CurrentAddress.Latitude,
                CLongitude = CurrentAddress.Longitude,
                ALatitude = model.Latitude.Value,
                ALongitude = model.Longitude.Value,

                DownPayment = Order.DownPayment,
                //TotalCost = selectedTier.Cost,
                TotalCost = model.Precio,
                RequestType = App.UserVM.SearchFilters.orderType,
                EventDateTime = requestedDateTime
            };
        }
        public void SetLabels()
        {
            lblAsset.Text = model.Name;// + " - " + model.CategoryName;
            //lblTotal.Text = model.Precio.ToString();
            double total = double.Parse(model.Precio.ToString());
            lblTotal.Text = string.Format("{0:C2}",total) + " MXN";// string.Format("{0:C2}", selectedTier.Cost) + " MXN";
                                                                   //lblTime.Text = selectedTier.Name;
            double anticipo = double.Parse(model.Precio.ToString()) * 0.2;
            lblAnticipo.Text = CustomerCred.Credito ?  "0" + " MXN" : string.Format("{0:C2}", anticipo) + " MXN";
            double pendiente = double.Parse(model.Precio.ToString()) * 0.8;
            lblPendiente.Text = string.Format("{0:C2}", pendiente) + " MXN";
            //if(!credito)
            if (CustomerCred.Credito)
            {
                // SegmentText.Text = "Reserva con $ 0.00 MXN , el monto pendiente se entrega en efectivo al recibir a tu invitad@";
                SegmentText.Text = "Cuentas con crédito para enviar una invitación sin costo.";
            }

            else
            {
                SegmentText.Text = "Reserva con $" + anticipo.ToString() + " MXN , el monto pendiente se entrega en efectivo a tu invitad@. Los gastos por traslado están incluidos";               
            }




            //if (selectedTier != ((ACategoryTierViewModel)null))
            //{
            //    lblTotal.Text = selectedTier.precio.ToString("{0:C2}");// string.Format("{0:C2}", selectedTier.Cost) + " MXN";
            //    //lblTime.Text = selectedTier.Name;
            //    double anticipo = double.Parse(selectedTier.precio.ToString()) * 0.2;
            //    lblAnticipo.Text = string.Format("{0:C2}", anticipo) + " MXN";
            //    double pendiente = double.Parse(selectedTier.precio.ToString()) * 0.8;
            //    lblPendiente.Text = string.Format("{0:C2}", pendiente) + " MXN";

            //    SegmentText.Text = "Reserva con $" + anticipo.ToString() + " MXN , la diferencia se cubrirá antes de empezar el evento";
            //}
            lblAddress.Text = Order.Address1;
            txtAddress.Text = MessageStrings.FormatAddress(CurrentAddress);
            //lblHours.Text = selectedTier.Name;
            //lblTotal.Text = string.Format("{0:C2}", model..TotalCost) + " MXN";
         //   SegmentText.Text = Helpers.MessageStrings.PaymentMode0;


            if (App.UserVM.SearchFilters.orderType == OrderType.ASAP)
                lblWhen.Text = "Lo antes posible";
            else///calendar
            {
                var selectedDate = App.UserVM.SearchFilters.SelectedDateTime;
                lblWhen.Text = DTUtil.ConvertDT(selectedDate.Value, true, true, false, true);
            }

            //if (Order.RequestType == OrderType.ASAP)
            //    lblWhen.Text = "Ahora";
            //else
            //{
            //    //string.Format("{0:dd}", Order.EventDateTime.Value) + " de " +
            //    //Util.GetMonthAsString(Order.EventDateTime.Value.Month).Substring(0, 3) + "" +
            //    //string.Format("{0:yy HH:mm}", Order.EventDateTime.Value);
            //    lblWhen.Text = DTUtil.ConvertDT(Order.EventDateTime.Value, true, true, true, true);
            //}

            //txtComments.WidthRequest = App.ScreenWidth;
            //txtComments.Text = MessageStrings.CommentsSample;

        }

        #endregion

        private  void Button_Clicked(object sender, EventArgs e)
        {
            
            EnviarInvitacionConCredito(CustomerCred);
        }

        private async void EnviarInvitacionConCredito(CustomerCredit cred)
        {

            bool assetValidateOnline = await App.apiService.assteOnlineValidate(Order.AssetId);

            if (!assetValidateOnline)
            {
                await DisplayAlert("¡Ups!", Order.Asset.Name.ToString() + " ya no está disponible", "Ok");
                return;
            }

            BindOrder();


            string customerName = ViewModels.Util.GetCustomerName(App.UserVM.UserName);
            var paymentModel = new CustomerPaymentViewModel
            {
                NameOnCard = customerName,
                CreditCardNumber = "####",
                ProviderId = "",
                ExpirationDate =DateTime.UtcNow,
                CreditCardType = "",
                CVC = string.Empty
            };

            PaymentChargeModel model = new PaymentChargeModel
            {
                Order = this.Order,
                Payment = paymentModel
            };

            model.Order.AddressId = -1;
            

            App.HUD.Show(MessageStrings.PleaseWait, Interfases.MaskType.Black);
               // Debug.WriteLine(result.ServerResponse.Response.Id);
                Order.paymentMethod = PaymentMethod.Credito;
                Order.PaypalTransId = "pago con credito";// result.ServerResponse.Response.Id;
                Order.PaymentMethodId = null; //reset
                Order.Id = long.Parse(cred.NoOrden);
                Order.CustomerName = cred.UserId;
           
                //var newInvite = await App.apiService.SaveOrder(Order);
            var newInvite = await App.apiService.SaveOrderAndPayment(model);

           
            if (newInvite.ChargeModel.ChargeStatus == OrderStatus.All)
            {
                newInvite.Address = model.Order.Address1;
                App.HUD.Dismiss();
                App.HUD.ShowSuccessWithStatus("Orden Generada", 2500);
                App.apiService.assteOffline(Order.AssetId);

                await App.apiService.UpdateCustomerCredit(cred.UserId);
                await Navigation.PushAsync(new Asset.AInvitation(newInvite), true);
            }

               
            
        }

        private async void GetMediosPago()
        {
            var pagos = await App.apiService.getMediosDePago();

            foreach (var Dis in pagos)
            {
                switch (Dis.id)
                {

                    case 1:
                        slTDC.IsVisible = Dis.status;
                        break;
                    case 2:
                        sloxxo.IsVisible = Dis.status;
                        break;                        
                    case 3:
                        slpp.IsVisible = Dis.status;
                        break;
                }
            }
        }
    }
}