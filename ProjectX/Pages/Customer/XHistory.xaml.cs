﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using ProjectX.Pages.Asset;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XHistory : ContentPage
    {
        #region Properties
        private bool IsLoaded;
        private NotificationRequestModel requestModel;
        private NotificationViewModel currentNotificationVM;
        private List<NotificationViewModel> nots;
        //private bool loginIsVisible;
        #endregion

        #region cTor
        public XHistory()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            
            customNavBar.SetTitle("Buzón");
            customNavBar.SetHamburguerMode();
          
            nots = new List<NotificationViewModel>();

            if (App.MSender == MessageSender.Customer)
            {
                requestModel = new NotificationRequestModel()
                {
                    SType = MessageSender.Customer,
                    CustomerId = App.UserVM.Id,
                    MType = MessageType.All
                };//traer todo       
            }
            else
            {
                requestModel = new NotificationRequestModel()
                {
                    SType = MessageSender.Asset,
                    AssetId = App.AssetVM.Id,
                    MType = MessageType.All
                };//traer todo   
            }     
        }

        #endregion

        #region Events
        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected async override void OnAppearing()
        {
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
            await GetNotifications();

            if (IsLoaded)
            {
                //Actualizar tiempos, porque puede ser que aparesca horario de hace 2 mins cuando en realidad fue de ayer
                UpdateDT();
            }
        }



        protected override bool OnBackButtonPressed()
        {
           
            return true;
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();



            //stopTimer();
        }
        private async void Notification_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;

            try
            {
                var row = sender as AssetNotificationViewTemplate;
                var context = row.BindingContext as NotificationViewModel;
                if (context.Status == MessageStatus.Created)
                {
                    context.Status = MessageStatus.Read;
                    //App.SetBadgeCount("Invitaciones", App.Nots.Where(x => x.Status == MessageStatus.Created).Count()-1);
                    row.BindingContext = context;//update

                    //fire and forget
                    await App.apiService.UpdateNotificationStatus(new NotificationUpdateModel()
                    { NotificationId = context.Id, MsgStatus = MessageStatus.Read });

                }

                currentNotificationVM = context;

                switch (context.MType)
                {
                    case MessageType.Invitation:
                        if (App.MSender == MessageSender.Asset)
                        {
                            var invite = new AInvitation(context.invitation, true, false);
                            invite.InvitationStatusChanged += Invite_InvitationStatusChanged;
                            await Navigation.PushAsync(invite, true);
                        }
                        break;
                    case MessageType.AcceptedInvitation:
                        break;
                    case MessageType.CanceledInvitation:
                        if (App.MSender == MessageSender.Customer)
                        {
                            // 1 mensage rechazo
                            await Navigation.PushAsync(new XMessageDetail(1), true);
                        }
                        else
                        {
                            var invite = new AInvitation(context.invitation, true, false);
                            invite.InvitationStatusChanged += Invite_InvitationStatusChanged;
                            await Navigation.PushAsync(invite, true);
                        }
                        break;
                    case MessageType.RejectedInvitation:
                        if (App.MSender == MessageSender.Customer)
                        {
                            // 1 mensage rechazo
                            await Navigation.PushAsync(new XMessageDetail(1), true);
                        }
                        else
                        {
                            var invite = new AInvitation(context.invitation, true, false);
                            invite.InvitationStatusChanged += Invite_InvitationStatusChanged;
                            await Navigation.PushAsync(invite, true);
                        }
                        break;

                    case MessageType.CustomerMessage:

                        NotificationRequestModel model1 = new NotificationRequestModel()
                        {
                            OrderId = 0,//context.OrderId,
                            AssetId = context.AssetId,
                            CustomerId = context.CustomerId,
                            IsChat = true
                        };

                        await Navigation.PushAsync(new Common.MessageList(model1, context.SenderName), true);
                        break;

                    case MessageType.AssetMessage:
                        NotificationRequestModel model = new NotificationRequestModel()
                        {
                            OrderId = context.OrderId,
                            AssetId = context.AssetId,
                            CustomerId = context.CustomerId,
                            IsChat = true
                        };
                        await Navigation.PushAsync(new Common.MessageList(model, context.SenderName), true);
                        break;
                    case MessageType.AuthRequest:

                        if (context.RequestActivationStatus != 4)//si no ha sido autorizado aun
                        {
                            var response = await DisplayActionSheet("¿Conoces a " + context.SenderName + "?"
                                , "No lo sé", "No lo conozco", new string[] { "Sí, autorizo" });

                            if (response == "No lo conozco")
                                context.RequestActivationStatus = 5;
                            else if (response == "Sí, autorizo")
                                context.RequestActivationStatus = 4;

                            //Si autoriza o no, actualizar
                            if (context.RequestActivationStatus == 4
                                || context.RequestActivationStatus == 5)
                            {
                                await App.apiService.UpdateRequestStatus(new RegisterViewModel()
                                {
                                    Id = context.RequestId,
                                    ActivationStatus = context.RequestActivationStatus
                                });

                                context.MessageText = ViewModels.Helper.GetRequestText(context.SenderName, 
                                    context.RequestActivationStatus);
                            }
                        }
                        row.BindingContext = null;
                        row.BindingContext = context;//update 
                        break;

                    case MessageType.All:
                        NotificationRequestModel modelA = new NotificationRequestModel()
                        {
                            OrderId = context.OrderId,
                            AssetId = context.AssetId,
                            CustomerId = context.CustomerId,
                            IsChat = true
                        };
                        await Navigation.PushAsync(new Common.MessageList(modelA, context.SenderName), true);
                        break;
                }
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                IsBusy = false;
            }
        }
        private void Invite_InvitationStatusChanged(object sender, InvitationStatusEventArgs e)
        {
            currentNotificationVM.invitation.oStatus = e.Status;
        }

        #endregion

        #region Methods
        private async Task GetNotifications(bool showActivity = false)
        {
            if (IsBusy) return;
            IsBusy = true;
            System.Diagnostics.Debug.WriteLine("Getting new notifications...");
            //if(showActivity) SetActivity(true);
            if (nots.Count == 0)
                SetActivity(true);
            else
            {
                requestModel.LastId = nots[0].Id;
            }
            try
            {
                bool append = nots.Count > 0;
                var newNots = await App.apiService.GetNotifications(requestModel);
                if (newNots.Count > 0)
                {
                    SetNots(newNots, append);
                    App.SetBadgeCount("Invitaciones", nots.Where(x => x.Status == MessageStatus.Created).Count());
                    nots.InsertRange(0, newNots);
                }

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                //if(showActivity)
                SetActivity(false);
                IsBusy = false;
            }

        }
        /**/
        private void SetNots(List<NotificationViewModel> newNots, bool append)
        {
            bool exist = false;
            foreach (var n in newNots)
            {
                var item = new AssetNotificationViewTemplate() { BindingContext = n };
                var tpr = new TapGestureRecognizer();
                tpr.Tapped += Notification_Tapped;
                item.GestureRecognizers.Add(tpr);
                exist = false;
                foreach (AssetNotificationViewTemplate tmp in slMessages.Children)
                {
                    if (((NotificationViewModel)tmp.BindingContext).Id == n.Id)
                        exist = true;
                }
                if (!append)//primera carga
                {
                    if(!exist)
                        slMessages.Children.Add(item);
                }
                else
                {
                    //Ir insertando al inicio
                    if(!exist)
                        slMessages.Children.Insert(0, item);
                }
            }
            IsLoaded = true;
        }
        private void SetActivity(bool IsEnabled)
        {
            notificationsActivity.IsVisible = notificationsActivity.IsEnabled = IsEnabled;
        }
        private void UpdateDT()
        {
            foreach (var i in slMessages.Children)
            {
                var item = i as AssetNotificationViewTemplate;
                if (item != null)
                {
                    var context = item.BindingContext;
                    item.BindingContext = null;
                    item.BindingContext = context;
                }
            }
        }
        #endregion

        #region Timer
        //private void startTimer()
        //{
        //    if (!App.AdvcTimerRunning)
        //    {
        //        App.AdvcTimer.InitTimer(App.DeviceTimerTimeOutSeconds * 1000, null, true);
        //        App.AdvcTimerRunning = true;
        //        App.AdvcTimer.timerElapsed += App_TimerElapsed;
        //    }
        //}
        //private void stopTimer()
        //{
        //    App.AdvcTimer.StopTimer();
        //    App.AdvcTimerRunning = false;
        //    App.AdvcTimer.timerElapsed -= App_TimerElapsed;
        //}
        
        #endregion

    }
}