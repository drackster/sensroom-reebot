﻿#region Using
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using ProjectX.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Asset;
using ProjectX.Helpers;
using System.IO;
using ProjectX.Pages.Templates;
using ProjectX.Models;
using Plugin.Geolocator;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System.Windows.Input;
using Plugin.Geolocator.Abstractions;
using System.Diagnostics;
using System.Threading;
using static ProjectX.Pages.RootPageMaster;

#endregion

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XList : ContentPage
    {
        #region Private Members
        //private bool IsLoaded;
        public event EventHandler<EventArgs> LogoutComplete;
        public bool Bupdate = false;
        private AssetListViewModel ViewModel;
        private Position currentPosition;
        private AddressViewModel currentAddress;
        private XSchedule schedulePage;
        private bool getPosition;
        private bool getCatalog;
        private bool getAddress;
        private CancellationTokenSource cts_ClosedOrders;
        private CancellationTokenSource cts_GPS;
        private int cuantos;
        private bool ValidaPais;
        //private bool disablePositionChange;
        #endregion

        #region Properties

        public DateTime tiempo1;
        public DateTime tiempo2;
        public DateTime tiempo3;

        public int currentPageIndex
        {
            get { return App.UserVM.SearchFilters.PageIndex; }
            set { App.UserVM.SearchFilters.PageIndex = value; }
        }
        
        public int currentPageSize = 5;//todo db
        #endregion

        #region cTor
        public XList()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //ToolbarItem item = new ToolbarItem("filtrar", "icono_filtros.png", OnFilterTapped, ToolbarItemOrder.Default);
            //ToolbarItems.Add(item);
          
            currentPageIndex = 1; //always default
            if (App.UserVM.SearchFilters.sortType == 0)
                App.UserVM.SearchFilters.sortType = SortType.Distance;
            if (App.UserVM.SearchFilters.Hours == 0)
                App.UserVM.SearchFilters.Hours = 1;//todo validate


            //lstAssets.RowHeight = Convert.ToInt32(App.ScreenHeight / 2.20);
            lstAssets.ItemSelected += lstAssets_ItemSelected;
            lstAssets.ItemAppearing += lstAssets_ItemAppearing;
            lstAssets.ItemDisappearing += lstAssets_ItemDisappearing;
          
            ViewModel = new AssetListViewModel();
            ViewModel.Assets = new ObservableCollection<AssetViewModel>();
            this.BindingContext = ViewModel;
            //AddBlanks();
           
            //force first time
            getPosition = getAddress = getCatalog = true;

            //Si el usuario cambia su ubicación, ir monitoreando direccion y coordenadas
            CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
            CrossGeolocator.Current.PositionError += Current_PositionError;
            // btnOrder.Text = "Ordenar por...";
            //btnStiengs.Text = "Preferncias";
           // MessageStrings.FormatAddress(schedulePage.selectedAddress, true);
        }
        
        protected async override void OnAppearing()
        {   
            Bupdate = true;
            base.OnAppearing();
            updateMessage();
            await Reload();
            await RefreshCatalog(true);
            update();
        }
        protected override void OnDisappearing()
        {
            tiempo2 =DateTime.UtcNow;
            base.OnDisappearing();
            if (cts_ClosedOrders != null && !cts_ClosedOrders.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_ClosedOrders...");
                cts_ClosedOrders.Cancel();
            }
            if (cts_GPS != null && !cts_GPS.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_GPS...");
                cts_GPS.Cancel();
            }

            Bupdate = false;
        }
        #endregion

        #region Events
        private async void SchedulePage_ScheduleUpdated(object sender, EventArgs e)
        {
            lblAddress.Text = getAddressString();
            if (schedulePage.orderType == OrderType.ASAP)
            {
                lblWhen.Text = "Ahora";
                //disablePositionChange = false;
                App.UserVM.SearchFilters.orderType = OrderType.ASAP;
            }
            else
            {
                //string.Format("{0:dddd, dd MMMM hh:mm tt}", schedulePage.SelectedDate);
                //lblWhen.Text = Util.GetMonthAsString(schedulePage.SelectedDate.Value.Month).Substring(0,3) + "" +
                //               string.Format("{0:dd HH:mm}", schedulePage.SelectedDate);
                
                var sdt = ViewModels.Util.StripMinSec(schedulePage.SelectedDate.Value);
                
                lblWhen.Text = DTUtil.ConvertDT(sdt, true, true, false, true);
                App.UserVM.SearchFilters.SelectedDateTime = sdt;
                App.UserVM.SearchFilters.orderType = OrderType.Calendar;
                //disablePositionChange = true;
            }
            App.UserVM.SearchFilters.PageIndex = 1;
            App.UserVM.SearchFilters.Hours = schedulePage.currentTime;
            getCatalog = false;//force
            await RefreshCatalog(getCatalog);
        }
        private string getAddressString()
        {
            if (schedulePage == null || schedulePage.selectedAddress == null)
            {
                return string.Empty;
            }
                
            if (schedulePage.selectedAddress.Thoroughfare == null)
            {
                //Quiere decir que el usuario selecciono una direccion nueva
                currentAddress = schedulePage.selectedAddress;
                App.UserVM.SearchFilters.Latitude = schedulePage.selectedAddress.Latitude;
                App.UserVM.SearchFilters.Longitude = schedulePage.selectedAddress.Longitude;
                return schedulePage.selectedAddress.A1;

            }
            App.UserVM.SearchFilters.Latitude = currentPosition.Latitude;
            App.UserVM.SearchFilters.Longitude = currentPosition.Longitude;
            //si no, nos quedamos con la ubicacion normal
            return MessageStrings.FormatAddress(schedulePage.selectedAddress, true);
        }
        private async void GPSRetry(object sender, EventArgs e)
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Location);
            if (status == PermissionStatus.Granted)
            {
                await Reload();
            }
        }
        private async void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            //if (!disablePositionChange)
            //{
                if (currentPosition != e.Position)
                {
                    currentPosition = e.Position;
                    await GetAddress();
                    getCatalog = true;
                }
            //}
        }
        private void Current_PositionError(object sender, PositionErrorEventArgs e)
        {
            SetGPSError();
        }
        private async void lstAssets_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            var selected = e.SelectedItem as AssetViewModel;
            if (selected != null)
            {
                SetNavColor(Color.White);
                try
                {
                    await Navigation.PushAsync(new ADetail(selected, currentAddress), true);
                }
                catch (Exception ex)
                {
                    await App.Database.LogException(ex);
                    await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
                }
            }
            lstAssets.SelectedItem = null;
            IsBusy = false;
        }
        private void lstAssets_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var item = e.Item as AssetViewModel;
            Debug.WriteLine("Appearing " + item.ItemIndex.ToString());

            //Check if its the last item of the list to trigger append
            if ((item.ItemIndex + 1) == (currentPageIndex * currentPageSize))
            {
                CheckForAppend();
            }
        }
        private void lstAssets_ItemDisappearing(object sender, ItemVisibilityEventArgs e)
        {
            var item = e.Item as AssetViewModel;
            Debug.WriteLine("DisAppearing " + item.ItemIndex.ToString());       
        }
        private async void OnOrderByTapped()
        {
           var result = await DisplayActionSheet("Ordenar por", "Cancelar", null,
                new string[] { "Distancia", "Precio" });

            if (!string.IsNullOrEmpty(result))
            {
                SortType s = SortType.Distance;
                switch (result)
                {
                    //case "Ingreso": s = SortType.JoinDate; break;
                   // case "Valoración": s = SortType.Rating; break;
                    case "Precio": s = SortType.Price; break;
                }
                if (App.UserVM.SearchFilters.sortType != s)
                {  //solamente si cambió
                    App.UserVM.SearchFilters.PageIndex = 1;//reset
                    App.UserVM.SearchFilters.sortType = s;
                    await RefreshCatalog(false);
                }
            }
        }
        public void OnHamburgerIconTapped(Object sender, EventArgs e)
        {
            Element current = this;
            while (current.Parent != null)
            {
                current = current.Parent;
                if (current.GetType().Name == "RootPage")
                {
                    break;
                }
            }

            var master = current as MasterDetailPage;

            if (master != null)
            {
                master.IsPresented = true;
            }
        }
        private async void Address_Tapped(object sender, EventArgs e)
        {
            if (currentAddress == null || IsBusy) return;
            IsBusy = true;
            try
            {
                if (schedulePage == null)
                {
                    schedulePage = new XSchedule(currentAddress, currentPosition, App.UserVM.SearchFilters.Hours);//new
                    schedulePage.ScheduleUpdated += SchedulePage_ScheduleUpdated;
                }
                else
                {
                    //esto hace que las busquedas sean alrededor de la posicion actual, aunque haya cambiado
                    schedulePage.currentPosition = new Xamarin.Forms.Maps.MapSpan(
                     new Xamarin.Forms.Maps.Position(currentPosition.Latitude, currentPosition.Longitude)
                    , currentPosition.Latitude, currentPosition.Longitude);
                }
                schedulePage.HasChanges = false;//reset
                                                //scheulePage.ScheduleUpdated -= SchedulePage_ScheduleUpdated; //destroy

                await Navigation.PushModalAsync(schedulePage, true);
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        #endregion

        #region Methods
        public async Task Reload()
        {
            if (IsBusy || (!getPosition && !getAddress && !getCatalog)) //return;
            IsBusy = true;
           // App.HUD.Show("Buscando personas cerca de tu ubicación...", Interfases.MaskType.Black);
            UpdateActivity(true);
            string errorMsg = null;
            slNoGPS.IsVisible = slSadFace.IsVisible = false;
            try
            {
                if (getPosition)
                {
                    errorMsg = await GetPosition();
                    if(errorMsg != null)
                    {
                        IsBusy = false;
                        return;
                    }
                }
                if (getAddress && errorMsg == null)
                {
                    errorMsg = await GetAddress();
                }

                if (getCatalog && errorMsg == null)
                {
                    errorMsg = await RefreshCatalog(false);
                }

                if (errorMsg == null)
                {
                    slButtons.IsVisible = true;
                    getPosition = getAddress = getCatalog = false;
                    //IsLoaded = true;
                }
                await checkClosedOrders();//do not wait
                ////await Navigation.PushModalAsync(new Common.CAddReview(App.UserVM), true);
            }
            catch (Exception ex)
            {
               // await App.Database.LogException(ex);
               // errorMsg = MessageStrings.GeneralError;
            }
            finally
            {
                IsBusy = false;
                UpdateActivity(false);
                //App.HUD.Dismiss();
            }
            if (errorMsg != null)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, errorMsg, "Ok");
            }
        }
        private async Task checkClosedOrders()
        {
            //Buscar ordenes que calificar, hacer un método que regrese ordenes sin calificar
            OrderSearchModel osm = new OrderSearchModel()
            {
                MSender = MessageSender.Customer,
                orderStatus = OrderStatus.Closed,
                reviewStatus = ReviewStatus.Pending
            };
            cts_ClosedOrders = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
            var closedOrders = await App.apiService.GetOrders(osm,cts_ClosedOrders.Token);
            
            foreach(var o in closedOrders)
            {
                await Navigation.PushModalAsync(new Common.CAddReview(o.Asset, o.Id), true);
            }
        }
        private void SetNavColor(Color color)
        {
            var np = this.Parent as NavigationPage;
            if (np != null)
                np.BarTextColor = color;
        }
        private async void CheckForAppend()
        {
            if (ViewModel.TotalCount > ViewModel.AssetCount)
            {
                Debug.WriteLine("Triggering append page " + (currentPageIndex + 1).ToString());
                currentPageIndex += 1;
                App.UserVM.SearchFilters.PageIndex = currentPageIndex;
                await RefreshCatalog(true);
            }
        }
        private void AddBlanks()
        {
            int count = Device.Idiom == TargetIdiom.Phone ? 2 : 3;
            for(int i=0; i< count; i++)
            {
                var item = new BlankAssetView();
                item.HeightRequest = App.ScreenHeight / 2.20;
                //lstAssets.Children.Add(item);
            }
        }
        private async Task<string> RefreshCatalog(bool Append)
        {
            //if (IsBusy) return null;
            //IsBusy = true;
            UpdateActivity(true, Append);
            try
            {
                if (App.UserVM.SearchFilters.sortType == 0)
                    App.UserVM.SearchFilters.sortType = SortType.Distance;

                if (App.UserVM.SearchFilters.orderType == 0)
                    App.UserVM.SearchFilters.orderType = OrderType.ASAP;

                if (App.UserVM.SearchFilters.PreferenceId == null)
                {
                    App.UserVM.SearchFilters.PreferenceId = 1;
                }
                var vm = await App.apiService.GetAssetList(App.UserVM.SearchFilters);
                if (Append)
                {
                    try
                    {
                         cuantos = vm.AssetCount;
                            ViewModel.Assets.Clear();
                            //Get the last item count before appending
                            int ix = ViewModel.AssetCount;
                            //Append new products to current ViewModel (Addrange is not supported)
                            foreach (AssetViewModel a in vm.Assets)
                                ViewModel.Assets.Add(a);
                            var itemToScroll = ViewModel.Assets[ix];
                            //listView
                            lstAssets.ScrollTo(itemToScroll, ScrollToPosition.MakeVisible, true);
                            if (ViewModel != null)
                                this.BindingContext = null;
                            ViewModel = vm;
                            this.BindingContext = ViewModel;
                            slSadFace.IsVisible = ViewModel.AssetCount == 0;
                            lstAssets.IsVisible = !slSadFace.IsVisible;
                        
                    }
                    catch (Exception ex)
                    { }
                    
                }
                else
                {
                    if (ViewModel != null)
                        this.BindingContext = null;
                    ViewModel = vm;
                    this.BindingContext = ViewModel;

                    //Mostrar error en caso de que no haya resultados
                    slSadFace.IsVisible = ViewModel.AssetCount == 0;
                    lstAssets.IsVisible = !slSadFace.IsVisible;
                }

                return null;
            }
            catch
            {

            }
            finally
            {
                UpdateActivity(false, Append);
            }
           // this.BindingContext = ViewModel;
            return MessageStrings.GeneralError;
        }
        private async Task<string> GetPosition()
        {
            try
            {
                if (currentPosition == null)
                {
                    cts_GPS = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
                    currentPosition = await CrossGeolocator.Current.GetPositionAsync(
                        new TimeSpan(0, 0, App.LocationTimeOutSeconds), cts_GPS.Token);
                }
                if(currentPosition!=null && currentPosition.Latitude>0)
                    return null;
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
                SetGPSError();
            }
            
            return "GPSERROR"; 
        }
        private async Task<string> GetAddress()
        {
            try
            {
                if (currentPosition != null)//currentAddress == null && 
                {
                    var addresses = await CrossGeolocator.Current.GetAddressesForPositionAsync(currentPosition);

                    var ca = addresses.FirstOrDefault();

                    ActivaFiltroxPais(ca.CountryCode);

                    if (ca.CountryCode != "MX")// SRVICIO SÓLO DENTRO DE LA REPÚBLICA MEXICANA.
                    {
                        if (App.UserVM.UserName != "AlbertoSosa" || App.UserVM.UserName != "beti1")// usuario de pruebas appstopre
                        {
                            return "Your location doesn´t support Luxx";
                        }
                         
                    }

                    if (ca != null)
                    {
                        currentAddress = SearchUtil.ParseAddressViewModel(ca);

                        App.UserVM.SearchFilters.Latitude = ca.Latitude;
                        App.UserVM.SearchFilters.Longitude = ca.Longitude;
                        App.UserVM.SearchFilters.orderType = OrderType.ASAP;//default

                        lblAddress.Text = MessageStrings.FormatAddress(currentAddress, true);
                        //lblAddress.TextColor = Color.Black; await DisplayAlert("", "Your location dosen´t support Luxx");
                        return null;
                    }
                }
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
                return "No se pudo determinar tu ubicación";
            }
            return null;
            
        }
        private void UpdateActivity(bool IsActive, bool Append=false)
        {
            if(Append)
                activityIndicatorBottom.IsVisible = activityIndicatorBottom.IsRunning = IsActive;
            else
                activityIndicator.IsVisible = activityIndicator.IsRunning = IsActive;
        }
        private void SetGPSError()
        {
            slNoGPS.IsVisible = true;
            lstAssets.IsVisible = false;
        }
        #endregion

        private void OnOrderByTapped(object sender, EventArgs e)
        {

        }

        protected override bool OnBackButtonPressed()
        {

            return true;
        }

        private async void update()
        {
            await Task.Delay(10000);
            if (Bupdate)
            {
                
                OnAppearing();
            }
            else
            {
                return;
            }
            
        }

        private async void updateMessage()
        {

            var requestModel = new NotificationRequestModel()
            {
                SType = App.MSender,
                MType = MessageType.All
            };//traer todo

            if (App.MSender == MessageSender.Customer)
                requestModel.CustomerId = App.UserVM.Id;
            else
                requestModel.AssetId = App.AssetVM.Id;

            var unread = await App.apiService.GetNotificationCount(requestModel);

            if (unread > 0)
            {
                lblNot.IsVisible = true;
                lblNot.Text = unread.ToString();
                bwNot.IsVisible = true;
            }
            else
            {
                lblNot.IsVisible = false;
                lblNot.Text = unread.ToString();
                bwNot.IsVisible = false;
            }

            if (App.MSender == MessageSender.Customer)
            {  
                var credito = await App.apiService.GetCustomCredit(App.UserVM.UserName);// revisamos si el cliente tiene crédito disponible
                await App.Database.DeleteCustomerCredit();
                await App.Database.SaveCustomerCredit(credito);
            }           

            if (App.MSender == MessageSender.Customer)
                App.SetBadgeCount("Buzón", unread);
           
        }

        private async void ActivaFiltroxPais(string Clave)
        {
            bool active = await App.apiService.GetFiltroPais(Clave);

            if (active)
            {
                ValidaPais =  true;
            }
            else
            {
                ValidaPais =  false;
            }          
        }
    }
}