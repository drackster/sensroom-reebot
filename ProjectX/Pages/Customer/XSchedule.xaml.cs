﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;
using System.Text;
using ProjectX.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Asset;
using ProjectX.Helpers;
using System.IO;
using ProjectX.Pages.Templates;
using ProjectX.Models;
using Plugin.Geolocator;
using System.Windows.Input;
using Plugin.Geolocator.Abstractions;
using TK.CustomMap.Api;
using Xamarin.Forms.Maps;
using System.Globalization;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XSchedule : ContentPage
    {
        #region Properties
        public event EventHandler<EventArgs> ScheduleUpdated;
        public DateTime? SelectedDate;
        public DateTime MinDate;
        public int currentTime;
        public AddressViewModel selectedAddress;
        public AddressViewModel currentAddress;
        public AddressViewModel newAddress;
        public OrderType orderType = OrderType.ASAP;//default
        public Xamarin.Forms.Maps.MapSpan currentPosition;
        private string currentQuery;
        private ObservableCollection<AddressViewModel> predAddresses;
        public bool HasChanges;
        private bool IsLoaded;
        public bool IsDroid = false;
        private bool scrollToHeight;
        //private bool loginIsVisible;
        
        #endregion

        #region cTor
        public XSchedule(AddressViewModel currentAddress, 
                        Plugin.Geolocator.Abstractions.Position position, 
                        int time)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //customNavBar.SetModalMode();
            customNavBar.SetTitle("Lugar y fecha");
            customNavBar.SetIconText("X");
            customNavBar.SetEditMode("Listo");

            selectedAddress = this.currentAddress = currentAddress;
            currentPosition = new MapSpan(new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude)
                ,position.Latitude,position.Longitude);

            predAddresses = new ObservableCollection<AddressViewModel>();
            lstPredictions.ItemsSource = predAddresses;
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            txtNewAddress.TextChanged += TxtNewAddress_TextChanged;
            txtNewAddress.Completed += TxtNewAddress_Completed;
            txtNewAddress.Focused += TxtNewAddress_Focused;
            lblCurrentAddress.Text = Helpers.MessageStrings.FormatAddress(currentAddress);

            var dt = DTUtil.FindNearestRSVPTime(App.ScheduleFutureMinutes);
            pckDate.MinimumDate = dt;
            pckDate.MaximumDate = dt.Date.AddDays(App.ScheduleFutureDays);
            pckDate.Date = MinDate = dt;
            //btnClose.Clicked += BtnClose_Clicked;
            pckDate.DTChanged += PckDate_DTChanged;
            currentTime = time;
            SetMaxPrice(1);
            //Solo para android
            if (Device.RuntimePlatform == Device.Android)
            {
                pckTime.Time = new TimeSpan(dt.Hour, dt.Minute, 0);
                pckDate.DateSelected += PckDate_DateSelected;
                //pckTime.Unfocused += PckTime_Unfocused;
                pckTime.PropertyChanged += PckTime_PropertyChanged;
                IsDroid = true;
            }


            if (App.SettingsVM != null)
            {
                InitPrefs();
                InitCategories();
                InitRanges();
                IsLoaded = true;
            }
        }

        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected async override void OnAppearing()
        {
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
            HasChanges = false;
            if (IsBusy) return;
            IsBusy = true;

            bool IsError = false;
            try
            {
                //test
                //App.SetBadgeCount("Pendientes", 11);
                if (App.SettingsVM == null)
                {
                    App.HUD.Show(MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                    App.SettingsVM = await App.apiService.GetXSettings();
                    InitPrefs();
                    InitCategories();
                    InitRanges();
                }
                else if (IsLoaded)
                {
                }
                
                //Si anteriormente tenía una fecha seleccionada
                //Validar que no haya pasado el tiempo
                if (App.UserVM.SearchFilters.SelectedDateTime.HasValue)
                {
                    var cdt = App.UserVM.SearchFilters.SelectedDateTime.Value;
                    //tiempo minimo
                    var mdt = DTUtil.FindNearestRSVPTime(App.ScheduleFutureMinutes);

                    if(cdt < mdt)
                    {
                        SelectedDate = mdt;
                        lblSelectedDate.Text = Helpers.DTUtil.ConvertDT(mdt, true, true, false, true); // string.Format("{0:dddd, dd MMMM hh:mm tt}", e.DT);
                        pckDate.MinimumDate = mdt;
                        pckDate.MaximumDate = mdt.Date.AddDays(App.ScheduleFutureDays);
                        pckDate.Date = MinDate = mdt;

                        //Solo para android
                        if (Device.RuntimePlatform == Device.Android)
                        {
                            pckTime.Time = new TimeSpan(mdt.Hour, mdt.Minute, 0);
                        }
                    }

                }


            }
            catch
            {
                IsError = true;
            }
            finally
            {
                IsLoaded = true;
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        
        #endregion

        #region Events
        private void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            if(HasChanges)
            {
                if (ScheduleUpdated != null)
                    ScheduleUpdated.Invoke(this, new EventArgs());
            }
            Navigation.PopModalAsync(true);
        }
        private async void PckDate_DTChanged(object sender, Extended.DateTimePickerArgs e)
        {
            if (e.DT >= MinDate)
            {
                HasChanges = true;
                orderType = OrderType.Calendar;
                SelectedDate = e.DT;
                lblSelectedDate.Text = Helpers.DTUtil.ConvertDT(e.DT, true, true, false, true); // string.Format("{0:dddd, dd MMMM hh:mm tt}", e.DT);
                                                                                                //Updated();
            }
            else
            {
                var minstr = Helpers.DTUtil.ConvertDT(MinDate, true, true, false, true);
                await DisplayAlert(MessageStrings.GeneralErrorTitle, "Seleccionar una fecha mayor a " + minstr, "Ok");
            }
        }
        private void TxtNewAddress_Focused(object sender, FocusEventArgs e)
        {
            chk1.IsVisible = false;
        }
        private async void TxtNewAddress_Completed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewAddress.Text) &&
                      txtNewAddress.Text != currentQuery &&
                      txtNewAddress.Text.Length > 3)
            {
                currentQuery = txtNewAddress.Text;
                await GetPredictions();
            }
            else
            {
                lstPredictions.IsVisible = false;
                scrollToHeight = false;
            }
        }
        private async void TxtNewAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
          if(!string.IsNullOrEmpty(txtNewAddress.Text) && 
                    e.NewTextValue!= e.OldTextValue && 
                    txtNewAddress.Text.Length > 2)
          {
                currentQuery = txtNewAddress.Text;
                await GetPredictions();
          }
          else
          {
                lstPredictions.IsVisible = false;
                scrollToHeight = false;
          }
        }
        private void CurrentLocation_Tapped(object sender, EventArgs e)
        {
            HasChanges = true;
            Device.BeginInvokeOnMainThread(() =>
            {
                chk1.IsVisible = true;
                chk0.IsVisible = false;
            });
            selectedAddress = currentAddress;
            //ShowHideNewAddress(false);
            //Updated();
        }
        private void NewLocation_Tapped(object sender, EventArgs e)
        {
            HasChanges = true;
            if (newAddress != null)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    chk1.IsVisible = false;
                    chk0.IsVisible = true;
                });
                selectedAddress = newAddress;
                //Updated();
            }
        }
        private void ASAP_Tapped(object sender, EventArgs e)
        {
            HasChanges = true;
            Device.BeginInvokeOnMainThread(() =>
            {
                chk2.IsVisible = true;
                chk3.IsVisible = false;

            });
            orderType = OrderType.ASAP;
            //SelectedDate = null;
            //Updated();
        }
        private void Schedule_Tapped(object sender, EventArgs e)
        {
            //POr default ponemos el tipo y la fecha
            orderType = OrderType.Calendar;
            var sdt = SelectedDate.HasValue ? SelectedDate.Value : MinDate;
            //Device.BeginInvokeOnMainThread(() =>
            //{
                chk2.IsVisible = false;
                chk3.IsVisible = true;
                pckDate.Date = sdt;
                lblSelectedDate.Text = Helpers.DTUtil.ConvertDT(
                    sdt, true, true, false, true); // string.Format("{0:dddd, dd MMMM hh:mm tt}", e.DT);
                pckDate.Focus();
            //});
        }
        private void PckDate_DateSelected(object sender, DateChangedEventArgs e)
        {
            SelectedDate = e.NewDate;
            //Device.BeginInvokeOnMainThread(() =>
            //{
                chk2.IsVisible = false;
                chk3.IsVisible = true;
                //pckDate.Date = sdt;
                lblSelectedDate.Text = Helpers.DTUtil.ConvertDT(
                    SelectedDate.Value, true, true, false, true); 
                pckTime.Focus();
            //});
            orderType = OrderType.Calendar;
        }

        private void PckTime_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName)
            {
                var dt = SelectedDate.Value;
                //Ahora establecer la hora
                SelectedDate = new DateTime(dt.Year, dt.Month, dt.Day,
                    pckTime.Time.Hours, pckTime.Time.Minutes, 0);
                lblSelectedDate.Text = Helpers.DTUtil.ConvertDT(
                   SelectedDate.Value, true, true, false, true);
                orderType = OrderType.Calendar;
            }
        }
        private async void lstPredictions_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as AddressViewModel;
            if(item!=null)
            {
                HasChanges = true;
                if (IsDroid)
                {
                    var pos = await TKNativePlacesApi.Instance.GetDetails(item.PlaceId);
                    item.Latitude = pos.Coordinate.Latitude;
                    item.Longitude = pos.Coordinate.Longitude;
                }
                selectedAddress = newAddress = item;
                txtNewAddress.Text = string.Empty;//limpiar texto busqueda
                //establecer nueva direccion
                lblNewAddress.Text = selectedAddress.A;
                chk1.IsVisible = false;
                chk0.IsVisible = true;
                ShowHideNewAddress(true);
            }
            //La lista desaparece de todas formas porque puede ser que el usuario seleccione
            //un registro en blanco
            lstPredictions.IsVisible = false;
            lstPredictions.HeightRequest = 0;
            predAddresses.Clear();
            lstPredictions.SelectedItem = null;
        }
        private void MinusGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if(currentTime>1)
            {
                currentTime--;
                SetHours();
                HasChanges = true;
                SetMaxPrice(currentTime);
            }
            //if (tierIndex > 0)
            //{
            //    tierIndex--;
            //    SetHours();
            //}
        }
        private void AddGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            if (currentTime<3)
            {
                currentTime++;
                SetHours();
                HasChanges = true;
                SetMaxPrice(currentTime);
            }
            //if (tierIndex < model.Tiers.Count - 1)
            //{
            //    tierIndex++;
            //    SetHours();
            //}
        }

        private void SetMaxPrice(int currentTime)
        {

            pckCategory.Items.Clear();            
            pckCategory.Items.Add("$1,000.00");
            pckCategory.Items.Add("$1,500.00");
            pckCategory.Items.Add("$2,000.00");
            pckCategory.Items.Add("$3,000.00");
            pckCategory.Items.Add("$4,000.00");
            pckCategory.Items.Add("$5,000.00");
            pckCategory.Items.Add("Más");

            //switch(currentTime)
            //{
            //    case 1:
            //    default:
            //        pckCategory.Items.Clear();
            //        pckCategory.Items.Add("$800.00");
            //        pckCategory.Items.Add("$1,000.00");
            //        pckCategory.Items.Add("$1,500.00");
            //        pckCategory.Items.Add("$2,000.00");
            //        pckCategory.Items.Add("$3,500.00");
            //        pckCategory.Items.Add("Más");
            //        break;
            //    case 2:
            //        pckCategory.Items.Clear();
            //        pckCategory.Items.Add("$1,000.00");
            //        pckCategory.Items.Add("$1,300.00");
            //        pckCategory.Items.Add("$2,000.00");
            //        pckCategory.Items.Add("$2,500.00");
            //        pckCategory.Items.Add("$4,000.00");
            //        pckCategory.Items.Add("Más");
            //        break;
            //    case 3:
            //        pckCategory.Items.Clear();
            //        pckCategory.Items.Add("$1,200.00");
            //        pckCategory.Items.Add("$1,500.00");
            //        pckCategory.Items.Add("$2,500.00");
            //        pckCategory.Items.Add("$3,000.00");
            //        pckCategory.Items.Add("$5,000.00");
            //        pckCategory.Items.Add("Más");
            //        break;
            //}

        }

        #endregion

        #region Methods
        public void Updated()
        {
            HasChanges = true;
        }
        public void ShowHideNewAddress(bool IsShowing)
        {
            //grdCurrentAddress.TranslationY = IsShowing ? 10 : 0;
            //grdSched.TranslationY = IsShowing ? 20 : 10;
            grdNewAddress.IsVisible = IsShowing;
        }
        private async Task GetPredictions()
        {
            try
            {
                UpdateActivity(true);
                var predictions = await TKNativePlacesApi.Instance.GetPredictions(currentQuery, currentPosition);
                predAddresses.Clear();                
                foreach (var pred in predictions)
                {
                    var newA = new AddressViewModel();
                    if (IsDroid)
                    {
                        newA.PlaceId = pred.Details.FormattedAddress;
                    }
                    else
                    {
                        newA.Latitude = pred.Details.Coordinate.Latitude;
                        newA.Longitude = pred.Details.Coordinate.Longitude;
                    }

                    var split = SearchUtil.GetSplittedPrediction(pred.Description,",");

                    newA.A = pred.Description;
                    newA.A1 = split[0];
                    newA.A2 = split[1];
                    newA.A3 = split[2];

                    predAddresses.Add(newA);
                    
                }
                if (predAddresses.Count > 0)
                {
                    //InitSearchResults();
                    lstPredictions.HeightRequest = predAddresses.Count * lstPredictions.RowHeight;
                    lstPredictions.IsVisible = true;//predictions.Any();

                    if (scrollToHeight == false)
                    {
                        scrollToHeight = true;
                        var coods = SearchUtil.GetScreenCoordinates(lstPredictions);
                        var hh = coods[1] - txtNewAddress.Height;
                        System.Diagnostics.Debug.WriteLine("scrolling to" + hh.ToString());
                        await scroll1.ScrollToAsync(coods[0], hh, true);
                    }
                }
                else
                {
                    lstPredictions.IsVisible = false;
                    lstPredictions.HeightRequest = 0;
                }
                //////todo MAINTHREAD
                //grdPredictionsRow.Height = 
                //    lstPredictions.HeightRequest = predAddresses.Count * 50;
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                UpdateActivity(false);
            }

        }
        //private void InitSearchResults()
        //{
        //    try
        //    {
        //        var coods = SearchUtil.GetScreenCoordinates(slNewAddress);
        //        //movemos la ventana dependiendo de la posicion y el scroll 
        //        grdPredictions.TranslationY = (coods[1] + (slNewAddress.Height * 2)); 
        //            //- (scroll1.ScrollY > 0 ? scroll1.ScrollY : 0);
        //    }
        //    catch { }
        //    System.Diagnostics.Debug.WriteLine("scrollY:" + scroll1.ScrollY.ToString());
        //}
        private void UpdateActivity(bool IsActive)
        {
            activityIndicator.IsVisible = activityIndicator.IsRunning = IsActive;
        }
        
        /// <summary>
        /// This function Splits the Title and Subtitle of the Map Callout
        /// </summary>
        /// <param name="Location"></param>
        /// <returns></returns>
        public string[] GetTitleSubtitle(string Location, string separator = "\n")
        {
            string[] result = new string[2];
            int i = Location.IndexOf(separator);
            if (i >= 0)
            {
                result[0] = Location.Substring(0, i).Trim();
                result[1] = Location.Substring(i + 1);
                return result;
                //VAS
                //Location = Location.Substring(i + 1);


                //i = Location.IndexOf(separator);
                //if (i >= 0)
                //{
                //    result[1] = Location.Substring(0, i).Trim();
                //}
                //else
                //    result[1] = Location.Trim();

                //return result;
            }

            result[0] = Location;
            result[1] = string.Empty;
            return result;

        }
        private void SetHours()
        {
            //lblHours.Text = currentTime.ToString() + " horas";
            //lblTierPrice.Text = string.Format("{0:C0}", selectedTier.Cost);
            //lblHours.Text = selectedTier.Name;
            //lblTotal.Text = string.Format("{0:C0}", selectedTier.Cost);
        }


        #endregion

        #region Filter Methods
        private void InitPrefs()
        {
            //Order by Assetview
            App.SettingsVM.Preferences = App.SettingsVM.Preferences.OrderBy(x => x.CIndex).ToList();

            int index = -1, count = 0;

            foreach (var pref in App.SettingsVM.Preferences)
            {
                if (App.UserVM.SearchFilters.PreferenceId == pref.Id)
                    index = count;
                pckPrefs.Items.Add(pref.Name);
                count++;
            }
            pckPrefs.SelectedIndex = index == -1 ? 0 : index;
            App.UserVM.SearchFilters.PreferenceId = App.SettingsVM.Preferences[pckPrefs.SelectedIndex].Id;
            pckPrefs.SelectedIndexChanged += PckPrefs_SelectedIndexChanged; ;
        }
        private void InitCategories()
        {
            //pckCategory.Items.Add("Todas");
            int index = 0;

            //foreach (var cat in App.SettingsVM.Categories)
            //{
            //    if (App.UserVM.SearchFilters.CategoryId == cat.Id)
            //        index = count;
            //    pckCategory.Items.Add(cat.Name);
            //    count++;
            //}
            //pckCategory.Items.Add("$800.00");
            //pckCategory.Items.Add("$1,000.00");
            //pckCategory.Items.Add("$1,500.00");
            //pckCategory.Items.Add("$2,000.00");
            //pckCategory.Items.Add("$3,500.00");
            //pckCategory.Items.Add("Más");
            //pckCategory.SelectedIndex = index;
            ////UpdateTiers();
            //pckCategory.SelectedIndexChanged += PckCategory_SelectedIndexChanged;
        }
        private void InitRanges()
        {
            var sf = App.SettingsVM;//Establecer defaults
            var csf = App.UserVM.SearchFilters;//Establecer los valores del usuario

            int maxAge = csf.MaxAge;
            int maxAgeIndex = 0;
            int maxDistance = csf.MaxDistanceRadius;
            int maxDistanceIndex = 0;

            //llenar combo de edades
            for (int i = sf.MinAge, index=0; i <= sf.MaxAge; i++,index++)
            {
                pckAge.Items.Add(i.ToString());
                if (i == maxAge)
                    maxAgeIndex = index;
            }
            pckAge.SelectedIndex = maxAgeIndex;
            pckAge.SelectedIndexChanged += PckAge_SelectedIndexChanged;
            //lblAge.Text = maxAge + " años";

            for (int d = 0, maxDistIndex = 0; d < sf.Distances.Count; d++, maxDistIndex++)
            {
                pckDistance.Items.Add(sf.Distances[d].Name);
                if (sf.Distances[d].Km == maxDistance)
                    maxDistanceIndex = maxDistIndex;
            }

            pckDistance.SelectedIndex = maxDistanceIndex;
            pckDistance.SelectedIndexChanged += PckDistance_SelectedIndexChanged;
            //lblDistance.Text = maxDistance + " km";
        }
        private void UpdateTiers()
        {
            //var cats = App.SettingsVM.Categories;
            //ACategoryTierViewModel iTier = null;
            //ACategoryTierViewModel fTier = null;

            //if (pckCategory.SelectedIndex == 0) //todos
            //{
            //    iTier = cats[0].Tiers[0];
            //    var lastCat = cats[cats.Count - 1];
            //    fTier = lastCat.Tiers[lastCat.Tiers.Count - 1];
            //}
            //else
            //{
            //    var cat = cats[pckCategory.SelectedIndex - 1];
            //    iTier = cat.Tiers[0];
            //    fTier = cat.Tiers[cat.Tiers.Count - 1];
            //}

            //if (iTier.Cost == fTier.Cost)
            //    lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost);
            //else
            //    lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost) + "-" + string.Format("{0:C0}", fTier.Cost);
        }

        #endregion

        #region Filter Events
        private void PckCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            App.UserVM.SearchFilters.CategoryId = 1;

            /*
                pckCategory.Items.Add("$1,000.00");
            pckCategory.Items.Add("$1,500.00");
            pckCategory.Items.Add("$2,000.00");
            pckCategory.Items.Add("$3,000.00");
            pckCategory.Items.Add("$4,000.00");
            pckCategory.Items.Add("$5,000.00");
            pckCategory.Items.Add("Más");
             */

            var precio = pckCategory.SelectedItem == "$1,000.00" ? 1000
                        : pckCategory.SelectedItem == "$1,500.00" ? 1500
                        : pckCategory.SelectedItem == "$2,000.00" ? 2000
                        : pckCategory.SelectedItem == "$3,000.00" ? 3000
                        : pckCategory.SelectedItem == "$4,000.00" ? 4000
                        : pckCategory.SelectedItem == "$5,000.00" ? 5000
                        : 1000000;

            App.UserVM.SearchFilters.Precio = (decimal)precio;

            //if (pckCategory.SelectedIndex == -1 || pckCategory.SelectedIndex == 5)
            //{
            //    App.UserVM.SearchFilters.CategoryId = null;
            //}
            //else
            //{
            //    int cost = int.Parse(pckCategory.Items[pckCategory.SelectedIndex], NumberStyles.Currency);
            //    switch (currentTime)
            //    {
            //        case 1:
            //            App.UserVM.SearchFilters.CategoryId = App.SettingsVM.Categories.Where(x => x.Cost1 == cost).FirstOrDefault().Id;
            //            break;
            //        case 2:
            //            App.UserVM.SearchFilters.CategoryId = App.SettingsVM.Categories.Where(x => x.Cost2 == cost).FirstOrDefault().Id;
            //            break;
            //        case 3:
            //            App.UserVM.SearchFilters.CategoryId = App.SettingsVM.Categories.Where(x => x.Cost3 == cost).FirstOrDefault().Id;
            //            break;
            //    }
            //}
            Updated();
        }
        private void PckPrefs_SelectedIndexChanged(object sender, EventArgs e)
        {
            App.UserVM.SearchFilters.PreferenceId = App.SettingsVM.Preferences[pckPrefs.SelectedIndex].Id;
            Updated();
        }
        private void PckAge_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lblAge.Text = pckAge.Items[pckAge.SelectedIndex] + " años";
            App.UserVM.SearchFilters.MaxAge = Convert.ToInt32(pckAge.Items[pckAge.SelectedIndex]);
        }
        private void PckDistance_SelectedIndexChanged(object sender, EventArgs e)
        {
           // PckDistance.Text = pckDistance.Items[pckDistance.SelectedIndex];
            var d = pckDistance.Items[pckDistance.SelectedIndex].Replace(" km", "");
            App.UserVM.SearchFilters.MaxDistanceRadius = Convert.ToInt32(d);
            //TODO SALVAR A BD LOCAL
            Updated();
        }
        private void BtnOk_Clicked(object sender, EventArgs e)
        {
            if (HasChanges)
            {
                if (ScheduleUpdated != null)
                    ScheduleUpdated.Invoke(this, new EventArgs());
            }
            Navigation.PopModalAsync(true);
        }

        #endregion

    }

}