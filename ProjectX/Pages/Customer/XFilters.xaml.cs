﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Customer
{
    public partial class XFilters : ContentPage
    {
        public event EventHandler<EventArgs> FiltersUpdated;
        
        #region Properties
        private bool IsLoaded;
        private bool HasChanges;
        #endregion
        public XFilters()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Filtros");
            pckCategory.SelectedIndex = 0;
            //pckProfile.SelectedIndex = 0;
            customNavBar.SetEditMode("Aplicar");
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;

            if(App.SettingsVM!=null)
            {
                InitPrefs();
                InitCategories();
                InitRanges();
                IsLoaded = true;
            }
        }

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            HasChanges = false;
            if (IsBusy) return;
            IsBusy = true;
            
            bool IsError = false;
            try
            {
                //test
                //App.SetBadgeCount("Pendientes", 11);
                if (App.SettingsVM == null)
                {
                    App.HUD.Show(MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                    App.SettingsVM = await App.apiService.GetXSettings();
                    InitPrefs();
                    InitCategories();
                    InitRanges();
                }
                else if(IsLoaded)
                {
                    //bug, el rango se resetea cuando desaparece la vista
                    if (App.SettingsVM != null)
                    {
                        var sf = App.SettingsVM;
                        sfRangeAge.RangeStart = (double)sf.MinAge;
                        sfRangeAge.RangeEnd = (double)sf.MaxAge;
                    }
                    var csf = App.UserVM.SearchFilters;
                    sfRangeAge.RangeStart = csf.MinAge;
                    sfRangeAge.RangeEnd = csf.MaxAge;
                }
            }
            catch
            {
                IsError = true;
            }
            finally
            {
                IsLoaded = true;
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if(IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        private void PckCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTiers();
            Update();
        }
        private void PckPrefs_SelectedIndexChanged(object sender, EventArgs e)
        {
            App.UserVM.SearchFilters.PreferenceId = App.SettingsVM.Preferences[pckPrefs.SelectedIndex].Id;
            Update();
        }
        private void SfRangeDistance_ValueChanging(object sender, Syncfusion.SfRangeSlider.XForms.ValueEventArgs e)
        {
            lblDistance.Text = Math.Round(e.Value, 0).ToString() + "km";
            Update();
        }
        private void SfRangeAge_RangeChanging(object sender, Syncfusion.SfRangeSlider.XForms.RangeEventArgs e)
        {
            App.UserVM.SearchFilters.MinAge = Convert.ToInt32(e.Start);
            App.UserVM.SearchFilters.MaxAge = Convert.ToInt32(e.End);
            lblAge.Text = App.UserVM.SearchFilters.MinAge.ToString() + "-" + App.UserVM.SearchFilters.MaxAge.ToString();
            Update();
        }
        private void CustomNavBar_EditButtonTapped(object sender, Templates.EditModeArgs e)
        {
            if (HasChanges)
            {
                if (pckCategory.SelectedIndex == 0)
                {
                    App.UserVM.SearchFilters.CategoryId = null;
                }
                else
                {
                    var selectedCat = App.SettingsVM.Categories.Where(x => x.Name == pckCategory.Items[pckCategory.SelectedIndex]).FirstOrDefault();
                    App.UserVM.SearchFilters.CategoryId = selectedCat.Id;
                }

                //App.UserVM.SearchFilters.MinAge = (int)sfRangeAge.RangeStart;
                //App.UserVM.SearchFilters.MaxAge = (int)sfRangeAge.RangeEnd;

                //App.UserVM.SearchFilters.MinDistanceRadius = Convert.ToInt32(Math.Round(sfRangeDistance.RangeStart));
                App.UserVM.SearchFilters.MaxDistanceRadius = Convert.ToInt32(Math.Round(sfRangeDistance.Value));
                
                //todo save ????
            }

            Navigation.PopAsync(true);
        }

        #endregion

        #region Methods
        private void InitPrefs()
        {
            //Order by Assetview
            App.SettingsVM.Preferences = App.SettingsVM.Preferences.OrderBy(x => x.CIndex).ToList();

            int index = -1, count = 0;

            foreach (var pref in App.SettingsVM.Preferences)
            {
                if (App.UserVM.SearchFilters.PreferenceId == pref.Id)
                    index = count;
                pckPrefs.Items.Add(pref.Name);
                count++;
            }
            pckPrefs.SelectedIndex = index == -1 ? 0 : index;
            App.UserVM.SearchFilters.PreferenceId = App.SettingsVM.Preferences[pckPrefs.SelectedIndex].Id;
            pckPrefs.SelectedIndexChanged += PckPrefs_SelectedIndexChanged; ;
        }
        private void InitCategories()
        {
            pckCategory.Items.Add("Todas");
            int index = 0, count = 0;

            foreach (var cat in App.SettingsVM.Categories)
            {
                if (App.UserVM.SearchFilters.CategoryId == cat.Id)
                    index = count;
                pckCategory.Items.Add(cat.Name);
                count++;
            }
            pckCategory.SelectedIndex = index;
            UpdateTiers();
            pckCategory.SelectedIndexChanged += PckCategory_SelectedIndexChanged;
        }
        private void InitRanges()
        {
            //Establecer defaults
            var sf = App.SettingsVM;
            sfRangeAge.Minimum = (double)sf.MinAge;
            sfRangeAge.Maximum = (double)sf.MaxAge;
            sfRangeDistance.Minimum = sf.Distances[0].Km;
            sfRangeDistance.Maximum = sf.Distances[sf.Distances.Count-1].Km;
            
            //Establecer los valores del usuario
            var csf = App.UserVM.SearchFilters;
            sfRangeAge.RangeStart = csf.MinAge;
            sfRangeAge.RangeEnd = csf.MaxAge;
            sfRangeDistance.Value = csf.MaxDistanceRadius;

            lblAge.Text = sfRangeAge.RangeStart.ToString() + "-" + sfRangeAge.RangeEnd.ToString();
            lblDistance.Text = Math.Round(sfRangeDistance.Value, 0).ToString() + "km";
            
            sfRangeAge.RangeChanging += SfRangeAge_RangeChanging;
            //sfRangeDistance.RangeChanging += SfRangeDistance_RangeChanging;
            sfRangeDistance.ValueChanging += SfRangeDistance_ValueChanging;
        }
        private void UpdateTiers()
        {
            var cats = App.SettingsVM.Categories;
            ACategoryTierViewModel iTier = null;
            ACategoryTierViewModel fTier = null;

            if (pckCategory.SelectedIndex == 0) //todos
            {
                iTier = cats[0].Tiers[0];
                var lastCat = cats[cats.Count - 1];
                fTier = lastCat.Tiers[lastCat.Tiers.Count - 1];
            }
            else
            {
                var cat = cats[pckCategory.SelectedIndex-1];
                iTier = cat.Tiers[0];
                fTier = cat.Tiers[cat.Tiers.Count - 1];
            }

            if (iTier.Cost == fTier.Cost)
                lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost);
            else
                lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost) + "-" + string.Format("{0:C0}", fTier.Cost);
        }
        private void Update()
        {
            HasChanges = true;
            if (FiltersUpdated != null)
                FiltersUpdated.Invoke(this, new EventArgs());
        }
        #endregion
    }
}