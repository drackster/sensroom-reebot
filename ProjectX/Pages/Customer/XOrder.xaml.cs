﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Common;

namespace ProjectX.Pages.Customer
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class XOrder : ContentPage
    {
        #region Properties
        private OrderViewModel model;
        private bool timerActive;
        private bool timerInitiated;
        public OrderStatus newOrderStatus;
        #endregion
        public XOrder(OrderViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            this.BindingContext = this.model = model;
            customNavBar.SetTitle("Pedido " + (model.Id + 1000).ToString());
            customNavBar.SetModalMode();

            this.BindingContext = model;
            InitHeader();
            InitTimer();

        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            //if (timerInitiated)
            //    InitTimer();
            timerActive = true;
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            timerActive = false;
        }
        private void InitHeader()
        {
            lblAsset.Text = model.Asset.Name;
            lblAssetCategory.Text = model.CategoryTier.Name;

            lblCreatedOn.Text = string.Format("{0:HH:mm}", model.CreatedOn.ToLocalTime());
            lblExpiresOn.Text = string.Format("{0:HH:mm}", model.ExpiresOn.Value.ToLocalTime());

            lblStatus.Text = "Pendiente";
        }
        private void InitTimer()
        {
            //Iniciar timer
            timerActive = true;
            Device.StartTimer(new TimeSpan(0, 0, 0, 7, 0), TimerElapsed);
        }
        private bool TimerElapsed()
        {
            //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToLocalTime().ToString() + " TimeElapsed:" + timerActive.ToString());
            if (timerActive)
            {
                CheckOrderStatus();
            }
            return true;
        }
        private async Task CheckOrderStatus()
        {
            newOrderStatus = await App.apiService.GetOrderStatus(model.Id);
            if(newOrderStatus != model.Status)
            {
                timerActive = false;
                if(newOrderStatus == OrderStatus.Canceled)
                {
                    await DisplayAlert("Lo sentimos", "La solicitud fue denegada.", "Ok");
                    await Navigation.PopModalAsync(true);
                }
                else if(newOrderStatus == OrderStatus.Confirmed)
                {
                    await Navigation.PopModalAsync(true);
                }
            }
        }
        private async void Cancel_Clicked(object sender, EventArgs e)
        {
            var response = await DisplayAlert("Confirmación", "¿Cancelar pedido?", "Sí", "No");
            if(response)
            {
                //todo cancel
                newOrderStatus = OrderStatus.Canceled;
                await Navigation.PopModalAsync(true);
            }

        }
    }
}