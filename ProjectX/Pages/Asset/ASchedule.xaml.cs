﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using Syncfusion.SfSchedule.XForms;
using ProjectX.Helpers;
using System.Threading;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ASchedule : ContentPage
    {
        #region Properties
        private ScheduleAppointmentCollection blockSpace;
        //private bool IsLoaded;
        private CancellationTokenSource cts_Appointments;
        #endregion

        #region cTor
        public ASchedule()
        {
            InitializeComponent();
            Init();
            customNavBar.SetHamburguerMode();
        }
        public ASchedule(bool IsPush)
        {
            InitializeComponent();
            Init();
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (IsBusy) return;
            IsBusy = true;

            bool IsError = false;
            try
            {
                if (App.scheduleViewModel == null)
                    App.HUD.Show(MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                await GetSchedule();                
            }
            catch(Exception ex)
            {
                IsError = true;
                await App.Database.LogException(ex);
            }
            finally
            {
                //IsLoaded = true;
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (!cts_Appointments.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_Appointments...");
                cts_Appointments.Cancel();
            }
        }
        #endregion

        #region Events
        private void Schedule_CellTapped(object sender, CellTappedEventArgs args)
        {
            var item = args.Appointment as ScheduleAppointment;
            if (item == null || IsBusy || item.Notes == null) return;
            IsBusy = true;
            //bool IsError = false;
            try
            {
                
                var oId = Convert.ToInt64(item.Notes.ToString());
                var invite = App.scheduleViewModel.Invitations.Where(x => x.OrderId == oId).FirstOrDefault();
                Navigation.PushAsync(new AInvitation(invite), true);
            }
            catch
            {
                //IsError = true;
            }
            finally
            {
                IsBusy = false;
            }
        }
        //private void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        //{
        //    if (IsBusy) return;
        //    IsBusy = true;
        //    try
        //    {
        //        //Schedule.DataSource = null;
        //        if (Schedule.ScheduleView == ScheduleView.DayView)
        //        {
        //            Schedule.ScheduleView = ScheduleView.WeekView;
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                customNavBar.UpdateEditModeText("Semana");
        //            });

        //        }
        //        else
        //        {
        //            Schedule.ScheduleView = ScheduleView.DayView;
        //            Device.BeginInvokeOnMainThread(() =>
        //            {
        //                customNavBar.UpdateEditModeText("Día");
        //            });
        //        }
        //        //Schedule.DataSource = meetings;
        //    }
        //    catch
        //    {

        //    }
        //    finally
        //    {
        //        IsBusy = false;
        //    }
        //}

        #endregion

        #region Methods
        private async Task GetSchedule()
        {
            //Ya no es UTC porque las reservas se graban con hora real
            //El primer día es el primero de la semana concurrente
            var udt = ViewModels.Util.GetStartDayFromDate(DateTime.Now.Date, DayOfWeek.Sunday);
            var cdt = new DateTime(udt.Year, udt.Month, udt.Day, 0, 0, 0);
            cts_Appointments = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
            var sched = await App.apiService.GetAppointments(new NotificationRequestModel()
            {
                SType = MessageSender.Asset,
                AssetId = App.AssetVM.Id,
                MType = MessageType.Invitation,
                StartDate = cdt
            },cts_Appointments.Token);

            if (sched != App.scheduleViewModel)
            {
                BindSchedule();
            }
        }
        private Color GetBGColor(ScheduleSpace space)
        {
            if (space.SType == ScheduleSpaceType.FreeSpace) return Color.FromHex("#f8cbad");
            if (space.SType == ScheduleSpaceType.SleepSpace) return Color.FromHex("#808080");
            return space.OType == OrderType.Calendar ? Color.FromHex("#ff0000") : Color.FromHex("#deebf7");
        }
        private void BindSchedule()
        {
            //limpiar
            blockSpace = new ScheduleAppointmentCollection();
            //Primero agregamos las citas agendadas
            foreach (var block in App.scheduleViewModel.Space)
                blockSpace.Add(new ScheduleAppointment
                {
                    Subject = block.LabelMsg,
                    StartTime = block.StartDate,
                    EndTime = block.EndDate,
                    Color = GetBGColor(block),
                    Notes = block.OrderId.HasValue ? block.OrderId.ToString() : null
                });

            Schedule.WeekViewSettings.WorkStartHour = App.scheduleViewModel.WorkDayStart;
            Schedule.WeekViewSettings.WorkEndHour = App.scheduleViewModel.WorkDayEnd;
            Schedule.DataSource = blockSpace;

        }
        private void Init()
        {
            NavigationPage.SetHasNavigationBar(this, false);

            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Mis Citas");
            //customNavBar.SetEditMode("Semana");
            //customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;

            //meetings = new ObservableCollection<Meeting>();
            blockSpace = new ScheduleAppointmentCollection();
            if (App.scheduleViewModel != null)
                BindSchedule();

            Schedule.HeaderHeight = 0;
            //Schedule.OnAppointmentLoadedEvent += Schedule_OnAppointmentLoadedEvent;
            Schedule.CellTapped += Schedule_CellTapped;
        }

        #endregion

    }
}