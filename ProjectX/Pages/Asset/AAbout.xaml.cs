﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using Syncfusion.SfAutoComplete.XForms;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AAbout : ContentPage
    {
        public bool HasChanges;
        private DateTime? DOB;
        private List<SfAutoCompleteItem> bankList;

        public AAbout()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Mi Perfil");
            txtGreeting.HorizontalTextAlignment = TextAlignment.Start;

            InitFields();
            txtIntroduction.WidthRequest = App.ScreenWidth;

            pckDate.DateSelected += PckDate_DateSelected;
            txtGreeting.TextChanged += LineEntryField_TextChanged;
            txtIntroduction.TextChanged += TxtIntroduction_TextChanged;

            customNavBar.SetEditMode("Listo");
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;

            bankList = new List<SfAutoCompleteItem>();
            txtAccountNum.Text = App.AssetVM.AccountNum;
            txtAccountName.Text = App.AssetVM.BankHolderName;
            txtAccountNum2.Text = App.AssetVM.AccountNum2;
            
            if (!string.IsNullOrEmpty(txtAccountNum2.Text))
            {
               // SegControl1.SelectedSegment = 0;//CHEQUES
                lblAccountCount2.Text = txtAccountNum2.Text.Length.ToString();
            }
            if (!string.IsNullOrEmpty(txtAccountNum.Text))
            {
              //  SegControl1.SelectedSegment = 1;//TARJETA
                lblAccountCount.Text = txtAccountNum.Text.Length.ToString();
            }

            txtAccountNum.TextChanged += txtAccountNum2_TextChanged;//txtAccountNum_TextChanged;
            txtAccountName.TextChanged += txtAccountNum2_TextChanged;// txtAccountInfo_TextChanged;
            txtAccountNum2.TextChanged += txtAccountNum2_TextChanged;// txtAccountNum2_TextChanged;

            autoCompleteBank.ValueChanged += AutoCompleteBank_ValueChanged;
          //  SegControl1.ValueChanged += SegControl1_ValueChanged;


            if (Device.RuntimePlatform == Device.Android)
            {
                autoCompleteBank.TextSize = 19;
            }

            Leyendas(2);
        }

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();
         //   if (SegControl1.SelectedSegment == 0)//cheques
          //      await LoadBankList();
        }
        private async void SegControl1_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            //if (SegControl1.SelectedSegment == 1)
            //{
            //    grd1.IsVisible = true;
            //    grd0.IsVisible = false;
            //}
            //else if (SegControl1.SelectedSegment == 0)
            //{
            //    //si carga bancos
            //    await LoadBankList();
            //    grd0.IsVisible = true;
            //    grd1.IsVisible = false;
            //}
        }
        private void AutoCompleteBank_ValueChanged(object sender, Syncfusion.SfAutoComplete.XForms.ValueChangedEventArgs e)
        {
            HasChanges = true;
        }
        private void txtAccountNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtAccountNum.Text == null ? 0 : txtAccountNum.Text.Length;
            if (ccount > 18)
                txtAccountNum.Text = e.OldTextValue;
            lblAccountCount.Text = ccount.ToString();
            HasChanges = true;
        }
        private void txtAccountNum2_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtAccountNum2.Text == null ? 0 : txtAccountNum2.Text.Length;
            if (ccount > 18)
                txtAccountNum2.Text = e.OldTextValue;
            lblAccountCount2.Text = ccount.ToString();
            HasChanges = true;
        }
        private void txtAccountInfo_TextChanged(object sender, TextChangedEventArgs e)
        {
            HasChanges = true;
            txtAccountName.Text = txtAccountName.Text.ToUpper();
        }
        private void DOB_Tapped(object sender, EventArgs e)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                pckDate.Focus();
            });
        }
        private void PckDate_DateSelected(object sender, DateChangedEventArgs e)
        {
            if (e.OldDate != e.NewDate)
            {
                HasChanges = true;
                DOB = e.NewDate;
                SetDOB();
            }
        }
        private void TxtIntroduction_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtIntroduction.Text == null ? 0 : txtIntroduction.Text.Length;
            if (ccount > 200)
                txtIntroduction.Text = e.OldTextValue;
            lblCharCount.Text = ccount.ToString();
            HasChanges = true;
        }
        private void LineEntryField_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtGreeting.Text == null ? 0 : txtGreeting.Text.Length;
            if (ccount > 25)
                txtGreeting.Text = e.OldTextValue;
            lblGreetingCount.Text = ccount.ToString();
            HasChanges = true;
        }
        private async void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            if (IsBusy) return;

            if (!HasChanges)
            {
                await Navigation.PopAsync(true);
                return;
            }
            if (!ValidateFields())
                return;
            if (!ValidateAccounts()) return;

            IsBusy = true;
            App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
            bool isError = false;
            try
            {
                App.AssetVM.DOB = DOB;
                App.AssetVM.Title = txtGreeting.Text;
                App.AssetVM.Description = txtIntroduction.Text;
                string selectedBank = null;
                if (autoCompleteBank.SelectedValue != null)
                {
                    selectedBank = autoCompleteBank.SelectedValue.ToString();
                }
                else if (!string.IsNullOrEmpty(autoCompleteBank.Text))
                {
                    selectedBank = autoCompleteBank.Text;
                }
                if (!string.IsNullOrEmpty(selectedBank))
                {
                    var exists = App.banks.Where(x => x.Name == selectedBank).FirstOrDefault();
                    if (exists == null)
                        App.AssetVM.BankId = null;
                    else
                        App.AssetVM.BankId = exists.Id;
                }
                else
                {
                    App.AssetVM.BankId = null;
                }

                App.AssetVM.AccountNum = "";// txtAccountNum.Text;     se hizo este movimiento porque no sirve el panel de clabe, se cacha la claba por el try de tdc y por eso se intercambió el objeto
                App.AssetVM.AccountNum2 = txtAccountNum.Text;// txtAccountNum2.Text;
                App.AssetVM.BankHolderName = txtAccountName.Text;
                await App.apiService.SaveAsset(App.AssetVM);
                await App.Database.SaveAsset(App.AssetVM);
            }
            catch
            {
                isError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }

            if (isError) await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            await Navigation.PopAsync(true);
        }

        #endregion

        #region Methods
        private async Task LoadBankList()
        {
            if (IsBusy || bankList.Count > 0)
            {
                return;
            }
                
            IsBusy = true;
            bool IsError = false;
            try
            {
                if (App.banks == null || App.banks.Count == 0)
                {
                    App.HUD.Show("Cargando lista de bancos...", Interfases.MaskType.Black);
                    App.banks = await App.apiService.GetBankList();
                }
                foreach (var bank in App.banks)
                    if (!string.IsNullOrEmpty(bank.Name))
                        bankList.Add(new SfAutoCompleteItem(bank.Name, "blankicon.png"));
                autoCompleteBank.ItemsSource = bankList;
                if (App.AssetVM.BankId.HasValue)
                {
                    var bName = App.banks.Where(x => x.Id == App.AssetVM.BankId).FirstOrDefault().Name;
                    autoCompleteBank.Text = bName;
                }

            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                IsError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        private bool ValidateAccounts()
        {
            if (!string.IsNullOrEmpty(txtAccountNum.Text) && txtAccountNum.Text.Length < 16)
            {
                DisplayAlert(MessageStrings.GeneralErrorTitle, "La cuenta CLABE debe de ser de 18 dígitos", "Ok");
                return false;
            }
            if (!string.IsNullOrEmpty(txtAccountNum2.Text) && txtAccountNum2.Text.Length < 18)
            {
                DisplayAlert(MessageStrings.GeneralErrorTitle, "La cuenta CLABE debe de ser de 18 dígitos", "Ok");
                return false;
            }
            return true;
        }

        private void InitFields()
        {
            lblAssetName.Text = App.AssetVM.Name;

            if (App.AssetVM.DOB.HasValue)
            {
                pckDate.Date = App.AssetVM.DOB.Value;
                DOB = App.AssetVM.DOB;
            }
            else
            {
                int y =DateTime.UtcNow.Date.Year - 16;
                DateTime defaultDT = new DateTime(y, 6, 15);//middle of calendar
                DOB = defaultDT;
                pckDate.Date = defaultDT;
            }
            SetDOB();

            if (string.IsNullOrEmpty(App.AssetVM.Title))
            {
                txtGreeting.Text = "";
                lblGreetingCount.Text = "0";
            }
            else
            {
                txtGreeting.Text = App.AssetVM.Title;
                lblGreetingCount.Text = App.AssetVM.Title.Length.ToString();
            }

            if (string.IsNullOrEmpty(App.AssetVM.Description))
            {
                txtIntroduction.Text = "";
                lblCharCount.Text = "0";
            }
            else
            {
                txtIntroduction.Text = App.AssetVM.Description;
                lblCharCount.Text = App.AssetVM.Description.Length.ToString();
            }
        }
        private bool ValidateFields()
        {
            if (DOB.HasValue)
            {
                if(Util.GetAge(DOB.Value) >= 18)
                {
                    App.AssetVM.DOB = DOB;
                    return true;
                }
                DisplayAlert(MessageStrings.GeneralErrorTitle, "Sólo mayores a 18. Ingresar una fecha válida.", "Ok");
            }
            return false;
        }

        private void SetDOB()
        {
            var cdt = DOB;
            var m = Util.GetMonthAsString(cdt.Value.Month).Substring(0, 3);
            txtDOB.Text = string.Format("{0:dd}", cdt) + "/" + m + "/" + string.Format("{0:yyyy}", cdt);
        }

        private async void Leyendas(int id)
        {
            var leyendas = await App.apiService.GetLeyenda(id);
          //  lblDescCTA.Text = "La cuenta servirá para pagarte los matchs generados de las personas que invitaste a unirse a LUXX";
            
             leyendas.leyenda.ToString();
        }
        #endregion

    }
}