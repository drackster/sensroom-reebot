﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Helpers;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AInvitation : ContentPage
    {
        public event EventHandler<InvitationStatusEventArgs> InvitationStatusChanged;
        
        #region Properties
        private InvitationViewModel Model;
        private bool fromHistory;
        private bool chatOpened;
        private bool showPaynetPDF;
        private bool IsLoaded;
        private bool IsBusy2;
        //private bool timerActive;
        #endregion

        #region cTor
        public AInvitation(InvitationViewModel model, bool FromHistory=false, bool ShowPaynetPDF=false)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
         
            customNavBar.SetTitle("Pedido " + model.OrderId.ToString());//todo

            model.CreatedOn = model.CreatedOn.ToLocalTime();
            model.ExpiresOn = model.ExpiresOn.ToLocalTime();
            fromHistory = FromHistory;

            this.BindingContext = Model = model;


            if (model.paymentMethod == PaymentMethod.Credito)
            {
                
                lblPendingPayment.Text = (model.Cost *.8M).ToString("C2");
            }
            else
            {
               
                lblPendingPayment.Text = (model.Cost - (model.DownPayment ?? 0)).ToString("C2");
            }


            if (model.paymentMethod == PaymentMethod.Paynet && App.MSender == MessageSender.Asset)
            {
                btnContinue.Text = "Por confirmar anticipo en Oxxo...";
                btnContinue.BackgroundColor = Color.LightBlue;
                btnContinue.IsEnabled = false;
                btnReject.IsEnabled = false;
            }

                showPaynetPDF = ShowPaynetPDF;

            if (App.MSender == MessageSender.Customer && !FromHistory)
            {
                //customNavBar.SetHamburguerMode();
                customNavBar.SetIconText("X");
                customNavBar.SetCarMode();
                //customNavBar.SetModalMode();
                customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            }
            else if(FromHistory)
            {
                //se supone que el boton de back estara visible
            }
      
            if(App.MSender == MessageSender.Customer)
            {
                //btnReject.Text = "CANCELAR";

                lblLeyenda.Text = "Tu invitación está pendiente de ser confirmada, te estaremos avisando en tu buzón en un máximo de 15 minutos";
                lblDirections1.IsVisible  = false;
                btnReject.IsVisible = false;
            }
            else
            {
                TapGestureRecognizer tgr = new TapGestureRecognizer();
                tgr.Tapped += Indications_Tapped;
                grdDirections.GestureRecognizers.Add(tgr);
                lblLeyenda.Text = "LUXX ha recibido el 20% por concepto de publicidad, tú recibirás el 80% en efectivo cuando estés en el evento. Recuerda que el traslado corre por tu cuenta.";
                slLeyenda.IsVisible = true;
            }
            if (string.IsNullOrEmpty(model.Comments))
                slNotes1.IsVisible = slNotes2.IsVisible = false;

            ValidateAcceptButton(true);
            InitPaymentButton();

            switch1.Toggled += Switch1_Toggled;

            #region Reviews
            if (model.ReviewCount == 0 || App.MSender == MessageSender.Customer)//esconder para el cliente
                slReview1.IsVisible = slReview2.IsVisible = false;
            else if (model.ReviewCount == 1)
                lblReviewCount.Text = "Leer evaluación";
            else lblReviewCount.Text = "Leer las (" + model.ReviewCount.ToString() + ") evaluaciones";
            #endregion
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (chatOpened)
            {
                //TODO
            }
            if(showPaynetPDF && Model.paymentMethod == PaymentMethod.Paynet 
                && !IsLoaded)
            {
                IsLoaded = true;
                await Navigation.PushAsync(new Customer.XReceipt(Model), true);
            }
                         
            if(Model.oStatus == OrderStatus.Pending)
            {
                await CheckOrderStatus();
            }  
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        #endregion

        #region Events
        //private async void App_TimerElapsed(object sender, EventArgs e)
        //{
        //    //System.Diagnostics.Debug.WriteLine(DateTime.Now.ToLocalTime().ToString() + " TimeElapsed:" + timerActive.ToString());
        //    if (timerActive && !IsBusy2)
        //    {
        //        await CheckOrderStatus();
        //    }
        //}
        private async void Switch1_Toggled(object sender, ToggledEventArgs e)
        {
            if(switch1.IsToggled)
            {
                var response = await DisplayAlert(MessageStrings.GeneralConfirmationTitle,
                    switch1.IsToggled ? "¿Marcar como pagado?"
                    : "¿Marcar como NO pagado?", "Sí", "No");
                if(response)
                {

                } 
            }
            
        }
        private void CustomNavBar_EditButtonTapped(object sender, Templates.EditModeArgs e)
        {
            if (App.MSender == MessageSender.Customer)
            {
                MessagingCenter.Send(this, "invitationcontinue", true);
                //await Navigation.PopModalAsync();
            }
        }
        private async void Indications_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                await Plugin.ExternalMaps.CrossExternalMaps.Current.NavigateTo(Model.CustomerName,
                    Model.CLatitude, Model.CLongitude, Plugin.ExternalMaps.Abstractions.NavigationType.Default);
            }
            catch
            {

            }
            finally
            {
                IsBusy = false;
            }
        }
        private void ReadReviews_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Common.CReviews(Model.CustomerId, Model.CustomerName), true);
        }
        private async void btnReject_Clicked(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            if (Model.oStatus == OrderStatus.Confirmed)
            {
                //abrir chat, con la chica esta bien
                if (App.MSender == MessageSender.Customer && !fromHistory)
                {
                    chatOpened = true;
                    await Navigation.PushModalAsync(new Common.MessageList(Model, true), true);
                }
                else
                {
                    await Navigation.PushAsync(new Common.MessageList(Model), true);
                }
                IsBusy = false;
            }
            else
            {
                bool isError = false;
                try
                {
                    var newStatus = App.MSender == MessageSender.Asset ? OrderStatus.Rejected : OrderStatus.Canceled;
                    
                    bool answer = await DisplayAlert(MessageStrings.GeneralConfirmationTitle, MessageStrings.DeleteConfirmation, "Sí", "No");
                    App.HUD.Show(MessageStrings.RejectingMsg);
                    if (answer)
                    {

                        //if (Model.paymentMethod != PaymentMethod.Credito)
                        //{
                            decimal monto = Model.Precio;
                            double porcentaje = double.Parse(monto.ToString()) * 0.2;
                            monto = decimal.Parse(porcentaje.ToString());
                            CustomerCredit SaveCredito = new CustomerCredit();
                            SaveCredito.UserId = Model.CustomerName; ;// Order.CustomerId.ToString();
                            SaveCredito.NoOrden = Model.OrderId.ToString();
                            SaveCredito.Monto = decimal.Parse(Model.DownPayment.ToString());
                            SaveCredito.Credito = true;
                            await App.apiService.SaveCustomerCredit(SaveCredito);
                        //}


                        var result = await App.apiService.UpdateOrderStatus(
                                new OrderViewModel()
                                {
                                    Id = Model.OrderId,
                                    Status = newStatus
                                });
                        if (result)
                        {
                            this.Model.oStatus = newStatus;

                            if (InvitationStatusChanged != null)
                                InvitationStatusChanged.Invoke(this, new InvitationStatusEventArgs(newStatus));
                            //await Navigation.PopAsync(true);
                            ValidateAcceptButton();
                        }
                    }

                }
                catch
                {
                    isError = true;
                }
                finally
                {
                    App.HUD.Dismiss();
                    IsBusy = false;
                }
                if (isError)
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                }
            }
        }
        private async void btnContinue_Clicked(object sender, EventArgs e)
         {
            if (App.MSender == MessageSender.Customer)
            {

                if (Model.paymentMethod == PaymentMethod.Credito)// credito por rechazo
                {
                   
                    var credito = await App.apiService.GetCustomCredit(Model.CustomerName);
                    await App.Database.DeleteCustomerCredit();
                    await App.Database.SaveCustomerCredit(credito);
                    MessagingCenter.Send(this, "invitationcontinue", true);
                }

                else if (Model.paymentMethod == PaymentMethod.OpenPay)//tdc
                {
                    MessagingCenter.Send(this, "invitationcontinue", true);
                }

                else if (Model.paymentMethod == PaymentMethod.Paynet)//oxxo
                {

                    var validaPagoOxxo = await App.apiService.VerificaPagoOxxo("ord_2ifYq8QUiefdf2k47");
                    //var validaPagoOxxo = CheckOrderStatus();
                    if (validaPagoOxxo)
                    {
                        MessagingCenter.Send(this, "invitationcontinue", true);
                    }
                    else
                    {
                        await DisplayAlert("¡Hola!", "La invitación la podrás enviar en cuanto esté confirmado tu pago", "Gracias! :)");
                    }
                    
                }
                else if (Model.paymentMethod == PaymentMethod.Paypal)//paypal
                {
                    MessagingCenter.Send(this, "invitationcontinue", true);
                }


                //await Navigation.PopModalAsync();
            }
            else
            {   
                await AcceptInvitation();
            }
        }
        private async void ViewPaynetPDF(object sender, EventArgs e)
        {
            //Las chicas no deben de ver el recibo
            if (App.MSender == MessageSender.Customer)
            {
                await Navigation.PushAsync(new Customer.XReceipt(Model), true);
            }
        }
        #endregion

        #region Methods
        private async Task AcceptInvitation()
        {
            if (IsBusy || this.Model.oStatus != OrderStatus.Pending)
            {
                await Navigation.PushAsync(new Common.MessageList(Model), true);
                return;
            }
                
            IsBusy = true;
            try
            {
                var result = await App.apiService.UpdateOrderStatus(
                    new OrderViewModel()
                    {
                        Id = Model.OrderId,
                        Status = OrderStatus.Confirmed
                    });
                if (result)
                {
                    btnContinue.Text = Helpers.MessageStrings.ConfirmedInvitation;
                    this.Model.oStatus = OrderStatus.Confirmed;

                    if (InvitationStatusChanged != null)
                        InvitationStatusChanged.Invoke(this, new InvitationStatusEventArgs(OrderStatus.Confirmed));

                    //customNavBar.OnlineButton.IsEnabled = false;
                    //todo abrir chat
                    //MessagingCenter.Send(this, "assetconfirmed", true);
                    ValidateAcceptButton();
                    // llamada a gastar el crédito del cliente
                    
                    await App.apiService.UpdateCustomerCredit(Model.CustomerName);



                    //Abrir chat automaticamente
                    await Navigation.PushAsync(new Common.MessageList(Model), true);

                }
            }
            catch
            {
                await DisplayAlert("Lo sentimos", "No se pudo aceptar la invitación", "Ok");
            }
            finally
            {
                IsBusy = false;
            }
        }
        private void InitPaymentButton()
        {
            //Aplica solo para deposito bancario
            if (Model.paymentMethod != PaymentMethod.Paynet)
            {
                //esconder si es con TC o Paypal
                slPayment1.IsVisible = slPayment0.IsVisible = false;
            }
            else
            {
                if (App.MSender == MessageSender.Customer)
                {
                    VerifiedPaymentNo.IsVisible = VerifiedPaymentYes.IsVisible = switch1.IsVisible = false;
                    lblPaymentTxt.Text = "Pagar ahora";
                }
                else
                {
                    verifiedArrow.IsVisible = false;
                }
            }
        }
        private void ValidatePaymentButton()
        {
            if(App.MSender == MessageSender.Asset)
                switch1.IsToggled = Model.dStatus == DepositStatus.Payed;
        }
        private void ValidateAcceptButton(bool isFirstLoad=false)
        {
            if(isFirstLoad && Model.oStatus == OrderStatus.Pending)
            {
                //InitTimer();
            }
            if (App.MSender == MessageSender.Asset)
            {
                btnContinue.Text = MessageStrings.GetInvitationStatus(Model.oStatus);
                if (Model.oStatus == OrderStatus.Pending || Model.oStatus == OrderStatus.Confirmed)
                {
                    btnContinue.BackgroundColor = Color.Green;
                }
                else if (Model.oStatus != OrderStatus.Confirmed)
                {
                    btnContinue.BackgroundColor = Color.Red;
                    btnContinue.IsEnabled = false;
                    btnReject.IsVisible = false;
                    //btnReject.IsEnabled = false;
                }
                if (Model.oStatus == OrderStatus.Confirmed)
                {
                    //customNavBar.OnlineButton.IsEnabled = false; //si lo deshabilito, no se ve el texto
                    //btnReject.Text = MessageStrings.InitChat;
                    //btnReject.BackgroundColor = Color.Black;
                    btnContinue.BackgroundColor = Color.Green;
                    btnReject.IsVisible = false;
                }
            }
            //else if (App.MSender == MessageSender.Customer)
            //{
            //    if (Model.oStatus == OrderStatus.Confirmed)
            //    {
            //        btnReject.Text = MessageStrings.InitChat;
            //        btnReject.BackgroundColor = Color.Black;
            //    }
            //}
        }
        private async Task CheckOrderStatus()
        {
            if (IsBusy2) return;
            IsBusy2 = true;
            try
            {
                var newStatus = await App.apiService.GetOrderStatus(this.Model.OrderId);
                if (Model.paymentMethod == PaymentMethod.Paynet)
                {
                    if (App.MSender == MessageSender.Asset)
                    {
                        if (newStatus.depositStatus == DepositStatus.Payed)
                        {
                            btnContinue.Text = "Aceptar invitación";
                            btnContinue.IsEnabled = true;
                            btnReject.IsEnabled = true;
                        }
                        else
                        {
                            btnContinue.Text = "Por confirmar anticipo en Oxxo...";
                            btnContinue.BackgroundColor = Color.LightBlue;
                            btnContinue.IsEnabled = false;
                            btnReject.IsEnabled = false;
                        }
                    }
                }
                if (newStatus.orderStatus != Model.oStatus)
                {
                    Model.oStatus = newStatus.orderStatus;
                    if (Model.oStatus == OrderStatus.Confirmed || Model.oStatus == OrderStatus.Canceled
                        || Model.oStatus == OrderStatus.Canceled)
                    {
                        //timerActive = false;
                        ValidateAcceptButton(false);
                    }
                }
                if (newStatus.depositStatus != Model.dStatus)
                {
                    ValidatePaymentButton();
                }
            }
            catch
            {
                
            }
            finally
            {
                IsBusy2 = false;
            }
        }

        #endregion

    }
    public class InvitationStatusEventArgs :EventArgs
    {
        private OrderStatus _status;
        public InvitationStatusEventArgs(OrderStatus newStatus)
        {
            _status = newStatus;
        }
        public OrderStatus Status
        {
            get { return _status; }
        }
    }
}