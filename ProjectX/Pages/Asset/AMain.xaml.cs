﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Pages.Templates;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Helpers;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;
using System.Threading;
using static ProjectX.Pages.RootPageMaster;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AMain : ContentPage
    {
        #region Properties
        //private bool IsLoaded;
        private Position position;
        private CancellationTokenSource cts_Appointments;
        private CancellationTokenSource cts_ClosedOrders;
        private CancellationTokenSource cts_ProfileUpdate;
        public bool Bupdate = false;
        DateTime t1;
        bool logout = false;
        public event EventHandler<EventArgs> LogoutComplete;
        #endregion

        #region cTor
        public AMain()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

           
            customNavBar.SetHamburguerMode();
            lblprecio.Text = App.AssetVM.Precio.ToString("C");
            this.BindingContext = App.AssetVM;
            customNavBar.SetTitle(App.AssetVM.Name);
            ToggleOnline();

            if (App.CalendarTimes == null)
                App.CalendarTimes = new List<CalendarTimeViewModel>();

            lblSex.Text = App.AssetVM.IsMan.HasValue ? App.AssetVM.IsMan.Value ? "Hombre" : "Mujer" : "";
            SetLabels();

            imgPhoto2.HeightRequest = App.ScreenHeight/ 3;
            imgPhoto2.WidthRequest = App.ScreenWidth;

        }
        
        #endregion

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            //Si ya hay información descargada, mostrar lo que ya cargamos
            if (App.scheduleViewModel != null
            && App.scheduleViewModel.Invitations.Count != slMessages.Children.Count)
            {
                SetDailySchedule();
            }
            if (App.CalendarTimes.Count == 0)
            {
                App.CalendarTimes = await App.apiService.GetCalendarTimes();
            }
            Bupdate = true;
            updateMessage();
            updateAs();



            if (logout)
            {
                var dif =DateTime.UtcNow.Subtract(t1).TotalMinutes;
                if (dif > 1)
                {
                    var log = new Common.Logout();
                    //LogoutComplete(this, new EventArgs());
                    log.logouttiempo(true);
                    logout = false;
                    return;
                }
                else
                {
                    logout = false;
                }
                var x = t1;
            } 
            await Update();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            CrossGeolocator.Current.PositionError -= Current_PositionError;
            CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
            if(cts_Appointments!=null && !cts_Appointments.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_Appointments...");
                cts_Appointments.Cancel();
            }
            if (cts_ClosedOrders != null && !cts_ClosedOrders.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_ClosedOrders...");
                cts_ClosedOrders.Cancel();
            }
            if (cts_ProfileUpdate != null && !cts_ProfileUpdate.IsCancellationRequested)
            {
                System.Diagnostics.Debug.WriteLine("Cancelling cts_ProfileUpdate...");
                cts_ProfileUpdate.Cancel();
            }
            t1 =DateTime.UtcNow;
            Bupdate = false;
            logout = true;
        }
        private async Task Update()
        {
            try
            {
                //Siempre checar al cargar
                await checkClosedOrders();
                await checkforProfileChanges();
                position = await CrossGeolocator.Current.GetPositionAsync(new TimeSpan(0, 0, App.LocationTimeOutSeconds));
                await App.apiService.UpdateAssetLocation(new AssetLocationModel()
                {
                    Latitude = position.Latitude,
                    Longitude = position.Longitude
                });
                CrossGeolocator.Current.PositionError += Current_PositionError;
                CrossGeolocator.Current.PositionChanged += Current_PositionChanged;

                SetLabels();

            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
        }
        private async void Current_PositionChanged(object sender, PositionEventArgs e)
        {
            if (position==null || (e.Position.Latitude!=position.Latitude && e.Position.Longitude!= position.Longitude))
            {
                position = e.Position;
            }
            await App.apiService.UpdateAssetLocation(new AssetLocationModel()
            {
                Latitude = position.Latitude,
                Longitude = position.Longitude
            });
        }
        private async void Current_PositionError(object sender, PositionErrorEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.Error);
            if (!CrossGeolocator.IsSupported)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.LocationError, "Ok");
            }
        }
        private async void Available_Tapped(object sender, EventArgs e)
        {
            DateTime? dt = null;
            bool isError = false;
            if (btnLine.Text == "offline")
            {
               

                dt = await GetTime();
                if (dt.HasValue)
                {
                    App.HUD.Show(Helpers.MessageStrings.PleaseWait);
                    var position = await CrossGeolocator.Current.GetPositionAsync(new TimeSpan(0, 0, App.LocationTimeOutSeconds));
                    App.AssetVM.Latitude = position.Latitude;
                    App.AssetVM.Longitude = position.Longitude;
                    App.AssetVM.LocationUpdate =DateTime.UtcNow;
                    App.AssetVM.OnlineExpires = dt;
                   
                }
                else
                {
                    return;
                }

                App.AssetVM.IsOnline = true;
                SetActivity(true);
                btnLine.Text = "online".ToLower();
                btnLine.BackgroundColor = Color.FromHex("#40FF00");//verde
                App.HUD.Dismiss();
            }
            else
            {
                btnLine.Text = "offline".ToLower();
                btnLine.BackgroundColor = Color.FromHex("#FF0000");//rojo
                App.AssetVM.IsOnline = false;
            }

            try
            {
                

                if (App.AssetVM.IsOnline) //Solo obtener ubicacion si se pondra online
                {
                    if (App.CalendarTimes.Count == 0)
                    {
                        try
                        {
                            App.HUD.Show(MessageStrings.PleaseWait);
                            App.CalendarTimes = await App.apiService.GetCalendarTimes();
                        }
                        catch (Exception ex)
                        {
                            await App.Database.LogException(ex);
                        }
                        finally
                        {
                            App.HUD.Dismiss();
                        }
                    }
                   
                }
                else
                {
                    App.AssetVM.OnlineExpires = null;
                }
                await UpdateAsset();
                if (dt.HasValue)
                    await DisplayAlert("¡Hola!", "Recuerda estar al pendiente de las posibles invitaciones mientras estás en línea.", "Ok");
            }
            catch
            {
                isError = true;
                App.AssetVM.IsOnline = false;//regresar
            }
            finally
            {
                ToggleOnline();//siempre no se puede poner online
                SetActivity(false);
            }
            if (isError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.LocationError, "Ok");
            }

        }
        private async void Activate_Tapped(object sender, EventArgs e)
        {
            if (lblCalendar.Text.Contains("Inactivo") == true)
            {
                App.AssetVM.CalendarActive = true;
                SetActivity(true);
                if (App.ASettings != null && App.AssetVM.Calendars.Count == 0)
                {
                    var response = await DisplayAlert(MessageStrings.GeneralErrorTitle, "Aún no has configurado tus horarios. ¿Deseas agregar ahora?", "Sí", "No");
                    if (response)
                        await Navigation.PushAsync(new ACalendarDetail(null), true);
                }
            }
            else
            {
                App.AssetVM.CalendarActive = false;
            }
            bool isError = false;
            try
            {
                await UpdateAsset();//FireNForget
            }
            catch
            {
                App.AssetVM.CalendarActive = false;
                isError = true;
            }
            finally
            {
                ToggleOnline();//siempre no se puede poner online
                SetActivity(false);
            }
            if (isError)
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.LocationError, "Ok");
        }
        private void Appointment_Tapped(object sender, EventArgs e)
        {
            var row = sender as AppointmentTemplate;
            var context = row.BindingContext as InvitationViewModel;
            if(context!=null)
            {
                var invite = new AInvitation(context);
                Navigation.PushAsync(invite, true);
            }
        }
        private void ViewReviews_Tapped(object sender, EventArgs e)
        {
            if (App.AssetVM.ReviewCount == 1)
            {
                Navigation.PushAsync(new Common.CReviewDetail(App.AssetVM.LastReview, App.AssetVM.Name), true);
            }
            else
            {
                Navigation.PushAsync(new Common.CReviews(App.AssetVM.Id, App.AssetVM.Name), true);
            }
        }
        private void ConfigureProfile_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new AConfiguration(true), true);
        }
        private void ViewCalendar_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ASchedule(true), true);
        }
        private void ProfileImage_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new APhotos(), true);
        }

        #endregion

        #region Methods
        private void SetLabels()
        {
            slInvalidProfile.IsVisible = App.AssetVM.ProfileInvalid;
            grdProfile.IsVisible = !App.AssetVM.ProfileInvalid;
            slReviews1.IsVisible = slReviews2.IsVisible = App.AssetVM.ReviewCount > 0; 
        }
        private async Task checkClosedOrders()
        {
            //Buscar ordenes que calificar, hacer un método que regrese ordenes sin calificar
            OrderSearchModel osm = new OrderSearchModel()
            {
                MSender = MessageSender.Asset,
                orderStatus = OrderStatus.Closed,
                reviewStatus = ReviewStatus.Pending
            };
            cts_ClosedOrders = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
            var closedOrders = await App.apiService.GetOrders(osm,cts_ClosedOrders.Token);

            foreach (var o in closedOrders)
            {
                await Navigation.PushModalAsync(new Common.CAddReview(o), true);
            }
        }
        private async Task checkforProfileChanges()
        {
            cts_ProfileUpdate = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
            var updatedProfile = await App.apiService.GetAsset(cts_ProfileUpdate.Token);
            if(updatedProfile != App.AssetVM)
            {
                App.RegisterVM.AssetVM = updatedProfile;
                await App.Database.SaveAsset(App.AssetVM);
                //await App.Database.SaveCalendar(App.AssetVM.Calendars);
                lblprecio.Text = App.AssetVM.Precio.ToString("C");
                this.BindingContext = App.AssetVM;
                ToggleOnline();
            }
        }
        public async Task<DateTime?> GetTime()
        {
            string[] times = new string[App.CalendarTimes.Count];
            for (int m = 0; m < App.CalendarTimes.Count; m++)
                times[m] = App.CalendarTimes[m].Name;

            var response = await DisplayActionSheet("¿Por cuánto tiempo?", "Cancelar", null, times);
            if (string.IsNullOrEmpty(response)) return null;
            var dt =DateTime.UtcNow;
            //Buscar si eligio una opcion
            var exists = App.CalendarTimes.Where(x => x.Name == response).FirstOrDefault();
            if (exists != null)
                
                return dt.AddMinutes(exists.Minutes);
            return null;
        }
        private void ToggleOnline()
        {
            if (App.AssetVM.IsOnline)
            {
                // lblEstatus.Text = "Estatus: Disponible";
                btnLine.Text = "online".ToLower();
                btnLine.BackgroundColor = Color.FromHex("#40FF00");//verde
                lblOnlineExpiration.Text = App.AssetVM.OnlineExpires.HasValue ?
                "Perfil visible hasta " + DTUtil.ConvertDT(App.AssetVM.OnlineExpires.Value.ToLocalTime(), true, true, false, true) : string.Empty;
             //   imgAvailable.Source = ImageSource.FromFile("Disponible.png");
            }
            else
            {
                //  lblEstatus.Text = "Estatus: No Disponible";
                btnLine.Text = "offline".ToLower();
                btnLine.BackgroundColor = Color.FromHex("#FF0000");//rojo
                lblOnlineExpiration.Text = string.Empty;
              //  imgAvailable.Source = ImageSource.FromFile("NoDisponible.png");
            }
            //if (App.AssetVM.CalendarActive)
            //{
            //    lblCalendar.Text = "Calendario: Activo";
            //}
            //else
            //{
            //    lblCalendar.Text = "Calendario: Inactivo";
            //}
            if (!App.AssetVM.IsOnline) // && !App.AssetVM.CalendarActive)
            {
                lblOnlineExpiration.Text = "No apareces en las búsquedas";
            }
            //else if(!App.AssetVM.IsOnline && App.AssetVM.CalendarActive)
            //{
            //    lblOnlineExpiration.Text = "Visible solo vía calendario";
            //}
            //if(App.AssetVM.Calendars == null || App.AssetVM.Calendars.Count==0)
            //{
            //    lblCalendarStatus.Text = "No tienes horarios configurados";
            //}
            //else
            //{
            //    lblCalendarStatus.Text = App.AssetVM.Calendars.Count.ToString() + " horarios disponibles";
            //}
        }
        private async Task UpdateAsset()
        {
            try
            {
                await App.apiService.SaveAsset(App.AssetVM);
                await App.Database.SaveAsset(App.AssetVM);
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
            }
            finally
            {

            }
        }
        private async Task GetAppointments()
        {
            if (IsBusy) return;
            IsBusy = true;
            System.Diagnostics.Debug.WriteLine("Getting new appointments...");
            if (slMessages.Children.Count == 0)
                SetActivity(true);
            try
            {
                var udt = ViewModels.Util.GetStartDayFromDate(DateTime.Now.Date, DayOfWeek.Sunday);
                var cdt = new DateTime(udt.Year, udt.Month, udt.Day, 0, 0, 0);

                cts_Appointments = new CancellationTokenSource(new TimeSpan(0, 0, App.CancelationTokenTimeoutSecs));
                var newSched = await App.apiService.GetAppointments(new NotificationRequestModel()
                {
                    SType = MessageSender.Asset,
                    AssetId = App.AssetVM.Id,
                    MType = MessageType.Invitation,
                    StartDate = cdt
                }, cts_Appointments.Token);
                //Actualizar solo si ha cambiado
                if (newSched != App.scheduleViewModel)
                {
                    App.scheduleViewModel = newSched;
                    SetDailySchedule();
                }
            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
            }
            finally
            {
                //if(showActivity)
                SetActivity(false);
                IsBusy = false;
            }
            
        }
        private void SetDailySchedule()
        {
            if(slMessages.Children.Count>0)
            {
                foreach(var child in slMessages.Children)
                {
                    var template = child as AppointmentTemplate;
                    if (template != null && template.GestureRecognizers.Count > 0)
                    {
                        var tgr = template.GestureRecognizers[0] as TapGestureRecognizer;
                        if (tgr != null)
                            tgr.Tapped -= Appointment_Tapped;
                    }
                }
                slMessages.Children.Clear();
            }

            //Solo las citas futuras
            var ndt =DateTime.UtcNow.Date;
            var future = App.scheduleViewModel.Invitations.Where(x => x.EventDateTime.Value > ndt).ToList();
            foreach (var invite in future)
            {
                var item = new AppointmentTemplate() { BindingContext = invite };
                var tpr = new TapGestureRecognizer();
                tpr.Tapped += Appointment_Tapped;
                item.GestureRecognizers.Add(tpr);
                //Ir insertando al inicio
                slMessages.Children.Insert(0, item);
            }
            slNoAppointments.IsVisible = App.scheduleViewModel.Invitations.Count == 0;
            slMessages.IsVisible = !slNoAppointments.IsVisible;
            //IsLoaded = true;
        }
        private void SetActivity(bool IsEnabled)
        {
            notificationsActivity.IsVisible = notificationsActivity.IsEnabled = IsEnabled;
        }

        //private void UpdateDT()
        //{
        //    foreach(var i in slMessages.Children)
        //    {
        //        var item = i as AssetNotificationViewTemplate;
        //        if(item!=null)
        //        {
        //            var context = item.BindingContext;
        //            item.BindingContext = null;
        //            item.BindingContext = context;
        //        }
        //    }
        //}

        protected override bool OnBackButtonPressed()
        {

            return true;
        }

        private async void updateAs()
        {
            await Task.Delay(10000);
            if (Bupdate)
            {

                OnAppearing();
            }
            else
            {

                OnDisappearing();
            }

        }

        private async void updateMessage()
        {

            var requestModel = new NotificationRequestModel()
            {
                SType = App.MSender,
                MType = MessageType.All
            };//traer todo

            if (App.MSender == MessageSender.Customer)
                requestModel.CustomerId = App.UserVM.Id;
            else
                requestModel.AssetId = App.AssetVM.Id;

            var unread = await App.apiService.GetNotificationCount(requestModel);

            if (unread > 0)
            {
                lblNot.IsVisible = true;
                lblNot.Text = unread.ToString();
                bwNot.IsVisible = true;
            }
            else
            {
                lblNot.IsVisible = false;
                lblNot.Text = unread.ToString();
                bwNot.IsVisible = false;
            }          

            if (App.MSender == MessageSender.Asset)
                App.SetBadgeCount("Mensajes", unread);

        }
        #endregion

    }
}