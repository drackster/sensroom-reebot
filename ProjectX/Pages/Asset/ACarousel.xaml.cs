﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ACarousel : CarouselPage
    {
        public ACarousel()
        {
            InitializeComponent();
           
        }

        private void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            Navigation.PopModalAsync(true);
        }
    }
}