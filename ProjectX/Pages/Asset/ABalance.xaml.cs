﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ABalance : ContentPage
    {
        #region private members
        private List<BalanceViewModel> viewModel;
        private BalanceViewModel currentSelection;
        #endregion

        #region cTor
        public ABalance()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Saldos y Movimientos");
            customNavBar.SetHamburguerMode();
            pckWeek.SelectedIndexChanged += PckWeek_SelectedIndexChanged;            
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (IsBusy) return;
            IsBusy = true;

            string errorMsg = null;
            try
            {
                viewModel = await App.apiService.GetBalance();
                if (viewModel.Count > 0)
                {
                    scrollView.IsVisible = true;
                    bindPicker();
                }
                else
                {
                    slSadFace.IsVisible = true;
                }
            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                errorMsg = MessageStrings.GeneralError;
            }
            finally
            {
                activityIndicator.IsRunning = activityIndicator.IsVisible = activityIndicator.IsEnabled = false;
                IsBusy = false;
            }
            if (errorMsg != null)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, errorMsg, "Ok");
            }
        }
        #endregion

        #region Events
        private void PckWeek_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pckWeek.Items.Count == 0 || viewModel.Count == 0) return;
            if (currentSelection == null || viewModel[pckWeek.SelectedIndex].Week.WeekDate == currentSelection.Week.WeekDate) return;
            this.BindingContext = null;
            this.BindingContext = currentSelection = viewModel[pckWeek.SelectedIndex];
        }
        private void Transactions_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ATransactions(currentSelection), true);
        }

        #endregion

        #region Methods
        private void bindPicker()
        {
            pckWeek.Items.Clear();

            if (viewModel.Count > 0)
            {
                foreach (var m in viewModel)
                    pckWeek.Items.Add(m.Week.WeekDate);
                pckWeek.SelectedIndex = 0;
                this.BindingContext = null;
                this.BindingContext = currentSelection = viewModel[0];//default
            }
        }
        #endregion

    }
}