﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Asset
{
    public partial class ASettings : ContentPage
    {
        #region Properties
        private bool IsLoaded;
        public bool HasChanges;
        private APrefViewModel selectedPref;
        private ACategoryViewModel selectedCat;
        private ACategoryViewModel selectPrecio;
        #endregion
        public ASettings()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Preferencias");
            //pckCategory.SelectedIndex = 0;
            //pckProfile.SelectedIndex = 0;
            customNavBar.SetEditMode("Aplicar");
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;

            if (App.SettingsVM != null)
            {
                InitSex();
                InitPrefs();
                InitCategories();
                InitRanges();
                IsLoaded = true;
            }
        }

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            HasChanges = false;
            if (IsBusy || IsLoaded) return;
            IsBusy = true;
            bool IsError = false;
            try
            {
                if (App.SettingsVM == null)
                {
                    App.HUD.Show(MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                    App.SettingsVM = await App.apiService.GetXSettings();
                }
                InitSex();
                InitPrefs();
                InitCategories();
                InitRanges();
                IsLoaded = true;
            }
            catch
            {
                IsError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        private void PckCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdateTiers();
            Update();
        }
        private void PckPrefs_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedPref = App.SettingsVM.Preferences[pckPrefs.SelectedIndex];
            Update();
        }
        private void PckSex_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (pckSex.SelectedIndex == 0)
                App.AssetVM.IsMan = null;
            else App.AssetVM.IsMan = pckSex.SelectedIndex == 2;
            Update();
        }
        private void PckDistance_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblDistance.Text = pckDistance.Items[pckDistance.SelectedIndex];
            ///SOLO SE SALVAR AL APLICAR
            //var d = pckDistance.Items[pckDistance.SelectedIndex].Replace(" km", "");
            //App.AssetVM.MaxDistance = Convert.ToInt32(d);
            Update();
        }

        //private void SfRangeDistance_ValueChanging(object sender, Syncfusion.SfRangeSlider.XForms.ValueEventArgs e)
        //{
        //    lblDistance.Text = Math.Round(e.Value, 0).ToString() + "km";
        //    Update();
        //}
        private async void CustomNavBar_EditButtonTapped(object sender, Templates.EditModeArgs e)
        {
            if (IsBusy) return;

            if (!HasChanges)
            {
                await Navigation.PopAsync(true);
                return;
            }

            IsBusy = true;
            App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
            bool isError = false;
            try
            {
                if (EnPrecio.Text == null || EnPrecio.Text == string.Empty)
                {
                    EnPrecio.Text = "0";
                }               
                double prec = double.Parse(EnPrecio.Text.ToString());
                if (prec < 1000)
                {
                    await DisplayAlert("¡Ups!", "El costo no puede ser menor a 1000 pesos", "Ok");
                    EnPrecio.Text = "";
                    EnPrecio.Focus();
                    return;
                }
                /*preferencias H M*/
                if (selectedPref == null)
                {
                    App.AssetVM.PreferenceId = null;
                }

                else
                {
                    App.AssetVM.PreferenceId = selectedPref.Id;
                }
                /*categorias costo*/
                if (selectedCat == null)
                {
                    App.AssetVM.CategoryId = 1;
                }               
                else
                {

                    App.AssetVM.CategoryId = 1;// selectedCat.Id;
                }
                /*costo del servicio*/
                //if (selectPrecio == null)
                //{
                //    App.AssetVM.precio = 0;
                //}
                //else
                //{
                //    App.AssetVM.precio = selectPrecio.precio;
                //}

                var d = pckDistance.Items[pckDistance.SelectedIndex].Replace(" km", "");
                App.AssetVM.MaxDistance = Convert.ToInt32(d);

                await App.apiService.SaveAsset(App.AssetVM);
                await App.Database.SaveAsset(App.AssetVM);

            }
            catch
            {
                isError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }

            if (isError) await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            await Navigation.PopAsync(true);
        }

        #endregion

        #region Methods
        private void InitSex()
        {
            if (App.AssetVM.IsMan.HasValue)
            {
                pckSex.SelectedIndex = App.AssetVM.IsMan.Value ? 2 : 1;
            }
            pckSex.SelectedIndexChanged += PckSex_SelectedIndexChanged;
        }
        private void InitPrefs()
        {
            //Order by Assetview
            App.SettingsVM.Preferences = App.SettingsVM.Preferences.OrderBy(x => x.AIndex).ToList();

            int index = -1, count = 0;

            foreach (var pref in App.SettingsVM.Preferences)
            {
                if (App.AssetVM.PreferenceId == pref.Id)
                {
                    selectedPref = pref;
                    index = count;
                }
                pckPrefs.Items.Add(pref.Name);
                count++;
            }
            pckPrefs.SelectedIndex = index == -1 ? 0 : index;
            selectedPref = App.SettingsVM.Preferences[pckPrefs.SelectedIndex];
            pckPrefs.SelectedIndexChanged += PckPrefs_SelectedIndexChanged;
        }
        private void InitCategories()
        {
            //pckCategory.Items.Add("Elige una opción");
            //int index = -1, count = 0;

            foreach (var cat in App.SettingsVM.Categories)
            {
                //if(cat.Cost1 < 1000)
                //    cat.Name = string.Format("{0:C2}    |  {1:C2}  |  {2:C2}", cat.Cost1, cat.Cost2, cat.Cost3);
                //else
                //    cat.Name = string.Format("{0:C2}  |  {1:C2}  |  {2:C2}", cat.Cost1, cat.Cost2, cat.Cost3);
                //var item = new Templates.CalendarDateView() { BindingContext = cat };
                //    var tpr = new TapGestureRecognizer();
                //    tpr.Tapped += Tpr_Tapped;
                //    item.SLItem.GestureRecognizers.Add(tpr);
                //    slCost.Children.Add(item);
                if (App.AssetVM.CategoryId == cat.Id)
                {
                    selectedCat = cat;
                    //index = count;
                }
                //pckCategory.Items.Add(cat.Name);
                //count++;
            }
            //pckCategory.SelectedIndex = index == -1? 0 : index+1;
            //UpdateTiers();
            //pckCategory.SelectedIndexChanged += PckCategory_SelectedIndexChanged;
        }
        private void Tpr_Tapped(object sender, EventArgs e)
        {
            var s = sender as StackLayout;
            if (s != null)
            {
                selectedCat.Id = Convert.ToInt32(s.ClassId);
                var child = s.Children[1];
                child.IsVisible = !child.IsVisible;
                if (child.IsVisible == true)
                {
                    selectedCat.IsChanged = 
                    HasChanges = true;
                }
            }
        }
        private void InitRanges()
        {
            //Establecer defaults
            var sf = App.SettingsVM;
            int maxDistance = App.AssetVM.MaxDistance;
            int maxDistanceIndex = 0;

            for (int d = 0, maxDistIndex = 0; d < sf.Distances.Count; d++, maxDistIndex++)
            {
                pckDistance.Items.Add(sf.Distances[d].Name);
                if (d == maxDistance)
                    maxDistanceIndex = maxDistIndex;
            }
            //Establecer los valores del usuario
            lblDistance.Text = maxDistance + " km";
            pckDistance.SelectedIndex = maxDistanceIndex;
            pckDistance.SelectedIndexChanged += PckDistance_SelectedIndexChanged;

        }
        private void UpdateTiers()
        {
            var cats = App.SettingsVM.Categories;
            //ACategoryTierViewModel iTier = null;
            //ACategoryTierViewModel fTier = null;
            slTiers.Children.Clear();

            //if (pckCategory.SelectedIndex == 0) //opcion
            //{
            //    //lblPriceRange.Text = string.Empty;
            //    selectedCat = null;
            //    return;
            //}
            //else
            //{
            //    selectedCat = cats[pckCategory.SelectedIndex - 1];
            ////    iTier = selectedCat.Tiers[0];
            ////    fTier = selectedCat.Tiers[selectedCat.Tiers.Count - 1];
            //}

            //if (iTier.Cost == fTier.Cost)
            //    lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost);
            //else
            //    lblPriceRange.Text = string.Format("{0:C0}", iTier.Cost) + "-" + string.Format("{0:C0}", fTier.Cost);


            foreach(var tier in selectedCat.Tiers)
            {
                var item = new Templates.TierView();
                item.BindingContext = tier;
                slTiers.Children.Add(item);
            }
        }
        private void Update()
        {
            HasChanges = true;
            //if (FiltersUpdated != null)
            //    FiltersUpdated.Invoke(this, new EventArgs());
        }

        #endregion

        private void EnPrecio_TextChanged(object sender, TextChangedEventArgs e)
        {
            string precio = EnPrecio.Text.Trim();
            if (precio == "" || precio == string.Empty)
            {
                precio = "0";
            }
            App.AssetVM.Precio = decimal.Parse(precio);
            //App.AssetVM.Precio = decimal.Parse(EnPrecio.Text);
            Update();
        }
    }
}