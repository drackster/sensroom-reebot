﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Pages.Templates;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AConfiguration : ContentPage
    {
        #region Properties
        private AAbout AboutPage;
        private ABankInfo ABankInfoPage;
        private ACalendarDetail CalendarPage;
        private ASettings SettingsPage;
        private APhotos PhotosPage;
        private bool IsLoaded;
        #endregion

        #region cTor
        public AConfiguration()
        {
            InitializeComponent();
            customNavBar.SetHamburguerMode();
            Init();    
        }
        public AConfiguration(bool IsPush)
        {
            InitializeComponent();
            Init();
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (IsLoaded)
            {
                bool shouldUpdate = false;
                if(AboutPage!=null && AboutPage.HasChanges)
                {
                    AboutPage.HasChanges = false;
                    shouldUpdate = true;
                }
                if (SettingsPage != null && SettingsPage.HasChanges)
                {
                    SettingsPage.HasChanges = false;
                    shouldUpdate = true;
                }
                if (PhotosPage != null && PhotosPage.HasChanges)
                {
                    shouldUpdate = true;
                }
                if (shouldUpdate)
                {
                    BindingContext = null;
                    BindingContext = App.AssetVM;
                    //App.apiService.SaveAsset(App.AssetVM);
                }
            }
            if (IsBusy) return;

            #region Pops
            //evitar volver hacer fetch
            if(CalendarPage!=null && (CalendarPage.HasChanges || CalendarPage.IsDeleted))
            {
                if(CalendarPage.IsNew)
                {
                    App.AssetVM.Calendars.Add(CalendarPage.Model);
                    var days = Helper.GetWeekDays();
                    AddCalendarDetail(CalendarPage.Model,days);
                }
                else if(!CalendarPage.IsNew && !CalendarPage.IsDeleted)
                {
                    UpdateCalendarDetail(CalendarPage.Model);
                }
                else if(CalendarPage.IsDeleted)
                {
                    RemoveCalendarDetail(CalendarPage.Model);
                }
                CalendarPage = null;
            }

            #endregion

            //if (!IsLoaded) //si no ha cargado traer todos 
            //{
                IsBusy = true;
                bool IsError = false;
                try
                {
                    ASettingsViewModel asvm = null;
                    //Mostrar mensaje solo si no se han cargado
                    if (App.ASettings == null)
                    {
                        App.HUD.Show(Helpers.MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                        asvm = await App.apiService.GetSettings();
                        App.ASettings = asvm;
                    }
                    InitCalendarDetails();
                    IsLoaded = true;
                }
                catch
                {
                    IsError = true;
                }
                finally
                {
                    App.HUD.Dismiss();
                    IsBusy = false;
                }

                if (IsError)
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                    await Navigation.PopAsync(true);
                }
            //}
        }

        #endregion

        #region Events
        private void Calendar_Tapped(object sender, EventArgs e)
        {
            var item = sender as CalendarView;
            if(item!=null)
            {
                var context = item.BindingContext as CalendarViewModel;
                CalendarPage = new ACalendarDetail(context);
                Navigation.PushAsync(CalendarPage, true);
            }
        }
        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            var sl = sender as StackLayout;
            if(sl!=null)
            {
                var lbl = sl.Children[1] as Label;
                if (lbl.Text == "Mi Perfil")
                {
                    
                    try
                    {
                        if (AboutPage == null)
                            AboutPage = new AAbout();


                        await Navigation.PushAsync(AboutPage, true);
                    }
                    catch (Exception ex)
                    {

                    }
                    
                }
                else if (lbl.Text == "Mis Fotos")
                {
                    //if (PhotosPage == null) 
                    //(por alguna razon truena, best guess: libera la memoria de las imagenes

                    try
                    {
                        PhotosPage = new APhotos();
                        await Navigation.PushAsync(PhotosPage, true);
                    }
                    catch (Exception ex)
                    {

                    }
                        
                }
                else if (lbl.Text == "Mis Horarios")
                {
                    //Limit reached
                    if (App.AssetVM.Calendars != null && App.AssetVM.Calendars.Count == 5)
                    {
                        await DisplayAlert(MessageStrings.GeneralErrorTitle, "Has llegado al límite de 5 horarios", "Ok");
                    }
                    else
                    {
                        CalendarPage = new ACalendarDetail(null);
                        await Navigation.PushAsync(CalendarPage, true);
                    }
                }
                else if (lbl.Text == "Preferencias")
                {
                    try
                    {
                        //if (SettingsPage == null)
                            SettingsPage = new ASettings();
                        await Navigation.PushAsync(SettingsPage, true);
                    }
                    catch (Exception ex)
                    { }
                    
                }
                //else if (lbl.Text == "Datos Bancarios")
                //{
                //    if (ABankInfoPage == null)
                //        ABankInfoPage = new ABankInfo();
                //    await Navigation.PushAsync(ABankInfoPage, true);
                //}
            }
        }

        #endregion

        #region Methods
        private void Init()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Configuración");
            BindingContext = App.AssetVM;
        }
        private void InitCalendarDetails()
        {
            var days = Helper.GetWeekDays();
            if(slCalendars.Children.Count>0)
                slCalendars.Children.Clear();
            foreach (var c in App.AssetVM.Calendars)
                AddCalendarDetail(c, days);
        }
        private void AddCalendarDetail(CalendarViewModel c, List<CalendarDetailViewModel> days)
        {
            foreach (var d in c.Detail)
                d.Name = days[(int)d.Day].Name;

            c.Days = Helper.GetDays(c.Detail);

            var item = new CalendarView() { BindingContext = c };
            var tpr = new TapGestureRecognizer();
            tpr.Tapped += Calendar_Tapped;
            item.GestureRecognizers.Add(tpr);
            slCalendars.Children.Add(item);
        }
        private void UpdateCalendarDetail(CalendarViewModel Model)
        {
            int index = GetCalendarIndex(Model);
            if (index >= 0)
            {
                var days = Helper.GetWeekDays();
                foreach (var d in Model.Detail)
                    d.Name = days[(int)d.Day].Name;
                Model.Days = Helper.GetDays(Model.Detail);
                slCalendars.Children[index].BindingContext = App.AssetVM.Calendars[index] = Model;
            }
        }
        private void RemoveCalendarDetail(CalendarViewModel Model)
        {
            int index = GetCalendarIndex(Model);
            if (index >= 0)
            {
                slCalendars.Children.RemoveAt(index);
                App.AssetVM.Calendars.RemoveAt(index);
            }
        }
        private int GetCalendarIndex(CalendarViewModel Model)
        {
            int index = 0;
            foreach (var m in App.AssetVM.Calendars)
            {
                if (m.Id == Model.Id)
                    return index;
                index++;
            }
            return -1;
        }

        #endregion
    }
}