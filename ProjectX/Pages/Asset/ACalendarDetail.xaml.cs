﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TK.CustomMap.Api;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using Xamarin.Forms.Maps;
using ProjectX.Extended;
using ProjectX.Helpers;
using Plugin.Geolocator;
using System.Collections.ObjectModel;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ACalendarDetail : ContentPage
    {
        #region Properties
        public CalendarViewModel Model;
        public bool IsNew;
        public bool HasChanges;
        public bool IsDeleted;
        private string currentQuery;
        //private bool IsLoaded;
        private ObservableCollection<AddressViewModel> predAddresses;
        public AddressViewModel selectedAddress;
        public AddressViewModel currentAddress;
        public AddressViewModel newAddress;
        public Xamarin.Forms.Maps.MapSpan currentPosition;
        public bool IsDroid = false;
        private bool scrollToHeight;
        #endregion

        #region cTor
        public ACalendarDetail(CalendarViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);

            Model = model;
            if(Model == null)
            {
                customNavBar.SetTitle("Agregar horario");
                customNavBar.SetEditMode("");
                Model = new CalendarViewModel();
                //dias default
                Model.Detail = Helper.GetWeekDays();
                IsNew = true;

                //Establecer horas default
                picker1.Time = new TimeSpan(12, 0, 0);
                picker2.Time = new TimeSpan(22, 0, 0);

            }
            else
            {
                customNavBar.SetTitle("Editar horario");
                customNavBar.SetEditMode("Listo");
               
                txtName.Text = Model.Name;
                picker1.Time = Model.From;
                picker2.Time = model.To;

                //rellenar los seleccionados
                var days = Helper.GetWeekDays();

                foreach (var d in days)
                {
                    var dd = Model.Detail.Where(x => x.Day == d.Day).FirstOrDefault();
                    if (dd != null)
                    {
                        d.Id = dd.Id;
                        d.IsSelected = dd.IsSelected;
                    }
                }

                Model.Detail = days;
                btnSave.BackgroundColor = Color.Red;//Delete
                btnSave.Text = "EMILINAR";

            }
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            txtName.TextChanged += LineEntryField_TextChanged;
            picker1.Unfocused += Picker_Unfocused;
            picker2.Unfocused += Picker_Unfocused;
            txtNewAddress.TextChanged += TxtNewAddress_TextChanged;
            txtNewAddress.Completed += TxtNewAddress_Completed;
            txtNewAddress.Focused += TxtNewAddress_Focused;


            if (Device.RuntimePlatform == Device.Android)
            {
                IsDroid = true;
                //lstPredictions.RowHeight = 65;
            }
                
            predAddresses = new ObservableCollection<AddressViewModel>();
            lstPredictions.ItemsSource = predAddresses;

            InitCalendar();
            InitMap();

        }

        #endregion

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            if (CrossGeolocator.IsSupported)
            {
                try
                {

                        var position = await CrossGeolocator.Current.GetPositionAsync(new TimeSpan(0, 0, 15), null, false);
                        currentPosition = new MapSpan(new Xamarin.Forms.Maps.Position(position.Latitude, position.Longitude)
                                            , position.Latitude, position.Longitude);
                        var addresses = await CrossGeolocator.Current.GetAddressesForPositionAsync(position);
                        var ca = addresses.FirstOrDefault();
                        if (ca != null)
                        {
                            currentAddress = SearchUtil.ParseAddressViewModel(ca);
                        }
                        else
                        {
                            currentAddress = null;
                        }
                        Model.Latitude = position.Latitude;
                        Model.Longitude = position.Longitude;

                        if (IsNew) //Si es nuevo, nos movemos a la ubicacion actual, si no, ya esta en la ubicacion grabada
                        {
                            MoveToRegion(currentAddress == null ? null : Helpers.MessageStrings.FormatAddress(currentAddress));
                        }
                        CrossGeolocator.Current.PositionError += Current_PositionError;

                    //CrossGeolocator.Current.PositionChanged += Current_PositionChanged;
                }
                catch (Exception ex)
                {
                    await App.Database.LogException(ex);
                    await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
                }
            }
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            CrossGeolocator.Current.PositionError -= Current_PositionError;
            //CrossGeolocator.Current.PositionChanged -= Current_PositionChanged;
        }
        private void TxtNewAddress_Focused(object sender, FocusEventArgs e)
        {
            //chk1.IsVisible = false;
        }
        private async void TxtNewAddress_Completed(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewAddress.Text) &&
                      txtNewAddress.Text != currentQuery &&
                      txtNewAddress.Text.Length > 3)
            {
                currentQuery = txtNewAddress.Text;
                await GetPredictions();
            }
            else
            {
                lstPredictions.IsVisible = false;
                lstPredictions.HeightRequest = 0;
                scrollToHeight = false;
            }
        }
        private async void TxtNewAddress_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNewAddress.Text) &&
                      e.NewTextValue != e.OldTextValue &&
                      txtNewAddress.Text.Length > 2)
            {
                currentQuery = txtNewAddress.Text;
                await GetPredictions();
            }
            else
            {
                lstPredictions.IsVisible = false;
                lstPredictions.HeightRequest = 0;
                scrollToHeight = false;
            }
        }
        private async void lstPredictions_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as AddressViewModel;
            if (item != null)
            {
                if (item.A1 != "Cancelar")
                {
                    HasChanges = true;

                    if (IsDroid)
                    {
                        var pos = await TKNativePlacesApi.Instance.GetDetails(item.PlaceId);
                        item.Latitude = pos.Coordinate.Latitude;
                        item.Longitude = pos.Coordinate.Longitude;
                    }
                    selectedAddress = newAddress = item;
                    txtNewAddress.Text = string.Empty;//limpiar texto busqueda
                }
                //establecer nueva direccion
                //lblNewAddress.Text = selectedAddress.A;
                lstPredictions.IsVisible = false;
                lstPredictions.HeightRequest = 0;
                predAddresses.Clear();
                //chk1.IsVisible = false;
                //chk0.IsVisible = true;
                //ShowHideNewAddress(true);

                if (item.A1 != "Cancelar")
                {
                    currentPosition = new MapSpan(new Xamarin.Forms.Maps.Position(selectedAddress.Latitude,
                            selectedAddress.Longitude), selectedAddress.Latitude, selectedAddress.Longitude);
                    Model.Latitude = selectedAddress.Latitude;
                    Model.Longitude = selectedAddress.Longitude;
                    selectedAddress.Parse();
                    MoveToRegion(selectedAddress.A);
                    await scroll1.ScrollToAsync(map1, 0, true);
                }
                //Mover el focus a otro elemento
                //txtName.Focus();
                scrollToHeight = false;
            }
            lstPredictions.SelectedItem = null;
        }
        private void Current_PositionError(object sender, Plugin.Geolocator.Abstractions.PositionErrorEventArgs e)
        {
            DisplayAlert(MessageStrings.GeneralErrorTitle, e.Error.ToString(), "Ok");
        }
        
        //private void Current_PositionChanged(object sender, Plugin.Geolocator.Abstractions.PositionEventArgs e)
        //{
        //    currentPosition = new MapSpan(new Xamarin.Forms.Maps.Position(e.Position.Latitude, 
        //        e.Position.Longitude), e.Position.Latitude, e.Position.Longitude);
        //    Model.Latitude = e.Position.Latitude;
        //    Model.Longitude = e.Position.Longitude;
        //    MoveToRegion();
        //}
        private async void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            if (!IsNew && HasChanges)
            {
                await DoSave();
            }
            else
            {
                await Navigation.PopAsync(true);
            }
        }
        private void Tpr_Tapped(object sender, EventArgs e)
        {
            var s = sender as StackLayout;
            if(s!=null)
            {
                int id = Convert.ToInt32(s.ClassId);
                var d = Model.Detail.Where(x => x.Id == id).FirstOrDefault();
                var child = s.Children[1];
                child.IsVisible = d.IsSelected = !d.IsSelected;
                d.IsChanged = true;
                if(!IsNew)HasChanges = true;
            }
        }
        private void LineEntryField_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!IsNew) HasChanges = true; 
        }
        private void Picker_Unfocused(object sender, FocusEventArgs e)
        {
            if (!IsNew) HasChanges = true;
        }
        private async void SaveButton_Clicked(object sender, EventArgs e)
        {
                if (IsNew)
                {
                    //if (!HasChanges)
                    //{
                    //    await Navigation.PopAsync(true);
                    //    return;
                    //}
                    await DoSave();
                }
                else
                {
                    var result = await DisplayAlert(MessageStrings.GeneralConfirmationTitle, MessageStrings.GeneralConfirmationTitle, "Sí", "No");
                    if(result)
                    {
                        await DoDelete();
                    }
                }   
        }

        #endregion

        #region Methods
        private void InitCalendar()
        {
            foreach (var d in Model.Detail)
            {
                var item = new CalendarDateView() { BindingContext = d };
                var tpr = new TapGestureRecognizer();
                tpr.Tapped += Tpr_Tapped;
                item.SLItem.GestureRecognizers.Add(tpr);
                slDays.Children.Add(item);
            }
        }
        private void InitMap()
        {
            map1.WidthRequest = App.ScreenWidth;
            map1.HeightRequest = App.ScreenHeight * .40;
            if (IsNew)
            {

                Model.Latitude = 25.663695;
                Model.Longitude = -100.3689;
            }
            
            //map1.Circle = new CustomCircle
            //{
            //    Position = new Position(Model.Latitude, Model.Longitude),
            //    Radius = 1000//metros
            //};

            MoveToRegion();
        }
        //private void InitSearchResults()
        //{
        //    try
        //    {
        //        //var coods = SearchUtil.GetScreenCoordinates(slNewAddress);
        //        //await scroll1.ScrollToAsync(0, coods[1], true);

        //        //System.Diagnostics.Debug.WriteLine("scrolling to:" + coods[1].ToString()
        //        //    + "slnewAddress:" + slNewAddress.Height.ToString() +
        //        //    "scrollY:" + scroll1.ScrollY.ToString());

        //        //grdPredictions.TranslationY = map1.Height + slNewAddress.Height + 
        //        //    txtNewAddress.Height + scroll1.ScrollY + 13;

        //        //movemos la ventana dependiendo de la posicion y el scroll 
        //        //grdPredictions.TranslationY = (coods[1] + (slNewAddress.Height*2));
        //        //- (scroll1.ScrollY > 0 ? scroll1.ScrollY : 0);

        //    }
        //    catch { }
        //}
        private void MoveToRegion(string address=null)
        {
            var position = new Position(Model.Latitude, Model.Longitude);
            //map1.Circle = new CustomCircle
            //{
            //    Position = position,
            //    Radius = 1000//metros
            //};


            ////quitar pines anteriores
            //foreach (var p in map1.Pins)
            //    map1.Pins.Remove(p);
            try
            {
                if (map1.Pins.Count > 0)
                    map1.Pins.RemoveAt(0);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            if (address!=null && address.Length > 25)
                address = address.Substring(0, 25)+"...";
            var pin = new Pin
            {
                Type = PinType.Place,
                Position = new Position(Model.Latitude, Model.Longitude),
                Label = address == null ? "" : address
            };
            map1.Pins.Add(pin);

            map1.MoveToRegion(MapSpan.FromCenterAndRadius(position, Distance.FromKilometers(1.0)));

        }
        private async Task GetPredictions()
        {
            try
            {
                UpdateActivity(true);
                
                var predictions = await TKNativePlacesApi.Instance.GetPredictions(currentQuery, currentPosition);
                predAddresses.Clear();
                foreach (var pred in predictions)
                {
                    var newA = new AddressViewModel();
                    if (IsDroid)
                    {
                        newA.PlaceId = pred.Details.FormattedAddress;
                    }
                    else
                    {
                        newA.Latitude = pred.Details.Coordinate.Latitude;
                        newA.Longitude = pred.Details.Coordinate.Longitude;
                    }
                    
                    var split = SearchUtil.GetSplittedPrediction(pred.Description, ",");

                    newA.A = pred.Description;
                    newA.A1 = split[0];
                    newA.A2 = split[1];
                    newA.A3 = split[2];

                    predAddresses.Add(newA);

                }
                if(predAddresses.Count>0)
                {
                    //predAddresses.Add(new AddressViewModel() { A1 = "Cancelar" });
                    //InitSearchResults();
                    lstPredictions.HeightRequest = predAddresses.Count * lstPredictions.RowHeight;
                    lstPredictions.IsVisible = true;//predictions.Any();

                    if(scrollToHeight==false)
                    {
                        scrollToHeight = true;
                        var coods = SearchUtil.GetScreenCoordinates(lstPredictions);
                        var hh = coods[1] - txtNewAddress.Height;
                        System.Diagnostics.Debug.WriteLine("scrolling to" + hh.ToString());
                        await scroll1.ScrollToAsync(coods[0],hh, true);
                    }

                }
                else
                {
                    lstPredictions.IsVisible = false;
                    lstPredictions.HeightRequest = 0;
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                UpdateActivity(false);
            }

        }
        private void UpdateActivity(bool IsActive)
        {
            activityIndicator.IsVisible = activityIndicator.IsRunning = IsActive;
        }
        private async Task DoSave()
        {
            if (IsBusy) return;

            IsBusy = true;
            bool IsError = false;
            bool MissingData = false;
            try
            {
                if (string.IsNullOrEmpty(txtName.Text))
                {
                    await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle,
                        "Favor de ingresar un nombre válido", "Ok");
                    HasChanges = false;
                    return;
                }
                var selectedDays = Model.Detail.Where(x => x.IsSelected).ToList().Count;
                if (selectedDays == 0)
                {
                    await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle,
                        "Seleccione por lo menos un día para continuar", "Ok");
                    HasChanges = false;
                    return;
                }
                Model.Name = txtName.Text;
                Model.From = picker1.Time;
                Model.To = picker2.Time;
                var ol = IsOverlapped();
                if (ol !=null)
                {
                    await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, ol, "Ok");
                    HasChanges = false;
                    return;
                }
                //Todo bien, entonces salvar
                App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
                if (IsNew)
                {
                    Model.Detail = Model.Detail.Where(x => x.IsSelected).ToList();
                    Model = await App.apiService.SaveCalendar(Model);
                    await App.Database.SaveCalendar(new List<CalendarViewModel>() { Model });
                    HasChanges = true;
                }
                else
                {
                    Model.Detail = Model.Detail.Where(x => x.IsChanged).ToList();
                    Model = await App.apiService.UpdateCalendar(Model);
                    await App.Database.UpdateCalendar(Model);
                }
            }
            catch(Exception ex)
            {
               await App.Database.LogException(ex);
                IsError = true;   
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }

            if(IsError) await DisplayAlert(MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            else if(!MissingData) await Navigation.PopAsync(true);
        }
        private async Task DoDelete()
        {
            if (IsBusy) return;
            IsBusy = true;
            App.HUD.Show(Helpers.MessageStrings.DeletingMsg, Interfases.MaskType.Black);
            bool IsError = false;
            try
            {
                await App.apiService.DeleteCalendar(Model);
                await App.Database.DeleteCalendar(Model.Id);

                IsDeleted = true;
            }
            catch
            {
                IsError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }

            if (IsError) await DisplayAlert(MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            else await Navigation.PopAsync(true);
        }
        private string IsOverlapped()
        {
            //Si aun no tiene calendarios configurados
            if (App.AssetVM.Calendars == null 
                || App.AssetVM.Calendars.Count == 0) return null;
            
            //Validar por sobreposiciones
            foreach(var c in App.AssetVM.Calendars)
            {
                if(IsNew && c.Name == Model.Name)
                    return "El nombre " + c.Name + " ya existe.";
                //if (c.Latitude == Model.Latitude && c.Longitude == Model.Longitude)
                //    return "El horario de nombre " + c.Name + " tiene la misma posición geográfica";
                
                //Primero validar el horario, que no se sobreponga
                if((Model.From >= c.From &&
                   Model.To >= c.To &&
                   Model.From < c.To)
                    ||
                   (Model.From <= c.From &&
                   Model.To <= c.To &&
                   Model.To > c.From))
                {
                    //Si se sobrepone, validar que tambien el día

                    foreach(var day in c.Detail)
                    {
                        var exists = Model.Detail.Where(x => x.IsSelected && x.Day == day.Day).FirstOrDefault();
                        if(exists!=null)
                        {
                            return "El horario de " + day.Name + " " + formatTS(c.From) + "-" +
                                formatTS(c.To) + " sobrepone al de " + c.Name;

                        }
                    }

                }
            }
            return null;
        }
        private string formatTS(TimeSpan ts)
        {
            //return ts.ToString("c");
            return string.Format("{0:hh\\:mm}", ts);
        }
        #endregion

    }
}