﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ARequest : ContentPage
    {
        public ARequest(NotificationViewModel Model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Solicitud");
            this.BindingContext = Model;
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
        private void btnReject_Clicked(object sender, EventArgs e)
        {

        }
        private void btnAccept_Clicked(object sender, EventArgs e)
        {

        }
    }
}