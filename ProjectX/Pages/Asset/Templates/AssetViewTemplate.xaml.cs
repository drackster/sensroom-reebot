﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using System.Runtime.CompilerServices;
using FFImageLoading.Forms;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AssetViewTemplate : ContentView
    {
        //public CachedImage ImgPhoto { get { return imgPhoto; } }
        //public Stream stream { get; set; }
        public AssetViewTemplate()
        {
            InitializeComponent();
            imgPhoto.CacheType = FFImageLoading.Cache.CacheType.All;
            imgPhoto.CacheDuration = new TimeSpan(App.ImageCacheDays, 0, 0, 0, 0);
            imgPhoto.Finish += ImgPhoto_Finish;
        }

        private void ImgPhoto_Finish(object sender, CachedImageEvents.FinishEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("Download complete");
        }

        //private void ImgPhoto_Error(object sender, CachedImageEvents.ErrorEventArgs e)
        //{
        //    imgPhoto.ReloadImage();
        //    //if (stream != null)
        //    //{
        //    //    imgPhoto.Source = ImageSource.FromStream(() =>
        //    //    {
        //    //        return stream;
        //    //    });
        //    //}
        //}
        //protected async override void OnBindingContextChanged()
        //{
        //    var model = BindingContext as AssetViewModel;
        //    if (model != null)// && imgPhoto.Source == null)
        //    {
        //        if (model.CoverImage.ImgSource == null)
        //            model.CoverImage.ImgSource = await App.apiService.GetImage(model.CoverImage.Id);
        //        stream = new MemoryStream(model.CoverImage.ImgSource);
        //        model.CoverImage.IsLoaded = true;
        //        imgPhoto.Source = ImageSource.FromStream(() =>
        //        {
        //            return stream;
        //        });
        //    }
        //    base.OnBindingContextChanged();
        //}
    }
}