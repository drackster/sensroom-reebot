﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Models;
using System.Runtime.CompilerServices;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AssetProfileImageTemplate : ContentView
    {
        #region Properties
        //public event EventHandler<AssetPhotoEventArgs> ImageTapped;
        //private AssetImageViewModel ViewModel { get { return this.BindingContext as AssetImageViewModel; } }
        #endregion

        #region cTor
        public AssetProfileImageTemplate()
        {
            InitializeComponent();
            AssetImg.CacheType = FFImageLoading.Cache.CacheType.All;
            AssetImg.CacheDuration = new TimeSpan(App.ImageCacheDays, 0, 0, 0, 0);
            AssetImg.Finish += AssetImg_Finish;
        }
        private void AssetImg_Finish(object sender, FFImageLoading.Forms.CachedImageEvents.FinishEventArgs e)
        {
            activityIndicator.IsEnabled = activityIndicator.IsRunning = activityIndicator.IsVisible = false;
        }

        #endregion

        //private void ImageDelete_Tapped(object sender, EventArgs e)
        //{
        //    if (ImageTapped != null)
        //        ImageTapped.Invoke(this, new AssetPhotoEventArgs(AssetPhotoAction.Delete));
        //}
        //private void Star_Tapped(object sender, EventArgs e)
        //{
        //    if (ImageTapped != null)
        //        ImageTapped.Invoke(this, new AssetPhotoEventArgs(AssetPhotoAction.Star));
        //}
    }


    //public class AssetPhotoEventArgs : EventArgs
    //{
    //    private AssetPhotoAction _pAction { get; set; }
    //    public AssetPhotoEventArgs(AssetPhotoAction pAction)
    //    {
    //        _pAction = pAction;
    //    }
    //    public AssetPhotoAction PAction { get { return _pAction; } }
    //}
    //public enum AssetPhotoAction
    //{
    //    Star=1,
    //    Delete=2
    //}
}