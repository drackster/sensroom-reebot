﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ATransactions : ContentPage
    {
        private BalanceViewModel viewModel;
        private List<OrderViewModel> transactions;
        public ATransactions(BalanceViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Transacciones");
            viewModel = model;
            lstTransactions.ItemSelected += LstTransactions_ItemSelected;
        }

        private void LstTransactions_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var order = e.SelectedItem as OrderViewModel;
            if (order != null)
            {
                //todo
            }
            lstTransactions.SelectedItem = null;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (IsBusy) return;
            IsBusy = true;

            string errorMsg = null;
            try
            {
                lstTransactions.ItemsSource = transactions = await App.apiService.GetTransactions(viewModel.Week);
                lstTransactions.IsVisible = true;
            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                errorMsg = MessageStrings.GeneralError;
            }
            finally
            {
                activityIndicator.IsRunning = activityIndicator.IsVisible = activityIndicator.IsEnabled = false;
                IsBusy = false;
            }
            if (errorMsg != null)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, errorMsg, "Ok");
            }
        }

    }
}