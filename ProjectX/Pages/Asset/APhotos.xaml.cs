﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Pages.Templates;
using ProjectX.Pages.Customer;
using ProjectX.Models;
using Plugin.Media;
using System.IO;
using ProjectX.Helpers;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class APhotos : ContentPage
    {
        #region Properties
        public bool HasChanges;
        private APhotosViewModel ViewModel;

        #endregion
        public APhotos()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, true);
            customNavBar.SetTitle("Mis Fotos");
          
            ViewModel = new APhotosViewModel();         
            ViewModel.imgList = App.AssetVM.Images;         
            LoadPhotos();
            this.BindingContext = ViewModel;
            ValidateCount();
        }

        protected override void OnAppearing()
        {           
            base.OnAppearing();        
             ViewModel.imgList = App.AssetVM.Images;         
            LoadPhotos();
            this.BindingContext = ViewModel;
            ValidateCount();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        #region Events
        //private async void LstPhotos_ItemTapped(object sender, ItemTappedEventArgs e)
        //{
        //    var response = await DisplayActionSheet("Opciones de foto", "Cancelar", "Eliminar",
        //        new string[] { "Foto de perfil" });

        //    if (response != "Cancelar")
        //    {
        //        if (IsBusy) return;
        //        IsBusy = true;
        //        try
        //        {
        //            if (response == "Foto de perfil")
        //            {
        //                var item = e.Item as AssetImageViewModel;
        //                //no haccer nada porque ya es la de perfil o solamente hay una imagen
        //                if (!item.IsProfile && ViewModel.imgList.Count > 1)
        //                {
        //                    //App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
        //                    //primero quitar la que ya es perfil
        //                    foreach (var i in ViewModel.imgList)
        //                    {
        //                        if (i.IsProfile)
        //                        {
        //                            i.IsProfile = false;
        //                            i.HasChanges = true;
        //                        }
        //                        else if (i.Id == item.Id)
        //                        {
        //                            i.IsProfile = true;
        //                            i.HasChanges = true;
        //                        }
        //                    }

        //                    lstPhotos.ItemsSource = null;
        //                    lstPhotos.ItemsSource = ViewModel.imgList;
        //                    //var lst = new List<AssetImageViewModel>(ViewModel.imgList);
        //                    await App.apiService.RearangeImages(ViewModel.imgList);
        //                    foreach(var img in ViewModel.imgList)
        //                        img.HasChanges = false;//reset
        //                    App.AssetVM.Images = ViewModel.imgList;
        //                    await App.Database.UpdateAssetImage(item);
        //                    HasChanges = true;
        //                }
        //            }
        //            else if (response == "Eliminar")
        //            {
        //                bool answer = await DisplayAlert(MessageStrings.GeneralConfirmationTitle, MessageStrings.DeleteConfirmation, "Sí", "No");
        //                if (answer)
        //                {
        //                    var item = e.Item as AssetImageViewModel;
        //                    var selectedImg = ViewModel.imgList.Where(x => x.Id == item.Id).FirstOrDefault();
        //                    if (selectedImg != null)
        //                    {
        //                        selectedImg.HasChanges = true;
        //                        selectedImg.IsDeleted = true;
        //                        //Si borramos la de perfil, establecer otra como perfil
        //                        bool autoSetProfile = selectedImg.IsProfile && ViewModel.imgList.Count > 1;
        //                        if (autoSetProfile)
        //                        {
        //                            var distinct = ViewModel.imgList.Where(x => x.Id != selectedImg.Id).FirstOrDefault();
        //                            if (distinct != null)
        //                            {
        //                                distinct.IsProfile = true;
        //                                distinct.HasChanges = true;
        //                            }
        //                        }
        //                        var changes = ViewModel.imgList.Where(x => x.HasChanges).ToList();
        //                        await App.apiService.RearangeImages(changes);
        //                        ///App.apiService.DeleteImage(item.Id);//Eliminar local y de la nube
        //                        await App.Database.DeleteAssetImage(item.Id);

        //                        ViewModel.imgList.Remove(selectedImg);
        //                        foreach (var img in ViewModel.imgList)
        //                            img.HasChanges = false;//reset

        //                        lstPhotos.ItemsSource = null;
        //                        lstPhotos.ItemsSource = ViewModel.imgList;
        //                        App.AssetVM.Images = ViewModel.imgList;
        //                        HasChanges = true;
        //                    }

        //                }
        //            }

        //        }
        //        catch
        //        {
        //            await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
        //        }
        //        finally
        //        {
        //            ValidateCount();
        //            //App.HUD.Dismiss();
        //            IsBusy = false;
        //        }
        //    }
        //}
        //private async void AddButtonTapped(object sender, EditModeArgs e)
        //{
        //    await LoadImage(new AssetProfileImageTemplate());
        //}
        private async void AddPhoto_Tapped(object sender, ItemTappedEventArgs e)
        {
            if (sender as Xamarin.Forms.Image != null)
            {
                await LoadImage(new AssetProfileImageTemplate());
                OnAppearing();
            }
            else
            {
                if (((sender as Xamarin.Forms.BindableObject).BindingContext as AssetImageViewModel).Id == null)
                {
                    return;
                }
                else
                {
                    string idImg = ((sender as Xamarin.Forms.BindableObject).BindingContext as AssetImageViewModel).Id;
                    var result = await DisplayActionSheet("Que hacer", "Cancelar", null,
                    new string[] { "De perfil", "Borrar" });

                    if (!string.IsNullOrEmpty(result))
                    {
                        switch (result)
                        {                           
                            case "De perfil":
                                SetProfileImage(idImg);
                              
                                break;
                            case "Borrar":
                                BorrarFoto(idImg);
                              
                                break;
                        }  
                    }
                }
                await App.apiService.SaveAsset(App.AssetVM);
                await App.Database.SaveAsset(App.AssetVM);
                OnAppearing();
            }
          //  ViewModel.imgList.Clear();
        }
        #endregion

        #region Methods
        private async Task LoadImage(AssetProfileImageTemplate img)
        {
            if (IsBusy) return;
            IsBusy = true;
            bool isError = false;
            try
            {
                if (CrossMedia.IsSupported && CrossMedia.Current.IsPickPhotoSupported)
                {
                    var file = await CrossMedia.Current.PickPhotoAsync(new Plugin.Media.Abstractions.PickMediaOptions
                    {
                        PhotoSize = Plugin.Media.Abstractions.PhotoSize.Medium,
                        //CompressionQuality = 80
                    });

                    if (file != null)
                    {
                        App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
                        var stream = file.GetStream();
                        byte[] bytes = null;
                        using (MemoryStream ms = new MemoryStream())
                        {
                            stream.CopyTo(ms);
                            bytes = ms.ToArray();
                        }
                        //Sincronizar imagen
                        //var imgId = await App.apiService.SaveImage(bytes);
                        var newImage = await App.apiService.SaveImage(bytes);
                        if (newImage == null)
                        {
                            isError = true;
                        }
                        else
                        {
                            ViewModel.imgList.Add(newImage);
                            App.AssetVM.Images = ViewModel.imgList;
                            LoadPhotos();
                            ValidateCount();
                            //newImage.ImgSource = bytes;
                            await App.Database.SaveAssetImages(new List<AssetImageViewModel>() { newImage });
                            HasChanges = true;

                            //AddLastItem();
                        }
                    }
                }
                else
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.ImageCaptureError, "Ok");
                }
            }
            catch (Exception ex)
            {
                isError = true;
                await App.Database.LogException(ex);
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (isError) await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");

        }

        private void LoadPhotos()
        {
            int i;
            for (i = 0; i < App.AssetVM.Images.Count; i++)
            {
                switch (i)
                {
                    case 0:
                        imgProfile.BindingContext = App.AssetVM.Images[i];
                        break;
                    case 1:
                        imgProfile2.BindingContext = App.AssetVM.Images[i];
                        break;
                    case 2:
                        imgProfile3.BindingContext = App.AssetVM.Images[i];
                        break;
                    case 3:
                        imgProfile4.BindingContext = App.AssetVM.Images[i];
                        break;
                    case 4:
                        imgProfile5.BindingContext = App.AssetVM.Images[i];
                        break;
                }
            }
            for (; i < 5; i++)
            {
                switch (i)
                {
                    case 0:
                        imgProfileAdd.IsVisible = !(imgProfile.IsVisible = false);
                        break;
                    case 1:
                        imgProfileAdd2.IsVisible = !(imgProfile2.IsVisible = false);
                        break;
                    case 2:
                        imgProfileAdd3.IsVisible = !(imgProfile3.IsVisible = false);
                        break;
                    case 3:
                        imgProfileAdd4.IsVisible = !(imgProfile4.IsVisible = false);
                        break;
                    case 4:
                        imgProfileAdd5.IsVisible = !(imgProfile5.IsVisible = false);
                        break;
                }
            }
        }

        private void ValidateCount()
        {
            slSadFace.IsVisible = ViewModel.imgList.Count == 0;
        }

        private async void BorrarFoto(string id)
        {            
            await App.apiService.DeleteImage(id);          
        }

        private async void SetProfileImage(string id)
        {
            await App.apiService.SetImageProfile(id);         
        }

        #endregion

    }
    public class APhotosViewModel : BaseViewModel
    {
        private List<AssetImageViewModel> _imgList;

        public APhotosViewModel()
        {
            imgList = new List<AssetImageViewModel>();
        }
        public List<AssetImageViewModel> imgList
        {
            get
            {
                return _imgList;
            }
            set
            {
                if (imgList != value)
                {
                    _imgList = value;
                    OnNotifyPropertyChanged();
                }
            }
        }
    }
}