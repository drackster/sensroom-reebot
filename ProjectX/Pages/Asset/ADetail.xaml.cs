﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Pages.Templates;
using ProjectX.Pages.Customer;
using ProjectX.Models;
using System.IO;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ADetail : ContentPage
    {
        #region Properties
        private AssetViewModel model;
        
        private AddressViewModel CurrentAddress;
        //private bool isLoaded;
        private bool fullScreenMode;
        //private bool loginIsVisible;
        #endregion

        #region cTor
        public ADetail(AssetViewModel model, AddressViewModel currentAddress)
        {
            InitializeComponent();
            this.model = model;
            NavigationPage.SetHasNavigationBar(this, false);
            //imgSource = new List<XImageSource>();

            rotatorRow.Height = App.ScreenHeight;

            CurrentAddress = currentAddress;

            
            this.BindingContext = model;
            lblEdad.Text = model.Age.ToString() + " años";
            txtTier.Text = model.Precio.ToString();
            #region rotator
            sfRotator.ItemsSource = model.Images;
            sfRotator.EnableAutoPlay = false;
            sfRotator.EnableLooping = false;
            sfRotator.EnableSwiping = true;
            #endregion

            InitReviews();
            InitTiers();
            InitCalendar();


            if (Device.RuntimePlatform == Device.Android)
                backButtonBox.IsVisible = false;
        }
        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected override void OnAppearing()
        {
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
        }

        #endregion

        #region Events
        private void BackButton_Tapped(object sender, EventArgs e)
        {
            Navigation.PopAsync(true);
        }
        //private async void ZoomButton_Tapped(object sender, EventArgs e)
        //{
        //    //try
        //    //{
        //    //    await Plugin.Share.CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage() { Text = "CODIGO XXX", Title = "Invitacion de Vics" }, new Plugin.Share.Abstractions.ShareOptions() { ChooserTitle = "Invitación de Vics" });
        //    //}
        //    //catch
        //    //{

        //    ////}
        //}
        private void ReadReviews_Tapped(object sender, EventArgs e)
        {
            if(model.ReviewCount == 1)
            {
                Navigation.PushAsync(new Common.CReviewDetail(model.LastReview, model.Name),true);
            }
            else
            {
                Navigation.PushAsync(new Common.CReviews(model.Id, model.Name), true);
            }
        }
        private async void AssetView_InviteButtonTapped(object sender, EventArgs e)
        {
            //var credito = await App.apiService.GetCustomCredit(currentAddress.);// revisamos si el cliente tiene crédito disponible
            //await App.Database.DeleteCustomerCredit();
            //await App.Database.SaveCustomerCredit(credito);

            CustomerCredit creditos = new CustomerCredit
            {

            };
            creditos = await App.Database.getCustomerCredit();          
            await Navigation.PushAsync(new XPayments(model, CurrentAddress, creditos), true);
        }
        private void ImgProfile_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                if (!fullScreenMode)
                {
                    //sfRotator.HeightRequest = sfRotator.Height;
                    //while (sfRotator.HeightRequest < App.ScreenHeight)
                    //    sfRotator.HeightRequest += 10;
                    sfRotator.HeightRequest = App.ScreenHeight;
                    Grid.SetRowSpan(sfRotator, 2);
                    btnZoomIn.IsVisible = false;
                    btnZoomOut.IsVisible = true;
                   
                }
                else
                {
                    //sfRotator.HeightRequest = sfRotator.Height;
                    //while (sfRotator.HeightRequest > 150)
                    //    sfRotator.HeightRequest -= 10;
                    sfRotator.HeightRequest = 150;
                    Grid.SetRowSpan(sfRotator, 1);
                    btnZoomIn.IsVisible = true;
                    btnZoomOut.IsVisible = false;
                }
                fullScreenMode = !fullScreenMode;

                if (model.Images.Count == 1)
                {
                    sfRotator.ItemsSource = null;
                    sfRotator.ItemsSource = model.Images;

                }
                else
                {
                    if (sfRotator.SelectedIndex == 0)
                    {
                        sfRotator.SelectedIndex = 1;
                        sfRotator.SelectedIndex = 0;
                    }
                    else
                    {
                        int old = sfRotator.SelectedIndex;
                        sfRotator.SelectedIndex--;
                        sfRotator.SelectedIndex = old;
                    }
                }


            }
            catch
            {

            }
            finally
            {
                IsBusy = false;
            }



        }
        #endregion

        #region Methods
        private void InitReviews()
        {
            if(model.LastReview!=null)
            {
                
                txtReviewerText.Text = model.LastReview.Review;
                txtReviewerDate.Text = Helpers.MessageStrings.GetMonthName(model.LastReview.CreatedOn.Month) + " " + model.LastReview.CreatedOn.Year.ToString();
                txtReviewerName.Text = model.LastReview.ReviewerName;
                txtReviewsRead.Text = model.ReviewCount == 1 ? "Leer evaluación completa" : "Leer las " + model.ReviewCount.ToString() + " evaluaciones";
                reviewRating.Value = model.LastReview.Rating;
            }
            txtReviewerText.IsVisible = slReview1.IsVisible = 
                slReview2.IsVisible = slReview3.IsVisible = model.LastReview != null;
            

        }
        private void InitTiers()
        {
            int selectedTime = App.UserVM.SearchFilters.Hours;
            ACategoryTierViewModel selectedTier = model.Tiers.Where(x => x.NumHours == selectedTime).FirstOrDefault();          
            //string.Format("{0:C0}", model.Tiers[0].Cost);//txtTier.Text = model.Precio.ToString();
            //txtTierDesc.Text = model.Tiers[0].Name;
            if (selectedTier != ((ACategoryTierViewModel)null))
            {
               string.Format("{0:C0}", selectedTier.Cost); // txtTier.Text = model.Precio.ToString();
                                                       // txtTierDesc.Text = selectedTier.Name;
            }
        }
        private void InitCalendar()
        {
            if (model.Calendars.Count > 0 && App.UserVM.SearchFilters.orderType == OrderType.Calendar)
            {
                var days = Helper.GetWeekDays();
                foreach (var c in model.Calendars)
                    AddCalendarDetail(c, days);
            }
            else
            {
                slCalendar1.IsVisible = slCalendar2.IsVisible = slCalendar3.IsVisible = false;
            }
        }
        private void AddCalendarDetail(CalendarViewModel c, List<CalendarDetailViewModel> days)
        {
            foreach (var d in c.Detail)
                d.Name = days[(int)d.Day].Name;
            c.Days = Helper.GetDays(c.Detail);
            var item = new XCalendarView() { BindingContext = c };
            slCalendar3.Children.Add(item);
        }
        
        #endregion

    }
}