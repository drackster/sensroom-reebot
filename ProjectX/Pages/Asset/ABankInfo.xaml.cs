﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Helpers;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using Syncfusion.SfAutoComplete.XForms;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ABankInfo : ContentPage
    {
        #region Properties
        private List<SfAutoCompleteItem> bankList;
        public bool HasChanges;
        #endregion
        public ABankInfo()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Datos Bancarios");
            customNavBar.SetEditMode("Listo");

            bankList = new  List<SfAutoCompleteItem>();
            //txtBankName.TextValue = App.AssetVM.BankName;
            txtAccountNum.Text = App.AssetVM.AccountNum;
            txtAccountName.Text = App.AssetVM.BankHolderName;
            txtAccountNum2.Text = App.AssetVM.AccountNum2;

            if (!string.IsNullOrEmpty(txtAccountNum2.Text))
            {
                SegControl1.SelectedSegment = 0;//CHEQUES
                lblAccountCount2.Text = txtAccountNum2.Text.Length.ToString();
            }
            if (!string.IsNullOrEmpty(txtAccountNum.Text))
            {
                SegControl1.SelectedSegment = 1;//TARJETA
                lblAccountCount.Text = txtAccountNum.Text.Length.ToString();
            }
            
            //txtBankName.LineEntryField.TextChanged += txtAccountInfo_TextChanged;
            txtAccountNum.TextChanged += txtAccountNum_TextChanged;
            txtAccountName.TextChanged += txtAccountInfo_TextChanged;
            txtAccountNum2.TextChanged += txtAccountNum2_TextChanged;
            
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            //autoCompleteBank.SelectionChanged += AutoCompleteBank_SelectionChanged;
            autoCompleteBank.ValueChanged += AutoCompleteBank_ValueChanged;
            SegControl1.ValueChanged += SegControl1_ValueChanged;


            if (Device.RuntimePlatform == Device.Android)
            {
                autoCompleteBank.TextSize = 19;
            }

        }

        #region Events
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (SegControl1.SelectedSegment == 0)//cheques
                await LoadBankList();          
        }
        private async void SegControl1_ValueChanged(object sender, SegmentedControl.FormsPlugin.Abstractions.ValueChangedEventArgs e)
        {
            if(SegControl1.SelectedSegment == 1)
            {
                grd1.IsVisible = true;
                grd0.IsVisible = false;
            }
            else if(SegControl1.SelectedSegment == 0)
            {
                //si carga bancos
                await LoadBankList();
                grd0.IsVisible = true;
                grd1.IsVisible = false;
            }
        }
        private void AutoCompleteBank_ValueChanged(object sender, Syncfusion.SfAutoComplete.XForms.ValueChangedEventArgs e)
        {
            HasChanges = true;
            //txtAccountNum.Focus();
        }
        private void txtAccountNum_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtAccountNum.Text == null ? 0 : txtAccountNum.Text.Length;
            if (ccount > 16)
                txtAccountNum.Text = e.OldTextValue;
            lblAccountCount.Text = ccount.ToString();
            HasChanges = true;
        }
        private void txtAccountNum2_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtAccountNum2.Text == null ? 0 : txtAccountNum2.Text.Length;
            if (ccount > 18)
                txtAccountNum2.Text = e.OldTextValue;
            lblAccountCount2.Text = ccount.ToString();
            HasChanges = true;
        }
        private void txtAccountInfo_TextChanged(object sender, TextChangedEventArgs e)
        {
            HasChanges = true;
            txtAccountName.Text = txtAccountName.Text.ToUpper();
        }
        private async void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            if (IsBusy) return;

            if (!HasChanges)
            {
                await Navigation.PopAsync(true);
                return;
            }

            if (!ValidateAccounts()) return;

            IsBusy = true;


            App.HUD.Show(Helpers.MessageStrings.SavingMsg, Interfases.MaskType.Black);
            bool isError = false;
            try
            {
                #region BankAutocomplete
                //App.AssetVM.BankName = txtBankName.TextValue;
                string selectedBank = null;
                if (autoCompleteBank.SelectedValue != null)
                {
                    selectedBank = autoCompleteBank.SelectedValue.ToString();
                    //if (autoCompleteBank.SelectedIndex >= 0)
                    //App.AssetVM.BankId = App.banks[autoCompleteBank.SelectedIndex].Id;
                }
                else if (!string.IsNullOrEmpty(autoCompleteBank.Text))
                {
                    selectedBank = autoCompleteBank.Text;
                }
                if (!string.IsNullOrEmpty(selectedBank))
                {
                    var exists = App.banks.Where(x => x.Name == selectedBank).FirstOrDefault();
                    if (exists == null)
                        App.AssetVM.BankId = null;
                    else
                        App.AssetVM.BankId = exists.Id;
                }
                else
                {
                    App.AssetVM.BankId = null;
                }
                #endregion

                App.AssetVM.AccountNum = txtAccountNum.Text;
                App.AssetVM.AccountNum2 = txtAccountNum2.Text;
                App.AssetVM.BankHolderName = txtAccountName.Text;
                await App.apiService.SaveAsset(App.AssetVM);
                await App.Database.SaveAsset(App.AssetVM);
            }
            catch
            {
                isError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }

            if (isError) await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            await Navigation.PopAsync(true);
        }

        #endregion

        #region Methods

        private async Task LoadBankList()
        {
            if (IsBusy || bankList.Count > 0) return;
            IsBusy = true;
            bool IsError = false;
            try
            {
                if (App.banks == null || App.banks.Count == 0)
                {
                    App.HUD.Show("Cargando lista de bancos...", Interfases.MaskType.Black);
                    App.banks = await App.apiService.GetBankList();
                }
                foreach (var bank in App.banks)
                    if (!string.IsNullOrEmpty(bank.Name))
                        bankList.Add(new SfAutoCompleteItem(bank.Name, "blankicon.png"));
                autoCompleteBank.ItemsSource = bankList;
                if (App.AssetVM.BankId.HasValue)
                {
                    var bName = App.banks.Where(x => x.Id == App.AssetVM.BankId).FirstOrDefault().Name;
                    //autoCompleteBank.SelectedValuePath = bName;
                    autoCompleteBank.Text = bName;
                }

            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                IsError = true;
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
            if (IsError)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await Navigation.PopAsync(true);
            }
        }
        private bool ValidateAccounts()
        {
            if(!string.IsNullOrEmpty(txtAccountNum.Text) && txtAccountNum.Text.Length < 16)
            {
                DisplayAlert(MessageStrings.GeneralErrorTitle, "El número de tarjeta debe de ser de 16 dígitos", "Ok");
                return false;
            }
            if (!string.IsNullOrEmpty(txtAccountNum2.Text) && txtAccountNum2.Text.Length < 18)
            {
                DisplayAlert(MessageStrings.GeneralErrorTitle, "La cuenta CLABE debe de ser de 18 dígitos", "Ok");
                return false;
            }
            return true;
        }
        #endregion
    }
}