﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;

namespace ProjectX.Pages.Asset
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ACalendar : ContentPage
    {
        public ACalendar()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Mis Horarios");
            customNavBar.SetEditMode("Agregar");
            customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            lstSched.ItemsSource = await App.apiService.GetCalendar();
        }
        private void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            Navigation.PushAsync(new ACalendarDetail(null), true);
        }
        private void lstSched_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {

        }
    }
}