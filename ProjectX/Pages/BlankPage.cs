﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using Xamarin.Forms;
using ProjectX.Pages.Common;
using ProjectX.Models;
namespace ProjectX.Pages
{
    public class BlankPage : ContentPage
    {
        //private bool IsLoaded;
        //private bool IsMainPageLoaded;
        public BlankPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            this.BackgroundColor = Color.FromRgb(54,79,118);
            this.Content = new Image()
            {
                Source = "logo.png",
                Aspect = Aspect.AspectFit
            };
        }
        

    }
}
