﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RootPageMaster : ContentPage
    {
        public event EventHandler<EventArgs> TCYTapped;
        public ListView ListView;
        public Image ProfileImage { get { return imgProfile; } }

        bool Bupdate = false;
        public RootPageMaster()
        {
            InitializeComponent();
            BindingContext = new RootPageMasterViewModel();
            ListView = MenuItemsListView;
            imgProfile.HeightRequest = 40;
            if(App.RegisterVM!=null && !string.IsNullOrEmpty(App.RegisterVM.UserName))
                lblUserName.Text = App.RegisterVM.UserName;

            if (Device.RuntimePlatform == Device.Android)
            {
                MenuItemsListView.RowHeight = 50;
            }
            
        }
        //public void RepopulateMenu()
        //{
        //    BindingContext = null;
        //    BindingContext = new RootPageMasterViewModel();
        //    lblUserName.Text = App.RegisterVM.UserName;
        //    ListView = MenuItemsListView;
        //    imgProfile.HeightRequest = 40;
        //}
        public void TYC_Tapped(object sender, EventArgs e)
        {
            if(TCYTapped!=null)
            {
                TCYTapped.Invoke(this, new EventArgs());
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            BindingContext = new RootPageMasterViewModel();
            update();
            Bupdate = true;
        }

        private async void update()
        {
            await Task.Delay(10000);
            if (Bupdate)
            {

                OnAppearing();
            }
            else
            {
                return;
            }

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Bupdate = false;
        }

        public class RootPageMasterViewModel : INotifyPropertyChanged
        {
            public ObservableCollection<RootPageMenuItem> MenuItems { get; set; }

            public RootPageMasterViewModel(int count=0)
            {

                if(App.MSender == ViewModels.MessageSender.Customer)
                {
                    MenuItems = new ObservableCollection<RootPageMenuItem>(new[]
                    {
                    new RootPageMenuItem { Id = 0, Icon = "home2.png", Title = "Inicio", TargetType = typeof(Customer.XList) },
                    new RootPageMenuItem { Id = 1, Icon = "sobre.png", Title = "Buzón", TargetType = typeof(Customer.XHistory) },
                    new RootPageMenuItem { Id = 2, Icon = "invitacion.png", Title = "Invitar amig@",TargetType = typeof(Customer.XInvitation) },
                    //new RootPageMenuItem { Id = 3, Icon = "\uf104", Title = "Autorizaciones",TargetType = typeof(Common.CPendingRequests) },
                    new RootPageMenuItem { Id = 10, Icon = "logout2.png", Title = "Cerrar Sesión",TargetType = typeof(Common.Logout) },
                //    new RootPageMenuItem { Id = 11, Icon = "Help.png", Title = "Ayuda",TargetType = typeof(Common.CHelp) },
                 //new RootPageMenuItem { Id = 11, Icon = "Help.png", Title = "Ayuda",TargetType = typeof(Common.CChatAdmin) },
                });
                }
                else if(App.MSender == ViewModels.MessageSender.Asset)
                {
                    MenuItems = new ObservableCollection<RootPageMenuItem>(new[]
                    {
                    new RootPageMenuItem { Id = 4, Icon = "home2.png", Title = "Inicio", TargetType = typeof(Asset.AMain) },
                   // new RootPageMenuItem { Id = 5, Icon = "Citas.png", Title = "Mis Citas",TargetType = typeof(Asset.ASchedule) },
                    new RootPageMenuItem { Id = 9, Icon = "mensaje2.png", Title = "Mensajes", TargetType = typeof(Customer.XHistory) },
                    new RootPageMenuItem { Id = 7, Icon = "setings2.png", Title = "Configuración",TargetType = typeof(Asset.AConfiguration) },
                   // new RootPageMenuItem { Id = 8, Icon = "Saldos.png", Title = "Saldos/Movimientos", TargetType = typeof(Asset.ABalance) },
                    new RootPageMenuItem { Id = 6, Icon = "invitacion.png", Title = "Invitar amig@",TargetType = typeof(Customer.XInvitation) },
                    new RootPageMenuItem { Id = 10, Icon = "logout2.png", Title = "Cerrar Sesión",TargetType = typeof(Common.Logout) },
                  //  new RootPageMenuItem { Id = 11, Icon = "Help.png", Title = "Ayuda",TargetType = typeof(Common.CHelp) },
                 // new RootPageMenuItem { Id = 11, Icon = "Help.png", Title = "Ayuda",TargetType = typeof(Common.CChatAdmin) },
                    });
                }

                
            }

            #region INotifyPropertyChanged Implementation
            public event PropertyChangedEventHandler PropertyChanged;
            void OnPropertyChanged([CallerMemberName] string propertyName = "")
            {
                if (PropertyChanged == null)
                    return;

                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            #endregion
        }

    }
}