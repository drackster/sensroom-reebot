﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace ProjectX.Pages.Common
{
    public class Logout : ContentPage
    {
        public event EventHandler<EventArgs> LogoutComplete;
        public Logout()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            //this.BackgroundColor = Color.Transparent;
            //var grd = new Grid();
            //grd.BackgroundColor = Color.White;
            //grd.Opacity = 0.2;
            //this.Content = grd;
            this.Content = new Image()
            {
                Source = "logo.png",
                Aspect = Aspect.AspectFit,
                Opacity = 0.2
            };
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var result = await DisplayAlert("Confirmación", "Cerrar sesión", "Sí", "No");
            if(result)
            {
                if (App.MSender == ViewModels.MessageSender.Customer)
                {
                    App.SetBadgeCount(null, 0);
                    await App.Database.DeleteSearchModel();
                    await App.Database.DeleteUser();
                    
                }
                else if (App.MSender == ViewModels.MessageSender.Asset)
                {
                    App.SetBadgeCount(null, 0);
                    await App.Database.DeleteAsset();
                    await App.Database.DeleteAssetImages();
                    await App.Database.DeleteCalendars();
                    
                }
                await App.Database.DeleteRegister();
                App.RegisterVM = null;
                App.scheduleViewModel = null;
                //App.SettingsVM = null;
                App.RootStep = 0;//reset
                if (LogoutComplete != null)
                    LogoutComplete.Invoke(this, new EventArgs());
            }
            else
            {
                await Navigation.PopModalAsync(true);
            }
        }

        public async void logouttiempo(bool aceptar)
        {

            //var result = await DisplayAlert("Confirmación", "Cerrar sesión", "Sí", "No");
            if (aceptar)
            {
                if (App.MSender == ViewModels.MessageSender.Customer)
                {
                    App.SetBadgeCount(null, 0);
                    await App.Database.DeleteSearchModel();
                    await App.Database.DeleteUser();

                }
                else if (App.MSender == ViewModels.MessageSender.Asset)
                {
                    App.SetBadgeCount(null, 0);
                    await App.Database.DeleteAsset();
                    await App.Database.DeleteAssetImages();
                    await App.Database.DeleteCalendars();

                }
                await App.Database.DeleteRegister();
                App.RegisterVM = null;
                App.scheduleViewModel = null;
                //App.SettingsVM = null;
                App.RootStep = 0;//reset
              //  if (LogoutComplete != null)
                    LogoutComplete.Invoke(this, new EventArgs());
            }
            else
            {
                await Navigation.PopModalAsync(true);
            }
        }
    }
}