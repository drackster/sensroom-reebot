﻿using Newtonsoft.Json;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CHelp : ContentPage
    {
        public CHelp()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }       

        private async void Button_Clicked(object sender, EventArgs e)
        {
            var model = new InvitationViewModel();
            model.CustomerId = 144;
            
            model.OrderId = 0;
            model.oType = OrderType.ASAP;
            model.oStatus = OrderStatus.Confirmed;
            model.StatusName = "";
            model.CustomerName="";
            model.GlobalRating = 0.0;
            model.CreatedOn =DateTime.UtcNow;
            model.RequestedTime1 = "";
            model.EventDateTime =DateTime.UtcNow;
            model.EventEnd =DateTime.UtcNow;
            model.ExpiresOn =DateTime.UtcNow.AddDays(1);
            model.TierName = "";
            model.Cost = 0.0m;
            model.dStatus = 0.0m;
            model.AddressId = 434;
            model.Address = "";
            model.ReviewCount = 0;
            model.ReviewDate =DateTime.UtcNow;
            model.Rating = 0.0f;
            model.DistanceFrom = "";
            model.CLatitude = 25.641739599999998;
            model.CLongitude = -100.3229793;
            model.paymentMethod = PaymentMethod.Paynet;
          
            //var json = JsonConvert.DeserializeObject(arch);
         
            //model = json as InvitationViewModel;
            // 144 user id 

            await Navigation.PushModalAsync(new Common.MessageList(model, true), true);
        }
    }
}