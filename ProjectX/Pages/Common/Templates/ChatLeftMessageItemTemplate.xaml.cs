﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FFImageLoading.Forms;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatLeftMessageItemTemplate : ContentView
    {
        public CachedImage AssetImage { get { return AssetImg; } }
        public ChatLeftMessageItemTemplate()
        {
            InitializeComponent();
            AssetImg.CacheType = FFImageLoading.Cache.CacheType.All;
            AssetImg.CacheDuration = new TimeSpan(App.ImageCacheDays, 0, 0, 0, 0);
        }
    }
}