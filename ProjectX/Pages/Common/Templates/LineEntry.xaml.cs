﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LineEntry : ContentView
    {
        public static readonly BindableProperty TextValueProperty =
            BindableProperty.Create("TextValue", typeof(string), typeof(LineEntry), "");
        public string TextValue
        {
            get { return (string)GetValue(TextValueProperty); }
            set { SetValue(TextValueProperty, value); }
        }

        public static readonly BindableProperty PlaceHolderValueProperty =
            BindableProperty.Create("PlaceHolderValue", typeof(string), typeof(LineEntry), "");
        public string PlaceHolderValue
        {
            get { return (string)GetValue(PlaceHolderValueProperty); }
            set { SetValue(PlaceHolderValueProperty, value); }
        }

        public static readonly BindableProperty ErrorValueProperty =
        BindableProperty.Create("ErrorValue", typeof(string), typeof(LineEntry), "");
        public string ErrorValue
        {
            get { return (string)GetValue(ErrorValueProperty); }
            set { SetValue(ErrorValueProperty, value); }
        }

        public static readonly BindableProperty IsPasswordValueProperty =
        BindableProperty.Create("IsPasswordValue", typeof(bool), typeof(LineEntry), false);
        public bool IsPasswordValue
        {
            get { return (bool)GetValue(IsPasswordValueProperty); }
            set { SetValue(IsPasswordValueProperty, value); }
        }

        public Entry LineEntryField
        {
            get { return TextField; }
        }
        public LineEntry()
        {
            InitializeComponent();
            BindingContext = this;
            TextField.Focused += TextField_Focused;
            TextField.Unfocused += TextField_Unfocused;

            //if (IsPasswordValue)
            //    LineEntryField.Keyboard = Keyboard.Numeric;

        }    
        private void TextField_Focused(object sender, FocusEventArgs e)
        {
            slUnder.BackgroundColor = Color.FromHex("2880f0"); //Color.Black;//(Color)Resources["FocusedUnderlineColor"];
        }
        private void TextField_Unfocused(object sender, FocusEventArgs e)
        {
            slUnder.BackgroundColor = Color.FromHex("4d4d4d");// (Color)Resources["UnderlineColor"];
        }


    }
}