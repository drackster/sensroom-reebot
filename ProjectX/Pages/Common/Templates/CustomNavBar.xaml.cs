﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Templates
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomNavBar : ContentView
    {
        #region Events
        public event EventHandler<EditModeArgs> EditButtonTapped;
        public event EventHandler<EditModeArgs> NextButtonTapped;
        public event EventHandler<EditModeArgs> NestedBackButtonTapped;
        #endregion

        #region Properties
        private bool IsModal;
        private bool IsCarousel;
        private bool IsEditMode;
        private string text1;
        private string text2;
        private bool returnToInitialPage;

        //public ProgressBar NavProgessBar { get { return progressBar1; } }
        //public Button OnlineButton { get { return btnOnline; } }
        #endregion
        public CustomNavBar()
        {
            InitializeComponent();
        }

        #region Methods
        public void SetTitle(string title)
        {
            this.lblTitle.Text = title;
            lblTitlePage.IsVisible = true;
        }
        public void SetModalMode()
        {
            this.btnBack.IsVisible = false;
            IsModal = true;
        }
        public void SetCarouselMode(string RightButtonTitle, bool ShowPrev = true)
        {
            IsCarousel = true;
            btnDonePage.IsVisible =
            btnDone.IsVisible = true;
            btnDone.Text = RightButtonTitle;
            if (!ShowPrev)
            {
                this.btnBack.IsVisible = false;
            }
        }
        public void SetIconText(string text)
        {
            this.btnBackLabel.Text = text;
            this.btnBackLabel.FontFamily = "";
            this.btnBackLabel.FontSize = 25;
            //this.btnBackLabel.VerticalOptions = LayoutOptions.End;
            //this.btnBackLabel.VerticalTextAlignment = TextAlignment.End;
            this.btnBackLabel.TranslationY = 3;
            IsModal = true;
            //alertCounter.IsVisible = alertIcon.IsVisible = false;
        }

        public void SetReturningPage(bool returnToInitialPage)
        {
            this.returnToInitialPage = returnToInitialPage;
        }

        //Significa que forzosamente tienes que completar la forma para continuar
        public void SetModalOnly()
        {
            btnBack.IsVisible = false;
            IsModal = true;
            //alertCounter.IsVisible = alertIcon.IsVisible = false;
        }
        public void SetHamburguerMode()
        {
            btnBack.IsVisible = false;
            ImgHamburguer.IsVisible = true;
            lblTitlePage.IsVisible = true;
        }

        //public void SetOnlineMode(bool ShowBackButton=false)
        //{
        //    btnBack.IsVisible = ShowBackButton;
        //    ImgHamburguer.IsVisible = !ShowBackButton;

        //    btnOnline.IsVisible = true;
        //    lblTitle.IsVisible = false;
        //}
        public void UpdateEditModeText(string Text)
        {
            btnDone.Text = Text;
        }
        public void SetEditMode(string Text1=null, string Text2=null)
        {
            //alertCounter.IsVisible = alertIcon.IsVisible = false;
            if(Text1!=null)
                btnDone.Text = text1 = Text1;
            btnDonePage.IsVisible =
            btnDone.IsVisible = true;
            IsEditMode = true;
        }

        //esto habilita el trigger del backbutton o X
        public void SetCarMode()
        {
            IsCarousel = true;
        }
        public void SetLastPageMode(string Text)
        {
            btnDone.Text = text1 = Text;
        }

        #endregion

        #region Events
        private void BackButton_Tapped(object sender, EventArgs e)
        {
            if(IsCarousel)
            {
                if (NestedBackButtonTapped != null)
                    NestedBackButtonTapped.Invoke(this, new EditModeArgs(false));
                return;
            }
            else if(IsEditMode)
            {
                
            }
            var page = this.Parent.Parent as Page;

            if(page==null)
            {
                page = this.Parent.Parent.Parent as Page;
            }

            if (returnToInitialPage)
            {
                App.RootStep = 0;
                page.Navigation.PopModalAsync(true);
                ((RootPage)((App)page.Parent.Parent).MainPage).LoadMainViews();
                return;
            }

            if (page != null)
            {
                if (IsModal)
                {
                    App.RootStep = 0; //reset, como le da tap en la X (cerrar el modal), 
                    //entonces puede irse a login de nuevo
                    //page.Navigation.PopAsync(true);
                    page.Navigation.PopModalAsync(true);

                    //if (NestedBackButtonTapped != null)
                    //    NestedBackButtonTapped.Invoke(this, new EditModeArgs(false));
                }
                else
                    page.Navigation.PopAsync(true);
            }
        }
        public void CierraModal()
        {
            BackButton_Tapped(null, EventArgs.Empty);
        }
        private void btnDone_Tapped(object sender, EventArgs e)
        {
            if (IsCarousel)
            {
                if (NextButtonTapped != null)
                    NextButtonTapped.Invoke(this, new EditModeArgs(false));
                return;
            }
            //IsEditMode = !IsEditMode;
            if (EditButtonTapped != null)
                EditButtonTapped.Invoke(this, new EditModeArgs(IsEditMode));
            btnDone.Text = IsEditMode ? text1 : text2;
        }
        private void ImgHamburguer_Tapped(object sender, EventArgs e)
        {
            Element current = this;
            while (current.Parent != null)
            {
                current = current.Parent;
                if (current.GetType().Name == "RootPage")
                {
                    break;
                }
            }

            var master = current as MasterDetailPage;

            if (master != null)
            {
                master.IsPresented = true;
            }
        }

        #endregion
    }
    public class EditModeArgs : EventArgs
    {
        private bool _isEditMode;
        public EditModeArgs(bool isEditMode)
        {
            _isEditMode = isEditMode;
        }
        public bool IsEditMode { get { return _isEditMode; } }
    }

}