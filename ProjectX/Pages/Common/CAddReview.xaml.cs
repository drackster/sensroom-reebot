﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CAddReview : ContentPage
    {
        #region Properties
        private MessageSender type;
        private ReviewViewModel review;
        #endregion

        #region cTor
        public CAddReview(AssetViewModel model, long orderId)
        {
            InitializeComponent();
            type = MessageSender.Asset;
            review = new ReviewViewModel();
            review.ReviewerName = lblName.Text = model.Name;
            review.ReviewedId = model.Id;
            review.ReviewerId = App.UserVM.Id;
            review.OrderId = orderId;
            Init();
        }
        public CAddReview(OrderViewModel order)
        {
            InitializeComponent();
            type = MessageSender.Customer;

            review = new ReviewViewModel();
            review.ReviewerName = lblName.Text = order.CustomerName;
            review.ReviewedId = order.CustomerId;
            review.ReviewerId = App.AssetVM.Id;
            review.OrderId = order.Id;
            Init();
        }

        #endregion

        #region Events
        private void reviewRating_ValueChanged(object sender, Syncfusion.SfRating.XForms.ValueEventArgs e)
        {
            lblReview.Text = string.Format("{0:N1}", reviewRating.Value);
        }
        private async void SaveReview_Clicked(object sender, EventArgs e)
        {
            if(reviewRating.Value == 0)
            {
                bool reviewWithZero = await DisplayAlert(Helpers.MessageStrings.GeneralConfirmationTitle, 
                    "¿Calificar a " + review.ReviewerName + " con 0.0?","Sí","No");
                if(!reviewWithZero)
                {
                    return;
                }
            }
            await SaveReview();
        }
        private void TxtDetails_TextChanged(object sender, TextChangedEventArgs e)
        {
            int ccount = txtDetails.Text == null ? 0 : txtDetails.Text.Length;
            if (ccount > 200)
                txtDetails.Text = e.OldTextValue;
            lblCharCount.Text = ccount.ToString();
        }
        private async void CloseButton_Tapped(object sender, Templates.EditModeArgs e)
        {
            //Significa que no desea calificar, salvar como no calificado
            //para que ya no vuelva aparecer el popup
            review.UserDeclined = true;
            await SaveReview();
        }

        #endregion

        #region Methods
        private void Init()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Evaluación");
            customNavBar.SetIconText("X");
            customNavBar.SetCarMode();
            customNavBar.NestedBackButtonTapped += CloseButton_Tapped;
            reviewRating.Value = 0.0;
            txtDetails.TextChanged += TxtDetails_TextChanged;

        }
        private async Task SaveReview()
        {
            if (IsBusy) return;
            IsBusy = true;
            //App.HUD.Show(Helpers.MessageStrings.PleaseWait, Interfases.MaskType.Black);
            bool isError = false;
            try
            {
                review.Rating = Math.Round(reviewRating.Value, 1);
                review.Review = string.IsNullOrEmpty(txtDetails.Text) ? null : txtDetails.Text;
                if (type == MessageSender.Asset)
                {
                    var reviewId = await App.apiService.AddAssetReview(review);
                }
                else
                {
                    var reviewId = await App.apiService.AddCustomerReview(review);
                }
                await Navigation.PopModalAsync(true);
            }
            catch
            {
                isError = true;
            }
            finally
            {
                //App.HUD.Dismiss();
                IsBusy = false;
            }

            if (isError) await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, 
                Helpers.MessageStrings.GeneralError, "Ok");
        }
        #endregion
    }
}