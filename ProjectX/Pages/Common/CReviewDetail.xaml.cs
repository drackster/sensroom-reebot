﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CReviewDetail : ContentPage
    {
        #region cTor
        public CReviewDetail(ReviewViewModel model, string name)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle(name);
            if (string.IsNullOrEmpty(model.Review))
            {
                txtReview.IsVisible = false;
            }
            BindingContext = model;
        }
        #endregion
    }
}