﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChatList : ContentPage
    {
        public ChatList()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Mensajes");
            //lstChats.ItemsSource = App.apiService.GetChatList();
        }
        private void lstChats_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var selectedItem = e.SelectedItem as MessageViewModel;
            if(selectedItem!=null)
                Navigation.PushAsync(new MessageList(new InvitationViewModel()), true);
            selectedItem = null;
        }
    }
}