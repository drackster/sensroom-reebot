﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Models;
using ProjectX.Helpers;
using Newtonsoft.Json;
using ProjectX.Pages;
namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CLogin2 : ContentPage
    {
        public event EventHandler<RegisterEventArgs> CompleteDone;
        private RegisterViewModel Model;
        public event EventHandler<RegisterEventArgs> LoginComplete;
        public ImageSource Shape { get; set; }
        private int psw = 0;
        bool Bversion;
        string version = "3";
        public CLogin2()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //customNavBar.SetIconText("X");
            if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
                customNavBar.IsVisible = false;
            txtUser.Unfocused += TxtUser_Unfocused;
            txtUser.Completed += TxtUser_Completed;
            //btnNext.IsVisible = false;
            //txtPassword.Unfocused += txtPassword_Unfocused;
            //txtPassword.Completed += txtPassword_Completed;

            //txtPassword.Keyboard = Keyboard.Numeric;
     
            App.MSender = MessageSender.None;
            App.NavBar = customNavBar;

            ValidaVersionApp(version);

        }
        protected override bool OnBackButtonPressed()
        {
            return true;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            UserViewModel user;
            if (!App.sqliteDBTablesInit)
            {
                await App.Database.InitializeTables();
                App.sqliteDBTablesInit = true;
            }

            if (!App.RegisterVM.IsActivated)
                App.RegisterVM = await App.Database.GetRegister();

            if (App.RegisterVM.IsActivated && !string.IsNullOrEmpty(App.RegisterVM.UserName))
            {
                txtUser.Text = App.RegisterVM.UserName;
                txtUser.IsVisible = false;
            }
            else
            {
                user = await App.Database.GetUser();
                if (user != null)
                {
                    txtUser.Text = user.UserName;
                    txtUser.IsVisible = false;
                }
                else
                    txtUser.IsVisible = true;
            }

            if (txtUser.Text == null || txtUser.Text == string.Empty)
            {
                txtUser.Focus();
            }
            else
            {
                txtPassword.Focus();
            }
        }

        public CLogin2(RegisterViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            //  customNavBar.SetIconText("X");
            //  customNavBar.SetTitle("Terminar");
            ValidaVersionApp(version);
            App.NavBar = customNavBar;
            txtUser.IsVisible = false;
            lblInstrucciones.IsVisible = true;
            //xBtn.IsVisible = false;
            // Shape = oBtn.Source;
            txtPassword.Placeholder = "Crea una contraseña";
            txtUser.Text = model.UserName;
            Model = model;
        }

        #region events
        private void TxtUser_Completed(object sender, EventArgs e)
        {
            ValidateUser();
        }
        private void TxtUser_Unfocused(object sender, FocusEventArgs e)
        {
            ValidateUser();
        }
        private void Shape_Tapped(object sender, EventArgs e)
        {
            Image img = (Image)sender;
            Shape = img.Source;
        }

        private async void Entrar_Cliked(object sender, EventArgs s)
        {//entrar

            if (Bversion || txtUser.Text.ToLower() == "prueba6" || txtUser.Text.ToLower() == "prueba7")
            {
                await DoLogin();
            }
            else
            {
                var leyenda = await App.apiService.GetLeyenda(1);
                await DisplayAlert(leyenda.leyenda.ToString(),"", "OK");
                App.Audio.Vibrate();
            }
            
        }

        private async void Type_Tapped(object sender, EventArgs e)
        {
            //Image img = (Image)sender;
            //if (Shape == null)
            //    return;
            //if (img.Source == null)
            //{
            //    psw++;
            //    img.Source = ImageSource.FromFile(((FileImageSource)Shape).File);
            //}
            //else
            //{
            //    img.Source = null;
            //    psw--;
            //}
            //if(psw >= 6 && xBtn.IsVisible == true)
            //    await DoLogin();
        }
        //private async void txtPassword_Completed(object sender, EventArgs e)
        //{
        //    await DoLogin();
        //}
        //private void txtPassword_Unfocused(object sender, FocusEventArgs e)
        //{
        //    ValidatePassword();
        //}
        private async void btnNext_Clicked(object sender, EventArgs e)
        {//54545454
            await DoLogin();
        }

        private void Restart_Clicked(object sender, EventArgs e)
        {
            //img1.Source =
            //img2.Source =
            //img3.Source =
            //img4.Source =
            //img5.Source =
            //img6.Source =
            //img7.Source =
            //img8.Source =
            //img9.Source = null;
            txtPassword.Text = string.Empty;
            psw = 0;
        }



        #endregion

        #region Methods
        public bool ValidateUser()
        {
            txtUser.Text = txtUser.Text == null ? "" : txtUser.Text.Trim();
            if (string.IsNullOrEmpty(txtUser.Text))
            {
                txtUserError.Text = Helpers.MessageStrings.InvalidField;
                App.Audio.Vibrate();
                return false;
            }
            txtUserError.Text = string.Empty;
            return true;

        }

        // -----X
        public bool ValidatePassword()
        {
            //if (img1.Source != null)
            //{
            //    txtPassword.Text = "1";
            //    if (((FileImageSource)img1.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img2.Source != null)
            //{
            //    txtPassword.Text += "2";
            //    if (((FileImageSource)img2.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img3.Source != null) { 
            //    txtPassword.Text += "3";
            //    if (((FileImageSource)img3.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img4.Source != null) { 
            //    txtPassword.Text += "4";
            //    if (((FileImageSource)img4.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img5.Source != null) { 
            //    txtPassword.Text += "5";
            //    if (((FileImageSource)img5.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img6.Source != null) { 
            //    txtPassword.Text += "6";
            //    if (((FileImageSource)img6.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img7.Source != null) { 
            //    txtPassword.Text += "7";
            //    if (((FileImageSource)img7.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img8.Source != null) { 
            //    txtPassword.Text += "8";
            //    if (((FileImageSource)img8.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            //if (img9.Source != null) { 
            //    txtPassword.Text += "9";
            //    if (((FileImageSource)img9.Source).File == "x.png")
            //    {
            //        txtPassword.Text = "";
            //        return false;
            //    }
            //}
            txtPassword.Text = txtPassword.Text == null ? "" : txtPassword.Text.Trim();
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                txtUserError.Text = Helpers.MessageStrings.InvalidField;
                App.Audio.Vibrate();
                return false;
            }
            txtUserError.Text = string.Empty;
            return true;
        }
        public async Task DoLogin()
        {
            if (Model == null)
                if (IsBusy || !ValidatePassword() || !ValidateUser())
                {
                    return;
                }
            IsBusy = true;

            try
            {
                App.HUD.Show(Helpers.MessageStrings.PleaseWait);
                if (Model != null)
                {
                    ValidatePassword();
                    await RegisterAccount();
                    return;
                }
                RegisterViewModel model = new RegisterViewModel()
                {
                    UserName = txtUser.Text,
                    Password = txtPassword.Text
                };
                model.DeviceToken = await App.Database.GetDeviceToken();
                model = await App.apiService.Login(model);
                if (model.IsActivated)
                {
                    await App.Database.SaveRegister(model);
                    App.apiService.UserName = model.UserName;
                    App.apiService.UserPassword = model.Password;
                    App.RegisterVM = model;
                    App.MSender = model.Sender;

                    if (App.MSender == MessageSender.Customer)
                    {

                        await App.Database.SaveUser(model.UserVM);
                        await App.Database.SaveSearchModel(model.UserVM.SearchFilters);
                        var credito = await App.apiService.GetCustomCredit(model.UserId);// revisamos si el cliente tiene crédito disponible
                        await App.Database.DeleteCustomerCredit();
                        await App.Database.SaveCustomerCredit(credito);
                    }
                    else if (App.MSender == MessageSender.Asset)
                    {
                        await App.Database.SaveAsset(model.AssetVM);
                        await App.Database.SaveAssetImages(model.AssetVM.Images);
                        await App.Database.SaveCalendar(model.AssetVM.Calendars);
                    }
                    //await Navigation.PopModalAsync(true);
                    if (LoginComplete != null)
                        //await Navigation.PopModalAsync(true);
                        LoginComplete.Invoke(this, new RegisterEventArgs(model));

                    var json = JsonConvert.SerializeObject(model);
                }
                else
                {
                    txtUserError.Text = "Usuario o contraseña inválidas";
                    App.Audio.Vibrate();
                }

            }
            catch (Exception ex)
            {
                App.Audio.Vibrate();
                await App.Database.LogException(ex);
                await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle,
                    Helpers.MessageStrings.GeneralError, "Ok");
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
        }

        #endregion

        private void NoCredentials_Tapped(object sender, EventArgs e)
        {
            this.Navigation.PopModalAsync(true);
        }

        private async Task RegisterAccount()
        {
            //if (IsBusy) return;
            //IsBusy = true;
            try
            {
                txtPassword.Text = txtPassword.Text == null ? "" : txtPassword.Text.Trim();

                if (txtPassword.Text.Length < 6)
                {
                    App.Audio.Vibrate();
                    txtUserError.Text = "Tu contraseña debe tener almenos 6 caracteres";
                    return;
                }
                else
                {
                    App.HUD.Show(MessageStrings.PleaseWait);
                    Model.Password = txtPassword.Text;
                    var json = JsonConvert.SerializeObject(Model);
                    Model = await App.apiService.Register(Model);

                    if (Model.IsActivated)
                    {
                        try
                        { await App.Database.SaveRegister(Model); }
                        catch (Exception ex)
                        { }

                        if (App.MSender == MessageSender.Customer)
                        {
                            await App.Database.SaveUser(Model.UserVM);
                            await App.Database.SaveSearchModel(Model.UserVM.SearchFilters);
                        }
                        else if (App.MSender == MessageSender.Asset)
                        {
                            await App.Database.SaveAsset(Model.AssetVM);
                            await App.Database.SaveAssetImages(Model.AssetVM.Images);
                            await App.Database.SaveCalendar(Model.AssetVM.Calendars);
                        }
                        App.apiService.UserName = Model.UserName;
                        App.apiService.UserPassword = Model.Password;
                        App.RootStep = 99;
                        if (CompleteDone != null)
                            CompleteDone.Invoke(this, new RegisterEventArgs(Model));
                    }
                    else
                    {
                        App.Audio.Vibrate();
                        txtUserError.Text = "Sucedió un error al generar la cuenta.";
                    }

                }
            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex);
                await DisplayAlert(MessageStrings.GeneralErrorTitle,
                    MessageStrings.GeneralError, "Ok");
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
        }      

        private async  void ValidaVersionApp(string version)
        {
          bool ver = await App.apiService.GetVersion(version);

            if (ver)
            {
                Bversion = true;               
            }
              
            else
            {               
                Bversion = false;
            }
        }

        public interface IAndroidMethods
        {
            void CloseApp();
        }

       
       
    }
}
    
