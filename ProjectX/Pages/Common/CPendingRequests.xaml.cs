﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CPendingRequests : ContentPage
    {
        public CPendingRequests()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Pendientes");
            customNavBar.SetHamburguerMode();
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            lstPending.ItemsSource = await App.apiService.GetActivationRequest();
        }
        private async void lstPending_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            var selectedItem = e.SelectedItem as RegisterViewModel;
            if (selectedItem != null)
            {
                var response = await DisplayAlert("Confirmación", "¿Autorizar " + selectedItem.UserName + "?", "Sí", "No");
                if (response)
                {
                    selectedItem.ActivationStatus = 4;//Autorizado
                }
                else
                {
                    selectedItem.ActivationStatus = 5;//Rechazado
                }
                await App.apiService.UpdateRequestStatus(selectedItem);

                lstPending.ItemsSource = null;
                lstPending.ItemsSource = await App.apiService.GetActivationRequest();

            }

            IsBusy = false;
        }
    }
}