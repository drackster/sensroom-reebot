﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Helpers;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CReviews : ContentPage
    {
        #region Properties
        private bool IsLoaded;
        private long UserId;
        private string userName;
        #endregion
        public CReviews(long UserId, string UserName)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Evaluaciones");
            this.UserId = UserId;
            userName = UserName;
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            if (IsBusy) return;

            if (!IsLoaded) //si no ha cargado traer todos 
            {
                IsBusy = true;
                App.HUD.Show(Helpers.MessageStrings.LoadingMsg, Interfases.MaskType.Black);
                bool IsError = false;
                try
                {
                    if(App.MSender == MessageSender.Customer)
                    {
                        lstReviews.ItemsSource = await App.apiService.GetAssetReviews(UserId);
                    }
                    else
                    {
                        lstReviews.ItemsSource = await App.apiService.GetCustomerReviews(UserId);
                    }

                    IsLoaded = true;
                }
                catch
                {
                    IsError = true;
                }
                finally
                {
                    App.HUD.Dismiss();
                    IsBusy = false;
                }

                if (IsError)
                {
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                }
            }
        }
        private void lstReviews_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            ReviewViewModel item = e.SelectedItem as ReviewViewModel;
            if (item != null && item.ShowDetail)
            {
                Navigation.PushAsync(new CReviewDetail(item, userName), true);
            }
            lstReviews.SelectedItem = null;
            IsBusy = false;
        }
    }
}