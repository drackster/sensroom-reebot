﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Helpers;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using ProjectX.Interfases;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CPermissions : ContentPage
    {
        #region Properties
        private INotificationHelper notificationHelper;
        private string DeviceToken;
        private bool location;
        private bool camera;
        public bool IsLoaded;
        //public bool PermissionsOk { get; set; }
        #endregion
        public CPermissions()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            DeviceToken = null;

            if (App.MSender == MessageSender.Customer)
                A1.IsVisible = A2.IsVisible = A3.IsVisible = A4.IsVisible =
                    check1.IsVisible = check3.IsVisible = false;
        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();
            bool isValid = false;
            if (!IsLoaded)
            {
                isValid = await Init();
                IsLoaded = true;
            }
            if(isValid || Validate())
            {
                App.RegisterVM.Permissions = true;
                await Navigation.PopModalAsync(true);
            }
        }
        public async Task<bool> Init()
        {
            var isValid = true;
            //El permiso de ubicacion es para ambos
            location = await CheckPermission(Permission.Location);
            if (location)
                check1.Source = ImageSource.FromFile("checkpermiso.png");
            else isValid = false;

            if (App.MSender == MessageSender.Asset)
            {
                camera = await CheckPermission(Permission.Photos);
                if (camera)
                    check2.Source = ImageSource.FromFile("checkpermiso.png");
                else isValid = false;
            }
            return isValid;
        }
        private async Task<bool> CheckPermission(Permission permission, string permitName)
        {
            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
                if (status != PermissionStatus.Granted)
                {
                    if (Device.RuntimePlatform == Device.Android && await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(permission))
                    {
                        await DisplayAlert(MessageStrings.GeneralErrorTitle, "Es necesario el permiso de " + permitName + " para continuar.", "OK");
                    }
                    var results = await CrossPermissions.Current.RequestPermissionsAsync(permission);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(permission))
                    {
                        status = results[permission];
                        return status == PermissionStatus.Granted ? true : false;
                    }
                    return false;
                }

                else if (status == PermissionStatus.Granted)
                {
                    return true;
                }
                //return false here;
                //else if (status != PermissionStatus.Unknown)
                //{
                //    await DisplayAlert("Location Denied", "Can not continue, try again.", "OK");
                //}
            }
            catch (Exception ex)
            {
                //var response = await DisplayAlert(MessageStrings.GeneralErrorTitle, "No se pudo obtener el permiso de ubicación. ¿Reintentar?","Sí","No");
                await App.Database.LogException(ex, permitName + " Permission");
            }
            return false;
        }
        public async Task<bool> CheckPermission(Permission permission)
        {
            var status = await CrossPermissions.Current.CheckPermissionStatusAsync(permission);
            return status == PermissionStatus.Granted;
        }
        private void CheckPushPermission()
        {
            if (notificationHelper == null)
                notificationHelper = DependencyService.Get<INotificationHelper>();
            else
                notificationHelper.DeviceTokenChanged -= NotificationHelper_DeviceTokenChanged;

            notificationHelper.DeviceTokenChanged += NotificationHelper_DeviceTokenChanged;
            notificationHelper.StartAuthorization();
        }
        private async void NotificationHelper_DeviceTokenChanged(object sender, DeviceTokenEventArgs e)
        {
            //test
            //if(e.ErrorMsg != null)
            //    e = new DeviceTokenEventArgs("TEST", null);
            
            if(e.ErrorMsg != null)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, e.ErrorMsg, "Ok");
            } 
            else if(!string.IsNullOrEmpty(e.DeviceToken))
            {
                DeviceToken = e.DeviceToken;
                //await DisplayAlert("Device Token", DeviceToken, "Ok");

                var model = new DeviceTokenViewModel
                {
                    DeviceId = Device.RuntimePlatform == Device.Android ? DeviceType.Google : DeviceType.Apple,
                    Token = DeviceToken
                };
                var dtId = await App.apiService.AddDeviceToken(model);
                if(dtId>0)
                {
                    model.Id = dtId;
                    await App.Database.SaveDeviceToken(model);
                }

            }
        }
        private async void btnStart_Clicked(object sender, EventArgs e)
        {
#if RELEASE
            do
            {
                if (!location)
                    location = await CheckPermission(Permission.Location, "ubicación");
                if(location)
                    check1.Source = ImageSource.FromFile("checkpermiso.png");

                if (App.MSender == MessageSender.Asset)
                {
                    if (!camera)
                        camera = await CheckPermission(Permission.Photos, "acceso a fotos");
                    if(camera)
                        check2.Source = ImageSource.FromFile("checkpermiso.png");


                    if (Device.RuntimePlatform != Device.Android &&
                        string.IsNullOrEmpty(DeviceToken) && 
                        notificationHelper == null)
                        CheckPushPermission();
                }

                if (Validate()) break;
                await System.Threading.Tasks.Task.Delay(1000);

            } while (true); //(!location && !camera);// && string.IsNullOrEmpty(DeviceToken));
#else
            App.RegisterVM.Permissions = true;
#endif
            await this.Navigation.PopModalAsync(true);
        }
        private bool Validate()
        {
            if ((location && camera && !string.IsNullOrEmpty(DeviceToken) 
                                && App.MSender == MessageSender.Asset) ||
                (location && App.MSender == MessageSender.Customer))
            {
                App.RegisterVM.Permissions = true;
                return true;
            }
            return false;
        }
    }
}