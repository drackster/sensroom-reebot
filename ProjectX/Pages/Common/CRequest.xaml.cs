﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Helpers;
using ProjectX.Models;
using ProjectX.ViewModels;
using ProjectX.Pages.Customer;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CRequest : ContentPage
    {

      

        #region Members
        public event EventHandler<RegisterEventArgs> RequestCompleted;
        public RegisterViewModel Model;
        private string hostCode;
        private string userName;
        private CComplete ccomplete;
        private CLogin2 generatePass;
        //private bool IsRequestValid;
        //private bool IsLoaded;
        #endregion

        #region cTor
        public CRequest()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Registro");
            customNavBar.SetIconText("X");
            customNavBar.SetReturningPage(false);
            customNavBar.NestedBackButtonTapped += CustomNavBar_NestedBackButtonTapped;

            this.Model = new RegisterViewModel()
            { Sender = MessageSender.Customer }; //DEFAULT

            txtCodeEntry.Completed += CodeEntry_Completed;
            txtCodeEntry.Unfocused += CodeEntry_Unfocused;

            txtUserName.Completed += UserNameEntry_Completed;
            txtUserName.Unfocused += UserNameEntry_Unfocused;

            App.NavBar = customNavBar;
            //SetFields();

            //Defaults Segments
            SegControl.SelectedSegment = 1;
            App.MSender = Model.Sender = MessageSender.Customer;
            //SegmentText.Text = Helpers.MessageStrings.UserTypeCustomer;
            txtCodeEntry.Placeholder = "ejemplo: Alberto82";
            txtUserName.Placeholder = "Juan33";
            SegControl.ValueChanged += SegControl_ValueChanged;
            txtUserName.IsEnabled = false;//default;
            lbEnviar.Text = "Enviar: Es el tipo de usuarios para buscar personas e invitarlas a eventos.";
            lbrecibir.Text = "Recibir: Usuarios para recibir invitaciones a eventos y cobrar por ello";
              
        }

        private void CustomNavBar_NestedBackButtonTapped(object sender, Templates.EditModeArgs e)
        {
            App.RootStep = 0;
            var temp = (RootPage)this.Parent;
              //if (((RootPage)this.Parent).SelectedTapped != null)
              //  SelectedTapped.Invoke(this, new EventArgs());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //Checar status siempre
            //await CheckRequestStatus();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //stopTimer();
        }

        #endregion

        #region Events
        private void SegControl_ValueChanged(object sender, EventArgs e)
        {
            if (SegControl.SelectedSegment == 1)
            {
                //SegmentText.Text = Helpers.MessageStrings.UserTypeCustomer;
                Model.Sender = App.MSender = MessageSender.Customer;
                txtCodeEntry.Placeholder = "ejemplo: Alberto82";
                txtUserName.Placeholder = "Juan33";
            }
            else
            {
                //SegmentText.Text = Helpers.MessageStrings.UserTypeAsset;
                Model.Sender = App.MSender = MessageSender.Asset;
                txtCodeEntry.Placeholder = "ejemplo: Melanie92";
                txtUserName.Placeholder = "Dulce390";
            }
            Reset();

        }
        private async void CodeEntry_Unfocused(object sender, FocusEventArgs e)
        {
            await ValidateCode();
        }
        private async void CodeEntry_Completed(object sender, EventArgs e)
        {
            await ValidateCode();
        }
        private async void UserNameEntry_Completed(object sender, EventArgs e)
        {
            await ValidateUserName();
        }
        private async void UserNameEntry_Unfocused(object sender, FocusEventArgs e)
        {
            await ValidateUserName();
        }
        private async void btnActivate_Clicked(object sender, EventArgs e)
        {
            var result = await DisplayAlert("Términos y Condiciones de Uso", "LUXX ('Nosotros' o el 'Sitio' o 'LUXX' o 'Plataforma') marca propiedad de la empresa  Notbank Tecnologías, S.A. de C.V., ofrece un medio interactivo digital para que los usuarios  del Sitio (los 'Usuarios') se comuniquen, se conozcan y puedan ampliar su red de conocidos  y/o prospectos. LUXX es simplemente una red social que permite que las personas se puedan  conocer. No fomentamos ni apoyamos ningún comportamiento ilegal en ninguna jurisdicción  en la que esta red social está habilitada. Los términos y condiciones establecidos a continuación  constituyen un acuerdo (los 'Términos') que rigen el uso del Sitio. Al utilizar o acceder al  Sitio, Usted ('Usted') acepta regirse por (1) los Términos, (2) la política de privacidad de  LUXX, que encontrará en este documento como 'Política de privacidad' y (3) las leyes y  normativas aplicables. Lea atentamente estos Términos antes de utilizar la Plataforma; si no  está de acuerdo con los ellos o con la Política de Privacidad, no acceda a ella ni la use, así como  tampoco la información que éste contiene.  Nos reservamos el derecho, a nuestra entera discreción, de modificar estos Términos en  cualquier momento. Las versiones actualizadas dejan sin efecto y reemplazan a todas las  versiones anteriores al ser publicadas, y la versión anterior ya no tendrá validez, a menos que  especifiquemos lo contrario. Indicaremos la última fecha de actualización al final de estos  Términos. Usted acepta tener en cuenta la última fecha de actualización de la revisión que lea  y acepta revisar periódicamente estos términos para ver si se han introducidos cambios. Si se  ha modificado la última fecha de actualización, sabrá que hemos introducido cambios en los  Términos y que debe revisarlos para determinar la manera en que han cambiado y como afecta  a los Usuarios y sus responsabilidades. El uso de la Plataforma después de la publicación de  cambios a estos Términos indicará que usted acepta esos cambios. También se le exigirá que  acepte estos Términos y cualquier modificación relevante de ellos de manera afirmativa.  Usted no puede usar la Plataforma si no tiene al menos 18 años y/o la mayoría de edad o edad  de consentimiento legal en la jurisdicción en la que vive o reside y la capacidad de derecho para  obligarse en función de estos Términos. Tampoco puede usar la Plataforma si lo han  condenado alguna vez por un delito sexual, o si tiene actualmente una acusación por algún  delito de naturaleza sexual.  Este acuerdo legal se celebra entre Usted y Nosotros. Al acceder a la Plataforma, está  aceptando estos Términos y acepta regirse por ellos, por las condiciones y por los avisos que  ellos contengan o a los que ellos hagan referencia. Si no está de acuerdo con estos Términos,  no use la Plataforma y no ingrese a él.  1. CONDUCTA DEL USUARIO  Al utilizar la Plataforma, Usted acepta actuar responsablemente y demostrando que tiene buen  juicio. Por ejemplo, usted acepta no realizar lo siguiente:  a. Violar leyes o reglamentaciones vigentes; o infringir los derechos de terceros,  b. Utilizar la información disponible a través de nuestro Sitio para fines ilícitos;  c. Interferir con nuestro el Sitio o dañarlo mediante el uso, entre otras cosas, de virus,  etc.,  d. 'Perseguir' o acosar a otros usuarios de LUXX;    e. Publicar, cargar o transmitir información o contenido falso, engañoso o ilegal en el  Sitio; así como realizar declaraciones falsas, intentar usar o usar información personal,  financiera o de otro tipo que no tenga autorización para usar;  Recopilar o almacenar datos personales acerca de los Usuarios sin su consentimiento.  2. SUS INTERACCIONES CON OTROS USUARIOS DE LA PLATAFORMA  Usted es el único responsable de la manera como se relaciona con otros Usuarios de LUXX,  incluso del contenido, los materiales, la información y mensajes que publica en el Sitio o envía  a otros Usuarios. Así mismo, Usted, acepta tomar precauciones en todas las comunicaciones  con otros Usuarios de LUXX y mayor precaución si, en especial, decide encontrarse con algún  otro Usuario en persona.  Acepta que Nosotros no seremos responsables de daños que resulten del uso de la Plataforma  o de su comunicación o encuentros con otros Usuarios. Si bien Nosotros hacemos lo posible  por filtrar a los Usuarios, Usted entiende y reconoce que no pre-aprobamos a ninguno de los  Usuarios ni investigamos sus antecedentes, como tampoco intentamos verificar sus  declaraciones. No realizamos verificaciones de antecedentes penales ni de otro tipo a nuestros  Usuarios. En ningún caso, seremos responsables de daños de cualquier tipo, sean directos,  indirectos, generales, especiales, compensatorios, emergentes, punitorios o incidentales que se  produzcan como consecuencia o en relación con su conducta o la de cualquier otra persona  por el uso de la Plataforma, lo que incluye, entre otros, lesiones o daños corporales, violencia  física o verbal, engaños, fraudes, perturbación emocional u otros daños que ocurran como  resultado del encuentro entre Usuarios de LUXX.  No somos responsables del uso que hagan otras personas de la información que Usted les  proporcione ni tampoco podemos controlarlo, por lo que debería ser cauteloso al seleccionar  la información personal que proporciona a otros a través de la Plataforma.  Usted es el único responsable de sus encuentros con otros Usuarios. Acepta eximir de  responsabilidad, tanto a LUXX, así como a nuestros accionistas, ejecutivos, directores,  asesores, consultores, empleados, etc., sobre todo reclamo, demanda, daño, pérdida o  responsabilidad de cualquier tipo -sea conocida o desconocida, directa y contingente, divulgada  o no- que surja de cualquier controversia al usar el Sitio o esté vinculada con el uso que de él  de Usted.  3. MENORES  LUXX está dirigido únicamente para personas mayores de 18 años o que hayan alcanzado la  mayoría de edad y la edad de consentimiento legal en la jurisdicción donde residen. No  permitiremos, si tenemos conocimiento de ello, que las personas que no cumplan con estos  criterios utilicen la Plataforma. Al utilizar el Sitio, usted declara y garantiza que tiene al menos  18 años y que tiene el derecho, la autoridad y la capacidad de aceptar estos Términos y de  atenerse a ellos. También declara y garantiza que no permitirá que ningún menor acceda a esta  Plataforma.  También acepta que Nosotros no seremos responsables de ningún malentendido respecto de la  edad de un Usuario, o del uso no autorizado de este Sitio por parte de un menor.    4. EL SERVICIO LUXX: REGISTRO, USO Y CANCELACIÓN  Los Usuarios de LUXX podrán ingresar a la Plataforma exclusivamente al ser admitidos por  un Usuario previamente registrado. Los Usuarios del LUXX deben, en algunos casos, crearse  un perfil para poder acceder a este.  Algunos Usuarios de LUXX podrán poner a disposición de otros Usuarios, o solicitar de ellos,  una entrevista en persona. Al ser Usuario acepta que el encuentro en persona entre Usuarios de  la Plataforma tendrá exclusivamente fines sociales, de entretenimiento, profesionales o de  negocios, por ningún motivo la entrevista entre Usuarios del Sitio podrá tener algún fin ilícito  o fuera de la Ley.  Los Usuarios que así lo deseen, podrán solicitar un honorario como contraprestación por el  tiempo invertido en la entrevista solicitada por otro Usuario. La contraprestación será pagada  por el Usuario que solicita la entrevista al Usuario con el que desea reunirse para fines sociales,  de entretenimiento, profesionales o de negocios. El Usuario que recibe la invitación de otro  Usuario a una entrevista o encuentro en persona, no está obligado a aceptarlo, y si acepta la  invitación, el Usuario que recibe la contraprestación no asume ninguna responsabilidad ni  obligación adicional que la asistencia durante el tiempo acordado a la entrevista en persona  para los fines establecidos previamente en su cita.  LUXX cobrará una comisión al Usuario que solicite una entrevista en persona con algún otro  Usuario del Sitio. La comisión de LUXX será un 20% de la contraprestación solicitada por el  Usuario que recibe la invitación y servirá como cuota publicitaria con la que LUXX puede  promover a los Usuarios entre sí, y el cobro de dicha comisión será efectuado con anterioridad  a cualquier notificación a otros Usuarios. El servicio otorgado por LUXX a los Usuarios será el  de facilitar la comunicación entre ellos y poderlos presentar exclusivamente para fines sociales,  de entretenimiento, profesionales o de negocios y por ningún motivo el encuentro entre  Usuarios podrá tener algún fin ilícito o fuera de la Ley. En caso de que la invitación a un  encuentro en persona sea rechazada, cualquier monto pagado a LUXX podrá ser usado  exclusivamente para hacer otra invitación, y no podrá por ningún motivo ser reembolsada.  Nos reservamos el derecho de modificar nuestra política de precios o la forma en que  cobramos por nuestro Servicio en cualquier momento y a nuestra entera discreción, sin más  aviso.  Si el Usuario no inicia sesión en la cuenta por 90 días, nos reservamos el derecho de cancelar el  acceso a la plataforma. Así mismo, Usted reconoce y acepta que, a nuestra entera discreción,  podremos cancelar su acceso a la Plataforma por cualquier motivo, incluso, entre otros, por  incumplir o violar estos Términos. Usted entiende que no estamos obligados a revelar las  razones por las cuales cancelamos el acceso su acceso a la Plataforma, incluso podríamos  prohibirlo completamente. Reconoce y acepta que la cancelación de su acceso a la plataforma  podrá realizarse sin notificación previa, y también acepta y reconoce que podremos desactivar  o eliminar inmediatamente cualquiera de sus cuentas, así como toda información relacionada y  archivos en tales cuentas, o prohibir el acceso a dichos archivos. Además, reconoce y acepta  que no seremos responsables ante Usted ni terceros por costos o daños de ningún tipo que  resulten de la cancelación de su acceso a la Plataforma.    Usted podrá cancelar su acceso a la Plataforma en cualquier momento a través de nuestro Sitio.  También podrá seleccionar la opción 'Eliminación completa del perfil', que se ofrece por  separado de la cancelación básica. Esta característica eliminará cualquier archivo en la  Plataforma y ya no se podrá recuperar.  El uso de la Plataforma también está regido por nuestra Política de Privacidad. Usted acepta  que, al registrar un Perfil o utilizar nuestro la Plataforma, estará aceptando nuestra Declaración  de Privacidad. Usted reconoce que, si bien hacemos todo lo posible por mantener la seguridad  necesaria para proteger sus datos personales, no podemos garantizar la seguridad o la  privacidad de la información que usted proporciona a través de Internet o de sus mensajes de  correo electrónico. Nuestra Política de Privacidad queda incorporada a los Términos mediante  esta referencia. Acepta eximirnos de responsabilidad, tanto a nosotros como a nuestra casa  matriz, nuestras subsidiarias y entidades afiliadas, así como a nuestros accionistas, ejecutivos,  directores, empleados y agentes, sucesores y cesionarios sobre todo reclamo, demanda, daño,  pérdida o responsabilidad de cualquier tipo –sea conocida o desconocida, directa y  contingente, divulgada o no– que surja de la divulgación o uso de dicha información por parte  de terceros.  5. CONTENIDO DEL USUARIO  A. Al enviar contenidos (como, entre otros, su(s) fotografías o información de su perfil  o de otro tipo) a nuestra Plataforma, Usted declara y nos garantiza que tal contenido, incluso  su fotografía, es publicado por Usted y que Usted es el autor exclusivo del contenido, incluso  su fotografía, y que el uso que hagamos nosotros de su contenido no infringirá ni violará  propiedad intelectual ni otros derechos de terceros. Renuncia absolutamente a todos los  derechos morales de ser identificado como autor del contenido, incluso su fotografía y perfil, y  derechos similares en cualquier jurisdicción del mundo. Al enviar contenidos (incluso, entre  otros, su fotografía y perfil) a nuestra Plataforma, usted automáticamente otorga, y declara y  garantiza que tiene derecho a otorgar, un derecho ilimitado, perpetuo, mundial, no exclusivo,  libre de regalías, irrevocable y transferible.  No tenemos ninguna obligación de eliminar su perfil, fotografía e información de nuestra  Plataforma, incluso después de haber cancelado su cuenta, a menos que utilice la opción  Eliminación completa del perfil. Usted declara y nos garantiza que tiene derecho absoluto de  otorgar la licencia y los otros derechos antes mencionados.  También entiende y acepta que no podemos garantizar la precisión de la información provista  por otros Usuarios de la Plataforma, y que no asumimos ningún compromiso de verificarla.  Todos los contenidos de LUXX se proporcionan tal como los recibimos. Los contenidos son  responsabilidad de la persona que los originó. Usted entiende y acepta que, bajo ninguna  circunstancia, seremos nosotros responsables por cualquier pérdida, responsabilidad o daño en  el que incurra como resultado de su uso del contenido que está en nuestro Sitio.  6. USO NO COMERCIAL  Usted comprende y acepta que esta Plataforma son solo para uso personal y no podrán  emplearse para fines comerciales. Las organizaciones, compañías o empresas independientes    no podrán utilizar la Plataforma para ningún fin. Los usos ilegales o no autorizados de nuestra  Plataforma, que incluyen prostitución, fraude financiero, recopilación de nombres de usuario o  direcciones de correo electrónico de usuarios por medios electrónicos o de otro tipo podrán  ser investigados, y se tomarán medidas legales adecuadas en cumplimiento de las leyes  aplicables.    7. NOMBRE DE USUARIO Y CONTRASEÑA  Al completar el proceso de registro, Usted deberá seleccionar un nombre de usuario y una  contraseña. Acepta no elegir un nombre que indique que Usted es otra persona o que viole de  otra forma los derechos de un tercero. Podremos rehusarnos a otorgarle un nombre de  usuario/apodo que sirva para hacerse pasar por otro individuo, que esté protegido por una  marca comercial o leyes sobre propiedad o que sea vulgar u ofensivo, según lo determinemos a  nuestra entera discreción. Su nombre de usuario y contraseña no son transferibles y no pueden  cederse. Usted es el único responsable de mantener la confidencialidad de su nombre de  usuario y contraseña, y el único responsable de las actividades que se realicen bajo su nombre  de usuario y contraseña.  8. INTERRUPCIÓN DEL SERVICIO  Nos reservamos el derecho de modificar o interrumpir, sea temporal o permanentemente,  cualquier uso o servicio de la Plataforma, con o sin notificación previa. Usted reconoce y  acepta que no seremos responsables ante Usted o ante terceros por las modificaciones o  interrupciones en la operación de la Plataforma.  9. BLOQUEO DE LA DIRECCIÓN DE IP  Para proteger la integridad de la Plataforma, nos reservamos el derecho de bloquear Usuarios  de ciertas direcciones de IP, en cualquier momento y a nuestra entera discreción, para impedir  que accedan al Sitio.  10. DECLARACIONES Y GARANTÍAS  Por el presente Usted declara y nos garantiza lo siguiente: (a) que tiene plenas facultades y  autoridad para celebrar este acuerdo y observar estos Términos; (b) que el uso de la Plataforma  no infringirá ni violará derechos de autor, marca comercial, derecho de publicidad ni ningún  otro derecho legal de terceros; (c) que cumplirá con todas las leyes y reglamentaciones  aplicables al utilizar la Plataforma y al participar en las actividades relacionadas con estos  Términos, que incluyen, entre otras, contactar a otros Usuarios de LUXX; (d) que nunca ha  sido condenado por un delito sexual y que no tiene cargos de este tipo pendientes en su contra;  y (e) que Usted es propietario del contenido que envía, o que tiene todos los derechos  necesarios para otorgar su licencia, y que la publicación o uso de su contenido que nosotros  realicemos no constituirá una infracción o violación de los derechos de terceros.  11. DESCARGO DE RESPONSABILIDAD DE GARANTÍAS USTED ACEPTA  QUE:    A. SI USA LA PLATAFORMA, LO HARÁ BAJO SU PROPIO RIESGO. LA  PLATAFORMA SE OFRECE TAL Y COMO ESTÁ Y SEGÚN DISPONIBILIDAD.  RECHAZAMOS EXPRESAMENTE TODO TIPO DE GARANTÍAS, SEAN EXPRESAS  O IMPLÍCITAS, INCLUSO, ENTRE OTRAS, LAS GARANTÍAS IMPLÍCITAS DE  COMERCIABILIDAD E IDONEIDAD PARA UN FIN ESPECÍFICO, DE TÍTULO Y  DE NO VIOLACIÓN.  B. NO GARANTIZAMOS LO SIGUIENTE: (A) QUE EL USO DE LA  PLATAFORMA CUMPLIRÁ CON SUS REQUISITOS; (B) QUE SU USO NO SUFRIRÁ  INTERRUPCIONES, SERÁ OPORTUNO, SEGURO Y ESTARÁ LIBRE DE ERRORES;  (C) QUE LA INFORMACIÓN QUE OBTENGA EN ELLA SERÁ PRECISA O  CONFIABLE; (D) QUE LA CALIDAD O FIABILIDAD DEL SERVICIO OFRECIDO  POR MEDIO DE LA PLATAFORMA, LOS DATOS U OTROS MATERIALES QUE  USTED COMPRE U OBTENGA A TRAVÉS DE ELLA ESTARÁ A LA ALTURA DE  SUS EXPECTATIVAS; (E) QUE LA INFORMACIÓN QUE USTED PROPORCIONE O  QUE NOSOTROS RECOPILEMOS NO SERÁ DIVULGADA A TERCEROS; (F) QUE  LOS PERFILES DEL SITIO SON PRECISOS, ACTUALIZADOS O AUTÉNTICOS; (G)  QUE LOS MATERIALES O ARCHIVOS QUE DESCARGUE DE INTERNET  ESTARÁN LIBRES DE VIRUS, GUSANOS, TROYANOS U OTROS CÓDIGOS QUE  PUEDAN PRODUCIR DAÑOS; (H) QUE OTRAS PERSONAS NO UTILIZARÁN SU  INFORMACIÓN CONFIDENCIAL DE MANERA NO AUTORIZADA; O (I) QUE LOS  ERRORES EN DATOS O SOFTWARE SERÁN CORREGIDOS.  C. SI USTED ACCEDE A CONTENIDOS O LOS TRANSMITE A TRAVÉS DEL  USO DE LA PLATAFORMA, LO HACE BAJO SU PROPIO RIESGO Y A SU ENTERA  DISCRECIÓN. USTED ES EL ÚNICO RESPONSABLE POR PÉRDIDAS O DAÑOS  QUE SUFRA EN RELACIÓN CON DICHAS ACCIONES. NOSOTROS NO SOMOS  RESPONSABLES DE NINGÚN CONTENIDO INCORRECTO O IMPRECISO  PUBLICADO EN NUESTRO SITIO O QUE SE RELACIONE CON EL SERVICIO,  HAYA SIDO DESARROLLADO POR LOS USUARIOS DE NUESTRO SITIO O POR  LOS PROGRAMADORES O EL EQUIPO ASOCIADOS CON LA PLAFAROMA O  UTILIZADO POR ESTE. NO SOMOS RESPONSABLES DE LA CONDUCTA, NI EN  LÍNEA NI FUERA DE LÍNEA, DE LOS USUARIOS DE NUESTRO SITIO O DE LA  PLATAFORMA.  12. INDEMNIZACIÓN.  Usted acepta defendernos, indemnizarnos y eximirnos de responsabilidad –tanto a Nosotros  como a nuestras subsidiarias, casas matrices, afiliadas, y a cada uno de sus directores,  ejecutivos, gerentes, miembros, socios, agentes, representantes, empleados, consejeros y  clientes (cada uno de ellos un 'Indemnizado', y en conjunto, los 'Indemnizados')– por  cualquier reclamo, demanda, acción, daño, pérdida, costo o gasto, incluso, entre otros,  honorarios y costos de abogados, costos de investigación y gastos de liquidación en los que  incurramos con relación a investigaciones, reclamos, acciones, demandas o procedimientos de  cualquier tipo entablados en contra de cualquiera de los Indemnizados y que surjan del uso de  la Plataforma.  13. ARBITRAJE Y RENUNCIA A INTERPONER UNA ACCIÓN LEGAL    Cualquier disputa que surja de o esté relacionada con estos Términos puede ser resuelta  mediante arbitraje vinculante de conformidad con los términos de esta cláusula de arbitraje, ya  sea suya o de la elección de LUXX , a menos que esté prohibido por la ley.  El término disputa (la 'Disputa'), significa cualquier disputa, reclamación o controversia  respecto a cualquier aspecto del uso de LUXX que se hayan acumulado o pudieran acumularse  en adelante, ya sea basado en contrato, estatuto, reglamento, agravio (incluyendo sin limitación  las reclamaciones derivadas de o en relación con falsedad o negligencia), e incluye a la validez y  aplicabilidad de esta disposición de arbitraje (con la excepción de la aplicabilidad de la sección  (b) de la cláusula de restricciones previstas a continuación).  Todas las desavenencias que deriven de estos Términos deberán ser resueltas definitivamente  de acuerdo con las Reglas de Arbitraje del Centro de Arbitraje de México (CAM), por uno o  más árbitros nombrados conforme a dichas Reglas.  14. LEY APLICABLE EN LA INTEPRETACIÓN DE LOS TÉRMINOS.  Este Sitio (excluyendo aquellos que se encuentren relacionados) es operado por LUXX, en sus  oficinas del estado de Nuevo León, México. Para cualquier cuestión de interpretación relativa a  estos Términos, las partes se someten al Código de Comercio y como ley supletoria el Código  Civil del Estado de Nuevo León, renunciando expresamente a cualquier otro fuero que por  razón del domicilio presente o futuro pudiera corresponderle.    AVISO DE PRIVACIDAD PARA LA PROTECCIÓN DE DATOS PERSONALES.  En términos de lo previsto en la Ley Federal de Protección de Datos Personales en Posesión  de los Particulares (en lo sucesivo denominada como la 'Ley de Datos'), LUXX, establece el  presente Aviso de Privacidad de conformidad con los siguiente Términos.  1. El presente Aviso de Privacidad tiene por objeto la protección de los datos personales de los  Usuarios de LUXX mediante su tratamiento legítimo, controlado e informado, a efecto de  garantizar su privacidad, así como tu derecho a la autodeterminación informativa.  2.- Dato personal es cualquier información concerniente a una persona física identificada o  identificable; el responsable de recabar los datos personales es el área sistemas y administración  de la Plataforma.  3.- El domicilio de LUXX y del área responsable, estará disponible en todo momento en la  sección de ayuda de la Plataforma.  4.- Usted al proporcionar sus Datos Personales por escrito, a través de la Plataforma, formato  en papel, formato digital, correo electrónico, o cualquier otro documento, acepta y autoriza a  LUXX a utilizar y tratar de forma automatizada esos datos personales e información  suministrados, los cuales formarán parte de nuestra base de datos con la finalidad de usarlos,  en forma enunciativa, más no limitativa, para: identificar, ubicar, comunicarnos, contactarlo,  enviar información y/o bienes, así como para enviarlos y/o transferirlos a terceros, dentro y  fuera del territorio nacional, por cualquier medio que permita la Ley de Datos para cumplir con    nuestros fin. Mediante la aceptación y autorización para el tratamiento de tus datos personales  en los términos antes señalados, nos facultas expresamente a transferirlos a autoridades de  cualquier nivel (Federales, Estatales, Municipales), organismos públicos y privados, diversas  empresas y/o personas físicas, dentro y fuera de México, con el propósito de proporcionar el  servicio objeto de LUXX.  5.- La temporalidad del manejo de los datos personales de los Usuarios será indefinida a partir  de la fecha en que sean proporcionados en la Plataforma, pudiendo oponerse al manejo de los  mismos en cualquier momento que sea considerado oportuno, con las limitaciones de Ley de  Datos; en caso de que la solicitud de oposición sea procedente, LUXX dejará de manejar tus  Datos Personales sin ninguna responsabilidad de nuestra parte.  6.- El área de LUXX responsable del tratamiento de tus datos personales, está obligada a  cumplir con los principios de licitud, consentimiento, información, calidad, finalidad, lealtad,  proporcionalidad y responsabilidad tutelados en la Ley; por tal motivo con fundamento en los  artículos 13 y 14 de la Ley, LUXX se compromete a guardar estricta confidencialidad de tus  datos personales, así como a mantener las medidas de seguridad administrativas, técnicas y  físicas que permitan protegerlos contra cualquier daño, pérdida, alteración, acceso o  tratamiento no autorizado.  7.- En términos de lo establecido por el artículo 22 de la Ley, Usted tiene derecho en cualquier  momento a ejercer tus derechos de acceso, rectificación, cancelación y oposición al tratamiento  de los datos personales, mediante la solicitud vía la Plataforma.  8.- LUXX ocasionalmente modificará y corregirá este Aviso de Privacidad, por lo tanto, te  pedimos que Usted lo revise regularmente en la Plataforma.  9.- LUXX protegerá tus datos personales en los términos de la Ley de Datos, y te comunicará  los elementos contenidos en las fracciones del artículo 16 de la Ley, los datos personales son  recabados por NOTBANK TECNOLOGIAS con domicilio en: Ricardo Margin 572-4548, San Pedro Garza García, Nuevo León. Si Usted desea hacer uso de su derecho de  ARCO (acceso, rectificación, cancelación y oposición), deberá mandar un correo a: ayuda@luxxapp.com  10.- Transmitir tus datos personales en la Plataforma, es un hecho que presume tu total  aceptación al contenido del presente Aviso de Privacidad.  11.- La aceptación de este Aviso de Privacidad implica su sometimiento expreso a los tribunales de la  Ciudad de Monterrey, Nuevo León, para cualquier controversia o reclamación derivada de este Aviso  de Privacidad.", "Aceptar","Rechazar");
            if (!result)
            { return; }
            if (Model.ActivationStatus == 4 && generatePass != null)
            {
                if (schTyC.IsToggled)
                {
                    await Navigation.PushAsync(generatePass, true);
                }
                else
                {
                    await DisplayAlert("", "Es necesario aceptar los términos y condiciones para continuar con el registro.", "OK");

                }
            }
            else
            {
                await DisplayAlert("", "Es necesario completar el formulario para continuar con el registro.", "OK");
            }
        }
        private void lblHelp_Tapped(object sender, EventArgs e)
        {

        }
        private async void Restart_Tapped(object sender, EventArgs e)
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                Reset();
            }
            catch(Exception ex)
            {
                await DisplayAlert(MessageStrings.GeneralErrorTitle, MessageStrings.GeneralError, "Ok");
                await App.Database.LogException(ex, "Restart_Tapped");
            }
            finally
            {
                IsBusy = false;
            }
        }
        private void Ccomplete_CompleteDone(object sender, RegisterEventArgs e)
        {
            Navigation.PopAsync(true);
            if (RequestCompleted != null)
                RequestCompleted.Invoke(this, new RegisterEventArgs(e.Model));
        }
        #endregion

        #region Methods
        private async Task ValidateCode()
        {
            //if (IsBusy) return;
            //IsBusy = true;
            try
            {
                if (string.IsNullOrEmpty(txtCodeEntry.Text))
                {
                    txtAliasError.Text = MessageStrings.InvalidHost;
                    App.Audio.Vibrate();
                }
                else if (hostCode != txtCodeEntry.Text)
                {
                    SetActivity(aliasActivity, true);
                    var h = txtCodeEntry.Text == null ? "" : txtCodeEntry.Text.Trim();
                    Model.HostCode = hostCode = txtCodeEntry.Text = h;
                    Model = await App.apiService.ValidateCode(Model);
                    if (Model.HostIdValid)
                    {
                        Model.Id = System.Guid.NewGuid().ToString();//generar un Id Unico
                        Model.ActivationStatus = 1;
                        //App.RegisterVM = Model;
                        //await App.Database.SaveRegister(Model);

                        txtAliasError.Text = string.Empty;
                        lblCheck.IsVisible = true;

                        txtCodeEntry.IsEnabled = false;
                        txtUserName.IsEnabled = true;
                        txtUserName.Focus();
                    }
                    else
                    {
                        txtAliasError.Text = Model.HostCodeError;
                        hostCode = null;
                        App.Audio.Vibrate();
                    }
                }
            }
            catch(Exception ex)
            {

                txtAliasError.Text = string.Empty;
                await DisplayAlert("error", ex.Message, "Ok");
            }
            finally
            {
                SetActivity(aliasActivity, false);
                IsBusy = false;
            }
        }
        private async Task ValidateUserName()
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                string usr = txtUserName.Text == null ? "" : txtUserName.Text.Replace(" ", "");
                txtUserName.Text = usr;
                if (string.IsNullOrEmpty(usr))
                {
                    txtUserError.Text = MessageStrings.InvalidField;
                    App.Audio.Vibrate();
                }
                else if(usr.Length > 12)
                {
                    txtUserError.Text = "* No puede ser mayor a 12 caracteres.";
                    App.Audio.Vibrate();
                }
                else if(userName != usr)
                {
                    SetActivity(userActivity, true);
                    userName = usr;
                    Model.UserName = userName;
                    Model = await App.apiService.ValidateUserName(Model);
                    if (Model.UserNameValid)
                    {
                        Model.ActivationStatus = 4;//Aprobado automatico
                        //App.RegisterVM = Model;
                        //no salvamos nada aun
                        //await App.Database.SaveRegister(Model);

                        txtUserError.Text = string.Empty;
                        lblCheck2.IsVisible = true;
                        txtUserName.IsEnabled = false;
                       
                        //Completar registro 

                        //if (ccomplete == null)
                        //{
                        //    ccomplete = new CComplete(Model);
                        //    ccomplete.CompleteDone += Ccomplete_CompleteDone;
                        //}
                        //await Navigation.PushAsync(ccomplete, true);

                        if(generatePass == null)
                        {
                            generatePass = new CLogin2(Model);
                            generatePass.CompleteDone += Ccomplete_CompleteDone;
                        }
                        //await Navigation.PushAsync(generatePass, true);
                        schTyC.IsEnabled = true;
                        //slStatus.IsVisible = true;
                        //customNavBar.SetModalMode();//esconder boton
                    }
                    else
                    {
                        txtUserError.Text = "El usuario " + Model.UserName + " ya existe.";
                        txtUserName.Text = string.Empty;
                        userName = null;
                        App.Audio.Vibrate();
                    }
                }
            }
            catch(Exception ex)
            {
                await DisplayAlert("error", ex.Message, "Ok");
            }
            finally
            {
                SetActivity(userActivity, false);
                IsBusy = false;
            }
        }
        private void Reset()
        {
            //Cada ves que cambie, reiniciar
            lblCheck2.IsVisible = lblCheck.IsVisible = false;
            txtUserName.Text = txtCodeEntry.Text = string.Empty;
            txtUserName.IsEnabled = txtCodeEntry.IsEnabled = true;
            txtUserError.Text = txtAliasError.Text = string.Empty;
            schTyC.IsToggled = false;
            Model.UserName = Model.HostCode = string.Empty;
            Model.ActivationStatus = 0;//reset
            //App.RegisterVM = Model;

        }
        private void SetActivity(ActivityIndicator activity, bool Enabled)
        {
            activity.IsVisible = activity.IsEnabled = activity.IsRunning = Enabled;
        }

        #endregion

       

        private void schTyC_Toggled(object sender, ToggledEventArgs e)
        {
            //if (schTyC.IsToggled)
            //{
            //    btnActivate.IsEnabled = true;
            //}
            //else
            //{
            //    btnActivate.IsEnabled = false;
            //}
        }

        private async void TapGestureRecognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new XTYC(), true);
        }
    }


}