﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CSelector : ContentPage
    {
        public event EventHandler<EventArgs> SelectedTapped;
        public CSelector()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }
        protected override void OnAppearing()
        {           
            base.OnAppearing();
        }
        private void btnRegister_Clicked(object sender, EventArgs e)
        {
            App.RootStep = 1;
            App.MSender = MessageSender.Customer;
            //App.MSender = MessageSender.Asset;
            Pop();
        }
        private void LoginIn_Tapped(object sender, EventArgs e)
        {
            App.RootStep = 2;
            //Permanece como None-para abrir la pantalla de login
            App.MSender = MessageSender.None;
            Pop();
        }

        private void LoginIn2_Tapped(object sender, EventArgs e)
        {
            App.RootStep = 2;
            //Permanece como None-para abrir la pantalla de login
            App.MSender = MessageSender.Grap;
            Pop();
        }
        private void Pop()
        {
            if (SelectedTapped != null)
                SelectedTapped.Invoke(this, new EventArgs());
        }

       
      

        protected override bool OnBackButtonPressed()
        {
          
            return false;
        }
    }
}