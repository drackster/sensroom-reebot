﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Helpers;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CComplete : ContentPage
    {
        public event EventHandler<RegisterEventArgs> CompleteDone;
        private RegisterViewModel Model;
        public CComplete(RegisterViewModel model)
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Terminar");
            //customNavBar.SetModalOnly();

            txtPassword.Keyboard = Keyboard.Numeric;
            txtPassword.Completed += txtPassword_Completed;
            txtPassword.Unfocused += txtPassword_Unfocused;
            //txtPassword.LineEntryField.TextChanged += txtPassword_TextChanged;

            txtRepeat.Keyboard = Keyboard.Numeric;
            txtRepeat.Completed += RepeatField_Completed;
            txtRepeat.Unfocused += RepeatField_Unfocused;
            //txtRepeat.LineEntryField.TextChanged += txtPassword_TextChanged;
            Model = model;
            App.NavBar = customNavBar;
        }

        #region Events
        private void txtPassword_Unfocused(object sender, FocusEventArgs e)
        {
            ValidatePassword();
        }
        private void txtPassword_Completed(object sender, EventArgs e)
        {
            ValidatePassword();
        }
        private async void RepeatField_Unfocused(object sender, FocusEventArgs e)
        {
            await RegisterAccount();
        }
        private async void RepeatField_Completed(object sender, EventArgs e)
        {
            await RegisterAccount();
        }
        private async void btnActivate_Clicked(object sender, EventArgs e)
        {
            await RegisterAccount();
        }
        private void TYC_Tapped(object sender, EventArgs e)
        {
            Navigation.PushAsync(new Customer.XTYC(), true);
        }

        #endregion

        #region Methods
        private void ValidatePassword()
        {
            if (String.IsNullOrEmpty(txtPassword.Text))
            {
                txtPasswordError.Text = MessageStrings.InvalidField;
                App.Audio.Vibrate();
            }
            else
            {
                txtPasswordError.Text = string.Empty;
                txtRepeat.Focus();
            }
        }
        private async Task RegisterAccount()
        {
            if (IsBusy) return;
            IsBusy = true;
            try
            {
                txtPassword.Text = txtPassword.Text == null ? "" : txtPassword.Text.Trim();
                txtRepeat.Text = txtRepeat.Text == null ? "" : txtRepeat.Text.Trim();

                if (txtRepeat.Text != txtPassword.Text)
                {
                    App.Audio.Vibrate();
                    txtRepeatError.Text = "* Las contraseñas no coinciden";
                    return;
                }
                else if (txtPassword.Text.Length < 6 || txtPassword.Text.Length > 12)
                {
                    App.Audio.Vibrate();
                    txtRepeatError.Text = "* Debe de ser entre 6 y 12 dígitos.";
                    return;
                }
                else if (SegControl.SelectedSegment == 0)
                {
                    App.Audio.Vibrate();
                    await DisplayAlert(MessageStrings.GeneralErrorTitle, "Debes de aceptar los términos y condiciones para continuar", "Ok");
                }
                else
                {
                    App.HUD.Show(MessageStrings.PleaseWait);
                    Model.Password = txtPassword.Text;
                    Model = await App.apiService.Register(Model);
                    txtRepeatError.Text = string.Empty;

                    if (Model.IsActivated)
                    {
                        await App.Database.SaveRegister(Model);
                        if (App.MSender == MessageSender.Customer)
                        {
                            await App.Database.SaveUser(Model.UserVM);
                            await App.Database.SaveSearchModel(Model.UserVM.SearchFilters);
                        }
                        else if (App.MSender == MessageSender.Asset)
                        {
                            await App.Database.SaveAsset(Model.AssetVM);
                            await App.Database.SaveAssetImages(Model.AssetVM.Images);
                            await App.Database.SaveCalendar(Model.AssetVM.Calendars);
                        }
                        App.apiService.UserName = Model.UserName;
                        App.apiService.UserPassword = Model.Password;
                        App.RootStep = 99;
                        //App.RegisterVM = Model;
                        //await Navigation.PopAsync(true);
                        //await Navigation.PopToRootAsync(true);
                        //await Navigation.PopModalAsync(true);
                        if (CompleteDone != null)
                            CompleteDone.Invoke(this, new RegisterEventArgs(Model));
                    }
                    else
                    {
                        App.Audio.Vibrate();
                        txtRepeatError.Text = "Sucedió un error al generar la cuenta.";
                    }

                }
            }
            catch(Exception ex)
            {
                await App.Database.LogException(ex);
                await DisplayAlert(MessageStrings.GeneralErrorTitle, 
                    MessageStrings.GeneralError, "Ok");
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
        }

        #endregion
    }
}