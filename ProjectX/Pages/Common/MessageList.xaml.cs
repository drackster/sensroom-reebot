﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.ViewModels;
using ProjectX.Pages.Templates;
using System.IO;
using Newtonsoft.Json;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MessageList : ContentPage
    {
        #region Properties
        private InvitationViewModel Invitation;
        private OrderViewModel Order;
        private NotificationRequestModel requestModel;
        //private bool IsLoaded;
        private long OrderId;
        private long? AssetId;
        private long? CustomerId;
        private string AssetImageUrl;
        private bool IsBusy2;
        private bool IsModal;
        private double kbHeight;
        private bool EditorChanged;
        //private bool loginIsVisible;
        #endregion

        #region cTors
        public MessageList(InvitationViewModel invitation, bool isModal=false)
        {
            InitializeComponent();
            Invitation = invitation;
            OrderId = invitation.OrderId;
            IsModal = isModal;

          

            if (App.MSender == MessageSender.Asset)
            {
                CustomerId = invitation.CustomerId;
                AssetId = App.AssetVM.Id;
            }
            else if(App.MSender == MessageSender.Customer)
            {
                CustomerId = App.UserVM.Id;
                AssetId = invitation.CustomerId;
            }

            if(isModal)
            {
                customNavBar.SetIconText("X");
                customNavBar.EditButtonTapped += CustomNavBar_EditButtonTapped;
            }

            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle(invitation.CustomerName);

            Init();
        }
        public MessageList(OrderViewModel order)
        {
            InitializeComponent();
            Order = order;
            OrderId = order.Id;
            CustomerId = order.CustomerId;
            AssetId = order.AssetId;
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle(order.Asset.Name);
            Init();

        }
        public MessageList(NotificationRequestModel model, string Name)
        {
            InitializeComponent();
            requestModel = model;
            OrderId = model.OrderId.Value;
            AssetId = model.AssetId;
            CustomerId = model.CustomerId;
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle(Name);
            Init();

        }

        //~MessageList()
        //{
        //    StopListening();
        //}
        //private async void Login_LoginComplete(object sender, Models.RegisterEventArgs e)
        //{
        //    if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
        //        Application.Current.Properties.Remove("resume");
        //    await Navigation.PopModalAsync(true);
        //}


        protected async override void OnAppearing()
        {
            //if (Application.Current.Properties.ContainsKey("resume") == true && (bool)Application.Current.Properties["resume"] == true)
            //{
            //    try
            //    {
            //        if (!loginIsVisible)
            //        {
            //            loginIsVisible = true;
            //            var login = new Common.CLogin2();
            //            login.LoginComplete += Login_LoginComplete;
            //            await Navigation.PushModalAsync(new NavigationPage(login) { BarTextColor = Color.White }, true);
            //        }
            //        else
            //            loginIsVisible = false;
            //    }
            //    catch (Exception exc)
            //    {
            //        await App.Database.LogException(exc);
            //        await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, Helpers.MessageStrings.GeneralError, "Ok");
            //    }
            //    return;
            //}
            base.OnAppearing();
            await GetMessages(true);
            //if(!App.AdvcTimer.IsTimerEnabled)
            //{
            //    App.AdvcTimer.InitTimer(10,)
            //}
            update();
        }
        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //StopListening();
        }

        private async void update()
        {
            await Task.Delay(10000);
            OnAppearing();
        }
        #endregion

        #region Events
        private async void App_TimerElapsed(object sender, EventArgs e)
        {
            if (!IsBusy2)
            {
                await GetMessages();
            }
        }
        private void CustomNavBar_EditButtonTapped(object sender, EditModeArgs e)
        {
            MessagingCenter.Send(this, "chatcontinue", true);
            //this.Navigation.PopAsync(true);
        }
        private void txtMessage_TextChanged(object sender, TextChangedEventArgs e)
        {
            //btnSend.IsEnabled = !string.IsNullOrEmpty(txtMessage.Text);
        }
        private async void btnSend_Clicked(object sender, EventArgs e)
        {
            if (IsBusy || string.IsNullOrEmpty(txtMessage.Text)) return;
            IsBusy = true;
            btnSend.IsEnabled = false;
            SetActivity(true);
            try
            {
                var model = new NotificationViewModel
                {
                     MType = App.MSender == MessageSender.Asset ? MessageType.AssetMessage : MessageType.CustomerMessage,
                     MReceiver = App.MSender == MessageSender.Asset ? MessageSender.Customer : MessageSender.Asset,
                     AssetId = AssetId,
                     CustomerId = CustomerId,
                     MessageText = txtMessage.Text, 
                     OrderId = OrderId
                };

                var newNotif = await App.apiService.SendNotification(model);
                model.Id = this.requestModel.LastId = newNotif.Id;
                model.CreatedOn =DateTime.UtcNow;
                var widget = GetWidget(model);
                //Ir insertando al final
                //ChatMessagesListView.Children.Add(widget);
                var itemCount = ChatMessagesListView.Children.Count;
                ChatMessagesListView.Children.Insert(itemCount - 1, widget);
                txtMessage.Text = string.Empty;
                DoScroll(widget);

            }
            catch (Exception ex)
            {
                await App.Database.LogException(ex,"MessageList");
            }
            finally
            {
                SetActivity(false);
                IsBusy = false;
                btnSend.IsEnabled = true;
            }
        }
        private void TxtMessage_Unfocused(object sender, FocusEventArgs e)
        {
            //msgRow.Height = 50;
            //slMessage.TranslationY = 0;
            //txtMessage.VerticalOptions = btnSend.VerticalOptions = LayoutOptions.Center;
        }
        private void TxtMessage_Focused(object sender, FocusEventArgs e)
        {
            //System.Diagnostics.Debug.WriteLine("Txt Focused...");
            if(!EditorChanged && txtMessage.Text == "Escribe tu mensaje...")
            {
                EditorChanged = true;
                txtMessage.Text = string.Empty;
                txtMessage.TextColor = Color.Black;
            }         
            //if (kbHeight > 0)
            //    MoveMessageField();
        }
        //private void Instance_KeyboardShowing(object sender, EventArgs e)
        //{
        //    var kea = e as Interfases.KeyboardHeightEventArgs;
        //    if (kea != null && kbHeight == 0)
        //    {
        //        kbHeight = kea.Height + (slMessage.Height);
        //    }
        //    if (kbHeight > 0)
        //    {
        //        MoveMessageField();
        //    }
        //}

        #endregion

        #region Methods
        private async Task GetMessages(bool FirstLoad=false)
        {
            if (IsBusy2) return;
            IsBusy2 = true;
            if(FirstLoad) SetActivity(true);
            try
            {
                if (requestModel == null)
                    requestModel = new NotificationRequestModel()
                    {
                        SType = App.MSender,
                        OrderId = OrderId,
                        IsChat = true,
                        AssetId = this.AssetId,
                        CustomerId = this.CustomerId,
                        MType = App.MSender == MessageSender.Asset ? MessageType.CustomerMessage : MessageType.AssetMessage
                    };

                var msgs = await App.apiService.GetNotifications(requestModel);
                if (msgs.Count > 0)
                    requestModel.LastId = msgs[msgs.Count-1].Id;///El chat es ASC

                View widget = null;
                for (int index = 0; index < msgs.Count; index++)
                {
                    widget = GetWidget(msgs[index]);
                    //if (!IsLoaded)//primera carga

                    //Ya no podemos insertar al final, 
                    //porque al final esta los campos de editor y boton de send
                    //ChatMessagesListView.Children.Add(widget);

                    //Ahora mejor insertamos al final * antes del editor
                    int numItems = ChatMessagesListView.Children.Count;
                    ChatMessagesListView.Children.Insert(numItems - 1, widget);
                    ///else
                    //Ir insertando al inicio
                    //ChatMessagesListView.Children.Insert(0, widget);
                }
                if (widget != null) //scroll al ultimo elemento
                {
                    await scrollView1.ScrollToAsync(0, 10000, false);
                    //DoScroll(widget);
                 }
                //IsLoaded = true;
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
            }
            finally
            {
                if (FirstLoad) SetActivity(false);
                IsBusy2 = false;             
            }            
        }
        private void SetActivity(bool IsEnabled)
        {
            messagesActivity.IsVisible = messagesActivity.IsEnabled = IsEnabled;
        }
        private View GetWidget(NotificationViewModel msg)
        {
            if (msg.MType == MessageType.CustomerMessage)
            {
                var widget1 = new ChatRightMessageItemTemplate();
                widget1.BindingContext = msg;
                return widget1;
            }
            else if (msg.MType == MessageType.AssetMessage)
            {
                var widget2 = new ChatLeftMessageItemTemplate();
                widget2.BindingContext = msg;

                if (App.MSender == MessageSender.Asset)
                {
                    widget2.AssetImage.Source = App.apiService.FormatImgUrl(App.AssetVM.CoverImage.ToString());
                }
                else if(App.MSender == MessageSender.Customer)
                {
                    if (this.AssetImageUrl == null)
                    {
                        if(Invitation==null)
                        {
                            //si es un cliente, y viene de notificaciones, 
                            //no hay manera de que venga la invitacion
                            //we need to get the converimageId of the Asset 
                            if (App.AssetVM == null && msg.AssetId.HasValue)
                            {
                                this.AssetImageUrl = App.apiService.FormatImgUrl(msg.AssetId.Value);
                            }
                            else
                            {
                                this.AssetImageUrl = App.apiService.FormatImgUrl(App.AssetVM.CoverImage);
                            }
                        }
                        else
                        {
                            this.AssetImageUrl = App.apiService.FormatImgUrl(Invitation.AssetCoverImageId);
                        }
                    }
                    widget2.AssetImage.Source = AssetImageUrl;
                }
                return widget2;
            }

            return null;
        }
        private void Init()
        {
            //txtMessage.Focused += TxtMessage_Focused;
            //txtMessage.Unfocused += TxtMessage_Unfocused;
            txtMessage.Completed += btnSend_Clicked;
            kbHeight = 0;
            
            //StartListening();
        }
        //private void StartListening()
        //{
        //    Helpers.KeyboardListenerHelper.Instance.StartListening();
        //    Helpers.KeyboardListenerHelper.Instance.KeyboardShowing += Instance_KeyboardShowing;
        //}
        //private void StopListening()
        //{
        //    Helpers.KeyboardListenerHelper.Instance.KeyboardShowing -= Instance_KeyboardShowing;
        //    Helpers.KeyboardListenerHelper.Instance.StopListening();
        //}
        private void MoveMessageField()
        {
            //msgRow.Height = kbHeight;
            //txtMessage.Text = kbHeight.ToString();
            //slMessage.TranslationY = (kea.Height - 50) * -1;
            //DisplayAlert("test", msgRow.Height.ToString() + " de " + App.ScreenHeight.ToString(), "Ok");
            System.Diagnostics.Debug.WriteLine("Keyboard height:" + kbHeight.ToString());
            txtMessage.VerticalOptions = btnSend.VerticalOptions = LayoutOptions.StartAndExpand;
        }
        //private async Task DoScroll(View widget=null)
        private void DoScroll(View widget=null)
        {
            System.Diagnostics.Debug.WriteLine("widgetY: " 
                + widget==null? "" : widget.Y.ToString()
                + "scrollY:" + scrollView1.ScrollY.ToString()
                + "height:" + (scrollView1.Height-20).ToString()
                + "stacklayout:" + ChatMessagesListView.Height.ToString());

            //double height = scrollView1.Height > ChatMessagesListView.Height ? 
            //  scrollView1.Height : ChatMessagesListView.Height;

            //if (ChatMessagesListView.Height > 0 && widget != null
            //    && ChatMessagesListView.Height > widget.Y)
            //if (x!=0)//todo
            //await scrollView1.ScrollToAsync(0, ChatMessagesListView.Height, true);
        }
        #endregion

    }
}