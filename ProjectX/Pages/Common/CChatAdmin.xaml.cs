﻿using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CChatAdmin : ContentPage
    {
        public MessageType MType { get; set; }
        private long OrderId;
        private long? AssetId;
        private long? CustomerId;
        private NotificationRequestModel requestModel;
        private double kbHeight;
        public CChatAdmin()
        {
            InitializeComponent();
          
            
            NavigationPage.SetHasNavigationBar(this, false);
           // customNavBar.SetTitle(Name);
            Init();

            EnviarMensaje();
        }
        //private async void btnSend_Clicked(object sender, EventArgs e)
        private async void EnviarMensaje()
        {
            if (IsBusy) return;
            IsBusy = true;
            btnSend.IsEnabled = false;
            SetActivity(true);

            if (App.MSender == MessageSender.Customer)
            {
                var model = new NotificationViewModel
                {
                    MType = App.MSender == MessageSender.Asset ? MessageType.AssetMessage : MessageType.CustomerMessage,
                    MReceiver = App.MSender == MessageSender.Asset ? MessageSender.Customer : MessageSender.Asset,
                    AssetId = 3,
                    CustomerId = App.UserVM.Id,
                    MessageText = "Ayuda luxx",
                    OrderId = null
                };
                try
                {
                    var newNotif = await App.apiService.SendNotificationAdmin(model);
                    await DisplayAlert("Hola!", "Hemos recibido tu solicitud de ayuda, en breve nos pondremos en contacto.", "Ok");
                    await Navigation.PushAsync(new Pages.Customer.XList());
                    //model.Id = this.requestModel.LastId = newNotif.Id;
                    //model.CreatedOn =DateTime.UtcNow;
                    //// var widget = GetWidget(model);
                    ////Ir insertando al final
                    ////ChatMessagesListView.Children.Add(widget);
                    //var itemCount = ChatMessagesListView.Children.Count;
                    ////ChatMessagesListView.Children.Insert(itemCount - 1, widget);
                    //txtMessage.Text = string.Empty;
                    //DoScroll();
                }
                catch (Exception ex)
                {

                }
               
            }

            if (App.MSender == MessageSender.Asset)
            {
                var model = new NotificationViewModel
                {
                    MType = App.MSender == MessageSender.Asset ? MessageType.All : MessageType.All,
                    MReceiver = App.MSender == MessageSender.Asset ? MessageSender.Customer : MessageSender.Asset,
                    AssetId = App.UserVM.Id,
                    CustomerId = 5,
                    MessageText = txtMessage.Text,
                    OrderId = null
                };
                try
                {
                    var newNotif = await App.apiService.SendNotificationAdmin(model);
                    await DisplayAlert("Hola!", "Hemos recibido tu solicitud de ayuda, en breve nos pondremos en contacto.", "Ok");
                    await Navigation.PushAsync(new Pages.Asset.AMain());
                    //model.Id = this.requestModel.LastId = newNotif.Id;
                    //model.CreatedOn =DateTime.UtcNow;
                    //// var widget = GetWidget(model);
                    ////Ir insertando al final
                    ////ChatMessagesListView.Children.Add(widget);
                    //var itemCount = ChatMessagesListView.Children.Count;
                    ////ChatMessagesListView.Children.Insert(itemCount - 1, widget);
                    //txtMessage.Text = string.Empty;
                    //DoScroll();
                }
                catch (Exception ex)
                {

                }
                
            }


           
        }

        private void SetActivity(bool IsEnabled)
        {
            messagesActivity.IsVisible = messagesActivity.IsEnabled = IsEnabled;
        }

        private void DoScroll(View widget = null)
        {
            System.Diagnostics.Debug.WriteLine("widgetY: "
                + widget == null ? "" : widget.Y.ToString()
                + "scrollY:" + scrollView1.ScrollY.ToString()
                + "height:" + (scrollView1.Height - 20).ToString()
                + "stacklayout:" + ChatMessagesListView.Height.ToString());

            //double height = scrollView1.Height > ChatMessagesListView.Height ? 
            //  scrollView1.Height : ChatMessagesListView.Height;

            //if (ChatMessagesListView.Height > 0 && widget != null
            //    && ChatMessagesListView.Height > widget.Y)
            //if (x!=0)//todo
            //await scrollView1.ScrollToAsync(0, ChatMessagesListView.Height, true);
        }

        private void Init()
        {
            //txtMessage.Focused += TxtMessage_Focused;
            //txtMessage.Unfocused += TxtMessage_Unfocused;
           // txtMessage.Completed += btnSend_Clicked;
            kbHeight = 0;

            //StartListening();
        }
    }
}