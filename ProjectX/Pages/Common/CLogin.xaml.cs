﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProjectX.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using ProjectX.Models;

namespace ProjectX.Pages.Common
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CLogin : ContentPage
    {
        public event EventHandler<RegisterEventArgs> LoginComplete;
        public CLogin()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
            customNavBar.SetTitle("Iniciar Sesión");
            customNavBar.SetIconText("X");
            
            txtUser.Unfocused += TxtUser_Unfocused;
            txtUser.Completed += TxtUser_Completed;
            txtPassword.Unfocused += txtPassword_Unfocused;
            txtPassword.Completed += txtPassword_Completed;

            txtPassword.Keyboard = Keyboard.Numeric;

            App.MSender = MessageSender.None;
            App.NavBar = customNavBar;
        }

        #region events
        private void TxtUser_Completed(object sender, EventArgs e)
        {
            ValidateUser();
        }
        private void TxtUser_Unfocused(object sender, FocusEventArgs e)
        {
            ValidateUser();
        }
        private async void txtPassword_Completed(object sender, EventArgs e)
        {
            await DoLogin();
        }
        private void txtPassword_Unfocused(object sender, FocusEventArgs e)
        {
            ValidatePassword();
        }
        private async void btnNext_Clicked(object sender, EventArgs e)
        {
            await DoLogin();
        }

        #endregion

        #region Methods
        public bool ValidateUser()
        {
            txtUser.Text = txtUser.Text == null ? "" : txtUser.Text.Trim();
            if (string.IsNullOrEmpty(txtUser.Text))
            {
                txtUserError.Text = Helpers.MessageStrings.InvalidField;
                App.Audio.Vibrate();
                return false;
            }
            txtUserError.Text = string.Empty;
            return true;
           
        }
        public bool ValidatePassword()
        {
            txtPassword.Text = txtPassword.Text == null ? "" : txtPassword.Text.Trim();
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                txtPasswordError.Text = Helpers.MessageStrings.InvalidField;
                App.Audio.Vibrate();
                return false;
            }
            txtPasswordError.Text = string.Empty;
            return true;
        }
        public async Task DoLogin()
        {
            if (IsBusy || !ValidateUser() || !ValidatePassword()) return;
            IsBusy = true;

            try
            {
                App.HUD.Show(Helpers.MessageStrings.PleaseWait);
                RegisterViewModel model = new RegisterViewModel()
                {
                    UserName = txtUser.Text,
                    Password = txtPassword.Text
                };
                model.DeviceToken = await App.Database.GetDeviceToken();
                model = await App.apiService.Login(model);
                if (model.IsActivated)
                {
                    await App.Database.SaveRegister(model);
                    App.apiService.UserName = model.UserName;
                    App.apiService.UserPassword = model.Password;
                    App.RegisterVM = model;
                    App.MSender = model.Sender;

                    if (App.MSender == MessageSender.Customer)
                    {
                        await App.Database.SaveUser(model.UserVM);
                        await App.Database.SaveSearchModel(model.UserVM.SearchFilters);
                    }
                    else if (App.MSender == MessageSender.Asset)
                    {
                        await App.Database.SaveAsset(model.AssetVM);
                        await App.Database.SaveAssetImages(model.AssetVM.Images);
                        await App.Database.SaveCalendar(model.AssetVM.Calendars);
                    }
                    //await Navigation.PopModalAsync(true);
                    if (LoginComplete != null)
                        LoginComplete.Invoke(this, new RegisterEventArgs(model));
                }
                else
                {
                    txtUserError.Text = "Usuario o contraseña inválidas";
                    App.Audio.Vibrate();
                }

            }
            catch (Exception ex)
            {
                App.Audio.Vibrate();
                await App.Database.LogException(ex);
                await DisplayAlert(Helpers.MessageStrings.GeneralErrorTitle, 
                    Helpers.MessageStrings.GeneralError, "Ok");
            }
            finally
            {
                App.HUD.Dismiss();
                IsBusy = false;
            }
        }

        #endregion

        private void NoCredentials_Tapped(object sender, EventArgs e)
        {
            this.Navigation.PopModalAsync(true);
        }
    }
}