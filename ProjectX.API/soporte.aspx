﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="soporte.aspx.cs" Inherits="ProjectX.API.soporte" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Soporte y ayuda Luxx</title>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding:50px">
            <table>
                <tr>
                    <td>
                        <div style="float: left">
                            <img src="Images/image.png" />
                        </div>
                    </td>
                </tr>
                <tr></tr>

                <tr>
                    <td>
                        <div style="float: left">
                            <h1>¡Hola!</h1>
                            <h4>Escríbenos aquí tus dudas, comentarios y sugerenias para mejorar</h4>
                        </div>
                    </td>
                </tr>

                <div style="float: left">
            </table>
            <table>

                <tr>
                    <td>Ingresa tu usuario Luxx </td>
                    <td>
                        <asp:TextBox ID="txtUsuario" runat="server"></asp:TextBox></td>
                </tr>
                <tr>
                    <td>Ingresa un medio de contacto</td>
                    <td>
                        <asp:TextBox ID="txtContacto" runat="server"></asp:TextBox></td>
                </tr>




                <tr>
                    <td>
                        <asp:TextBox ID="txtComentario" runat="server" TextMode="MultiLine" Width="200" Height="300"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="Enviar" OnClick="Button1_Click" />
                    </td>
                </tr>
            </table>

            <div>
                <asp:GridView ID="GridView1" runat="server"></asp:GridView>
            </div>
        </div>

    </form>
</body>
</html>
