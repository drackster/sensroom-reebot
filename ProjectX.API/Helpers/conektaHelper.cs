﻿using Conekta.Xamarin;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace ProjectX.API.Helpers
{
    public class conektaHelper : IConektaHelper
    {

        public async Task<string> TokenizeCard(string cardNumber, string name, string cvc, int year, int month)
        {
            //Parece que ya no se configura así
            //Conekta._delegate = this;
            //Conekta.PublicKey = "key_KJysdbf6PotS2ut2";
            //Conekta.collectDevice();

            //#if DEBUG  
            // pruebas
            // ConektaTokenizer tok = new ConektaTokenizer("key_NNzvY2AqR4R4SxLiN9Jgq8A", RuntimePlatform.Android);

            // pruebas2
            ConektaTokenizer tok = new ConektaTokenizer("key_CWq4i4kpX6TdaqWeo8RmcxA", RuntimePlatform.Android);

            //produccion
            //ConektaTokenizer tok = new ConektaTokenizer("key_csXHrmyZq4EbkfcPTrqsnjg", RuntimePlatform.Android);
            //#else

            //#endif
            string cToken = await tok.GetTokenAsync(cardNumber, name, cvc, year, month);
            return cToken;
        }
    }
}