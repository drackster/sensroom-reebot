﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false"
    CodeBehind="test.aspx.cs" Inherits="ProjectX.API.test" Async="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <h3>Generar Random Dummys</h3>
        <table>
            <tr>
                <td><asp:Label Text="Latitud" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtLat" Text="32.477058" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Longitud" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtLong" Text="-114.760061" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Radio (Mts)" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtRadius" Text="10000" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Cantidad" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtQty" Text="" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Cant Fotos" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtImageCount" Text="2" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Ancho foto (px)" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtWidth" Text="480" /></td>
            </tr>
            <tr>
                <td><asp:Label Text="Alto foto (px)" runat="server" /></td>
                <td><asp:TextBox runat="server" ID="txtHeight" Text="300" /></td>
            </tr>
        </table>
        <asp:Button runat="server" ID="testbtn" Text="Generar" OnClick="Generate_Click" />
        <br /><br />
        <asp:Button runat="server" ID="Button1" Text="Probar OxxoPay" OnClick="OxxoPay_Click" />
    </div>
    </form>
</body>
</html>
