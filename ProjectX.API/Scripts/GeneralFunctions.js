﻿Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};

var getMonthsBetween = function (date1, date2) {
    'use strict';

    // Months will be calculated between start and end dates.
    // Make sure start date is less than end date.
    // But remember if the difference should be negative.
    var start_date = date1;
    var end_date = date2;
    var inverse = false;

    if (date1 > date2) {
        start_date = date2;
        end_date = date1;
        inverse = true;
    }

    end_date = new Date(end_date); //If you don't do this, the original date passed will be changed. Dates are mutable objects.
    end_date.setDate(end_date.getDate() + 1);

    // Calculate the differences between the start and end dates
    var yearsDifference = end_date.getFullYear() - start_date.getFullYear();
    var monthsDifference = end_date.getMonth() - start_date.getMonth();
    var daysDifference = end_date.getDate() - start_date.getDate();

    return (inverse ? -1 : 1) * (yearsDifference * 12 + monthsDifference + daysDifference / 30); // Add fractional month
}

//Pantalla cargando
var waitingDialog = waitingDialog || (function ($) {
    'use strict';

    // Creating modal dialog's DOM
    var $dialog = $(
        '<div class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true" style="padding-top:15%; overflow-y:visible;">' +
        '<div class="modal-dialog modal-m">' +
        '<div class="modal-content">' +
        '<div class="modal-header"><h3 style="margin:0;"></h3></div>' +
        '<div class="modal-body">' +
        '<div class="progress progress-striped active" style="margin-bottom:0;"><div class="progress-bar" style="width: 100%"></div></div>' +
        '</div>' +
        '</div></div></div>');

    return {
        /**
         * Opens our dialog
         * @param message Custom message
         * @param options Custom options:
         * 				  options.dialogSize - bootstrap postfix for dialog size, e.g. "sm", "m";
         * 				  options.progressType - bootstrap postfix for progress bar type, e.g. "success", "warning".
         */
        show: function (message, options) {
            // Assigning defaults
            if (typeof options === 'undefined') {
                options = {};
            }
            if (typeof message === 'undefined') {
                message = 'Loading';
            }
            var settings = $.extend({
                dialogSize: 'm',
                progressType: '',
                onHide: null // This callback runs after the dialog was hidden
            }, options);

            // Configuring dialog
            $dialog.find('.modal-dialog').attr('class', 'modal-dialog').addClass('modal-' + settings.dialogSize);
            $dialog.find('.progress-bar').attr('class', 'progress-bar');
            if (settings.progressType) {
                $dialog.find('.progress-bar').addClass('progress-bar-' + settings.progressType);
            }
            $dialog.find('h3').text(message);
            // Adding callbacks
            if (typeof settings.onHide === 'function') {
                $dialog.off('hidden.bs.modal').on('hidden.bs.modal', function (e) {
                    settings.onHide.call($dialog);
                });
            }
            // Opening dialog
            $dialog.modal();
        },
        /**
         * Closes dialog
         */
        hide: function () {
            $dialog.modal('hide');
        }
    };
})(jQuery);

function showMsg(msg, type, timer) {
    waitingDialog.show(msg, { progressType: type });
    if (timer == true)
        setTimeout(function () { waitingDialog.hide(); }, 3000);
}

function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

$(function () {

    if ($("#tblOrdenes").length > 0 && $("#tblOrdenes > tbody > tr")[0].cells.length > 1)
        $("#tblOrdenes").DataTable({
            responsive: true,
            info: false,
            pageLength: 20,
            initComplete: function () {
                $(".dataTables_filter").hide();
                var i = 0;
                this.api().columns().every(function () {
                    var column = this;
                    var td = $('<th></th>');
                    if (column.header().textContent == "Acciones" || column.header().textContent == "Monto") {
                        td.appendTo($("#filtros"));
                        return;
                    }
                    var results = null;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo(td)
                        .on('change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );

                            results = column.search(val ? '^' + val + '$' : '', true, false);
                            results.draw();
                        });
                    td.appendTo($("#filtros"));
                    column.data().unique().sort().each(function (d, j) {
                        select.append('<option value="' + d + '">' + d + '</option>')
                    });
                });
            }
        });
});