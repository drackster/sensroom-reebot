﻿
    initiate_geolocation();
    var cord = null;
    var lat = null;
    var lon = null;
    function initiate_geolocation() {
        navigator.geolocation.getCurrentPosition(handle_geolocation_query, handle_errors);
    }
    function handle_errors(error) {
        alert("Tu ubicación nunca la compartiremos, pero es necesario el permiso " + "\n" + "para que tu perfil aparezca en búsquedas.");
    }
    function handle_geolocation_query(position) {
        cord = position.coords.latitude + "," + position.coords.longitude;
        lati = position.coords.latitude;
        long = position.coords.longitude;

        var geocoder = new google.maps.Geocoder,
            latitude = lati, //sub in your latitude
            longitude = long, //sub in your longitude
            postal_code,
            city,
            state,
            country;

        geocoder.geocode({'location': {lat: latitude, lng: longitude } }, function (results, status) {
            if (status === google.maps.GeocoderStatus.OK) {
        results.forEach(function (element) {
            element.address_components.forEach(function (element2) {
                element2.types.forEach(function (element3) {
                    switch (element3) {
                        case 'postal_code':
                            postal_code = element2.long_name;
                            break;
                        case 'administrative_area_level_1':
                            state = element2.long_name;
                            $("#inestado").val(state);
                            break;
                        case 'locality':
                            city = element2.long_name;
                            $("#inciudad").val(city);
                            break;
                        case 'country':
                            country = element2.long_name;
                            $("#inpais").val(country);
                            break;
                    }
                })
            });
        });
            }
            locationUpdate(latitude, longitude, city, state, country);
        });
    }

        function locationUpdate(latitud, longitud, ciudad, estado, pais) {
            $.ajax({
                type: "POST",
                url: "/Xlist/locationUpdate",
                data: { 'latitud': latitud, 'longitud': longitud, 'ciudad': ciudad, 'estado': estado, 'pais': pais },
                success: function (data) {
                }
            });
    }
