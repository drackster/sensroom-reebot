﻿using Newtonsoft.Json;
using ProjectX.API.Controllers;
using ProjectX.API.Helpers;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http.Results;

namespace ProjectX.API.Service
{
    public class ApiServices
    {


        public RegisterViewModel Login(RegisterViewModel model)
        {
            var log = new UserController();
            var reg = log.Login(model);
            var contentResult = reg as OkNegotiatedContentResult<RegisterViewModel>;
            return contentResult.Content;
        }


        public bool Credito(string name)
        {
            var order = new OrderController();
            //  var credito = order.GetCustomerCredit(name);
            //  var contentResult = credito as OkNegotiatedContentResult<bool>;
            return true;// contentResult.Content;
        }

        public AssetImage GetImage(int id)
        {
            var controller = new AssetImageController();
            var img = controller.AssetImage(id);
            var contentResult = img as OkNegotiatedContentResult<AssetImage>;
            return contentResult.Content;

        }

        public List<Asset> GetAssetList()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var detail = db.Assets.Where(x => x.IsOnline == true && x.ASex == false).ToList();
                //var images = db.AssetImages.Where(x => x.UserId == id).ToList();
                return detail;
            }
        }

        public Asset GetAsset(string id)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == id).FirstOrDefault();
                if (asset != null)
                {  
                    return asset;
                }
                else { return null; }               

            }
        }

        public bool SaveAssetProfil(AssetViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
               
                var asset = db.Assets.Where(x => x.Id == model.Id).FirstOrDefault();
                if (asset != null)
                {
                    //Profile
                    //asset.Age = model.Age;
                    asset.DOB = model.DOB;
                    asset.ASex = model.IsMan;

                    asset.Title = model.Title;
                    asset.Description = model.Description;
                    asset.IsOnline = model.IsOnline;
                    asset.OnlineExpires = model.OnlineExpires;
                    asset.CalendarActive = model.CalendarActive;
                    asset.IsActive = model.IsActive;

                    asset.CategoryId = model.CategoryId;
                    asset.precio = model.Precio;
                    asset.APrefId = model.PreferenceId;
                    asset.MaxDistance = model.MaxDistance;

                    //Bank Info
                    if (model.BankId > 0)
                        asset.BankId = model.BankId;
                    asset.AccountNum = model.AccountNum;
                    asset.AccountNum2 = model.AccountNum2;
                    asset.BankHolderName = model.BankHolderName;

                    asset.Latitude = model.Latitude;
                    asset.Longitude = model.Longitude;
                    asset.LocationUpdate = model.LocationUpdate;

                    db.SaveChanges();
                    return true;
                }
                else
                    return false;

            }
        }

       

    }
}