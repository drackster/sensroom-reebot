﻿using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ProjectX.API
{
    public partial class soporte : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {

            txtUsuario.Text.Trim();
            txtContacto.Text.Trim();
            txtComentario.Text.Trim();

            if (txtUsuario.Text == string.Empty ||
            txtContacto.Text == string.Empty ||
            txtComentario.Text == string.Empty)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Por favor llene los campos requeridos')", true);
                return;
            }

            Soporte Comentarios = new Soporte();


            Comentarios.Usuario = txtUsuario.Text;
            Comentarios.Contacto = txtContacto.Text;
            Comentarios.Comentario = txtComentario.Text;
            Comentarios.Respuesta = "";
            try
            {
               var Result =  Business.CustomerBC.ComentariosSoporte(Comentarios);

                GridView1.DataSource = Result;
                GridView1.DataBind();
            }
            catch (Exception ex)
            {

            }

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Recibimos tus comentarios, nos comunicaremos a la brevedad')", true);
        }
    }
}