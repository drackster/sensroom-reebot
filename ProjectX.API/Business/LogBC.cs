﻿using System;
using System.Diagnostics;
using System.IO;

namespace ProjectX.API.Business
{
    public class LogBC
    {
        private static string GetCurrentDT { get { return String.Format("{0:HH:mm:ss}:",DateTime.UtcNow); } }
        public static void Log(string Message, EventLogEntryType et = EventLogEntryType.Information)
        {
            #if DEBUG
            Debug.WriteLine(GetCurrentDT + Message);
#else
            FileWrite(GetCurrentDT + Message);
            
            //var log = GetLogInstance();
            //log.WriteEntry(Message, et);
#endif

        }
        public static void LogException(Exception ex)
        {
            #if DEBUG
            Debug.WriteLine(GetCurrentDT + "Error! " + ex.Message);
#else
            FileWrite(GetCurrentDT + "---->[EXCEPTION]" + ex.Message);
            //var log = GetLogInstance();
            //log.WriteEntry(ex.Message, EventLogEntryType.Error);
#endif
        }
        private static EventLog GetLogInstance()
        {
            string Source = "AlcioneAPI";
            string LogName = "AlcioneAPILog";
            if (!EventLog.SourceExists(Source))
                EventLog.CreateEventSource(Source, LogName);

            EventLog log = new EventLog(LogName);
            log.Source = Source;
            log.Log = "Test";
            return log;
        }

        private static void FileWrite(string Message)
        {
            try
            {
                string logDate = String.Format("{0:yyyyMMdd}.txt",DateTime.UtcNow.Date);
                string logPath = System.Configuration.ConfigurationManager.AppSettings["LogFilePath"];
                File.AppendAllText(logPath + logDate, Message + "\r\n");
                //File.AppendAllText(@"c:\tmp\logs\" + logDate, Message + "\r\n");
            }
            catch
            {
                //IGNORE
            }
         }

    }
}