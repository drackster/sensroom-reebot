﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.ViewModels;
using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using ProjectX.API.Models;
using System.IO;

namespace ProjectX.API.Business
{
    public class CustomerBC
    {
        #region Account
        public static UserViewModel CreateCustomer(string Id, string email)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var defaults = db.XConfigs.FirstOrDefault();
                var newCustomer = db.Customers.Add(new Customer()
                {
                    UserId = Id,
                    MinAge = defaults.MinAge,
                    MaxAge = defaults.MaxAge,
                    MinDistance = defaults.MinDistance,
                    MaxDistance = defaults.MaxDistance,
                    CreatedOn =DateTime.UtcNow,
                    Calificacion = 5
                });
                db.SaveChanges();

                return new UserViewModel
                {
                    Id = newCustomer.Id,
                    UserName = email,
                    IsAuthenticated = true,
                    SearchFilters = new SearchModel
                    {
                        Id = Guid.NewGuid().ToString(),
                        MinAge = defaults.MinAge,
                        MaxAge = defaults.MaxAge,
                        //MinDistanceRadius = defaults.MinDistance,
                        MaxDistanceRadius = defaults.MaxDistance,
                        orderType = OrderType.ASAP,//default
                        sortType = SortType.Distance//default
                    }
                };
            }

        }
        public static void UpdateCustomer(long CustomerId, string ProviderId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var customer = db.Customers.Where(x => x.Id == CustomerId).FirstOrDefault();
                customer.UpdatedOn =DateTime.UtcNow;
                customer.ProviderId = ProviderId;
                db.SaveChanges();
            }
        }
        #endregion
        public static UserViewModel GetCustomerViewModel(string uId)
        {
            //var uId = Helpers.Helper.GetId();
            using (var db = DBHelper.GetDatabase())
            {
                var c = db.Customers.Include("ACategory").Include("AspNetUser")
                    .Where(x => x.UserId == uId).FirstOrDefault();
                if (c != null)
                {
                    return new UserViewModel
                    {
                        Id = c.Id,
                        UserName = c.AspNetUser.UserName,
                        SearchFilters = new SearchModel()
                        {
                            Id = Guid.NewGuid().ToString(),
                            MinAge = c.MinAge,
                            MaxAge = c.MaxAge,
                            CategoryId = c.ACategoryId,
                            //MinDistanceRadius = c.MinDistance,
                            MaxDistanceRadius = c.MaxDistance,
                            orderType = OrderType.ASAP
                        }
                    };
                }
                return null;
            }
        }
        public static XSettingsViewModel GetXSettings()
        {
            using (var db = DBHelper.GetDatabase())
            {
                var x = db.XConfigs.FirstOrDefault();
                return new XSettingsViewModel
                {
                    Categories = Business.AssetBC.GetCategories(),
                    Distances = Business.AssetBC.GetDistances(),
                    Preferences = Business.AssetBC.GetPreferences(),
                    MinAge = x.MinAge,
                    MaxAge = x.MaxAge,
                    ScheduleFutureDays = x.ScheduleFutureDays,
                    ScheduleFutureMinutes = x.ScheduleFutureMinutes,
                    ReservationAfterMinutes = x.ReservationAfterMinutes
                };
            }
        }

        public static bool SaveCustomerCredit(CustomerCredit model)
        {
            //var aspnetUser = Helpers.Helper.GetAspnetUser();
            //model.UserId = aspnetUser.UserName;
            using (var db = DBHelper.GetDatabase())
            {
                //     var newCustomer = db.Customers.Add(new Customer()

                var newCustomerCredit = db.tCreditos.Add(new tCredito()
                {
                    UserId = model.UserId,
                    NoOrden = model.NoOrden,
                    Monto = model.Monto,
                    Credito = model.Credito,
                    CreatedOn =DateTime.UtcNow
                    
                });
                try
                {

                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
            return true;
        }



        public static bool UpdateCustomerCredit(string name)
        {
            var aspnetUser = Helpers.Helper.GetAspnetUser();
          //  model.UserId = aspnetUser.UserName;
            using (var db = DBHelper.GetDatabase())
            {
                var CustomerCredit = db.tCreditos.Where(x => x.UserId == name).ToList();
                if (CustomerCredit != null)
                {
                    foreach(var credit in CustomerCredit)
                    {
                        credit.Credito = false;
                    }
                }           
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                }
            }
            return true;
        }

        public static List<tComentario> ComentariosSoporte(Soporte model)
        {
            
            using (var db = DBHelper.GetDatabase())
            {
                var nc = db.tComentarios.Add(new tComentario()
                {
                    Usuario = model.Usuario,
                    Contacto = model.Contacto,
                    Comentario = model.Comentario,
                    Respuesta = model.Respuesta
                });

                try
                {
                    db.SaveChanges();
                    var result = db.tComentarios.ToList();
                    return result;
                }
                catch (Exception ex)
                {
                    return null;
                }
                

               



            }
        }
    }
}