﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using System.Configuration;
//using System.IO;
//using System.Web.Script.Serialization;
//using SendGrid;
//using SendGrid.Helpers.Mail;
//using ProjectX.ViewModels;
//using System.Text;
//using System.Threading.Tasks;
//using ProjectX.API.Helpers;
//using ProjectX.API.Models;

//namespace ProjectX.API.Business
//{
//    public static class EmailBC
//    {
//        private static string SendGridAPIKey = ConfigurationManager.AppSettings["sendgridapikey"].ToString();
//        private static string htmlRow { get { return "<tr style=\"margin-top: 10px;bgcolor:%BGCOLOR%;\"><td>%CODIGO%</td><td>%DESC%</td><td style=\"text-align: center;\"> %CANT%</td><td style=\"text-align: right;\">%SUBTOTAL%</td></tr>"; } }

//        public static string CreateEmailBody(OrderViewModel Order, string CustomerEmail, string AXAccountId)
//        {
//            int count = 0;
//            string rowColor = null;
//            string siteUrl = ConfigurationManager.AppSettings["ImagesUrl"];

//            //No funciona en job, el job no tiene acceso a system.web
//            //var path = System.Web.HttpContext.Current.Server.MapPath("Alcione-Confirm.html");

//            string path = ConfigurationManager.AppSettings["TemplateFilePath"];
//            if(CustomerEmail != null)
//                path += "Alcione-Confirm-Sales.html";
//            else
//                path += "Alcione-Confirm.html";
//            var htmlTemplate = File.ReadAllText(path);

//            htmlTemplate = htmlTemplate.Replace("%LOGOURL%", siteUrl.Replace("catalog/","logo-alcione.png"));

//            htmlTemplate = htmlTemplate.Replace("%ORDEN#%", "Cotización No. " + Order.documentId);
//            htmlTemplate = htmlTemplate.Replace("%FACT_RFC%", Helper.ToTitleCase(Order.BillToAddress.Name));
//            htmlTemplate = htmlTemplate.Replace("%FACT_CALLE%", Helper.ToTitleCase(Order.BillToAddress.Address));
//            htmlTemplate = htmlTemplate.Replace("%FACT_CIUDAD%", Helper.ToTitleCase(Order.BillToAddress.City));
//            htmlTemplate = htmlTemplate.Replace("%FACT_ESTADO%", Helper.ToTitleCase(Order.BillToAddress.Address3));

//            htmlTemplate = htmlTemplate.Replace("%SHIP_RFC%", Helper.ToTitleCase(Order.ShipToAddress.Name));
//            htmlTemplate = htmlTemplate.Replace("%SHIP_CALLE%", Helper.ToTitleCase(Order.ShipToAddress.Address));
//            htmlTemplate = htmlTemplate.Replace("%SHIP_CIUDAD%", Helper.ToTitleCase(Order.ShipToAddress.City));
//            htmlTemplate = htmlTemplate.Replace("%SHIP_ESTADO%", Helper.ToTitleCase(Order.ShipToAddress.Address3));

//            //V1.2 COMENTARIOS Y REFERENCIA

//            var reference = Order.ReferenceNo;
//            var comments = Order.ShipToAddress.Comment;

//            reference = string.IsNullOrEmpty(reference) ? "" : "<b>Referencia:</b> " + reference + "";
//            comments = string.IsNullOrEmpty(comments) ? "" : "<p><b>Comentarios:</b> " + comments + "</p>";

//            htmlTemplate = htmlTemplate.Replace("%REFERENCIA%", reference);
//            htmlTemplate = htmlTemplate.Replace("%COMENTARIOS%", comments);


//            //PARA VENDEDORES
//            if (CustomerEmail != null)
//            {
//                htmlTemplate = htmlTemplate.Replace("%CUSTOMEREMAIL%", CustomerEmail);
//                htmlTemplate = htmlTemplate.Replace("%AXACCOUNT%", AXAccountId);
//            }

//            StringBuilder body = new StringBuilder();
//            foreach (SaleLineViewModel line in Order.SalesLines)
//            {
//                count = count + 1;
//                rowColor = count % 2 == 0 ? "#c5c5c5" : "#FFFFFF";
//                string row = htmlRow.Replace("%BGCOLOR%", rowColor);
//                row = row.Replace("%CODIGO%", line.Id);
//                row = row.Replace("%DESC%", line.Name);
//                row = row.Replace("%CANT%", String.Format("{0:N2}", line.Quantity) + " " + line.Unit);
//                row = row.Replace("%SUBTOTAL%", String.Format("{0:C2}",line.SubTotal));
//                body.Append(row);
//            }
//            htmlTemplate = htmlTemplate.Replace("%LINEITEMS%", body.ToString());

//            htmlTemplate = htmlTemplate.Replace("%TOTALDESC%", String.Format("{0:C2}", Order.TotalLineDiscount));
//            htmlTemplate = htmlTemplate.Replace("%SUBTOTAL%", String.Format("{0:C2}", Order.TotalExclTax));
//            htmlTemplate = htmlTemplate.Replace("%IVA%", String.Format("{0:C2}", Order.TaxAmount));
//            htmlTemplate = htmlTemplate.Replace("%TOTAL%", String.Format("{0:C2}", Order.TotalInclTax));

//            return htmlTemplate;
//        }

//        public static async Task<bool> SendMail(SendGrid.Helpers.Mail.Mail mail)
//        {
//            SendGridAPIClient sg = new SendGridAPIClient(SendGridAPIKey);
//            //SendGrid.CSharp.HTTP.Client.Response response = sg.client.mail.send.post(requestBody: mail.Get());
//            var response = await sg.client.mail.send.post(requestBody: mail.Get());
//            return response.StatusCode == System.Net.HttpStatusCode.Accepted ? true : false;

//        }

//        public static bool SendMail(string EmailAddress, string Body, string OrderNumber, Personalization CCs)
//        {
//            try
//            {
//                var fromEmailAddress = ConfigurationManager.AppSettings["fromEmailAddress"].ToString();
//                var fromEmailDisplayName = ConfigurationManager.AppSettings["fromEmailDisplayName"].ToString();
//                Email emailFrom = new Email(fromEmailAddress, fromEmailDisplayName);
//                Email emailTo = new Email(EmailAddress, EmailAddress);

//                Content content = new Content("text/html", Body);
//                Mail mail = new Mail(emailFrom, "Confirmación de cotización #" + OrderNumber, emailTo, content);

//                if(CCs != null) //Copiar multiples recipientes
//                    mail.Personalization.Add(CCs);

//                return SendMail(mail).Result;

//            }
//            catch (Exception ex)
//            {
//                throw ex;
//            }
//        }

//        public static void CreateOrderLog(string customerEmail, string orderId)
//        {
//            using (var db = DBHelper.GetDatabase())
//            {
//                var newOrderLog = db.OrderLogs.Create();
//                newOrderLog.UserId = customerEmail;
//                newOrderLog.OrderId = orderId;
//                newOrderLog.CreatedOn =DateTime.UtcNow;
//                db.OrderLogs.Add(newOrderLog);
//                db.SaveChanges();
//            }
//        }

//        public static void ProcessOrderLog()
//        {
//            bool isProduction = System.Configuration.ConfigurationManager.AppSettings["SendEmails"].ToString().ToLower() == "true" ? true : false;
//            using (var db = DBHelper.GetDatabase())
//            {
//                var pendingEmails = db.OrderLogs.Where(o => o.SentOn == null && o.Tries <= 3).ToList();
//                foreach (OrderLog order in pendingEmails)
//                {
//                    order.Tries += 1;
//                    try
//                    {
//                        OrderViewModel orderData = Business.OrderBC.GetOrderWithSalesLines(order.OrderId);
//                        //PRIMERO SE LO ENVIAMOS AL CLIENTE
//                        var mailBody = Business.EmailBC.CreateEmailBody(orderData, null, null);
//                        string recipent = isProduction ? order.UserId : "victor.arce@daltum.mx";//TODO
//                        //test
//                        var mailResult = Business.EmailBC.SendMail(recipent, mailBody, order.OrderId, null);
//                        //If email was sent, no errors
//                        if(mailResult)
//                            order.SentOn =DateTime.UtcNow;

//                        //AHORA OBTENEMOS LA LISTA DE VENDEDORES, GERENTES, ETC
//                        if(isProduction)// && mailResult)
//                        {

//                            //Obtener el AX Account No.
//                            var AXAccount = db.AspNetUsers.Where(u => u.UserName == order.UserId).FirstOrDefault();

//                            if (AXAccount != null)
//                            {
//                                var AXAccountId = AXAccount.AXAccountId;
//                                var recipents = Business.UserBC.GetEmailRecipents(AXAccountId);
//                                mailBody = Business.EmailBC.CreateEmailBody(orderData, order.UserId, AXAccountId);
//                                recipent = "ga@alcione.mx"; //main
//                                Personalization CCs = new Personalization();
//                                CCs.AddTo(new Email(recipent));

//                                //test
//                                //recipent = "victor.arce@daltum.mx";
//                                //CCs.AddTo(new Email("victor.arce@daltum.mx"));
//                                //CCs.AddCc(new Email("victorarce@gmail.com"));
//                                //CCs.AddCc(new Email("victor@telirium.com"));

//                                foreach (string email in recipents)
//                                    if (!string.IsNullOrEmpty(email) && email.Contains("@"))
//                                    {
//                                        CCs.AddCc(new Email(email));
//                                        //try
//                                        //{
//                                            //mailResult = Business.EmailBC.SendMail(email, mailBody, order.OrderId, null);
//                                        //}
//                                        //catch
//                                        //{

//                                        //}
//                                    }

//                                System.Threading.Thread.Sleep(3000);//Esperar unos segundos antes de enviar el siguiente correo, puede que esto
//                                                 //este previniendo a que se envien correctamente todos los correos
//                                                 //enviar a todos en alcione, gerente, vendedores, etc
//                                mailResult = Business.EmailBC.SendMail(recipent, mailBody, order.OrderId, CCs);

//                            }
//                        } 

//                    }
//                    catch(Exception ex)
//                    {
//                        Business.LogBC.LogException(ex);
//                    }
//                }
//                if (pendingEmails.Count > 0)
//                {
//                    db.SaveChanges();
//                }

//            }
//        }

//    }
//}