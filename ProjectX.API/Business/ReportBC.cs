﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Web;
//using Alcione.ViewModels;
//using ProjectX.API.Helpers;
//using ProjectX.API.Models;
//using System.Threading.Tasks;
//using System.Collections.ObjectModel;
//using NinjaNye.SearchExtensions;
//using System.Security.Claims;
//using System.Globalization;
//using iTextSharp.text;
//using iTextSharp.text.pdf;
//using System.Data.Entity;
//using System.IO;

//namespace ProjectX.API.Business
//{
//    public class ReportBC
//    {

//        public static List<ProjectX.API.Models.Action> GetActionCatalog()
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                return db.Actions.OrderBy(x => x.Name).ToList();
//            }
//        }

//        public static List<ActiveUserViewModel> GetActiveConnections()
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                DateTime dt =DateTime.UtcNow.AddMinutes(-5);
//                //obtener activos los ultimos 5 minutos
//                var usrs = db.LoginUserLogs.Include(u => u.AspNetUser).Where(x => x.LastActionTime >= dt).ToList();
//                dt =DateTime.UtcNow;

//                return usrs.Select(x => new ActiveUserViewModel()
//                {
//                    Email = x.AspNetUser.UserName,
//                    UserId = x.AspNetUser.Id,
//                    AXAccount = x.AspNetUser.AXAccountId,
//                    LastSeen = x.LastActionTime,
//                    MinutesAgo = "Hace " + string.Format("{0:n1}", dt.Subtract(x.LastActionTime).TotalMinutes) + " minutos"
//                }).ToList();
//            }
//        }

//        public static List<ActiveUserViewModel> GetActiveUsers(DateTime StartDate, DateTime EndDate)
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {

//                List<ActiveUserViewModel> result = new List<ActiveUserViewModel>();

//                //Obtener ordenes hechas en el periodo
//                var orders = (from t in db.OrderLogs.Where(x => x.CreatedOn >= StartDate && x.CreatedOn <= EndDate)
//                              group t by new { t.UserId }
//                              into grp
//                              select new
//                              {
//                                  grp.Key.UserId,
//                                  Total = grp.Count()
//                              }).ToList();

//                DateTime dt =DateTime.UtcNow;
//                var usrs = db.LoginUserLogs.Include(u => u.AspNetUser).ToList();

//                foreach (var o in orders)
//                {
//                    var lst = usrs.Where(x => x.AspNetUser.UserName == o.UserId).ToList();
//                    if (lst.Count > 0)
//                    {
//                        var x = lst[lst.Count - 1];

//                        result.Add(new ActiveUserViewModel()
//                        {
//                            Email = x.AspNetUser.UserName,
//                            UserId = x.AspNetUser.Id,
//                            AXAccount = x.AspNetUser.AXAccountId,
//                            LastSeen = x.LastActionTime,
//                            TotalOrders = o.Total,
//                            MinutesAgo = "Hace " + string.Format("{0:n2}", Math.Abs(x.LastActionTime.Subtract(dt).TotalDays)) + " días"
//                        });
//                    }
//                }
//                return result;
//            }
//        }

//        public static List<UserActionLogViewModel> GetActionRank(DateTime StartDate, DateTime EndDate)
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                var actions = (from t in db.UserActionLogs.Include(z => z.Action).Where(x => x.ActionDate >= StartDate && x.ActionDate <= EndDate)
//                               group t by new { t.ActionDate, t.Action.Name }
//                               into grp
//                               select new
//                               {
//                                   grp.Key.ActionDate,
//                                   grp.Key.Name,
//                                   Total = grp.Sum(t => t.ActionCount)
//                               }).ToList();

//                return actions.Select(x => new UserActionLogViewModel()
//                {
//                    ActionDate = x.ActionDate,
//                    ActionName = x.Name,
//                    ActionCount = x.Total
//                }).OrderByDescending(x => x.ActionDate).ToList();
//            }
//        }

//        public static List<ProductActivityLogViewModel> GetProductRank(DateTime StartDate, DateTime EndDate)
//        {
//            List<ProductActivityLogViewModel> result = new List<ProductActivityLogViewModel>();

//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                var actions = (from t in db.ProductActivityLogs.Where(x => x.ActivityDate >= StartDate && x.ActivityDate <= EndDate)
//                               group t by new { t.ActivityDate, t.ProductId }
//                               into grp
//                               select new
//                               {
//                                   grp.Key.ActivityDate,
//                                   grp.Key.ProductId,
//                                   Total = grp.Sum(t => t.ViewCount)
//                               }).ToList();

//                result = actions.Select(x => new ProductActivityLogViewModel()
//                {
//                    ActivityDate = x.ActivityDate,
//                    ProductId = x.ProductId,
//                    ViewCount = x.Total
//                }).OrderByDescending(x => x.ActivityDate).ToList();

//            }

//            var pCat = CacheBC.GetProductCatalog();
//            foreach (var r in result)
//            {
//                var product = pCat.Where(p => p.Id == r.ProductId).FirstOrDefault();
//                if (product != null) r.Name = product.Name.Substring(0, 40) + "...";
//                else r.Name = string.Empty;
//            }

//            return result;
//        }

//        public static List<SearchActivityLogViewModel> GetSearchRank(DateTime StartDate, DateTime EndDate)
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                var actions = (from t in db.SearchActivityLogs.Where(x => x.ActivityDate >= StartDate && x.ActivityDate <= EndDate)
//                               group t by new { t.ActivityDate, t.SearchTerm }
//                               into grp
//                               select new
//                               {
//                                   grp.Key.ActivityDate,
//                                   grp.Key.SearchTerm,
//                                   Total = grp.Sum(t => t.SearchCount)
//                               }).ToList();

//                return actions.Select(x => new SearchActivityLogViewModel()
//                {
//                    ActivityDate = x.ActivityDate,
//                    SearchTerm = x.SearchTerm,
//                    ViewCount = x.Total
//                }).OrderByDescending(x => x.ActivityDate).ToList();

//            }
//        }

//        public static List<CategoryUserLogViewModel> GetCategoryRank(DateTime StartDate, DateTime EndDate)
//        {
//            List<CategoryUserLogViewModel> result = new List<CategoryUserLogViewModel>();
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {

//                if (StartDate != null && EndDate != null)
//                {
//                    result = db.CategoryUserLogs.Where(x => x.ActivityDate >= StartDate && x.ActivityDate <= EndDate)
//                     .Select(x => new CategoryUserLogViewModel()
//                     {
//                         ActivityDate = x.ActivityDate,
//                         Combined = x.SegmentId.ToString() + "-" + x.FamilyId.ToString() + "-" + x.GroupId.ToString(),
//                         Segment = x.SegmentId,
//                         Family = x.FamilyId,
//                         Group = x.GroupId,
//                         ViewCount = x.ViewCount
//                     }).ToList();


//                    var cats = (from t in result
//                                group t by new { t.ActivityDate, t.Combined }
//                                   into grp
//                                select new
//                                {
//                                    grp.Key.ActivityDate,
//                                    grp.Key.Combined,
//                                    SegmentId = grp.First().Segment,
//                                    FamilyId = grp.First().Family,
//                                    GroupId = grp.First().Group,
//                                    Total = grp.Sum(t => t.ViewCount)
//                                }).ToList();

//                    result = cats.Select(x => new CategoryUserLogViewModel()
//                    {
//                        ActivityDate = x.ActivityDate,
//                        Segment = x.SegmentId,
//                        Family = x.FamilyId,
//                        Group = x.GroupId,
//                        ViewCount = x.Total
//                    }).OrderByDescending(x => x.ViewCount).ToList();


//                }



//                var cachedCat = CacheBC.GetCategoryCatalog();
//                foreach (var r in result)
//                {
//                    var s = cachedCat.Where(c => Int32.Parse(c.Id) == r.Segment).FirstOrDefault();
//                    if (s != null)
//                    {
//                        var f = s.SubCategories.Where(c => Int32.Parse(c.Id) == r.Family).FirstOrDefault();
//                        if (f != null)
//                        {
//                            var g = f.SubCategories.Where(c => Int32.Parse(c.Id) == r.Group).FirstOrDefault();

//                            if (g != null)
//                            {
//                                r.Combined = g.Name;
//                            }
//                        }
//                    }
//                }

//                return result;

//            }
//        }

//        public static List<UniqueSessionViewModel> GetSessionRank(DateTime StartDate, DateTime EndDate)
//        {
//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                var sessions = db.LoginUserLogs.Where(x => x.LoginTime >= StartDate && x.LoginTime <= EndDate)
//                    .Select(x => new UniqueSessionViewModel
//                    {
//                        SessionDate = x.LoginTime,
//                        UserId = x.UserId,
//                        LastAction = x.LastActionTime
//                    }).ToList();

//                foreach (var s in sessions)
//                {
//                    s.AverageTime = s.LastAction.Subtract(s.SessionDate).TotalSeconds;
//                    s.SessionDate = s.SessionDate.Date;
//                }

//                var UniqueSessions = (from t in sessions
//                                      group t by new { t.SessionDate, t.UserId }
//                                        into grp
//                                      select new
//                                      {
//                                          grp.Key.SessionDate,
//                                          grp.Key.UserId,
//                                          AvgSessionTime = grp.Average(x => x.AverageTime)
//                                      }).ToList();


//                var TotalSessions = (from t in sessions
//                                     group t by new { t.SessionDate }
//                                      into grp
//                                     select new
//                                     {
//                                         grp.Key.SessionDate,
//                                         SessionCount = grp.Count()
//                                     }).ToList();


//                var result = TotalSessions.Select(x => new UniqueSessionViewModel()
//                {
//                    SessionDate = x.SessionDate,
//                    SessionCount = x.SessionCount,
//                }).OrderByDescending(x => x.SessionDate).ToList();

//                foreach (var r in result)
//                {
//                    var us1 = UniqueSessions.Where(x => x.SessionDate == r.SessionDate);
//                    r.UniqueSessionCount = us1.GroupBy(x => x.UserId).Count();
//                    r.AverageTime = us1.Average(x => x.AvgSessionTime);
//                }

//                return result;
//            }
//        }

//        public static List<UniqueSessionViewModel> GetConversionRank(DateTime StartDate, DateTime EndDate)
//        {
//            var sessions = GetSessionRank(StartDate, EndDate);

//            using (var db = Helpers.DBHelper.GetDatabase())
//            {
//                var orders = db.OrderLogs.Where(x => x.CreatedOn >= StartDate && x.CreatedOn <= EndDate).ToList();
//                foreach (var order in orders)
//                    order.CreatedOn = order.CreatedOn.Date;

//                foreach (var session in sessions)
//                {
//                    session.OrderCount = orders.Where(x => x.CreatedOn == session.SessionDate).Count();
//                    session.ConversionRate = session.UniqueSessionCount == 0 ? 0 : (session.OrderCount / session.UniqueSessionCount) * 100;
//                }

//                return sessions;
//            }
//        }
//    }
//}