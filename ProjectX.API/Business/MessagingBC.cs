﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.ViewModels;
using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using ProjectX.API.Models;

namespace ProjectX.API.Business
{
    public class MessagingBC
    {
        public static List<NotificationViewModel> GetNotifications(NotificationRequestModel model, bool ProcessInvite=true, bool ProcessRequest=true)
        {
            List<NotificationViewModel> nots = new List<NotificationViewModel>();
            int cTypeId = 0;
            using (var db = DBHelper.GetDatabase())
            {
                List<Notification> result = null;
                IQueryable<Notification> q = null;

                int mType = model.MType == MessageType.All ? -1 : (int)model.MType;

                #region Main Select
                //Este es para obtener la lista lista chats de un nuemero de orden
                if (model.OrderId.HasValue)
                {
                    q = db.Notifications.Include("Customer")
                    .Include("Asset").Where(x => x.OrderId == model.OrderId);
                }
                else if (model.SType == MessageSender.Customer)
                    q = db.Notifications.Include("Customer")
                        .Include("Asset").Include("Order.ACategoryTier").Where(x => x.CustomerId == model.CustomerId);

                else if (model.SType == MessageSender.Asset)
                    q = db.Notifications.Include("Customer")
                        .Include("Asset").Include("Order.ACategoryTier").Where(x => x.AssetId == model.AssetId);

                if (model.StartDate.HasValue)
                {
                    q = q.Where(x => x.EventDateTime.HasValue && x.EventDateTime.Value >= model.StartDate);
                }

                //Solo los menores a 7 dias si es chat, caso contrario sólo las notificaciones de 2 hrs de antigüedad
                DateTime dtn;
                if (model.IsChat)
                {
                    dtn =DateTime.UtcNow.AddDays(-7).Date;
                    q = q.Where(x => x.CreatedOn >= dtn);
                }
                else
                {
                    dtn =DateTime.UtcNow.AddHours(-2);
                    q = q.Where(x => x.CreatedOn >= dtn);
                }

                #endregion

                #region Chat

                if (mType != -1 && !model.OrderId.HasValue)
                    q = q.Where(x => x.MType == mType);

                else if (!model.IsChat) //si es chat, obtener ambas ntoficaciones
                {
                    //notificaciones para chica, filtrar los
                    //mensajes que ella misma envió
                    if (model.SType == MessageSender.Asset)
                    {
                        cTypeId = (int)MessageType.AssetMessage;
                        q = q.Where(x => x.MType != cTypeId);
                    }
                    else if (model.SType == MessageSender.Customer)
                    {
                        cTypeId = (int)MessageType.CustomerMessage;
                        q = q.Where(x => x.MType != cTypeId);
                    }

                    if (model.LastId == 0)
                        result = q.OrderByDescending(x => x.Id).ToList();
                    else
                        result = q.Where(x => x.Id > model.LastId).OrderByDescending(x => x.Id).ToList();

                }
                else if (model.IsChat)
                {
                    int cTypeId1 = (int)MessageType.AssetMessage;
                    int cTypeId2 = (int)MessageType.CustomerMessage;
                    q = q.Where(x => x.MType == cTypeId1 || x.MType == cTypeId2);

                    //el chat es ascending
                    if (model.LastId == 0)
                        result = q.OrderBy(x => x.Id).ToList();
                    else
                        result = q.Where(x => x.Id > model.LastId).OrderBy(x => x.Id).ToList();

                }
                #endregion

                #region Final Select

                //Todas las que no sean autorizaciones
                nots = result.Where(x => x.MType != 0).Select(y => new NotificationViewModel
                {
                    Id = y.Id,
                    MTypeStr = ViewModels.Helper.GetMessageTitle((MessageType)y.MType, (MessageStatus)y.MessageStatus),

                    MessageText = (MessageType)y.MType == MessageType.AssetMessage
                    || (MessageType)y.MType == MessageType.CustomerMessage ? y.MessageText :
                    ViewModels.Helper.GetMessageText((MessageType)y.MType, model.SType,
                    ViewModels.Helper.GetMessageSenderName(model.SType, y.Customer.AspNetUser.UserName, y.Asset.Name, model.IsChat, (MessageType)y.MType), false),

                    MType = (MessageType)y.MType,
                    MReceiver = (MessageSender)y.MReceiver,
                    CreatedOn = y.CreatedOn,
                    Status = (MessageStatus)y.MessageStatus,
                    MessageStatusId = y.MessageStatus,
                    OrderId = y.OrderId,
                    //TierName = y.Order == null ? null : y.Order.ACategoryTier.Name,

                    SenderName = ViewModels.Helper.GetMessageSenderName(model.SType, y.Customer.AspNetUser.UserName, y.Asset.Name, model.IsChat, (MessageType)y.MType),

                    AssetId = y.AssetId,
                    CustomerId = y.CustomerId,
                }).ToList();


                //el id de Autorizaciones es 0
                var reqs = result.Where(x => x.MType == 0).ToList();
                foreach (var y in reqs)
                {
                    nots.Add(new NotificationViewModel
                    {
                        Id = y.Id,
                        MTypeStr = ViewModels.Helper.GetMessageTitle((MessageType)y.MType, (MessageStatus)y.MessageStatus),
                        MType = (MessageType)y.MType,
                        MReceiver = (MessageSender)y.MReceiver,
                        CreatedOn = y.CreatedOn,
                        Status = (MessageStatus)y.MessageStatus,
                        MessageStatusId = y.MessageStatus,
                        AssetId = y.AssetId,
                        CustomerId = y.CustomerId,
                        MessageText = ViewModels.Helper.GetRequestText(y.UserRequestLog.UserName, y.UserRequestLog.Status),
                        RequestId = y.UserRequestLog.Id,
                        RequestActivationStatus = y.UserRequestLog.Status,
                        SenderName = y.UserRequestLog.UserName
                    });
                }
                #endregion

            }

            #region Message Group
            //Nuevo, agrupar mensajes de chat por orden
            if (cTypeId > 0)
            {
                var grouped = (from o in nots.Where(x => x.MType == MessageType.AssetMessage ||
                                                    x.MType == MessageType.CustomerMessage)
                               group o by new { o.OrderId }
                                    into grp
                               select new
                               {
                                   grp.Key.OrderId,
                                   Total = grp.Count()
                               }).ToList();


                foreach (var g in grouped)
                {
                    //quitar todos menos uno de cada grupo de ordenes
                    var ns = nots.Where(n => (n.MType == MessageType.AssetMessage || n.MType == MessageType.CustomerMessage)
                    && n.OrderId == g.OrderId).ToList();
                    if (ns.Count > 1)//si solo tiene un mensaje de la orden, dejarlo asi
                    {
                        var last = ns[0];
                        for (int r = 1; r < ns.Count; r++)
                            nots.Remove(ns[r]);
                        last.MessageText = "Tienes " + g.Total + " mensajes";
                    }
                }


            }
            #endregion

            #region Add Invite
            if (ProcessInvite)
            {
                List<InvitationViewModel> invites = new List<InvitationViewModel>();
                foreach (var n in nots)
                {
                    if (n.MType == MessageType.Invitation 
                        || n.MType == MessageType.AcceptedInvitation
                        || n.MType == MessageType.CanceledInvitation 
                        || n.MType == MessageType.RejectedInvitation)
                    {
                        var inv = invites.Where(x => x.OrderId == n.OrderId.Value).FirstOrDefault();
                        if (inv != null)
                        {
                            n.invitation = inv;
                        }
                        else
                        {
                            var ninv = GetInvitation(n.OrderId.Value, model.SType);
                            invites.Add(ninv);
                            n.invitation = ninv;
                        }

                    }

                }
            }
            #endregion

 
            #region Final Sort
            if (!model.IsChat)
            {
                nots = nots.OrderByDescending(x => x.CreatedOn).ToList();
            }
            #endregion

            #region Filter out rating customer notifications
            if (model.SType == MessageSender.Customer)
                nots = nots.Where(x => x.MType != MessageType.Rating).ToList();
            #endregion


            return nots;
        }
        public static NotificationViewModel AddNotificationViewModel(NotificationViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var y = db.Notifications.Add(new Notification
                {
                    MType = (int)model.MType,
                    MReceiver = (int)model.MReceiver,
                    AssetId = model.AssetId,
                    CustomerId = model.CustomerId,
                    OrderId = model.OrderId,
                    MessageText = model.MessageText,
                    MessageStatus = (int)MessageStatus.Created,
                    CreatedOn =DateTime.UtcNow
                });

                if (model.OrderId != 0)
                {
                    db.SaveChanges();
                }
                else
                { AddNotificationViewModelAdmin(model); }
                                

                string SName = string.Empty;
                MessageSender sender = MessageSender.Customer;
                if(model.MType == MessageType.CustomerMessage)
                {
                    var c = db.Customers.Include("AspNetUser").Where(x => x.Id == model.CustomerId).FirstOrDefault();
                    SName = c.AspNetUser.UserName;
                }
                else if(model.MType == MessageType.AssetMessage)
                {
                    var a = db.Assets.Where(x => x.Id == model.AssetId).FirstOrDefault();
                    SName = a.Name;
                    sender = MessageSender.Asset;
                }

                return new NotificationViewModel
                {
                    Id = y.Id,
                    MTypeStr = ViewModels.Helper.GetMessageTitle((MessageType)y.MType, (MessageStatus)y.MessageStatus),

                    MessageText = (MessageType)y.MType == MessageType.AssetMessage 
                    || (MessageType)y.MType == MessageType.CustomerMessage ? y.MessageText : 
                    ViewModels.Helper.GetMessageText((MessageType)y.MType, sender, SName, false),
                    
                    MType = (MessageType)y.MType,
                    MReceiver = (MessageSender)y.MReceiver,
                    CreatedOn = y.CreatedOn,
                    Status = (MessageStatus)y.MessageStatus,
                    MessageStatusId = y.MessageStatus,
                    OrderId = y.OrderId,
                    SenderName = SName,
                    AssetId = y.AssetId,
                    CustomerId = y.CustomerId
                };
            }
        }
        public static NotificationViewModel AddNotificationViewModelAdmin(NotificationViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var y = db.Notifications.Add(new Notification
                {
                    MType = (int)model.MType,
                    MReceiver = (int)model.MReceiver,
                    AssetId = model.AssetId,
                    CustomerId = model.CustomerId,
                    OrderId = 1,//null,//model.OrderId,
                    MessageText = model.MessageText,
                    MessageStatus = (int)MessageStatus.Created,
                    CreatedOn =DateTime.UtcNow
                });

              
                    db.SaveChanges();
                


                string SName = string.Empty;
                MessageSender sender = MessageSender.Customer;
                if (model.MType == MessageType.CustomerMessage)
                {
                    var c = db.Customers.Include("AspNetUser").Where(x => x.Id == model.CustomerId).FirstOrDefault();
                    SName = c.AspNetUser.UserName;
                }
                else if (model.MType == MessageType.AssetMessage)
                {
                    var a = db.Assets.Where(x => x.Id == model.AssetId).FirstOrDefault();
                    SName = a.Name;
                    sender = MessageSender.Asset;
                }

                return new NotificationViewModel
                {
                    Id = y.Id,
                    MTypeStr = ViewModels.Helper.GetMessageTitle((MessageType)y.MType, (MessageStatus)y.MessageStatus),

                    MessageText = (MessageType)y.MType == MessageType.AssetMessage
                    || (MessageType)y.MType == MessageType.CustomerMessage ? y.MessageText :
                    ViewModels.Helper.GetMessageText((MessageType)y.MType, sender, SName, false),

                    MType = (MessageType)y.MType,
                    MReceiver = (MessageSender)y.MReceiver,
                    CreatedOn = y.CreatedOn,
                    Status = (MessageStatus)y.MessageStatus,
                    MessageStatusId = y.MessageStatus,
                    OrderId = y.OrderId,
                    SenderName = SName,
                    AssetId = y.AssetId,
                    CustomerId = y.CustomerId
                };
            }
        }
        public static void AddNotification(Notification entity)
        {
            using (var db = DBHelper.GetDatabase())
            {
                db.Notifications.Add(entity);
                db.SaveChanges();
            }
        }
        public static InvitationViewModel GetInvitation(long OrderId, MessageSender MType)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var o = db.Orders.Include("Customer.AspNetUser")
                    .Include("Asset.AspNetUser")
                    .Include("CAddress")
                    .Where(x => x.Id == OrderId).FirstOrDefault();
                InvitationViewModel r = new InvitationViewModel();
                r.OrderId = o.Id;
                r.Precio = (decimal)o.Asset.precio;
                r.oType = (OrderType)o.RequestType;
                r.oStatus = (OrderStatus)o.OrderStatus;
                r.dStatus = (DepositStatus)o.DepositStatus;
                r.StatusName = ProjectX.ViewModels.Util.GetOrderStatus(r.oStatus);
                r.CreatedOn = o.CreatedOn;
                r.ExpiresOn = o.ExpiresOn.Value;

                var tier = db.ACategoryTiers.Where(x => x.Id == o.CategoryTierId).FirstOrDefault();
                r.TierName = tier.Name;


                if (o.RequestType == (int)OrderType.ASAP)
                {
                    r.RequestedTime1 = "Lo Antes Posible";
                    //r.RequestedTime2 = "";
                }
                else
                {
                    r.RequestedTime1 =
                    ViewModels.Util.ConvertToFriendlyDT(o.RequestedDateTime.Value, true, true, false, true);
                }
                r.EventDateTime = o.RequestedDateTime;
                r.EventEnd = r.EventDateTime.Value.AddHours(tier.NumHours);

                r.Cost = o.TotalCost;
                r.DownPayment = o.DownPayment;

                r.CLatitude = o.CLatitude;
                r.CLongitude = o.CLongitude;
                r.DistanceFrom = OrderBC.FormatDistance(o.StartingDistance);

                r.AddressId = o.AdressId;
                r.Address = null;
                r.Comments = o.AdressId.HasValue ? "": null;

                List<ReviewViewModel> cReviews = new List<ReviewViewModel>();
                if (MType == MessageSender.Asset)
                {
                    r.CustomerName = o.Customer.AspNetUser.UserName;
                    r.CustomerId = o.CustomerId;
                    cReviews = Business.AssetBC.GetCustomerReviews(r.CustomerId);
                }
                else if (MType == MessageSender.Customer)
                {
                    r.CustomerName = o.Asset.AspNetUser.UserName;
                    r.CustomerId = o.Asset.Id;
                    cReviews = Business.AssetBC.GetAssetReviews(r.CustomerId);
                    if (o.Asset.AssetImages != null && o.Asset.AssetImages.Count > 0)
                        r.AssetCoverImageId = o.Asset.AssetImages.ToList()[0].Id;

                }
                if (cReviews.Count > 0)
                {
                    var review = cReviews[0];
                    r.Review = review.Review == null ? "" : review.Review;
                    if (r.Review.Length > 101)
                        r.Review = r.Review.Substring(0, 100) + "...";

                    r.ReviewDate = review.CreatedOn;
                    r.ReviewerName = review.ReviewerName;
                    r.Rating = review.Rating;

                    r.GlobalRating = Math.Round(cReviews.Average(x => x.Rating),1);
                }
                r.ReviewCount = cReviews.Count;

                r.paymentMethod = (PaymentMethod)o.PaymentMethod;
                r.TransactionId = r.paymentMethod == PaymentMethod.Paypal ? 
                o.PaypalTransId : r.TransactionId = o.ChargeId;
                r.OxxoRef = o.OxxoRef;
                return r;

            }
        }
        public static string FormatAddress(CAddress address)
        {
            string result = string.Empty;
            if (!string.IsNullOrEmpty(address.Address1))
                result += address.Address1;
            if (!string.IsNullOrEmpty(address.Address2))
                result += address.Address2;
            if (!string.IsNullOrEmpty(address.Address3))
                result += address.Address3;

            return result;
        }
        public static bool UpdateNotificationStatus(NotificationUpdateModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var n = db.Notifications.Where(x => x.Id == model.NotificationId).FirstOrDefault();
                if (n != null)
                    n.MessageStatus = (int)model.MsgStatus;
                db.SaveChanges();
                return true;
            }
        }
        public static void UpdateNotificationPushStatus(long NotificationId, NotificationStatus Status)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var n = db.Notifications.Where(x => x.Id == NotificationId).FirstOrDefault();
                if(Status == NotificationStatus.Sent)
                {
                    n.SentOn =DateTime.UtcNow;
                }
                n.SendTries += 1;
                db.SaveChanges();
            }
        }
        public static void ExpireDeviceId(string OldId, string NewId)
        {
            using (var db = DBHelper.GetDatabase())
            {

                //Primero expiramos el ID actual, y si se obtiene el nuevo, entonces agregamos el nuevo
                //La notificacion será enviada en la próxima ejecucion ya con el ID nuevo
                var oldDeviceId = db.DeviceTokens.Where(x => x.Token == OldId).FirstOrDefault();
                if (oldDeviceId != null)
                {
                    oldDeviceId.ExpiredOn =DateTime.UtcNow;
                    //Si existe, agregar nuevo
                    if (!string.IsNullOrEmpty(NewId))
                    {
                        db.DeviceTokens.Add(new DeviceToken
                        {
                             AssetId = oldDeviceId.AssetId,
                             DeviceId = oldDeviceId.DeviceId,
                             Token = NewId,
                             UpdatedOn =DateTime.UtcNow
                        });
                    }
                    db.SaveChanges();
                }

            }
        }
    }
}