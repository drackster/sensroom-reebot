﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.ViewModels;
using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using ProjectX.API.Models;
using System.IO;

namespace ProjectX.API.Business
{
    public class VersionBC
    {
        public static bool GetVersion(string ver)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var versionC = db.VersionControlLuxxApps.Where(u => u.sVersion == ver && u.fBaja == null).FirstOrDefault();
                if (versionC != null)
                {
                    if (versionC.sVersion == ver)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
                 
            }
        }

        public static GetLeyendas GetLeyenda(int id)
        {

            var ley = new GetLeyendas();
            using (var db = DBHelper.GetDatabase())
            {
                var Leyenda = db.tLeyendas.Where(x => x.id == id).FirstOrDefault();
                if (Leyenda != null)
                {

                    ley.id = Leyenda.id;
                    ley.leyenda = Leyenda.leyenda;
                    return ley;

                }
                else
                {
                    return null;
                }

            }
        }

        public static bool GetFiltroPais(string Clave)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var Pais = db.tRestriccionPais.Where(x => x.ClavePais == Clave).FirstOrDefault();
                if (Pais != null)
                {
                    return (bool)Pais.Restrccion;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}