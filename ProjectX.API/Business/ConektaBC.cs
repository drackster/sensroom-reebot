﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Threading.Tasks;
using System.Configuration;
using conekta;
using ProjectX.ViewModels;
using Newtonsoft.Json;
using System.Dynamic;
using Newtonsoft.Json.Linq;
using System.IO;
using Org.BouncyCastle.Asn1.Ocsp;
using System.Web.Mvc;
using System.Net;

namespace ProjectX.API.Business
{
    public class ConektaBC
    {
        internal static readonly DateTime Epoch = DateTime.SpecifyKind(new DateTime(1970, 1, 1, 0, 0, 0, 0), DateTimeKind.Utc);

        public string ConektaPrivateKey = ConfigurationManager.AppSettings["ConektaKeyPrivate"];
        public string ConektaPublicKey = ConfigurationManager.AppSettings["ConektaKeyPublic"];
        public bool apiProduction = Convert.ToBoolean(ConfigurationManager.AppSettings["OpenPayapiProduction"]);

        #region general params
        private string orderName;
        private string customerName;
        private string customerPhone;
        private string customerEmail;
        private string customerAddress;
        private string customerCity;
        private string customerState;
        private string customerPostalCode;
        #endregion
        public ConektaBC(CustomerPaymentViewModel customerName)
        {
            conekta.Api.apiKey = ConektaPrivateKey;
            conekta.Api.version = "2.0.0";
            conekta.Api.locale = "es";
            
            if (customerName == null)
            {
                this.customerName = "Comercializadora";
                customerPhone = "+525546464646";//TODO TBD
                customerEmail = "cliente@dominio.com";// customerName.NameOnCard+"@gmail.com";//TODO TBD
            }
            else
            {
                this.customerName = customerName.Nombre + " " + customerName.Apellidos;// customerName;
                customerPhone = customerName.Telefono;//TODO TBD
                customerEmail = customerName.Email;//TODO TBD
            }
            

            orderName = "TuModa";
            
            customerAddress = "Calle 123 int 2 Col. Mexico";
            customerCity = "Monterrey";
            customerState = "NL";
            customerPostalCode = "66290";
            
        }
        public CustomerPaymentViewModel AddCustomer(CustomerPaymentViewModel model)
        {
            #region Ejemplo de conekta
            //try
            //{
            //    conekta.Customer customer = new conekta.Customer().create(@"{
            //                        ""name"":""Fulanito Pérez"",
            //                        ""email"":""fulanito@conekta.com"",
            //                        ""phone"":""+52181818181"",
            //                        ""payment_sources"":[{
            //                          ""type"": ""card"",
            //                          ""token_id"":""tok_test_visa_4242""
            //    }]
            //  }");
            //}
            //catch (ConektaException e)
            //{
            //    foreach (JObject obj in e.details)
            //    {
            //        System.Console.WriteLine("\n [ERROR]:\n");
            //        System.Console.WriteLine("message:\t" + obj.GetValue("message"));
            //        System.Console.WriteLine("debug:\t" + obj.GetValue("debug_message"));
            //        System.Console.WriteLine("code:\t" + obj.GetValue("code"));
            //    }
            //}
            #endregion

            try
            {
                string request = @"{
                                    ""name"":""%CUSTOMERNAME%"",
                                    ""email"":""%CUSTOMEREMAIL%"",
                                    ""phone"":""%CUSTOMERPHONE%"",
                                    ""payment_sources"":[{
                                      ""type"": ""card"",
                                      ""token_id"":""%CONEKTACARDTOKEN%""
                }]}";

                request = request.Replace("%CONEKTACARDTOKEN%", model.ProviderId);
                ParseCustomerInfo(ref request);
                conekta.Customer customer = new conekta.Customer().create(request);
                var currentPaymentSource = customer.payment_sources[0];//.Where(x => x.last4 == model.CreditCardNumber).FirstOrDefault();
                if(currentPaymentSource==null)
                    return new CustomerPaymentViewModel() { Success = false, ProviderId = "No se pudo procesar la tarjeta con terminación " + model.CreditCardNumber };
                return new CustomerPaymentViewModel() { Success = true, ProviderId = customer.id, CreditCardNumber = currentPaymentSource.id };
            }
            catch (ConektaException e)
            {
                string err = string.Empty;
                foreach (JObject obj in e.details)
                {
                    //System.Console.WriteLine("\n [ERROR]:\n");
                    err += "message:\t" + obj.GetValue("message");
                    err += "debug:\t" + obj.GetValue("debug_message");
                    err += "code:\t" + obj.GetValue("code");
                    System.Diagnostics.Debug.WriteLine(err);
                }
                return new CustomerPaymentViewModel() { Success = false, ProviderId = err };
            }
            catch (Exception ex1)
            {
                System.Diagnostics.Debug.WriteLine(ex1.Message);
                return new CustomerPaymentViewModel() { Success = false, ProviderId = "Error desconocido" };
            }
        }
        public CustomerPaymentViewModel AddPaymentSource(string customerToken, CustomerPaymentViewModel model)
        {
            #region Ejemplo de conekta
            //customer = Conekta::Customer.find("cus_zzmjKsnM9oacyCwV3")
            //source = customer.create_payment_source(type: "card", token_id: "tok_test_visa_4242")
            #endregion

            try
            {
                conekta.Customer customer = new conekta.Customer().find(customerToken);
                if (customer != null)
                {
                    string request = @"{""type"": ""card"",
                                        ""token_id"":""%CONEKTACARDTOKEN%""}";

                    request = request.Replace("%CONEKTACARDTOKEN%", model.ProviderId);//ProviderId contiene el token
                    var pSource = customer.payment_sources[0];//customer.createPaymentSource(request);
                    if (pSource != null && pSource.id!=null)
                    {
                        //sucess
                        return new CustomerPaymentViewModel() { Success = true, ProviderId = pSource.id };
                    }
                }
                return new CustomerPaymentViewModel() { Success = false, ProviderId = "No se pudo procesar la tarjeta con terminación " + model.CreditCardNumber };
            }
            catch (ConektaException e)
            {
                string err = string.Empty;
                string err1 = string.Empty;
                foreach (JObject obj in e.details)
                {
                    //System.Console.WriteLine("\n [ERROR]:\n");
                    err += "message:\t" + obj.GetValue("message");
                    err1 += "message:\t" + obj.GetValue("message");
                    err += "debug:\t" + obj.GetValue("debug_message");
                    err += "code:\t" + obj.GetValue("code");
                    System.Diagnostics.Debug.WriteLine(err);
                }
                return new CustomerPaymentViewModel() { Success = false, ProviderId = err1 };
            }
            catch (Exception ex1)
            {
                System.Diagnostics.Debug.WriteLine(ex1.Message);
                return new CustomerPaymentViewModel() { Success = false, ProviderId = "Error desconocido" };
            }
        }


        public ChargeViewModel CreateCardPayCharge(string CustomerProviderId, OrderViewModel xOrder, CustomerPaymentViewModel payment)
                                    
        {
            try
            {
                ChargeViewModel result = new ChargeViewModel();
                
                #region Ejemplo de conekta
                //conekta.Order order = new conekta.Order().create(@"{
                //          ""line_items"": [{
                //              ""name"": ""Tacos"",
                //              ""unit_price"": 1000,
                //              ""quantity"": 12
                //          }],
                //          ""shipping_lines"": [{
                //              ""amount"": 1500,
                //              ""carrier"": ""mi compañia""
                //          }],
                //          ""currency"": ""MXN"",
                //          ""customer_info"": {
                //            ""name"": ""Fulanito Pérez"",
                //            ""email"": ""fulanito@conekta.com"",
                //            ""phone"": ""+5218181818181""
                //          },
                //          ""shipping_contact"":{
                //             ""phone"": ""5555555555"",
                //             ""receiver"": ""Bruce Wayne"",
                //             ""address"": {
                //               ""street1"": ""Calle 123 int 2 Col. Chida"",
                //               ""city"": ""Cuahutemoc"",
                //               ""state"": ""Ciudad de Mexico"",
                //               ""country"": ""MX"",
                //               ""postal_code"": ""06100"",
                //               ""residential"": true
                //             }
                //           },
                //          ""charges"":[{
                //            ""payment_method"": {
                //              ""type"": ""oxxo_cash""
                //            }
                //          }]
                //        }");

                #endregion

                string request = @"{
                          ""line_items"": [{
                              ""name"": ""%ORDERNAME%"",
                              ""unit_price"": %PXAMOUNT%,
                              ""quantity"": 1
                          }],
                          ""currency"": ""MXN"",
                          ""customer_info"": {
                            ""customer_id"": ""%CUSTOMERIDTOKEN%""
                          },
                          ""shipping_lines"": [{
                              ""amount"": 0,
                              ""carrier"": ""No aplica""
                          }],
                          ""shipping_contact"":{
                             ""phone"": ""%CUSTOMERPHONE%"",
                             ""receiver"": ""%CUSTOMERNAME%"",
                             ""address"": {
                               ""street1"": ""%CUSTOMERADDRESS%"",
                               ""city"": ""%CUSTOMERCITY%"",
                               ""state"": ""%CUSTOMERSTATE%"",
                               ""country"": ""MX"",
                               ""postal_code"": ""%CUSTOMERPOSTALCODE%"",
                               ""residential"": true
                             }
                           },
                          ""charges"":[{
                            ""payment_method"": {
                              ""type"": ""card"",
                              ""payment_source_id"": ""%PAYMENTSOURCEID%""
                            }
                          }]
                        }";

                request = request.Replace("%CUSTOMERIDTOKEN%", CustomerProviderId);
                request = request.Replace("%PAYMENTSOURCEID%", payment.ProviderId);
                request = request.Replace("%PXAMOUNT%", (xOrder.Cost * 100).ToString());
                ParseCustomerInfo(ref request);


                conekta.Order order = new conekta.Order().create(request);


               


                if (order.charges.data != null)
                {
                    string jsonText = toJSON(order.charges.data[0]);
                    System.Diagnostics.Debug.WriteLine(jsonText);
                    Charge charge = JsonConvert.DeserializeObject<Charge>(jsonText);
                    return new ChargeViewModel
                    {
                        ChargeStatus = OrderStatus.PreChargeSucess,
                        ChargeId = charge.id,
                        Reference = Convert.ToInt64(charge.payment_method.reference),
                    };
                }

                //TODO LOG
                return new ChargeViewModel()
                {
                    ChargeStatus = OrderStatus.PreChargeFailure,
                    ErrorCode = "Lo sentimos",
                    ErrorMessage = "No se pudo generar la orden OxxoPay"
                };


            }
            catch (Exception ex)
            {
                return new ChargeViewModel()
                {
                    ChargeStatus = OrderStatus.PreChargeFailure,
                    ErrorCode = "Lo sentimos",
                    ErrorMessage = "No se pudo generar la orden OxxoPay"
                };
            }
        }
        public ChargeViewModel CreateOxxoPayCharge(OrderViewModel xOrder, DateTime dtExpires)
        {
            try
            {
                ChargeViewModel result = new ChargeViewModel();
                var epoch = ToEpoch(dtExpires);

                #region Ejemplo de conekta
                //conekta.Order order = new conekta.Order().create(@"{
                //          ""line_items"": [{
                //              ""name"": ""Tacos"",
                //              ""unit_price"": 1000,
                //              ""quantity"": 12
                //          }],
                //          ""shipping_lines"": [{
                //              ""amount"": 1500,
                //              ""carrier"": ""mi compañia""
                //          }],
                //          ""currency"": ""MXN"",
                //          ""customer_info"": {
                //            ""name"": ""Fulanito Pérez"",
                //            ""email"": ""fulanito@conekta.com"",
                //            ""phone"": ""+5218181818181""
                //          },
                //          ""shipping_contact"":{
                //             ""phone"": ""5555555555"",
                //             ""receiver"": ""Bruce Wayne"",
                //             ""address"": {
                //               ""street1"": ""Calle 123 int 2 Col. Chida"",
                //               ""city"": ""Cuahutemoc"",
                //               ""state"": ""Ciudad de Mexico"",
                //               ""country"": ""MX"",
                //               ""postal_code"": ""06100"",
                //               ""residential"": true
                //             }
                //           },
                //          ""charges"":[{
                //            ""payment_method"": {
                //              ""type"": ""oxxo_cash""
                //            }
                //          }]
                //        }");

                #endregion

                string request = @"{
                          ""line_items"": [{
                              ""name"": ""%ORDERNAME%"",
                              ""unit_price"": %PXAMOUNT%,
                              ""quantity"": 1
                          }],
                          ""currency"": ""MXN"",
                          ""customer_info"": {
                            ""name"": ""%CUSTOMERNAME%"",
                            ""email"": ""%CUSTOMEREMAIL%"",
                            ""phone"": ""%CUSTOMERPHONE%""
                          },
                          ""shipping_lines"": [{
                              ""amount"": 0,
                              ""carrier"": ""No aplica""
                          }],
                          ""shipping_contact"":{
                             ""phone"": ""%CUSTOMERPHONE%"",
                             ""receiver"": ""%CUSTOMERNAME%"",
                             ""address"": {
                               ""street1"": ""%CUSTOMERADDRESS%"",
                               ""city"": ""%CUSTOMERCITY%"",
                               ""state"": ""%CUSTOMERSTATE%"",
                               ""country"": ""MX"",
                               ""postal_code"": ""%CUSTOMERPOSTALCODE%"",
                               ""residential"": true
                             }
                           },
                          ""charges"":[{
                            ""payment_method"": {
                              ""type"": ""oxxo_cash"",
                              ""expires_at"": %EPOCHVALUE%   
                            }
                          }]
                        }";

                ////var expiresOn = DateTime.UtcNow.AddMinutes(90);//TODO
                ////var epoch = ToEpoch(expiresOn);

                request = request.Replace("%PXAMOUNT%", (xOrder.Cost * 100).ToString());
                request = request.Replace("%EPOCHVALUE%", epoch.ToString());
                ParseCustomerInfo(ref request);
                //conekta.Order order = new conekta.Order().find("ord_2ifYq8QUiefdf2k47");

                conekta.Order order = new conekta.Order().create(request);
                //var xxx = new conekta.Webhook().

                if (order.charges.data!=null)
                {
                    string jsonText = toJSON(order.charges.data[0]);
                    System.Diagnostics.Debug.WriteLine(jsonText);
                    Charge charge = JsonConvert.DeserializeObject<Charge>(jsonText);

                  //  Order chk = new Order().find(order.id);

                    return new ChargeViewModel
                    {
                        ChargeStatus = OrderStatus.PreChargeSucess,
                        ChargeId = charge.id,
                        Reference = Convert.ToInt64(charge.payment_method.reference),
                        
                    };
                }
              
                //TODO LOG
                return new ChargeViewModel()
                {
                    ChargeStatus = OrderStatus.PreChargeFailure,
                    ErrorCode = "Lo sentimos",
                    ErrorMessage = "No se pudo generar."
                };


            }
            catch (Exception ex)
            {
                return new ChargeViewModel()
                {
                    ChargeStatus = OrderStatus.PreChargeFailure,
                    ErrorCode = "Lo sentimos",
                    ErrorMessage = "No se pudo generar."
                };
            }
        }

        public string verificaPagoOxxo(string id)
        {
            try
            {
                Order order = new Order().find(id);
                var statuspay = order.payment_status;
                return statuspay;
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            
        }

        private String toJSON(object obj)
        {
            String json = JsonConvert.SerializeObject(obj, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });

            ///* Order optional fields */
            //json = json.Replace("\"monthly_installments\":0,", "");
            //json = json.Replace("\"bank\":{\"type\":\"spei\",\"expires_at\":\"0001-01-01T00:00:00\"},", "\"bank\":{\"type\":\"spei\"},");
            //json = json.Replace(",\"logged_in\":false,\"successful_purchases\":0,\"created_at\":0,\"updated_at\":0,\"offline_payments\":0,\"score\":0", "");
            //json = json.Replace(",\"frequency\":0,\"trial_period_days\":0,\"expiry_count\":0", "");
            return json;
        }
        private void ParseCustomerInfo(ref string request)
        {         
            request = request.Replace("%ORDERNAME%", orderName);//tumoda
            request = request.Replace("%CUSTOMERNAME%", customerName);
            request = request.Replace("%CUSTOMEREMAIL%", customerEmail);
            request = request.Replace("%CUSTOMERPHONE%", customerPhone);
            request = request.Replace("%CUSTOMERADDRESS%", customerAddress);
            request = request.Replace("%CUSTOMERCITY%", customerCity);
            request = request.Replace("%CUSTOMERSTATE%", customerState);
            request = request.Replace("%CUSTOMERPOSTALCODE%", customerPostalCode);
        }

        //public void test()
        //{
        //    long eSeconds = ToEpoch(DateTime.UtcNow.AddMinutes(30));
        //    System.Diagnostics.Debug.WriteLine(eSeconds.ToString());
        //    System.Diagnostics.Debug.WriteLine(ToHumanDT(eSeconds).ToString());

        //}
        public long ToEpoch(DateTime dt)
        {
            return (long)(dt - Epoch).TotalSeconds;
        }
        public DateTime ToHumanDT(long EpochTotalSeconds)
        {
            return Epoch.AddSeconds(EpochTotalSeconds);
        }

        public Customer GetTDCData(string id)
        {
            Customer getCoustiomer = new Customer();

            try
            {
                var cliente = getCoustiomer.find(id);
                return cliente;
            }

            catch (Exception e)
            {
                e.Message.ToString();
            }
           

            return getCoustiomer;
        }
    }


}