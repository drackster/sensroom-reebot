﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using ProjectX.API.Models;
using System.IO;
using ProjectX.ViewModels;
using System.Net.Http;
using System.Configuration;
using System.Threading.Tasks;
using Amazon;
using Amazon.S3;
using Amazon.S3.Transfer;

namespace ProjectX.API.Business
{
    public class AssetBC
    {
        private IAmazonS3 s3Client;
        #region Account
        public static void CreateAsset(string Id, string email)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var defaults = db.XConfigs.FirstOrDefault();
                db.Assets.Add(new Asset()
                {
                    UserId = Id,
                    Name = email,
                    MaxDistance = defaults.MaxDistance,
                    CreatedOn = DateTime.UtcNow,
                    Calificacion = 5,
                    offlineView = false
                });
                db.SaveChanges();               
            }

        }

        #endregion

        #region Profile
        public static AssetViewModel GetAsset(string uId=null)
        {
            if(uId == null)
                uId = Helpers.Helper.GetId();
            using (var db = DBHelper.GetDatabase())
            {
                var asset = db.Assets.Include("ABank").Include("ACategory").Include("APref")
                                .Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    #region Reviews
                    double gRating = 0;
                    var aRating = Business.AssetBC.GetAssetReviews(asset.Id);
                    if (aRating.Count > 0)
                        gRating = aRating.Average(z => z.Rating);
                    int reviewCount = aRating.Count;
                    ReviewViewModel lastReview = null;
                    if (reviewCount > 0)
                        lastReview = aRating[0];
                    #endregion

                    var aCalendars = GetCalendar(-1, false, asset.Id);

                    #region Online and CalendarActive BusinessRules
                    bool updateAsset = false;
                    //Validar que el tiempo online aun siga siendo válido
                    //Tambien, que no haya sido deshabilitado por falta de info
                    if (asset.IsOnline && ((!asset.IsActive ||
                        (asset.OnlineExpires.HasValue 
                        && asset.OnlineExpires<DateTime.Now))))
                    {
                        asset.CalendarActive = asset.IsActive;//solo desactivar si falta info
                        //ya que puede ser que expire el tiempo en linea pero el calendario sigue activo
                        asset.IsOnline = false;
                        asset.OnlineExpires = null;
                        updateAsset = true;
                    }
                    //Tambien, si ya no tiene calendarios guardados, apagar funcion
                    if(asset.CalendarActive && aCalendars.Count == 0)
                    {
                        asset.CalendarActive = false;
                        updateAsset = true;
                    }
                    if(updateAsset)
                    {
                        db.SaveChanges();
                    }
                    #endregion

                    return new AssetViewModel
                    {
                        Calendars = aCalendars,
                        Id = asset.Id,
                        Name = asset.Name,
                        DOB = asset.DOB,
                        IsMan = asset.ASex,
                        //Age = asset.Age,
                        Title = asset.Title,
                        Description = asset.Description,
                        IsOnline = asset.IsOnline,
                        OnlineExpires = asset.OnlineExpires,

                        GlobalRating = Math.Round(gRating, 1),//new
                        LastReview = lastReview,//

                        PreferenceId = asset.APrefId,
                        PreferenceName = asset.APref != null ? asset.APref.Name : null,

                        Precio = asset.precio == null ? 0 : decimal.Parse(asset.precio.ToString()),

                        CategoryId = asset.CategoryId,
                        CategoryName = asset.ACategory!=null ? asset.ACategory.Name : null,

                        MaxDistance = asset.MaxDistance.Value,
                        
                        Images = GetImageList(asset.Id),
                        CalendarActive = asset.CalendarActive,

                        BankId = asset.BankId,
                        BankName = asset.ABank != null ? asset.ABank.Name : null,
                        AccountNum = asset.AccountNum,
                        AccountNum2 = asset.AccountNum2,
                        BankHolderName = asset.BankHolderName,

                    };
                }
            }
            return new AssetViewModel();
            
        }
        public static bool UpdateAssetLocation(AssetLocationModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    asset.Latitude = model.Latitude;
                    asset.Longitude = model.Longitude;
                    asset.LocationUpdate =DateTime.UtcNow;
                }
                return true;
            }
        }

        public static bool offlineAsset(long assteId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.Id == assteId).FirstOrDefault();
                if (asset != null)
                {
                    asset.IsOnline = false;
                }
                try
                {
                    db.SaveChanges();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static bool onlineAssetValidate(long assteId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.Id == assteId).FirstOrDefault();
                if (asset != null)
                {
                    return asset.IsOnline;
                }
                else
                { return false; }
                                   
            }
        }

        public static bool SaveAsset(AssetViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId =  Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    //Profile
                    //asset.Age = model.Age;
                    asset.DOB = model.DOB;
                    asset.ASex = model.IsMan;

                    asset.Title = model.Title;
                    asset.Description = model.Description;
                    asset.IsOnline = model.IsOnline;
                    asset.OnlineExpires = model.OnlineExpires;
                    asset.CalendarActive = model.CalendarActive;
                    asset.IsActive = model.IsActive;

                    asset.CategoryId = model.CategoryId;
                    asset.precio = model.Precio;
                    asset.APrefId = model.PreferenceId;
                    asset.MaxDistance = model.MaxDistance;

                    //Bank Info
                    if(model.BankId>0)
                        asset.BankId = model.BankId;
                    asset.AccountNum = model.AccountNum;
                    asset.AccountNum2 = model.AccountNum2;
                    asset.BankHolderName = model.BankHolderName;

                    asset.Latitude = model.Latitude;
                    asset.Longitude = model.Longitude;
                    asset.LocationUpdate = model.LocationUpdate;
                   
                    db.SaveChanges();
                    return true;
                }
                else
                    return false;
               
            }
        }

        #endregion

        #region Images
        public async Task<AssetImageViewModel> SaveImage(byte[] bs)
        {
            var provider = new MultipartMemoryStreamProvider();
            var bucketUrl = ConfigurationManager.AppSettings["S3BucketURL"].ToString();
            var keyname = string.Empty;
          

            var fileURL = "";
            var incidenteFileName = "";

          

            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    try
                    {

                        Stream stream = new MemoryStream(bs);
                        var newFilename = Guid.NewGuid().ToString() + Path.GetExtension(asset.Name).ToLower();
                        incidenteFileName = newFilename;
                        keyname = string.Format("repo/s{0}/ent{1}/{2}", 1, DateTime.Now.ToString("yyyyMMddHHmmss"), newFilename);
                        UploadFileAsync(stream, keyname, 1, 0, "entrada");
                        fileURL = bucketUrl + keyname;
                         

                        DateTime dt =DateTime.UtcNow;
                        var index = db.AssetImages.Where(x => x.UserId == asset.Id).Count();
                        var newImage = db.AssetImages.Add(new AssetImage
                        {
                            Id = System.Guid.NewGuid().ToString(),
                            UserId = asset.Id,
                            ImageStream = null,
                            IndexNum = index,
                            CreatedOn = dt,
                            urlImage = fileURL
                        });
                        db.SaveChanges();
                        return new AssetImageViewModel
                        {
                           Id = newImage.Id,
                           CreatedOn = dt,
                           IndexNum = index
                        };
                    }
                    catch (Exception ex)
                    {
                        Business.LogBC.LogException(ex);
                    }
                }
                return null;
            }
        }

        public void UploadFileAsync(Stream fileStream, string keyName, int id_sector, int id_casa, string tipo)
        {
            try
            { 
                var bucketName = ConfigurationManager.AppSettings["S3Bucket"].ToString();

                var AccessKey = ConfigurationManager.AppSettings["AWSAccessKey"].ToString();
                var SecretKey = ConfigurationManager.AppSettings["AWSSecretKey"].ToString();
                var RegionKey = ConfigurationManager.AppSettings["AWSRegion"].ToString();
                var region = RegionEndpoint.GetBySystemName(RegionKey);
                using (s3Client = new AmazonS3Client(AccessKey, SecretKey, region))
                {

                    var fileTransferUtility =
                           new TransferUtility(s3Client);

                    var fileTransferUtilityRequest = new TransferUtilityUploadRequest
                    {
                        BucketName = bucketName,
                        StorageClass = S3StorageClass.Standard,
                        InputStream = fileStream,
                        Key = keyName,
                        CannedACL = S3CannedACL.PublicRead
                    };
                    fileTransferUtilityRequest.Metadata.Add("id_sector", id_sector.ToString());
                    fileTransferUtilityRequest.Metadata.Add("id_casa", id_casa.ToString());
                    fileTransferUtilityRequest.Metadata.Add("tipo", tipo.ToUpper());
                    fileTransferUtility.Upload(fileTransferUtilityRequest);
                }

            }
            catch (AmazonS3Exception e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static byte[] GetImage(string ImageId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                //var uId = Helpers.Helper.GetId();
                //var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                //if (asset != null)
                //{
                try
                {
                    var img = db.AssetImages.Where(x => x.Id == ImageId).FirstOrDefault();
                    if (img != null)
                    {
                        return img.ImageStream;
                        //return new AssetImageViewModel
                        //{
                        //    Id = img.Id,
                        //    ImgSource = img.ImageStream
                        //};
                    }
                }
                catch (Exception ex)
                {
                    Business.LogBC.LogException(ex);
                }
                //}
                return null;
            }
        }
        public static List<AssetImageViewModel> GetImageList()
        {
            long aId = -1;
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                    aId = asset.Id;
            }
            if (aId >= 0)
                return GetImageList(aId);
            return null;
        }
        public static List<AssetImageViewModel> GetImageList(long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var result = db.AssetImages.Where(x => x.UserId == AssetId)
                .Select(y => new AssetImageViewModel()
                {
                    Id = y.Id,
                    IndexNum = y.IndexNum,
                    CreatedOn = y.CreatedOn,
                    IsProfile = y.IsProfile
                }).OrderBy(z => z.IndexNum).ToList();

               

                //Establecer como default la primera si aun no esta seleccionado
                if(result.Count>0 && result.Where(x => x.IsProfile).Count() == 0)
                    result[0].IsProfile = true;
                else if(result.Count>0 && !result[0].IsProfile)
                {
                    //significa que la del perfil no es la primera en la lista, tenemos que moverla
                    var profileImg = result.Where(x => x.IsProfile).FirstOrDefault();
                    result.Remove(profileImg);//quitamos
                    result.Insert(0,profileImg);//e insertamos al inicio
                }
                


                return result;
            }
        }
        public static bool DeleteImage(string ImageId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    try
                    {
                        var img = db.AssetImages.Where(x => x.Id == ImageId).FirstOrDefault();
                        db.AssetImages.Remove(img);
                        db.SaveChanges();
                        return true;
                    }
                    catch (Exception ex)
                    {
                        Business.LogBC.LogException(ex);
                    }
                }
                return false;
            }
        }

        public static bool SetProfileImage(string ImageId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    try
                    {
                        var img = db.AssetImages.Where(x => x.IsProfile == true).FirstOrDefault();
                        if (img != null)
                        {
                            img.IsProfile = false;
                            db.SaveChanges();
                        }                       

                        var pImg = db.AssetImages.Where(x => x.Id == ImageId).FirstOrDefault();
                        pImg.IsProfile = true;
                        db.SaveChanges();
                        return true;
                    }
                    catch(Exception ex)
                    {
                        Business.LogBC.LogException(ex);
                    }
                }
                return false;
            }               
        }

        public static AssetImageViewModel GetCoverImage(long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var firstImage = db.AssetImages.Where(x => x.UserId == AssetId && x.IsProfile).FirstOrDefault();
                if (firstImage == null)
                {
                    firstImage = db.AssetImages.Where(x => x.UserId == AssetId).OrderBy(x => x.IndexNum).FirstOrDefault();
                    if(firstImage==null) return null;
                }
                return new AssetImageViewModel()
                {
                    Id = firstImage.Id,
                    IndexNum = firstImage.IndexNum,
                    CreatedOn = firstImage.CreatedOn
                };
            }
        }
        public static bool RearangeImages(List<AssetImageViewModel> model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                var aImages = db.AssetImages.Where(x => x.UserId == asset.Id)
                                                            .OrderBy(x => x.IndexNum).ToList();
                bool found = false;
                for (int i = 0; i < model.Count; i++)
                {
                    var m = aImages.Where(x => x.Id == model[i].Id).FirstOrDefault();
                    if (m != null)
                    {
                        if (!model[i].IsDeleted)
                        {
                            m.IndexNum = i;
                            m.IsProfile = model[i].IsProfile;
                        }
                        else
                        {
                            db.AssetImages.Remove(m);
                        }
                        found = true;
                    }
                }
                if(found)
                    db.SaveChanges();
                return found;
            }
        }

        #endregion

        #region Categories and Services
        public static ASettingsViewModel GetSettings()
        {
            var cats = GetCategories();
            var prefs = GetPreferences();
            prefs = prefs.OrderBy(x => x.AIndex).ToList();

            using (var db = DBHelper.GetDatabase())
            {
                ASettingsViewModel result = new ASettingsViewModel();
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {

                    //if (asset.APrefId.HasValue)
                    //    cats.Where(x => x.Id == asset.APrefId.Value).FirstOrDefault().IsSelected = true;

                    //Seleccionar
                    if (asset.CategoryId.HasValue)
                        cats.Where(x => x.Id == asset.CategoryId.Value).FirstOrDefault().IsSelected = true;
                    
                    var servs = db.AServices.Where(y => y.IsEnabled)
                        .OrderBy(x => x.IndexNum).Select(x => new AServiceViewModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();

                    var AssetServices = db.AssetServices.Where(x => x.UserId == asset.Id).ToList();
                    foreach(var s in servs)
                    {
                        var exists = AssetServices.Where(x => x.ServiceId == s.Id).FirstOrDefault();
                        if(exists!=null)
                            s.IsSelected = true;
                    }
                    result.Preferences = prefs;
                    result.Categories = cats;
                    result.Services = servs;
                    result.Distances = GetDistances();

                    if(asset.MaxDistance.HasValue)
                        result.Distances.Where(x => x.Km == asset.MaxDistance.Value).FirstOrDefault().IsSelected = true;
                    
                }

                //result.Calendars = GetCalendar();
                //result.CalendarTimes = GetCalendarTimes();
                return result;
            }
        }
        public static bool SaveSettings(ASettingsViewModel model)
        {
            model.Services = model.Services.Where(x => x.IsChanged).ToList();

            using (var db = DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    asset.CategoryId = model.Categories.Where(x => x.IsSelected).FirstOrDefault().Id;
                    asset.MaxDistance = model.Distances.Where(x => x.IsSelected).FirstOrDefault().Id;
                    asset.precio = model.Precio.Where(x => x.IsChanged).FirstOrDefault().Id;

                    if (model.Services.Count > 0)
                    {
                        var AssetServices = db.AssetServices.Where(x => x.UserId == asset.Id).ToList();
                        foreach (var s in model.Services)
                        {
                            var exists = AssetServices.Where(x => x.ServiceId == s.Id).FirstOrDefault();
                            if (exists != null && !s.IsSelected)//si existe y ya no esta seleccionado, borrar
                            {
                                db.AssetServices.Remove(exists);
                            }
                            else if(exists == null && s.IsSelected)//si no existe y esta seleccionado, agregar
                            {
                                db.AssetServices.Add(new AssetService
                                {
                                     UserId = asset.Id,
                                     ServiceId = s.Id
                                });
                            }
                                
                        }
                    }
                    
                    db.SaveChanges();
                }
                return true;
            }
        }
        public static List<DistanceViewModel> GetDistances()
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.ADistances.Where(x => x.IsEnabled)
                       .OrderBy(x => x.IndexNum)
                       .Select(x => new DistanceViewModel
                       {
                           Id = x.Id,
                           Name = x.Name,
                           Km = x.Km
                       }).ToList();
            }
        }
        public static List<ACategoryViewModel> GetCategories()
        {
            using (var db = DBHelper.GetDatabase())
            {
                //Agregar todos
                var cats = db.ACategories
                .Where(x => x.IsEnabled)
                .OrderBy(y => y.IndexNum).Select(x => new ACategoryViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Cost1 = x.cost1 ?? 0,
                    Cost2 = x.cost2 ?? 0,
                    Cost3 = x.cost3 ?? 0,
                }).ToList();


                var tiers = db.ACategoryTiers.Where(x => x.IsEnabled)
                    .OrderBy(x => x.CategoryId)
                    .OrderBy(x => x.IndexNum).ToList();


                foreach (var c in cats)
                    c.Tiers = tiers.Where(x => x.CategoryId == c.Id)
                        .Select(x => new ACategoryTierViewModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Description = x.Description,
                            Cost = x.Cost,
                            NumHours = x.NumHours
                        })
                        .ToList();

                return cats;
            }
        }
        public static List<APrefViewModel> GetPreferences()
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.APrefs
                .Where(x => x.IsEnabled)
                .Select(x => new APrefViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    CIndex = x.CIndexNum,
                    AIndex = x.AIndexNum
                }).ToList();
            }
        }
        public static List<CalendarTimeViewModel> GetCalendarTimes()
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.ACalendarTimes.Where(x => x.IsEnabled)
                       .OrderBy(x => x.IndexNum)
                       .Select(x => new CalendarTimeViewModel
                       {
                           Id = x.Id,
                           Name = x.Name,
                           Minutes = x.Minutes
                       }).ToList();
            }
        }

        #endregion

        #region Calendar
        public static List<CalendarViewModel> GetCalendar(long Id=-1, bool FromCustomer=false, long aId=-1)
        {
            var uId = Helpers.Helper.GetId();//"7c6a7435-84da-40e9-b486-f3d2fe4166d8";//
            using (var db = DBHelper.GetDatabase())
            {
                List<ACalendar> cals = null;
                if (!FromCustomer)
                {
                    Asset asset = null;
                    if(aId > 0)
                        asset = db.Assets.Where(x => x.Id == aId).FirstOrDefault();
                    else
                        asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();

                    if (asset == null)
                        cals = new List<ACalendar>();
                    else
                    cals = Id > 0 ? db.ACalendars.Include("ACalendarDetails").Where(x => x.Id == Id).ToList()
                                  : db.ACalendars.Include("ACalendarDetails").Where(x => x.UserId == asset.Id).ToList();
                }
                else
                {
                    //cuando viene del cliente
                    cals = db.ACalendars.Include("ACalendarDetails").Where(x => x.UserId == Id).ToList();
                }

                var result = cals.Select(x => new CalendarViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    From = new TimeSpan(x.From),
                    To = new TimeSpan(x.To),
                    Detail = x.ACalendarDetails.Select(y => new CalendarDetailViewModel
                    {
                        Id = y.Id,
                        CalendarId = x.Id,//fixedbug
                        Day = (DayOfWeek)y.DayNum,
                        IsSelected = true//default
                    }).ToList()

                }).ToList();

                return result;
            }
        }
        public static CalendarViewModel SaveCalendar(CalendarViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var UId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == UId).FirstOrDefault();

                var newCat = db.ACalendars.Add(new ACalendar
                {
                    UserId = asset.Id,
                    Name = model.Name,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    From = model.From.Ticks,
                    To = model.To.Ticks,
                    CreatedOn =DateTime.UtcNow
                });

                db.SaveChanges();

                foreach (var det in model.Detail)
                {
                    var newCatDet = db.ACalendarDetails.Add(new ACalendarDetail
                    {
                        CalendarId = newCat.Id,
                        DayNum = (int)det.Day,
                        CreatedOn =DateTime.UtcNow
                    });
                }
                db.SaveChanges();
                
                return GetCalendar(newCat.Id)[0];
            }
        }
        public static CalendarViewModel UpdateCalendar(CalendarViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var exists = db.ACalendars.Where(x => x.Id == model.Id).FirstOrDefault();
                if (exists != null)
                {
                    exists.Name = model.Name;
                    exists.Latitude = model.Latitude;
                    exists.Longitude = model.Longitude;
                    exists.From = model.From.Ticks;
                    exists.To = model.To.Ticks;

                    foreach (var det in model.Detail)
                    {
                        var child = db.ACalendarDetails.Where(x => x.Id == det.Id).FirstOrDefault();
                        if (child != null && !det.IsSelected)
                        {
                            db.ACalendarDetails.Remove(child);
                        }
                        else
                        {
                            //if (det.IsSelected)
                            //{
                            var newCatDet = db.ACalendarDetails.Add(new ACalendarDetail
                            {
                                CalendarId = model.Id,
                                DayNum = (int)det.Day,
                                CreatedOn =DateTime.UtcNow
                            });
                            //}
                        }
                    }
                    db.SaveChanges();

                }

                return GetCalendar(model.Id)[0];
            }
        }
        public static bool DeleteCalendar(CalendarViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var exists = db.ACalendars.Where(x => x.Id == model.Id).FirstOrDefault();
                if (exists != null)
                {
                    var details = db.ACalendarDetails.Where(x => x.CalendarId == exists.Id).ToList();
                    foreach (var det in details)
                        db.ACalendarDetails.Remove(det);
                    db.ACalendars.Remove(exists);
                    db.SaveChanges();
                }
                return true;
            }
        }

        #endregion

        #region Banking
        public static List<ABankViewModel> GetBankList(string uId = null)
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.ABanks.Where(x => x.IsEnabled).OrderBy(x => x.Name)
                    .Select(x => new ABankViewModel
                    {
                        Id = x.Id,
                        Name = x.Name
                    }).ToList();
            }
        }
        #endregion

        #region Reviews
        public static long AddAssetReview(ReviewViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var order = db.Orders.Where(x => x.Id == model.OrderId 
                && !x.CReviewedOn.HasValue && !x.CDeclinedOn.HasValue).FirstOrDefault();
                if (order != null)
                {
                    var dt =DateTime.UtcNow;
                    if (model.UserDeclined)
                    {
                        order.CDeclinedOn = dt;
                        db.SaveChanges();
                        return order.Id;
                    }
                    else
                    {
                        order.CReviewedOn = dt;
                        //agregar el review
                        var newReview = db.AssetReviews.Add(
                            new AssetReview
                            {
                                AssetId = model.ReviewedId,
                                CustomerId = model.ReviewerId,
                                Review = model.Review,
                                Rating = model.Rating,
                                OrderId = model.OrderId,
                                CreatedOn = dt
                            });

                        //ahora la notificación, y solo va para la chica
                        var newNotification = db.Notifications.Add(new Notification
                        {
                            //TODO
                            MType = (int)MessageType.Rating,//indicar que recibio un rating
                            AssetId = order.AssetId,
                            CustomerId = order.CustomerId,
                            OrderId = order.Id,
                            MessageText = null, //Los reviews no llevan, solo los chats
                            MessageStatus = (int)MessageStatus.Created,
                            EventDateTime = order.RequestedDateTime,
                            CreatedOn = dt,
                            MReceiver = (int)MessageSender.Asset //solo chicas
                        });

                        db.SaveChanges();
                        return newReview.Id;
                    }
                }
                else //ya fue calificada, declinada, o no se encontro
                {
                    return -1;
                }
            }
        }
        public static long AddCustomerReview(ReviewViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var order = db.Orders.Where(x => x.Id == model.OrderId
                && !x.AReviewedOn.HasValue && !x.ADeclinedOn.HasValue).FirstOrDefault();
                if (order != null)
                {
                    var dt =DateTime.UtcNow;
                    if (model.UserDeclined)
                    {
                        order.ADeclinedOn = dt;
                        db.SaveChanges();
                        return order.Id;
                    }
                    else
                    {
                        order.AReviewedOn = dt;
                        var newReview = db.CustomerReviews.Add(
                        new CustomerReview
                        {
                            AssetId = model.ReviewerId,
                            CustomerId = model.ReviewedId,
                            Review = model.Review,
                            Rating = model.Rating,
                            OrderId = model.OrderId,
                            CreatedOn =DateTime.UtcNow
                        });           
                        db.SaveChanges();
                        return newReview.Id;
                    }
                }
                else //ya fue calificada, declinada, o no se encontro
                {
                    return -1;
                }
            }
        }
        public static List<ReviewViewModel> GetAssetReviews(long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.AssetReviews
                .Include("Customers.AspnetUsers")
                .Where(x => x.AssetId == AssetId)
                .OrderByDescending(x => x.CreatedOn)
                .Select(x => new ReviewViewModel
                {
                    Id = x.Id,
                    ReviewerId = x.CustomerId,
                    ReviewerName = x.Customer.AspNetUser.UserName,
                    Review = x.Review,
                    Rating = x.Rating,         
                    CreatedOn = x.CreatedOn,
                    ShowDetail = !string.IsNullOrEmpty(x.Review) && x.Review.Length > 140
                }).ToList();
                
            }
        }
        public static List<ReviewViewModel> GetCustomerReviews(long CustomerId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.CustomerReviews
                .Include("Assets.AspnetUsers")
                .Where(x => x.CustomerId == CustomerId)
                .OrderByDescending(x => x.CreatedOn)
                .Select(x => new ReviewViewModel
                {
                    Id = x.Id,
                    ReviewerId = x.AssetId,
                    ReviewerName = x.Asset.AspNetUser.UserName,
                    Review = x.Review,
                    Rating = x.Rating,
                    CreatedOn = x.CreatedOn
                }).ToList();
            }
        }

        #endregion

        #region Balance

        public static List<BalanceViewModel> GetBalance()
        {
            var uId = Helpers.Helper.GetId();
            using (var db = DBHelper.GetDatabase())
            {
                List<BalanceViewModel> result = new List<BalanceViewModel>();
                DayOfWeek firstDay = DayOfWeek.Monday;

                //Obtener ordenes con estatus.. al parecer todas
                var orders = OrderBC.GetOrders(new OrderSearchModel
                {
                    orderStatus = OrderStatus.All,//deberia de ser cerradas
                    reviewStatus = ReviewStatus.All,
                    CalendarOnly = false,
                    MSender = MessageSender.Asset,
                    week = null
                });

                //Establecer el numero de semana
                foreach(var o in orders)
                {
                    o.Week = Util.GetWeekNumber(o.ServiceEnd, firstDay);
                }

                //Obtener configuracion de comisiones
                var config = db.XConfigs.FirstOrDefault();

                //Agrupar por semana
                var grouped = (from o in orders
                               group o by new { o.Week }
                    into grp
                               select new
                               {
                                   grp.Key.Week,
                                   ServiceEnd = grp.FirstOrDefault().ServiceEnd,
                                   Transactions = grp.Count(),
                                   TotalCost = grp.Sum(x => x.TotalCost),
                                   TotalComision = grp.Sum(x => x.TotalCost) * ((decimal)config.Comision),
                                   DownPayment = grp.Where(x => x.DownPayment.HasValue).Sum(x => x.DownPayment),
                                   CashRecollected = grp.Sum(x => x.CashRecollected)
                               }).ToList();

                //ahora por cada semana, crear un estado de balance
                foreach(var g in grouped)
                {
                    result.Add(new BalanceViewModel
                    {
                        TotalCost = g.TotalCost,
                        TotalComission = g.TotalComision,
                        TotalEarnings = g.CashRecollected,
                        TotalDeposit = g.TotalCost - g.TotalComision - g.CashRecollected,
                        Transactions = g.Transactions,
                        Week = new TransactionWeek()
                        {
                            From = Util.GetStartDayFromWeekNumber(g.Week, g.ServiceEnd.Year, firstDay),
                            To = Util.GetStartDayFromWeekNumber(g.Week, g.ServiceEnd.Year, firstDay).AddDays(6)
                        }
                    });
                }

                return result;
            }
        }
        public static List<OrderViewModel> GetTransactions(TransactionWeek Week)
        {
            var orders = OrderBC.GetOrders(new OrderSearchModel
            {
                 orderStatus = OrderStatus.All,//deberia de ser cerradas
                 reviewStatus = ReviewStatus.All,
                 CalendarOnly = false,
                 MSender = MessageSender.Asset,
                 week = Week
            });
            
            using (var db = DBHelper.GetDatabase())
            {
                XConfig config = db.XConfigs.FirstOrDefault();
                foreach(var x in orders)
                {
                    x.TotalComision = x.TotalCost * ((decimal)config.Comision);
                    x.TotalDeposit = x.TotalCost - x.TotalComision - x.CashRecollected;
                }
            }
            return orders;
        }

        #endregion

        #region ViewCount
        public static void AddViewCount(long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var a = db.Assets.Where(x => x.Id == AssetId).FirstOrDefault();
                //if(a!=null)
                //todo
            }
        }
        #endregion

        #region Asset/Customer
        public static AssetListViewModel GetAssetList(SearchModel model)
        {
            List<Asset> list = new List<Asset>();
            using (var db = DBHelper.GetDatabase())
            {
                //test
                //model.Latitude = 32.476989;
                //model.Longitude = -114.760071;
                //todas las chicas habilitadas deben de tener posicion
                //Aplicar filtros default: Estatus Activo y Edad
                //Mas filtros adelante (ubicacion, fecha, hora, etc)
                //&& x.Age >= model.MinAge
                //&& x.Age <= model.MaxAge

                var q = db.Assets.Include("ACategory").Include("APref")
                    .Where(x => x.IsActive
                    && x.Latitude.HasValue
                    && x.Longitude.HasValue);

                //puede filtrar por todas o una categoria especifica
                if (model.CategoryId.HasValue)
                {
                    q = q.Where(x => x.CategoryId.Value <= model.CategoryId.Value);


                    q = q.Where(x => x.precio <= model.Precio);

                    //ACategory selectedCategory;
                    //filtrar por precio máximo de acuerdo al número de horas seleccionado
                    //switch (model.Hours)
                    //{
                    //    case 1:
                    //        break;
                    //    case 2:
                    //        selectedCategory = db.ACategories.Where(c => c.Id == model.CategoryId.Value).FirstOrDefault();
                    //        q = q.Where(x => x.ACategory.cost1 * model.Hours <= selectedCategory.cost2 || x.CategoryId.Value <= model.CategoryId.Value);
                    //        break;
                    //    case 3:
                    //        selectedCategory = db.ACategories.Where(c => c.Id == model.CategoryId.Value).FirstOrDefault();
                    //        q = q.Where(x => x.ACategory.cost1 * model.Hours <= selectedCategory.cost2 || x.ACategory.cost1 + x.ACategory.cost2 <= selectedCategory.cost2 || x.CategoryId.Value <= model.CategoryId.Value);
                    //        break;
                    //}
                }
                if (model.PreferenceId.HasValue)
                {
                    if(model.PreferenceId.Value == 1)//Mujeres
                    {
                        q = q.Where(x => x.ASex.HasValue && !x.ASex.Value);
                    }
                    else if (model.PreferenceId.Value == 2)//Hombres
                    {
                        q = q.Where(x => x.ASex.HasValue && x.ASex.Value);
                    }
                }
                //Si es ASAP tiene que estar Online si no, no importa
                if (model.orderType == OrderType.ASAP)
                {
                    q = q.Where(x => x.IsOnline);
                }
                //Si no, tiene que tener habilitado el switch de Calendario
                else if(model.orderType == OrderType.Calendar)
                {
                    q = q.Where(x => x.CalendarActive);
                }

                list = q.ToList();

                //asumimos que si ya estan activas, tienen DOB
                list = list.Where(x => Util.GetAge(x.DOB.Value) >= model.MinAge 
                && Util.GetAge(x.DOB.Value) <= model.MaxAge).ToList();

            }
            return GetAssetList(list,model);
        }
        public static AssetListViewModel GetAssetList(List<Asset> list, SearchModel model=null)
        {
            using (var db = DBHelper.GetDatabase())
            {
                //primero filtrar por distancia
                List<AssetViewModel> assets = new List<AssetViewModel>();
                List<Order> overlapedOrders = new List<Order>();
                int totalCount = 0;
                var dtNow =DateTime.UtcNow;

                if (model != null)//cuando es un solo item
                {
                    DateTime? requestedDateTime = null;
                    DateTime? requestedEndDateTime = null;

                    #region Future Confirmed Orders
                    //Si es busqueda por calendario, entonces obtener todas las ordenes
                    //en el futuro ya confirmadas para validar que no sobrepongan la fecha y hora
                    //if (model.orderType == OrderType.Calendar)
                    //{
                    if (model.orderType == OrderType.ASAP)
                        model.SelectedDateTime =DateTime.UtcNow;
                        requestedDateTime = model.SelectedDateTime;
                        requestedEndDateTime = requestedDateTime.Value.AddHours(model.Hours);

                        //TODO : TEST AFTERMINUTES .. estos son los minutos de compensacion entre citas
                        var afterMinutes = 30;//db.XConfigs.Single().ReservationAfterMinutes.Value;
                        requestedDateTime = requestedDateTime.Value.AddMinutes(afterMinutes * -1);//Restar
                        requestedEndDateTime = requestedEndDateTime.Value.AddMinutes(afterMinutes);//Agregar

                        int OrderConfirmedValue = (int)OrderStatus.Confirmed;
                        //Obtener la lista de ordenes confirmadas que hacen overlap para validarlas despues contra la chicas 
                        overlapedOrders =
                            db.Orders.Where(p => p.OrderStatus == OrderConfirmedValue &&
                                                 ((requestedDateTime >= p.RequestedDateTime.Value &&
                                                   requestedEndDateTime.Value >= p.ServiceEnd &&
                                                   requestedDateTime < p.ServiceEnd)
                                                 ||
                                                 (requestedDateTime <= p.RequestedDateTime.Value &&
                                                  requestedEndDateTime <= p.ServiceEnd &&
                                                  requestedEndDateTime > p.RequestedDateTime.Value))
                                                ).ToList();

                        System.Diagnostics.Debug.WriteLine(overlapedOrders.Count.ToString());

                        //reset (quitar afterminutes)
                        requestedDateTime = model.SelectedDateTime;
                        requestedEndDateTime = requestedDateTime.Value.AddHours(model.Hours);

                    //}
                    #endregion

                    foreach (var x in list)
                    {
                        double distance = -1;
                        List<CalendarViewModel> assetCaledars = null;
                        #region ASAP/Calendar/Distance/Time
                        //Si es ASAP, entonces solo las viejas online, cerca del cliente
                        if (model.orderType == OrderType.ASAP)
                        {
                            //primero que este online
                            if (x.OnlineExpires.HasValue && x.OnlineExpires.Value >= dtNow)
                            {
                                //validamos que no haya tralape de horarios con otra invitación
                                if (overlapedOrders.Where(o => o.AssetId == x.Id).Count() == 0)
                                {
                                    distance = OrderBC.GetDistance(x.Latitude.Value, x.Longitude.Value, model.Latitude, model.Longitude);
                                    if (distance >= 0 && (distance <= (model.MaxDistanceRadius * 1000)))
                                    {
                                        //Match total, ahora agregar view model 
                                        assets.Add(CreateNewAssetViewModel(x, distance));
                                    }
                                }
                            }
                        }
                        //Si es calendario no importa si esta online u offline, 
                        //solo importa, la ubicacion del lugar de calendario y el horario
                        else if (model.orderType == OrderType.Calendar)
                        {
                            //primero validamos que no este haciendo overlap con otras ordenes
                            if (overlapedOrders.Where(o => o.AssetId == x.Id).Count() == 0)
                            {
                                //si no hace overlap ninguna de las odenes confirmadas
                                //ahora obtenemos todos sus calendarios
                                assetCaledars = GetCalendar(x.Id, true);
                                foreach (var ca in assetCaledars)
                                {
                                    //primero validar la ubicacion
                                    distance = OrderBC.GetDistance(ca.Latitude, ca.Longitude, model.Latitude, model.Longitude);
                                    if (distance >= 0 && (distance <= (model.MaxDistanceRadius * 1000)))
                                    {
                                        //ahora la hora
                                        TimeSpan rts = requestedDateTime.Value.TimeOfDay;
                                        if (rts >= ca.From && rts <= ca.To)
                                        {
                                            //match!
                                            //ahora el día
                                            var ValidDay = ca.Detail.Where(z => z.Day == requestedDateTime.Value.DayOfWeek).FirstOrDefault();
                                            if (ValidDay != null)
                                            {
                                                //Match de día y hora, 
                                                //Ahora falta buscar que no choque con otras reservaciones

                                                //ahora agregar view model 
                                                var newAss = CreateNewAssetViewModel(x, distance);
                                                newAss.Calendars = assetCaledars;//lo asignamos, para evitar doble lectura
                                                assets.Add(newAss);
                                                break;//este break rompe el ciclo dde assetCaledars

                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                    }

                    #region Sorting
                    if (model.sortType == SortType.Distance)
                        assets = assets.OrderBy(x => x.Distance).ToList();
                    else if (model.sortType == SortType.Price)
                        assets = assets.OrderByDescending(x => x.CategoryId).ToList();
                    else if (model.sortType == SortType.Rating)
                        assets = assets.OrderByDescending(x => x.GlobalRating).ToList();
                    else if (model.sortType == SortType.JoinDate)
                        assets = assets.OrderByDescending(x => x.JoinDate).ToList();
                    #endregion

                    #region Paging
                    totalCount = assets.Count; //gran total
                    for (int i = 0; i < assets.Count; i++)
                        assets[i].ItemIndex = i;

                    if (model.PageIndex > 0 && assets.Count > 0) //0 = all
                    {
                        int PageSize = 5; //todo add to table
                        assets = new List<AssetViewModel>(
                            assets.Skip((model.PageIndex - 1) * PageSize).Take(PageSize));
                    }

                    #endregion
                }
                else
                {
                    //cuando es solamente un registro para generar el vm
                    assets.Add(CreateNewAssetViewModel(list[0], 0));
                }
                
                if(assets.Count > 0)
                {
                    #region All Tiers
                    var tiers = db.ACategoryTiers.Where(x => x.IsEnabled)
                                .OrderBy(x => x.CategoryId)
                                .OrderBy(x => x.IndexNum).ToList();
                    #endregion

                    //Datos faltantes de los resultados
                    foreach (var a in assets)
                    {
                        #region Tiers
                        if(model != null)
                            a.Tiers = tiers.Where(x => x.CategoryId == a.CategoryId && x.NumHours == model.Hours)
                            .Select(x => new ACategoryTierViewModel
                            {
                                Id = x.Id,
                                Name = x.Name,
                                Description = x.Description,
                                Cost = x.Cost,
                                NumHours = x.NumHours
                            })
                            .ToList();
                        else
                            a.Tiers = tiers.Where(x => x.CategoryId == a.CategoryId)
                                .Select(x => new ACategoryTierViewModel
                                {
                                    Id = x.Id,
                                    Name = x.Name,
                                    Description = x.Description,
                                    Cost = x.Cost,
                                    NumHours = x.NumHours
                                })
                                .ToList();


                        if (a.Tiers.Count > 0)
                            a.CategoryName = a.Tiers[0].Name + " - " + a.Tiers[0].Cost.ToString("C0");
                        #endregion

                        #region Images
                        a.Images = GetImageList(a.Id);
                        #endregion

                       

                        //Services is deprecated
                        #region Services
                        //a.Services = db.AssetServices.Include("AServices")
                        //    .Where(x => x.AService.IsEnabled)
                        //    .OrderBy(x => x.AService.IndexNum)
                        //    .Where(x => x.UserId == a.Id)
                        //    .Select(x => new AServiceViewModel
                        //    {
                        //        Id = x.AService.Id,
                        //        Name = x.AService.Name,
                        //        IsSelected = true
                        //    }).ToList();
                        #endregion

                        #region Calendars
                        if(a.Calendars==null)
                            a.Calendars = GetCalendar(a.Id, true);
                        #endregion

                    }

                }

                return new AssetListViewModel()
                { Assets = new ObservableCollection<AssetViewModel>(assets),
                  TotalCount = totalCount};
            }
        }

        public static AssetViewModel CreateNewAssetViewModel(Asset x, double distance)
        {
            #region Reviews
            double gRating = 0;
            var aRating = Business.AssetBC.GetAssetReviews(x.Id);
            if(aRating.Count>0)
                gRating = aRating.Average(z => z.Rating);
            int reviewCount = aRating.Count;
            ReviewViewModel lastReview = null;
            if (reviewCount > 0)
                lastReview = aRating[0];
            #endregion

            #region Main Select

                return new AssetViewModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Title = x.Title,
                    IsOnline = x.IsOnline,
                    OnlineExpires = x.OnlineExpires,
                    CalendarActive = x.CalendarActive,
                    Description = x.Description,
                    CategoryId = x.ACategory.Id,
                    CategoryName = x.ACategory.Name,
                    Precio = decimal.Parse(x.precio.ToString()),
                    PreferenceId = x.APref.Id,
                    PreferenceName = x.APref.Name,

                    MaxDistance = 40,// x.MaxDistance.Value == 0 ? 40: x.MaxDistance.Value,
                    Latitude = x.Latitude,
                    Longitude = x.Longitude,
                    LocationUpdate = x.LocationUpdate,
                    DOB = x.DOB,

                    IsMan = x.ASex,
                    JoinDate = x.CreatedOn,

                    //Age = x.Age.Value,
                    DistanceFrom = OrderBC.FormatDistance(distance),
                    Distance = distance,
                    GlobalRating = Math.Round(gRating, 1),
                    LastReview = lastReview,
                    ReviewCount = reviewCount
                };
            
           
            

            
            #endregion
        }
        public static AssetViewModel GetAssetViewModel(Asset asset)
        {
            return GetAssetList(new List<Asset>(){ asset }).Assets[0];
        }
        #endregion

        #region PushNotifications
        public static long AddDeviceToken(DeviceTokenViewModel model)
        {
            var uId = Helpers.Helper.GetId();
            using (var db = DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset != null)
                {
                    var newToken = db.DeviceTokens.Add(
                    new DeviceToken
                    {
                        AssetId = asset.Id,
                        DeviceId = (int)model.DeviceId,
                        Token = model.Token,
                        UpdatedOn =DateTime.UtcNow
                    });
                    db.SaveChanges();
                    return newToken.Id;
                }
            }
            return -1;
        }
        public static void UpdateDeviceToken(DeviceTokenViewModel model, long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                //primero checar que el token no pertenesca a otro usuario
                var tokenExists = db.DeviceTokens.Where(x => x.Token == model.Token
                                                    && !x.ExpiredOn.HasValue).FirstOrDefault();
                if (tokenExists != null)
                {
                    //Checamos que no sea del mismo usuario que se esta logeando
                    if (tokenExists.AssetId != AssetId)
                    {
                        tokenExists.AssetId = AssetId;
                        tokenExists.UpdatedOn =DateTime.UtcNow;
                        db.SaveChanges();
                    }
                    //si es del mismo usuario, no hacemos cambios
                }
                else//si no existe, entonces es nuevo y lo agregamos
                {
                    db.DeviceTokens.Add(
                    new DeviceToken
                    {
                        AssetId = AssetId,
                        DeviceId = (int)model.DeviceId,
                        Token = model.Token,
                        UpdatedOn =DateTime.UtcNow
                    });
                    db.SaveChanges();
                }
            }
        }
        public static List<DeviceTokenViewModel> GetDeviceTokens(long AssetId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                return db.DeviceTokens
                .Where(x => x.AssetId == AssetId && x.ExpiredOn == null)
                .OrderByDescending(x => x.UpdatedOn)
                .Select(x => new DeviceTokenViewModel
                {
                    Id = x.Id,
                    AssetId = x.AssetId,
                    DeviceId = (DeviceType) x.DeviceId,
                    Token = x.Token,
                    UpdatedOn = x.UpdatedOn,
                }).ToList();
            }
        }
        #endregion


    }
}