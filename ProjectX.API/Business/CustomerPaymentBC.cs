﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using ProjectX.API.Helpers;
using Openpay.Entities;

namespace ProjectX.API.Business
{
    public static class CustomerPaymentBC
    {
        public static List<CustomerPaymentMethod> GetCustomerPaymentList()
        {
            long customerId =  Helpers.Helper.GetCustomerId();
            return DBHelper.GetDatabase().CustomerPaymentMethods.Where(c => c.CustomerId == customerId && !c.IsDeleted)
                .OrderBy(i => i.Id).ToList();
                                    //.Select(x => new CustomerPaymentViewModel()
                                    //{
                                    //    Id = x.Id,
                                    //    CustomerId = x.CustomerId,
                                    //    ProviderId = x.ProviderId,
                                    //    NameOnCard = x.NameOnCard,
                                    //    CreditCardType = x.CreditCardType,
                                    //    CreditCardNumber = x.CreditCardNumber,
                                    //    CVC = x.CVC,
                                    //    ExpirationDate = x.ExpirationDate,
                                    //    CreatedOn = x.CreatedOn
                                    //}).ToList();
        }
        public static CustomerPaymentMethod CreateCustomerPayment(CustomerPaymentViewModel newPayment)
        {
            long customerId = Helpers.Helper.GetCustomerId();
            using (var db = DBHelper.GetDatabase())
            {
                var pay = db.CustomerPaymentMethods.Create();
                BindCustomerPaymentModel(pay, newPayment);
                pay.CustomerId = customerId;
                pay.CreatedOn =DateTime.UtcNow;
                db.CustomerPaymentMethods.Add(pay);
                db.SaveChanges();
                return pay;
            }
        }

        public static List<MetodosPagoVM> GetMediosDePago()
        {
            List<MetodosPagoVM> lista = new List<MetodosPagoVM>();          
            using (var db = DBHelper.GetDatabase())
            {
                var mp = db.tMediosDePagoes.ToList();
                foreach (var x in mp)
                {
                    lista.Add(new MetodosPagoVM
                    {
                        id = x.id,
                        Nombre = x.Nombre,
                        status = (bool)x.Status
                    });
                }
                return lista;
            }
        }

        public static void UpdateCustomerPayment(CustomerPaymentViewModel newPayment)
        {
            var db = DBHelper.GetDatabase();
            var pay = db.CustomerPaymentMethods.Find(newPayment.Id);
            BindCustomerPaymentModel(pay, newPayment); //UPDATE VALUES
            db.SaveChanges();
        }
        public static bool DeleteCustomerPayment(long id)
        {
            OpenPayBC openPayBC = new OpenPayBC();
            //DELETE IT FROM LOCAL DB
            var db = DBHelper.GetDatabase();
            //GET THE CARD RECORD
            var pay = db.CustomerPaymentMethods.Find(id);
            //REMOVE IT FROM OPENPAY, CONTINUE ONLY IF DELETION IS SUCCESSFUL
            if (openPayBC.DeleteCard(pay.Customer.ProviderId, pay.ProviderId))
            {
                pay.IsDeleted = true;
                db.SaveChanges();
                return true;
            }
            return false;
        }
        private static void BindCustomerPaymentModel(CustomerPaymentMethod pay, CustomerPaymentViewModel vm)
        {
            pay.NameOnCard = vm.NameOnCard == null ? "" : vm.NameOnCard;
            pay.ProviderId = vm.ProviderId;
            pay.CreditCardNumber = vm.CreditCardNumber;
            pay.ExpirationDate = vm.ExpirationDate;
            pay.CVC = string.Empty;//ya no salvamos el cvc vm.CVC;
            pay.CreditCardType = vm.CreditCardType;
            pay.Email = vm.Email;
            pay.Telefono = vm.Telefono;
            pay.Nombre = vm.Nombre;
            pay.Apellidos = vm.Apellidos;
        }

        //OPENPAY
        //public static CustomerPaymentViewModel CreateCustomerOpenPayId(ProjectX.API.Models.Customer customer)
        //{
        //    try
        //    {
        //        string cname = customer.AspNetUser.UserName.ToLower();
        //        string cemail = cname + "@px.com";
        //        OpenPayBC openPayBC = new OpenPayBC(); //OpenPayManager


        //        var opCustomer = openPayBC.AddOpenPayCustomer("PX", cname,
        //        "0000000000", cemail, customer.Id.ToString());
        //        customer.ProviderId = opCustomer.Id;//now that we have created the customer in OpenPay, lets set its ProviderId
        //        Business.CustomerBC.UpdateCustomer(customer.Id, customer.ProviderId); //and save it to the lava db
        //        return new CustomerPaymentViewModel() { Success = true, ProviderId = customer.ProviderId };
        //    }
        //    catch (Openpay.OpenpayException ex)
        //    {
        //        return new CustomerPaymentViewModel() { Success = false, ProviderId = ex.Description };
        //        //return InternalServerError(new Exception(ex.Description, ex));
        //    }
        //    catch(Exception ex1)
        //    {
        //        return new CustomerPaymentViewModel() { Success = false, ProviderId = "Error desconocido" };
        //    }
        //}
        //public static ChargeViewModel CreateCharge(OrderViewModel order)
        //{
        //    ChargeViewModel result = new ChargeViewModel();
        //    using (var db = Helpers.DBHelper.GetDatabase())
        //    {
        //        OpenPayBC openPayBC = new OpenPayBC();
        //        Charge charge = new Charge();
        //        string ServiceOrderName = "Orden de servicio PX";

        //        var customer = db.Customers.Where(x => x.Id == order.CustomerId).FirstOrDefault();

        //        if (order.PaymentMethodId.HasValue)//Pago con tarjeta
        //        {
        //            //var order = db.Orders.Include("Customer").Where(s => s.Id == model.OrderId).FirstOrDefault();
        //            var payment = db.CustomerPaymentMethods.Where(x => x.Id == order.PaymentMethodId).FirstOrDefault();

        //            if (payment != null && customer != null)
        //            {
        //                var guid = Guid.NewGuid().ToString().Substring(0, 8);
        //                charge = openPayBC.CreateChargeCard(
        //                    payment.ProviderId,
        //                    customer.ProviderId,
        //                    order.Cost,
        //                    ServiceOrderName,
        //                    false,
        //                    guid);
        //            }
        //        }
        //        else //Pago en tienda///DEPRECATED// YA NO SE UTILIZA PAYNET, AHORA ES OXXOPAY, VER CONEKTABC
        //        {
        //            if (customer.ProviderId == null)
        //            {
        //                var cid = CreateCustomerOpenPayId(customer);
        //                if (cid.Success)
        //                {
        //                    customer.ProviderId = cid.ProviderId;
        //                }
        //                else
        //                {
        //                    charge.ErrorMessage = cid.ProviderId;
        //                }
        //            }
        //            if (customer.ProviderId != null)
        //            {
        //                charge = openPayBC.CreateChargeStore(customer.ProviderId, order.Cost, ServiceOrderName);

        //                if (charge.PaymentMethod != null)
        //                {
        //                    //result.Reference = charge.PaymentMethod.Reference;
        //                    result.MerchantId = System.Configuration.ConfigurationManager.AppSettings["OpenPayMerchant"];
        //                }
        //            }
        //        }

        //        result.ChargeId = charge.Id;
        //        if (charge.ErrorMessage != null)// || order.ChargeRequestId == null)
        //        {
        //            var split = charge.ErrorMessage.Split(new char[] { ':' });
        //            if (split.Length == 2)
        //            {
        //                result.ErrorCode = split[0].Trim();
        //                result.ErrorMessage = split[1].Trim();
        //            }
        //            else
        //            {
        //                result.ErrorCode = "Código desconocido";
        //                result.ErrorMessage = charge.ErrorMessage;
        //            }
        //            result.ChargeStatus = OrderStatus.PreChargeFailure;
        //        }
        //        else
        //        {
        //            result.ChargeStatus = OrderStatus.PreChargeSucess;
        //            result.ProviderId = customer.ProviderId;
        //            //db.SaveChangesAsync();
        //        }
        //        return result;
        //    }

        //    #region Test
        //    //var service = ReservationBC.GetServiceByRequestToken(RequestToken);

        //    //if(service.CustomerPaymentMethod.CreditCardNumber.EndsWith("2222"))
        //    //{
        //    //    service.StatusId = (int)ServiceStatus.PreChargeFailure;
        //    //    db.SaveChanges();
        //    //    return Ok("Fondos Insuficientes");
        //    //}
        //    //else
        //    //{
        //    //    service.StatusId = (int)ServiceStatus.PreChargeSucess;
        //    //    db.SaveChanges();
        //    //    return Ok(string.Empty);
        //    //}
        //    #endregion
        //}
    }
}