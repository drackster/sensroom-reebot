﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using System.Data.Entity;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using System.Device.Location;

namespace ProjectX.API.Business
{
    public class OrderBC
    {
        public static InvitationViewModel SaveOrder(OrderViewModel model, CustomerPaymentViewModel payment=null)
        {
            try
            {
                var aspnetUser = Helpers.Helper.GetAspnetUser();
                DateTime dt = ViewModels.Util.StripMinSec(DateTime.Now);
                model.CustomerName = aspnetUser.UserName;
               // ConektaBC conekta;
                if (payment == null)
                {
                    //var nombre = ViewModels.Util.GetCustomerName(payment.NameOnCard);
                  //  conekta = new ConektaBC(payment);
                }
                else
                {
                    //conekta = new ConektaBC(payment);
                }

                using (var db = DBHelper.GetDatabase())
                {
                    var config = db.XConfigs.FirstOrDefault();
                    var customer = db.Customers.Where(x => x.UserId == aspnetUser.Id).FirstOrDefault();
                    model.CustomerId = customer.Id;

                    //Establecer la fecha de expiración
                    DateTime dtExpires = dt.AddMinutes(config.CartExpireHours);

                    //Ahora ChargeViewModel contiene el resultado del cargo, ya sea a tarjeta u oxxo
                    ChargeViewModel chargeResult = new ChargeViewModel();

                    #region Pagos con Tarjeta
                    //if (model.paymentMethod == PaymentMethod.OpenPay)
                    //{
                    //    //YA NO SE UTILIZA OPENPAY, AHORA ES CONEKTA
                    //    //Primero checamos que el cliente ya tenga un token de cliente generado en Conekta
                    //    if (customer.ProviderId == null)
                    //    {
                    //       // var cvpm = conekta.AddCustomer(payment); 
                    //        if(cvpm.Success)
                    //        {
                    //           // customer.ProviderId = cvpm.ProviderId;
                    //            db.SaveChanges();//salvar cambios de cliente
                    //            //Ahora agregamos el nuevo método de pago a la BD
                    //            //payment.ProviderId = cvpm.CreditCardNumber; //esta propiedad contiene el ID del recurso de pago
                    //            var newPaymentMethod = CustomerPaymentBC.CreateCustomerPayment(payment);//Lo salvamos en la BD
                    //            model.PaymentMethodId = newPaymentMethod.Id;
                    //        }
                    //        else
                    //        {
                    //            //Si hubo error, entonces detenemos la ejecucion, logeamos y regresamos el error mismo
                    //            //LogBC.LogException(new Exception(cvpm.ProviderId));
                    //            return GenerateErrorModel();
                    //        }
                    //    }
                    //    else//Si el cliente ya tiene providerId, significa que ya esta dado de alta en Conekta
                    //    {
                    //        //primero buscamos sus metodos de pago
                    //        var pMethods = CustomerPaymentBC.GetCustomerPaymentList();
                    //        var exists = pMethods.Where(x => x.CustomerId == model.CustomerId
                    //                                         && x.CreditCardNumber == payment.CreditCardNumber
                    //                                         && x.ExpirationDate == payment.ExpirationDate).FirstOrDefault();
                    //        if(exists==null)//Si no existe la tarjeta, la agregamos
                    //        {
                    //           // var newPaymentSource = conekta.AddPaymentSource(customer.ProviderId, payment);
                    //            if(newPaymentSource.Success)
                    //            {
                    //                payment.ProviderId = newPaymentSource.ProviderId;
                    //                var newPaymentMethod2 = CustomerPaymentBC.CreateCustomerPayment(payment);//Lo salvamos en la BD
                    //                model.PaymentMethodId = newPaymentMethod2.Id;
                    //                payment.Id = newPaymentMethod2.Id;
                    //            }
                    //            else
                    //            {
                    //                return GenerateErrorModel(newPaymentSource.ProviderId);
                    //            }
                    //        }
                    //        else//esta tarjeta ya fue agregada anteriormente, utilizar el mismo provider id o token
                    //        {
                    //            model.PaymentMethodId = exists.Id;
                    //            payment.ProviderId = exists.ProviderId;
                    //        }
                    //    }


                    //    //Una ves que ya tenemos el ProviderId
                    //    //
                    //    chargeResult = conekta.CreateCardPayCharge(customer.ProviderId, model, payment);
                    //    if (string.IsNullOrEmpty(chargeResult.ChargeId)) return new InvitationViewModel() { ChargeModel = chargeResult };
                    //}
                    #endregion
                    //Pagos con Oxxxo
                    //else if (model.paymentMethod == PaymentMethod.Paynet)
                    //{
                    //   // chargeResult = conekta.CreateOxxoPayCharge(model, dtExpires);
                    //}

                    #region Address
                    //Agregar direccion si esta no ha sido dada dealta
                    if (model.AddressId == -1)
                    {
                        var newAddress = db.CAddresses.Add(new CAddress
                        {
                            CustomerId = model.CustomerId,
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            Address3 = model.Address3,
                            Comments = model.Comments
                        });
                        db.SaveChanges();
                        model.AddressId = newAddress.Id;
                    }
                    #endregion

                    #region Order
                    var startingDistance = GetDistance(model.ALatitude, model.ALongitude, model.CLatitude, model.CLongitude);

                    //Establecer la fecha y hora final del servicio aprox
                    //TODO: esta es la fecha final estimada, falta que la chica acepte.
                    //Probablemente sea bueno actualizarla

                    //DateTime serviceWillEnd = model.RequestType == OrderType.ASAP ?
                    //    dt.AddHours(model.CategoryTier.NumHours) :

                    DateTime serviceWillEnd =  model.EventDateTime.Value.AddHours(model.CategoryTier.NumHours);
                    //Establecer el valor en efectivo a recolectar en caso de que no se haya cubierto el 100%
                    decimal? cash = model.DownPayment.HasValue ? model.TotalCost - model.DownPayment : null;

                    
                    
                        var newOrder = db.Orders.Add(new Order
                        {
                            CustomerId = model.CustomerId,
                            AssetId = model.AssetId,
                            CategoryTierId = model.CategoryTier.Id,
                            AdressId = model.AddressId,

                            CLatitude = model.CLatitude,
                            CLongitude = model.CLongitude,
                            ALatitude = model.ALatitude,
                            ALongitude = model.ALongitude,

                            DownPayment = model.DownPayment,
                            TotalCost = model.TotalCost,

                            CreatedOn = dt,
                            ExpiresOn = dtExpires,//Minutos
                            ServiceEnd = serviceWillEnd,
                            CashRecollected = cash,

                            OrderStatus = (int)OrderStatus.Pending,
                            RequestType = (int)model.RequestType,
                            RequestedDateTime = model.EventDateTime,
                            StartingDistance = startingDistance,
                            CustomerPaymentId = model.PaymentMethodId,
                            ChargeId = chargeResult.ChargeId,
                            PaymentMethod = (int)model.paymentMethod,
                            PaypalTransId = model.PaypalTransId,
                            OxxoRef = chargeResult.Reference
                        });
                        db.SaveChanges();
                        #endregion

                        #region Notification
                        var newNotification = db.Notifications.Add(new Notification
                        {
                            //TODO
                            MType = (int)MessageType.Invitation,
                            AssetId = model.AssetId,
                            CustomerId = model.CustomerId,
                            OrderId = newOrder.Id,
                            MessageText = null, //Las invitaciones no llevan, solo los chats
                            MessageStatus = (int)MessageStatus.Created,
                            EventDateTime = model.EventDateTime,
                            CreatedOn = dt,
                            MReceiver = (int)MessageSender.None //both
                        });
                        db.SaveChanges();
                        #endregion

                        //todo
                        model.Id = newOrder.Id;
                        model.CreatedOn = newOrder.CreatedOn;
                        model.ExpiresOn = newOrder.ExpiresOn;

                        var asset = db.Assets.Where(x => x.Id == model.AssetId).FirstOrDefault();
                        if (asset != null)
                            model.Asset = AssetBC.GetAssetViewModel(asset);

                        ///UPDATE AHORA REGRESAMOS LA INVITACION
                        ///SIEmprE el MessageSender sera Customer
                        var invite = MessagingBC.GetInvitation(model.Id, MessageSender.Customer);

                        if (model.paymentMethod != PaymentMethod.Paypal)
                            invite.ChargeModel = chargeResult;
                        return invite;
                    
                  
                }
            }
            catch(Exception ex)
            {
                LogBC.LogException(ex);
            }
            return GenerateErrorModel();
        }
        private static InvitationViewModel GenerateErrorModel(string err=null)
        {
            return new InvitationViewModel()
            {
                ChargeModel = new ChargeViewModel()
                {
                    ErrorCode = "Lo sentimos",
                    ErrorMessage = err == null ? "No se pudo generar el pedido" : err,
                    ChargeStatus = OrderStatus.PreChargeFailure
                }
            };
        }
        public static List<OrderViewModel> GetOrders(OrderSearchModel model)
        {
            List<OrderViewModel> result = new List<OrderViewModel>();
            IQueryable<Order> qOrders = null;
            List<Models.Order> orders = new List<Models.Order>();
            var aspnetUser = Helpers.Helper.GetAspnetUser();
            int OrderStatusId = (int)model.orderStatus;

            using (var db = DBHelper.GetDatabase())
            {
                if (model.MSender != MessageSender.Admin)
                {
                    if (aspnetUser.AdminType == 1)
                    {
                        var customer = db.Customers.Where(x => x.UserId == aspnetUser.Id).FirstOrDefault();
                        qOrders = db.Orders.Where(x => x.CustomerId == customer.Id)
                            .Include("CAddress")
                            .Include("Asset.AspNetUser")
                            .Include("Customer.AspNetUser");
                    }
                    else
                    {
                        var asset = db.Assets.Where(x => x.UserId == aspnetUser.Id).FirstOrDefault();
                        qOrders = db.Orders.Where(x => x.AssetId == asset.Id)
                            .Include("CAddress")
                            .Include("Asset.AspNetUser")
                            .Include("Customer.AspNetUser");
                    }
                }
                else
                    qOrders = db.Orders.Include("CAddress")
                            .Include("Asset.AspNetUser")
                            .Include("Customer.AspNetUser");
                if (OrderStatusId > 0)
                    qOrders = qOrders.Where(x => x.OrderStatus == OrderStatusId);

                //Obtener ordenes pendientes de revision
                if (model.reviewStatus == ReviewStatus.Pending)
                {
                    //El que pide es cliente, entonces buscar 
                    if (model.MSender == MessageSender.Customer)
                        qOrders = qOrders.Where(x => !x.CReviewedOn.HasValue && !x.CDeclinedOn.HasValue);
                    else if (model.MSender == MessageSender.Asset)
                        qOrders = qOrders.Where(x => !x.AReviewedOn.HasValue && !x.ADeclinedOn.HasValue);
                }

                //Filtrar por semana en caso de que se requiera
                if (model.week != null)
                {
                    qOrders = qOrders.Where(x => x.ServiceEnd >= model.week.From
                         && x.ServiceEnd <= model.week.To);
                }

                //Definir el sorting dependiendo del tipo de request
                if (model.MSender != MessageSender.Admin)
                {
                    if (model.CalendarOnly)
                        qOrders = qOrders.Where(x => x.RequestType == (int)OrderType.Calendar)
                                .OrderBy(x => x.RequestedDateTime);
                    else
                        qOrders = qOrders.OrderByDescending(x => x.CreatedOn);
                }



                orders = qOrders.ToList();


                //Obtener el tiempo maximo, una ves ya ejecutada la lista
                if (orders.Count > 0 && model.reviewStatus == ReviewStatus.Pending)
                {
                    var minutes = db.XConfigs.FirstOrDefault().ReviewAwaitMinutes;
                    DateTime dt =DateTime.UtcNow;
                    TimeSpan ts = new TimeSpan(0, minutes, 0);
                    orders = orders.Where(x => x.CompletedOn.Value.Add(ts) >= dt).ToList();
                    if (orders.Count > 1)
                    {
                        //si hay más de una, tomar la más antigua, porque solo podrá calificar de una en una
                        orders = orders.OrderBy(x => x.CompletedOn).Take(1).ToList();
                    }
                }

                //obtener lista de tiers, se ocuparan despues para la lista final
                var tiers = db.ACategoryTiers.Select(x => new ACategoryTierViewModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Description = x.Description,
                            Cost = x.Cost,
                            NumHours = x.NumHours
                        })
                        .ToList();


                result = orders.Select(x => new OrderViewModel
                {
                    Id = x.Id,
                    Status = (OrderStatus)x.OrderStatus,

                    AssetId = x.AssetId,
                    //no es necesario si es para las transacciones del la semana
                    Asset = model.week != null ? null : AssetBC.GetAssetViewModel(x.Asset),

                    CustomerId = x.CustomerId,
                    CustomerName = x.Customer.AspNetUser.UserName,

                    CategoryTier = tiers.Where(y => y.Id == x.CategoryTierId).FirstOrDefault(),

                    DownPayment = x.DownPayment,
                    TotalCost = x.TotalCost,
                    CashRecollected = x.CashRecollected.HasValue ? x.CashRecollected.Value : 0,
                    comisionStatus = (ComisionStatus) x.ComisionStatus,

                    CLatitude = x.CLatitude,
                    CLongitude = x.CLongitude,
                    ALatitude = x.ALatitude,
                    ALongitude = x.ALongitude,

                    AddressId = x.AdressId.HasValue ? x.AdressId.Value : -1,
                    //Address1 = x.CAddress == null ? null : x.CAddress.Address1,
                    //Address2 = x.CAddress == null ? null : x.CAddress.Address2,
                    //Address3 = x.CAddress == null ? null : x.CAddress.Address3,
                    //Comments = x.CAddress == null ? null : x.CAddress.Comments,

                    CreatedOn = x.CreatedOn,
                    ConfirmedOn = x.ConfirmedOn,
                    ExpiresOn = x.ExpiresOn,
                    Arrival = x.Arrival,
                    ServiceEnd = x.ServiceEnd,
                    
                    RequestType = (OrderType)x.RequestType,
                    EventDateTime = x.RequestedDateTime,

                    paymentMethod = (PaymentMethod)x.PaymentMethod,
                    PaymentMethodId = x.CustomerPaymentId,
                    PaypalTransId = x.PaypalTransId,
                  
                    ADeclinedOn = x.ADeclinedOn,
                    CDeclinedOn = x.CDeclinedOn,
                    AReviewedOn = x.AReviewedOn,
                    CReviewedOn = x.CReviewedOn,
                    CustomerRating = x.AssetReviews != null && x.AssetReviews.Count > 0 ? x.AssetReviews.First().Rating : 0.0

                }).ToList();

                foreach (var r in result)
                {
                    if (r.EventDateTime.HasValue)
                    {
                        r.EventEnd = r.EventDateTime.Value.AddHours(r.CategoryTier.NumHours);
                        r.Arrival = r.EventDateTime;
                    }
                    else
                    {
                        r.Arrival = r.CreatedOn;
                    }
                    if(r.CReviewedOn.HasValue)//Significa que ya fue calificada por el cliente
                    {
                        //r.CustomerRating = 4.2;
                        r.HasRating = true;
                    }
                }

                return result;
            }
        }
        public static AScheduleViewModel GetSchedule(NotificationRequestModel model)
        {
            AScheduleViewModel result = new AScheduleViewModel();
            int maxdays = 10;
            var calendar = AssetBC.GetCalendar();

            using (var db = DBHelper.GetDatabase())
            {
                //int oType = (int)OrderType.Calendar;//Calendar Only 
                //AHORA OBTENEMOS CALENDARIO Y ASAP, PORQUE TODAS TIENEN UNA FECHA DE INICIO Y FIN
                int pendingStatus = (int)OrderStatus.Pending;
                int confirmedStatus = (int)OrderStatus.Confirmed;

                var q = db.Orders.Where(x => x.AssetId == model.AssetId
                                            && DbFunctions.TruncateTime(x.RequestedDateTime) >= model.StartDate.Value
                                            && (x.OrderStatus == pendingStatus
                                             || x.OrderStatus == confirmedStatus)).ToList();
                //&& x.RequestType == oType
                //&& x.RequestedDateTime.HasValue).ToList();
                ///Debemos de traer todas? && x.RequestedDateTime.Value >= model.StartDate
                foreach (var o in q)
                {
                    var newInvite = MessagingBC.GetInvitation(o.Id, MessageSender.Asset);
                    result.Invitations.Add(newInvite);
                    result.Space.Add(new ScheduleSpace
                    {
                        SType = ScheduleSpaceType.Invitation,
                        StartDate = newInvite.EventDateTime.Value,
                        EndDate = newInvite.EventEnd.Value,
                        OrderId = newInvite.OrderId,
                        LabelMsg = newInvite.CustomerName,
                        OType = (OrderType)o.RequestType
                    });
                }

                maxdays = db.XConfigs.FirstOrDefault().ScheduleFutureDays;


                if (calendar.Count > 0)
                {
                    //Lista de bloques reservados
                    var reserved = result.Invitations.Select(x => new ScheduleSpace
                    {
                        StartDate = x.EventDateTime.Value,
                        EndDate = x.EventEnd.Value
                    }).ToList();

                    //por cada día obtener el inicio y el final
                    //primero obtener todos los calendarios configurados, independiente de la hora o dia
                    var dayCals = db.ACalendarDetails.Where(x => x.ACalendar.Asset.Id == model.AssetId).ToList();
                    var wds = new DateTime(dayCals.Min(x => x.ACalendar.From));
                    var wde = new DateTime(dayCals.Max(x => x.ACalendar.To));
                    result.WorkDayStart = wds.Hour;
                    result.WorkDayEnd = wde.Hour;

                    List<ScheduleSpace> workTimes = new List<ScheduleSpace>();
                    for (int dd = 0; dd <= 6; dd++)//0=domingo,6=sabado
                    {
                        var days = dayCals.Where(x => x.DayNum == dd).ToList();
                        if (days.Count > 0)
                        {
                            var min = days.Min(x => x.ACalendar.From);
                            var max = days.Max(x => x.ACalendar.To);

                            workTimes.Add(new ScheduleSpace
                            {
                                StartDate = new DateTime(min),
                                EndDate = new DateTime(max),
                                day = dd,
                                LabelMsg = days[0].ACalendar.Name
                            });
                        }
                    }

                    //OBTENER ESPACIOS DISPONIBLES
                    //Primero obtener el ultimo día, puede ser el ultimo día de la cita o de la semana
                    DateTime maxDate = model.StartDate.Value.AddDays(maxdays);
                    int interval = 15;//de 15 en 15 minutos
                    DateTime i =DateTime.UtcNow;
                    DateTime f =DateTime.UtcNow;
                    for (DateTime ct = model.StartDate.Value; ct <= maxDate; ct = ct.AddDays(1))
                    {

                        //iniciar a las 12:00am o 00:00:00
                        DateTime final = ct.Add(new TimeSpan(23, 59, 59));
                        bool hasRSVP = false;
                        i = ct;
                        for (DateTime inicial = ct; inicial <= final; inicial = inicial.Add(new TimeSpan(0, interval, 0)))
                        {
                            //Buscar intersecciones
                            var overlaped = reserved.Where(p =>
                             ((inicial >= p.StartDate &&
                               final >= p.EndDate &&
                               inicial < p.EndDate)
                             ||
                             (inicial <= p.StartDate &&
                              final <= p.EndDate &&
                              final > p.StartDate))
                            ).OrderBy(x => x.StartDate).ToList();
                            

                            if (overlaped.Count > 0)
                            {
                                //Si si hay, entonces, agregar un nuevo bloque con el rango disponible
                                //con la fecha inicial y la inicial de la reservacion en cuestionm

                                AppendTime(ref result, ref workTimes, i, overlaped[0].StartDate, false, true,wds,wde);
                                hasRSVP = true;
                                inicial = i = overlaped[0].EndDate;
                            }
                        }

                        if (!hasRSVP)
                        {
                            var vct = ct.Date;
                            hasRSVP = reserved.Where(x => x.StartDate.Date == vct).ToList().Count() > 0;
                        }
                        AppendTime(ref result, ref workTimes, i, final, true, hasRSVP,wds,wde);
                    }
      
                }


            }
            result.Space = result.Space.Where(x => x.SType != ScheduleSpaceType.SleepSpace)
                .OrderBy(x => x.StartDate).ToList();
            
            return result;
        }
        public static InvitationViewModel SaveOrderOxxo(OrderViewModel model, CustomerPaymentViewModel payment = null)
        {
            try
            {
                var aspnetUser = Helpers.Helper.GetAspnetUser();
                DateTime dt = ViewModels.Util.StripMinSec(DateTime.Now);
                model.CustomerName = aspnetUser.UserName;
              //  ConektaBC conekta;
                if (payment == null)
                {
                    //var nombre = ViewModels.Util.GetCustomerName(payment.NameOnCard);
                //    conekta = new ConektaBC(payment);
                }
                else
                {
                  //  conekta = new ConektaBC(payment);
                }

                using (var db = DBHelper.GetDatabase())
                {
                    var config = db.XConfigs.FirstOrDefault();
                    var customer = db.Customers.Where(x => x.UserId == aspnetUser.Id).FirstOrDefault();
                    model.CustomerId = customer.Id;

                    //Establecer la fecha de expiración
                    DateTime dtExpires = dt.AddMinutes(config.CartExpireHours);

                    //Ahora ChargeViewModel contiene el resultado del cargo, ya sea a tarjeta u oxxo
                    ChargeViewModel chargeResult = new ChargeViewModel();

                   // chargeResult = conekta.CreateOxxoPayCharge(model, dtExpires);
                    var invite = new InvitationViewModel();
                    invite.OxxoRef = 0;// chargeResult.Reference;
                    invite.OrderId = 0;
                    invite.ChargeModel.ChargeStatus = OrderStatus.Pending;


                    if (model.AddressId == -1)
                    {
                        var newAddress = db.CAddresses.Add(new CAddress
                        {
                            CustomerId = model.CustomerId,
                            Address1 = model.Address1,
                            Address2 = model.Address2,
                            Address3 = model.Address3,
                            Comments = model.Comments
                        });
                        db.SaveChanges();
                        model.AddressId = newAddress.Id;
                    }


                    var startingDistance = GetDistance(model.ALatitude, model.ALongitude, model.CLatitude, model.CLongitude);

                    //Establecer la fecha y hora final del servicio aprox
                    //TODO: esta es la fecha final estimada, falta que la chica acepte.
                    //Probablemente sea bueno actualizarla

                    //DateTime serviceWillEnd = model.RequestType == OrderType.ASAP ?
                    //    dt.AddHours(model.CategoryTier.NumHours) :

                    DateTime serviceWillEnd = model.EventDateTime.Value.AddHours(model.CategoryTier.NumHours);
                    //Establecer el valor en efectivo a recolectar en caso de que no se haya cubierto el 100%
                    decimal? cash = model.DownPayment.HasValue ? model.TotalCost - model.DownPayment : null;

                    var newOrder = db.orderSBs.Add(new orderSB
                    {
                        CustomerId = model.CustomerId,
                        AssetId = model.AssetId,
                        CategoryTierId = model.CategoryTier.Id,
                        AdressId = model.AddressId,

                        CLatitude = model.CLatitude,
                        CLongitude = model.CLongitude,
                        ALatitude = model.ALatitude,
                        ALongitude = model.ALongitude,

                        DownPayment = model.DownPayment,
                        TotalCost = model.TotalCost,

                        CreatedOn = dt,
                        ExpiresOn = dtExpires,//Minutos
                        ServiceEnd = serviceWillEnd,
                        CashRecollected = cash,

                        OrderStatus = (int)OrderStatus.Pending,
                        RequestType = (int)model.RequestType,
                        RequestedDateTime = model.EventDateTime,
                        StartingDistance = startingDistance,
                        CustomerPaymentId = model.PaymentMethodId,
                        ChargeId = chargeResult.ChargeId,
                        PaymentMethod = (int)model.paymentMethod,
                        PaypalTransId = model.PaypalTransId,
                        OxxoRef = chargeResult.Reference
                    });
                    db.SaveChanges();
                    return invite;
                }

               
              
            }
            catch (Exception ex)
            {
                LogBC.LogException(ex);
            }
            return GenerateErrorModel();
        }
        private static void AppendTime(ref AScheduleViewModel result,
            ref List<ScheduleSpace> workTimes, DateTime From, DateTime To, bool IsEndOfDay, 
            bool HasReservations, DateTime wds, DateTime wde)
        {
            //obtener horarios del dia
            var cs = workTimes.Where(x => x.day == (int)From.DayOfWeek).FirstOrDefault();
            if (cs != null)
            {
                cs.StartDate = new DateTime(From.Year, From.Month, From.Day, cs.StartDate.Hour, cs.StartDate.Minute, 0);
                cs.EndDate = new DateTime(To.Year, To.Month, To.Day, cs.EndDate.Hour, cs.EndDate.Minute, 0);
            }
            //No hay calendario para ese día, ejemplo: el domingo no se trabaja
            else
            {
                //es posible que a pesar de que no tiene calendario ese dia, tenga una RSVP via online
                if(!IsEndOfDay)
                {
                    //todo
                }
                else
                    result.Space.Add(new ScheduleSpace
                    {
                        StartDate = new DateTime(From.Year, From.Month, From.Day, 0, 0, 0),
                        EndDate = new DateTime(From.Year, From.Month, From.Day, 23, 59, 59),
                        SType = ScheduleSpaceType.SleepSpace,
                        LabelMsg = string.Empty,
                        OType = OrderType.NotDefined
                    });
                return;
            }

            if (IsEndOfDay && !HasReservations && cs != null)
            {
                wds = new DateTime(cs.StartDate.Year, cs.StartDate.Month, cs.StartDate.Day, wds.Hour, wds.Minute, 0);
                wde = new DateTime(cs.EndDate.Year, cs.EndDate.Month, cs.EndDate.Day, wde.Hour, wde.Minute, 0);
                //Validar llenar espacios
                if (cs.StartDate > wds)
                    result.Space.Add(new ScheduleSpace
                    {
                        StartDate = wds,
                        EndDate = cs.StartDate,
                        SType = ScheduleSpaceType.SleepSpace,
                        LabelMsg = string.Empty,
                        OType = OrderType.NotDefined
                    });

                result.Space.Add(new ScheduleSpace
                {
                    StartDate = cs.StartDate,
                    EndDate = cs.EndDate,
                    SType = ScheduleSpaceType.FreeSpace,
                    LabelMsg = cs.LabelMsg,
                    OType = OrderType.NotDefined
                });

                if (cs.EndDate < wde)
                    result.Space.Add(new ScheduleSpace
                    {
                        StartDate = cs.EndDate,
                        EndDate = wde,
                        SType = ScheduleSpaceType.SleepSpace,
                        LabelMsg = string.Empty,
                        OType = OrderType.NotDefined
                    });

                return;
            }



            bool applyEndOfDay = false;
            bool AddTime = true;
            //Agregar tiempo muerto
            if (!IsEndOfDay)// || (cs.StartDate <= From && ))
            {
                //Si entra aquí, significa que es una invitacion y el elemento ya fue agregado
                //Anteriormente, solamente necesitamos rellenar espacios
                //if (cs.StartDate <= From)
                //{
                result.Space.Add(new ScheduleSpace
                {
                    SType = ScheduleSpaceType.FreeSpace,
                    StartDate = cs.StartDate,
                    EndDate = To,
                    LabelMsg = cs.LabelMsg,
                    OType = OrderType.NotDefined
                });
                AddTime = false;
                //From = cs.StartDate;
                //}
            }
            else if (IsEndOfDay && cs.EndDate >= To)
            {
                applyEndOfDay = true;
            }

            if (AddTime)
            {
                if(cs.EndDate>From)
                {
                    result.Space.Add(new ScheduleSpace
                    {
                        SType = ScheduleSpaceType.FreeSpace,
                        LabelMsg = cs.LabelMsg,
                        StartDate = From,
                        EndDate = applyEndOfDay ? cs.EndDate : To,
                        OType = OrderType.NotDefined
                    });
                }

            }

            if(applyEndOfDay)
            {
                result.Space.Add(new ScheduleSpace
                {
                    SType = ScheduleSpaceType.SleepSpace,
                    LabelMsg = string.Empty,
                    StartDate = cs.EndDate,
                    EndDate = To
                });
            }

        }


        public static bool UpdateOrderStatus(OrderViewModel model)
        {
            DateTime dt =DateTime.UtcNow;

            using (var db = DBHelper.GetDatabase())
            {
                var order = db.Orders
                        .Include("Asset")
                        .Include("Customer")
                        .Where(x => x.Id == model.Id).FirstOrDefault();
                order.OrderStatus = (int)model.Status;
                order.UpdatedOn = dt;

                #region Status Notification
                //En caso de que la chica este confirmando, automaticamente
                //enviar un saludo al cliente al chat
                if (model.Status == OrderStatus.Confirmed)
                {
                    #region Notifications
                    //Avisarle al cliente que fue aceptada
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.AcceptedInvitation,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = null,
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    var Credito = db.tCreditos.Where(x => x.NoOrden == order.Id.ToString()).FirstOrDefault();
                    if (Credito != null)
                    {
                        Credito.Credito = false;
                    }

                    //El saludo
                    var greetNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.AssetMessage,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = GetGreetingMessage(),
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    #endregion

                    #region Balance
                    //db.ATransactions.Add(new ATransaction)
                    //todo
                    var a = order.Asset;
                    var config = db.XConfigs.FirstOrDefault();
                    //Agregar el monto total menos la comision *
                    a.Balance += order.TotalCost - (order.TotalCost * ((decimal)config.Comision));
                    order.ComisionStatus = (int)ComisionStatus.Complete;
                    #endregion

                }
                //En caso de que la chica rechaze la invitacion
                if (model.Status == OrderStatus.Rejected)
                {
                    #region Notifications
                    //Avisarle al cliente que fue Declinada
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.RejectedInvitation,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = null,
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    #endregion
                }
                //En caso de que el cliente cancele la invitacion
                if (model.Status == OrderStatus.Canceled)
                {
                    #region Notifications
                    //Avisarle al cliente que fue Declinada
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.CanceledInvitation,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = null,
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Asset
                    });

                    #endregion

                    #region Balance
                    var a = order.Asset;
                    var config = db.XConfigs.FirstOrDefault();
                    //Agregar el monto total menos la comision *
                    a.Balance -= order.TotalCost - (order.TotalCost * ((decimal)config.Comision));
                    order.ComisionStatus = (int)ComisionStatus.Cancelled;
                    #endregion
                }
                #endregion

                db.SaveChanges();
            }

            return true;
        }
        public static OrderStatusViewModel GetOrderStatus(long OrderId)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var o = db.Orders.Where(x => x.Id == OrderId).FirstOrDefault();
                return new OrderStatusViewModel
                {
                    orderStatus = (OrderStatus)o.OrderStatus,
                    depositStatus = (DepositStatus)o.DepositStatus
                };
            }
        }
        public static double GetDistance(double ALatitude, double ALongitude, 
                                         double CLatitude, double CLongitude)
        {
            var coord = new GeoCoordinate(ALatitude, ALongitude);
            return new GeoCoordinate(CLatitude, CLongitude).GetDistanceTo(coord);
        }

        //Format distance in meters to meters o kms
        public static string FormatDistance(double distance)
        {
            if (distance < 1000)
                return Math.Round(distance, 0).ToString() + " metros";
            return Math.Round(distance / 1000, 1).ToString() + "km";
        }
        public static string GetGreetingMessage()
        {
            return "Gracias por invitarme, estamos en contacto por este chat mientras nos vemos.";

        }

        public static string verificaOxxo(string id)
        {
            string result = "";
         //   var conekta = new ConektaBC(ViewModels.Util.GetCustomerName("xxx"));            
          //  result = conekta.verificaPagoOxxo(id);
            return result;
        }

        public static void UpdateStatusOxxo(object id)
        {
            using (var db = DBHelper.GetDatabase())
            {
                long OxxoRf = long.Parse(id.ToString());
                var order = db.Orders.Where(x => x.OxxoRef == OxxoRf).FirstOrDefault();
                if (order != null)
                {
                    order.DepositStatus = (int)DepositStatus.Payed;                   
                    db.SaveChanges();
                }
            }
        }

        public static CustomerCredit GetCustomerCredit(string name)
        {
            CustomerCredit cc = new CustomerCredit();
            using (var db = DBHelper.GetDatabase())
            {
                var Credito = db.tCreditos.Where(x => x.UserId == name && x.Credito == true).FirstOrDefault();

                if (Credito != null)
                {
                    cc.UserId = Credito.UserId;
                    cc.NoOrden = Credito.NoOrden;
                    cc.Monto = (decimal)Credito.Monto;
                    cc.Credito = (bool)Credito.Credito;
                    cc.CreatedOn = (DateTime)Credito.CreatedOn;
                    return cc;
                }
                else
                {
                    cc.UserId = name;
                    cc.NoOrden = "";
                    cc.Monto = 0;
                    cc.Credito = false;
                    cc.CreatedOn = null;
                    return cc;
                }

                  
            }
        }

        public static CustomerPaymentViewModel getDatosTDC(int id)
        {
            var result = new CustomerPaymentViewModel();
            using (var db = DBHelper.GetDatabase())
            {
                var Metodos = db.CustomerPaymentMethods;
              var payMeth =   Metodos.Where(x => x.CustomerId == id).FirstOrDefault();

                if (payMeth != null)
                {
                    result.Nombre = payMeth.Nombre;
                    result.Apellidos = payMeth.Apellidos;
                    result.Email = payMeth.Email;
                    result.CreditCardNumber = payMeth.CreditCardNumber;
                    result.ProviderId = payMeth.ProviderId;
                    result.Telefono = payMeth.Telefono;

                    return result;
                }
                else
                    return null;
            }   
            
        }
    }
}