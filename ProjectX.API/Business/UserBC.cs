﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.ViewModels;
using ProjectX.API.Helpers;
using System.Collections.ObjectModel;
using ProjectX.API.Models;
using System.Security.Claims;
using System.Runtime.Caching;

namespace ProjectX.API.Business
{
    public class UserBC
    {

        #region Login/Registration/Activation
        public static AspNetUser UserExists(string email)
        {
            AspNetUser aspnetUser = null;
            using (var db = DBHelper.GetDatabase())
            {
                aspnetUser = db.AspNetUsers.Where(u => u.UserName == email).FirstOrDefault();
            }
            return aspnetUser;
        }
        public static void UpdateUser(string email, int Value)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var aspnetUser = db.AspNetUsers.Where(u => u.UserName == email).FirstOrDefault();
                if(aspnetUser!=null)
                {
                    aspnetUser.AdminType = Value;
                    db.SaveChanges();
                    //Customer = 1,
                    //Asset = 2,
                }
            }           
        }

        public static void Contadordevisitas(string userId)
        {
            using (var db = DBHelper.GetDatabase())
            {              
                var cus = db.Customers.Where(x => x.UserId == userId).FirstOrDefault();
                if (cus != null)
                {
                    if (cus.NoVisitas == null)
                    {
                        cus.NoVisitas = 1;
                    }
                    else {
                        int novis = (int)cus.NoVisitas + 1;
                        cus.NoVisitas = novis;// contador de visitas de clientes
                    }                    
                    db.SaveChanges();
                }
            }
        }


        public static RegisterViewModel SaveRegisterModel(RegisterViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                //Primera ves, agregar
                var newModel = db.UserRequestLogs.Add(new UserRequestLog()
                {
                    Id = model.Id,
                    UserId = model.HostId,
                    UserName = model.UserName,
                    CreatedOn =DateTime.UtcNow,
                    Status = model.ActivationStatus//created
                });

                long? assetId = null;
                long? userId = null;
                var u = db.AspNetUsers.Where(x => x.Id == model.HostId).FirstOrDefault();
                                        
                //si el host es cliente
                if(u.AdminType == 1)
                {
                    var c = db.Customers.Include("AspNetUser").Where(x => x.UserId == u.Id).FirstOrDefault();
                    userId = c.Id;                    
                }
                else
                {
                    var a = db.Assets.Where(x => x.UserId == model.HostId).FirstOrDefault();
                    assetId = a.Id;
                }

                int receiver = (int)(assetId != null ? MessageSender.Asset : MessageSender.Customer);
                //no se puede porque aun no existe el assetId o el CustomerId
                var n = db.Notifications.Add(new Notification
                {
                    MType = (int)MessageType.AuthRequest,
                    MReceiver = receiver,
                    AssetId = assetId,
                    CustomerId = userId,
                    MessageText = null,
                    OrderId = null,
                    MessageStatus = (int)MessageStatus.Created,
                    CreatedOn =DateTime.UtcNow,
                    RequestId = model.Id
                });                
                    db.SaveChanges();   
                return model;
            }
        }
        public static bool UpdateRegisterModelStatus(RegisterViewModel model)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var reg = db.UserRequestLogs.Where(x => x.Id == model.Id).FirstOrDefault();
                if (reg != null)
                {
                    reg.Status = model.ActivationStatus;
                    reg.UpdatedOn =DateTime.UtcNow;
                    db.SaveChanges();
                    return true;
                }
                return false;
            }
        }
        public static List<RegisterViewModel> GetActivationRequest()
        {
            using (var db = DBHelper.GetDatabase())
            {
                var result = db.UserRequestLogs.Where(x => x.Status == 3)
                    .Select(y => new RegisterViewModel
                {
                    Id = y.Id,
                    UserId = y.UserId,
                    UserName = y.UserName,
                    CreateOn = y.CreatedOn,
                    ActivationStatus = y.Status
                   
                }).ToList();

                foreach (var r in result)
                {
                    r.ExpiresOn = r.CreateOn.AddDays(30);//todo
                    r.ActivationStatusStr = GetActivationStatusStr(r.ActivationStatus);
                }
                return result;
                   
            }
        }
        public static string GetActivationStatusStr(short ActivationStatus)
        {
            switch(ActivationStatus)
            {
                case 2: return "Sin Enviar";
                case 3: return "Esperando Activación";
                case 4: return "Autorizado";
                case 5: return "Rechazado";
            }

            return "Unknown";
        }

        #endregion

        
    }
}