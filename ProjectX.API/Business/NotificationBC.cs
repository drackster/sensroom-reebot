﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using PushSharp;
using PushSharp.Core;
using PushSharp.Google;
using PushSharp.Apple;
using ProjectX.API.Models;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using ProjectX.ViewModels;

namespace ProjectX.API.Business
{
    public sealed class NotificationSender
    {
        #region Private Members

        private static NotificationSender instance = null;
        private static readonly object padlock = new object();
        private ApnsServiceBroker push_iOS;
        private GcmServiceBroker push_Droid;
        private bool isBussy;
        private bool isBussy_iOS;
        private bool isBussy_Droid;
        private bool isInitialized;
        
        #region Configuration
        private ApnsConfiguration apnsConfig;
        private GcmConfiguration gcmConfig;
        private string appleCert;
        private string appleCertPswd;
        private string gcmSenderId;
        private string gcmAPIKey;
        private string gcmAppId;
        #endregion

        #endregion

        #region Configuration Properties
        public ApnsConfiguration ApnsConfig
        {
            get
            {
                if (apnsConfig == null)
                {
                    var appleCert = File.ReadAllBytes(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, AppleCert));
                    apnsConfig = new ApnsConfiguration(ApnsConfiguration.ApnsServerEnvironment.Production,
                    appleCert, AppleCertPswd);

                }

                return apnsConfig;
            }
        }
        public GcmConfiguration GcmConfig
        {
            get
            {
                if (gcmConfig == null)
                    gcmConfig = new GcmConfiguration(GcmSenderId, GcmAPIKey, GcmAppId);
                return gcmConfig;
            }
        }
        public string AppleCert
        {
            get
            {
                if (appleCert == null)
                    appleCert = System.Configuration.ConfigurationManager.AppSettings["AppleCert"].ToString();
                return appleCert;
            }
        }
        public string AppleCertPswd
        {
            get
            {
                if (appleCertPswd == null)
                    appleCertPswd = System.Configuration.ConfigurationManager.AppSettings["AppleCertPswd"].ToString();
                return appleCertPswd;
            }
        }
        public string GcmSenderId
        {
            get
            {
                if (gcmSenderId == null)
                    gcmSenderId = System.Configuration.ConfigurationManager.AppSettings["GcmSenderId"].ToString();
                return gcmSenderId;
            }
        }
        public string GcmAPIKey
        {
            get
            {
                if (gcmAPIKey == null)
                    gcmAPIKey = System.Configuration.ConfigurationManager.AppSettings["GcmAPIKey"].ToString();
                return gcmAPIKey;
            }
        }
        public string GcmAppId
        {
            get
            {
                if (gcmAppId == null)
                    gcmAppId = System.Configuration.ConfigurationManager.AppSettings["GcmAppId"].ToString();
                return gcmAppId;
            }
        }
        #endregion

        #region Singleton
        public static NotificationSender Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new NotificationSender();
                    }
                    return instance;
                }
            }
        }

        #endregion

        #region Properties
        //public bool IsInitialized { get { return isInitialized; } }
        public bool IsBussy_iOS { get { return isBussy_iOS; } set { isBussy_iOS = value; } }
        public bool IsBussy_Droid { get { return isBussy_Droid; } set { isBussy_Droid = value; } }
        #endregion

        #region Ctor/Dtor
        public NotificationSender() { }
        ~NotificationSender()
        {
            Log("Waiting for Queue to Finish...");
            //Stop and wait for the queues to drains
           // push_iOS.Stop();
           // push_Droid.Stop();
            //System.Threading.Thread.Sleep(1000);//wait
            Log("Done Stopping Push Services");

        }

        public void Init()
        {
            Log("Initializing Singleton NotificationSender");
            
            push_iOS = new ApnsServiceBroker(ApnsConfig);
            push_Droid = new GcmServiceBroker(GcmConfig);

            //Startup objects for each platform and scope
            Log("Initializing ApnsServiceBroker...");
            Init(push_iOS);
            Log("Initializing GcmServiceBroker...");
            Init(push_Droid);
            //Init(push_Droid_Op, ScopeType.Operator, 2);
            isInitialized = true;

        }
        private void Init(ApnsServiceBroker push)
        {
            try
            {
                Log("Initializing APNS NotificationSender...");
                push.OnNotificationSucceeded += Push_OnNotificationSucceeded;// += NotificationSent;
                push.OnNotificationFailed += Push_OnNotificationFailed;//push.OnNotificationFailed += NotificationFailed;
                push.Start();
                //push.ChangeScale(10);
            }
            catch (Exception ex)
            {
                Log(ex.Message, true);
            }
        }
        private void Init(GcmServiceBroker push)
        {
            try
            {
                Log("Initializing GCM NotificationSender...");
                push.OnNotificationSucceeded += Gcm_Push_OnNotificationSucceeded;
                push.OnNotificationFailed += Gcm_Push_OnNotificationFailed;
                push.Start();
                //push.ChangeScale(10);
            }
            catch (Exception ex)
            {
                Log(ex.Message, true);
            }
        }

        #endregion

        #region Apple Events
        private void Push_OnNotificationSucceeded(ApnsNotification notification)
        {
            Log("Sent: -> Id: " + notification.Identifier.ToString() + " Token:" + notification.DeviceToken);
            var notificationId = Convert.ToInt64(notification.Tag.ToString());
            MessagingBC.UpdateNotificationPushStatus(notificationId, NotificationStatus.Sent);
        }
        private void Push_OnNotificationFailed(ApnsNotification notification, AggregateException exception)
        {
            Log("Failed: -> Id: " + notification.Identifier.ToString() + " Token:" + notification.DeviceToken);
            ApnsNotificationErrorStatusCode statusCode = ApnsNotificationErrorStatusCode.Unknown;

            // See what kind of exception it was to further diagnose
            exception.Handle(ex => {

                // See what kind of exception it was to further diagnose
                if (ex is ApnsNotificationException)
                {
                    var notificationException = (ApnsNotificationException)ex;

                    // Deal with the failed notification
                    var apnsNotification = notificationException.Notification;
                    statusCode = notificationException.ErrorStatusCode;
                    Log($"Apple Notification Failed: ID={apnsNotification.Identifier}, Code={statusCode}, IsValid={apnsNotification.IsDeviceRegistrationIdValid().ToString()}");

                    //expirar token si tiene errores
                    if (statusCode == ApnsNotificationErrorStatusCode.InvalidToken || statusCode == ApnsNotificationErrorStatusCode.InvalidTokenSize
                       || statusCode == ApnsNotificationErrorStatusCode.MissingDeviceToken)
                    {
                      //  MessagingBC.ExpireDeviceId(notification.DeviceToken, null);
                    }
                }
                else
                {
                    // Inner exception might hold more useful information like an ApnsConnectionException			
                    Log($"Apple Notification Failed for some unknown reason : {ex.InnerException}");
                }

                // Mark it as handled
                return true;
            });
        }

        #endregion

        #region Google Events
        private void Gcm_Push_OnNotificationSucceeded(GcmNotification notification)
        {
            Log("Sent: -> Id: " + notification.MessageId != null ? notification.MessageId : "unknown"
                + " Token:" + notification.RegistrationIds[0]);
            var notificationId = Convert.ToInt64(notification.Tag.ToString());
            MessagingBC.UpdateNotificationPushStatus(notificationId, NotificationStatus.Sent);

        }
        private void Gcm_Push_OnNotificationFailed(GcmNotification notification, AggregateException exception)
        {
            Log("Failed: -> Id: " + notification.MessageId != null ? notification.MessageId : "unknown"
                + " Token:" + notification.RegistrationIds[0]);

            //obtenemos el Id de la notificacion
            var notificationId = Convert.ToInt64(notification.Tag.ToString());

            //Vanos a reutilizar el de apple porque no existe en de google
            ApnsNotificationErrorStatusCode statusCode = ApnsNotificationErrorStatusCode.ProcessingError;

            // See what kind of exception it was to further diagnose
            exception.Handle(ex =>
            {
                // See what kind of exception it was to further diagnose
                if (ex is GcmNotificationException)
                {
                    var notificationException = (GcmNotificationException)ex;

                    // Deal with the failed notification
                    var gcmNotification = notificationException.Notification;
                    var description = notificationException.Description;
                    Log($"GCM Notification Failed: ID={gcmNotification.MessageId}, Desc={description}");
                }
                else if (ex is GcmMulticastResultException)
                {
                    var multicastException = (GcmMulticastResultException)ex;
                    foreach (var succeededNotification in multicastException.Succeeded)
                        Log($"GCM Notification Succeeded: ID={succeededNotification.MessageId}");

                    foreach (var failedKvp in multicastException.Failed)
                    {
                        var n = failedKvp.Key;
                        var e = failedKvp.Value;
                        Log($"GCM Notification Failed: ID={n.MessageId}, Desc={e.Message}");
                    }
                }
                else if (ex is DeviceSubscriptionExpiredException)
                {
                    var expiredException = (DeviceSubscriptionExpiredException)ex;
                    var oldId = expiredException.OldSubscriptionId;
                    var newId = expiredException.NewSubscriptionId;
                    Console.WriteLine($"Device RegistrationId Expired: {oldId}");
                    if (!string.IsNullOrWhiteSpace(newId))
                        // If this value isn't null, our subscription changed and we should update our database
                        Console.WriteLine($"Device RegistrationId Changed To: {newId}");
                    //expirar el viejo y agregar el nuevo id
                    MessagingBC.ExpireDeviceId(oldId, newId);
                    statusCode = ApnsNotificationErrorStatusCode.InvalidToken;
                }
                else if (ex is RetryAfterException)
                {
                    var retryException = (RetryAfterException)ex;
                    // If you get rate limited, you should stop sending messages until after the RetryAfterUtc date
                    Console.WriteLine($"GCM Rate Limited, don't send more until after {retryException.RetryAfterUtc}");
                }
                else
                {
                    Console.WriteLine("GCM Notification Failed for some unknown reason");
                }
                // Mark it as handled
                return true;
            });

            if(statusCode == ApnsNotificationErrorStatusCode.ProcessingError)
            {
                //aumentamos el numero de intentos, para que deje de intentar al 3er intento
                MessagingBC.UpdateNotificationPushStatus(notificationId, NotificationStatus.Failed);
            }
        }

        #endregion
        public void SendNotifications(List<PushNotificationModel> Notifications)
        {
            //test
            Notifications = Notifications.ToList();//Android Only .Where(x => x.Token.DeviceId == 1)

            //PushBroker push = GetCurrentPushBroker(Scope, DeviceType);
            if (isBussy)
            {
                Log("Currently bussy initializing");
                return;
            }

            if (!isInitialized)
            {
                isBussy = true;
                Init();
                isBussy = false;
            }
            try
            {
                foreach (PushNotificationModel notification in Notifications)
                {
                    Log("Sending notification to Token/Id " + notification.Token.Token + "/" + notification.NotificationId.ToString()
                        + " to Device:" + notification.Token.DeviceId.ToString());
                    
                    #region Apple
                    if (notification.Token.DeviceId == (int)DeviceType.Apple)
                    {
                        //Fluent construction of an iOS notification
                        //IMPORTANT: For iOS you MUST MUST MUST use your own DeviceToken here that gets generated within your iOS app itself when the Application Delegate
                        //  for registered for remote notifications is called, and the device token is passed back to you
                        push_iOS.QueueNotification(new ApnsNotification()
                        {
                            Tag = notification.NotificationId,
                            DeviceToken = notification.Token.Token,
                            Payload = JObject.Parse("{\"aps\":{\"badge\":7,\"sound\":\"default\",\"alert\":{\"body\":\"" + notification.Message + "\",\"title\":\"" + notification.Title + "\"}}}")
                        });
                    }
                    #endregion

                    #region Android
                    else if (notification.Token.DeviceId == (int)DeviceType.Google)
                    {
                        //Fluent construction of an Android GCM Notification
                        //IMPORTANT: For Android you MUST use your own RegistrationId here that gets generated within your Android app itself!
                        //REF https://docs.pushtechnology.com/docs/5.7.9/manual/html/administratorguide/pushnotifications/pnb_json_notification.html
                        var encodedMsg = System.Net.WebUtility.UrlEncode(notification.Message);
                        var encodedTitle = System.Net.WebUtility.UrlEncode(notification.Title);
                        push_Droid.QueueNotification(new GcmNotification()
                        {
                            To = notification.Token.Token,
                            //RegistrationIds = new List<string> { notification.Token.Token },
                            Tag = notification.NotificationId,
                            Priority = GcmNotificationPriority.High,
                            //DelayWhileIdle = true,
                            //TimeToLive = 3,
                            Notification = JObject.Parse("{\"title\":\"" + encodedTitle + "\",\"body\":\"" + encodedMsg + "\",\"vibrate\":1, \"sound\":\"default\"}")
                        });
                    }
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Log(ex.Message, true);
            }
        }

        #region Util Methods
        private void Log(string message, bool isError = false)
        {
            Debug.WriteLine(isError ? "ERROR:" : "" + message);
            
            //if (isError)
            //    LogHelper.LogException(new Exception(message), "NotificationBC");
        }

        #endregion
    }
}