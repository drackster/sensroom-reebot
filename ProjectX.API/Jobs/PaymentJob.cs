﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using ProjectX.ViewModels;
using System.Web;
using ProjectX.API.Models;


namespace ProjectX.API.Jobs
{
    public class PaymentJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var tiempoActual =DateTime.UtcNow;
                var ordenes = db.Orders;
                int[] type = { 1, 2, 4 };
                var cantOrders = ordenes.Where(x => x.OrderStatus == 1 && type.Contains(x.PaymentMethod) && x.ModeType == 1).FirstOrDefault();
                if (cantOrders != null)
                {
                    var dif = tiempoActual.Subtract(cantOrders.CreatedOn).TotalMinutes;

                    if (dif > 35)
                    {
                        cantOrders.OrderStatus = (int)OrderStatus.Canceled;

                        var credito = db.tCreditos.Add(new tCredito
                        {
                            Credito = true,
                            CreatedOn =DateTime.UtcNow,
                            Monto = cantOrders.DownPayment,
                            NoOrden = cantOrders.Id.ToString(),
                            UserId = cantOrders.Customer.AspNetUser.UserName
                        });

                        var order = new OrderViewModel();
                        order.Id = cantOrders.Id;
                        order.Status = OrderStatus.Canceled;
                        UpdateOrderStatus(order);
                        try
                        {
                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
        }

        private void UpdateOrderStatus(OrderViewModel model)
        {
            Business.OrderBC.UpdateOrderStatus(model);            
        }
    }
}