﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Jobs
{
    public class AlertMessageChat : IJob
    {      
        public void Execute(IJobExecutionContext context)
        {           
            AlertaMensajeChat();
        }

        public void AlertaMensajeChat()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var mensajes = db.Notifications.Where(x => x.MReceiver == 1 && x.MType == 3 && x.MessageStatus == 0 && x.SendTries == 0).ToList();
                if (mensajes.Count > 0)
                {
                    var itemList = (from t in mensajes
                                    select t).OrderByDescending(t => t.CreatedOn);

                    var message = itemList.FirstOrDefault();

                    var result = (DateTime.UtcNow - message.CreatedOn).TotalMinutes;
                    if (result > 6)
                    {
                        var asset = db.Assets.Where(x => x.Id == message.AssetId).FirstOrDefault();
                        msm sms = new msm();
                        string[] mensaje = { asset.telefono, "Tienes mensajes sin leer en el chat, entra a" + " sens-room.com/Account/login" };                        
                        sms.enviamsm(mensaje);
                        var mensajesList = db.Notifications.Where(x => x.MReceiver == 1 && x.MType == 3 && x.MessageStatus == 0 && x.SendTries == 0).ToList();
                        mensajesList.Select(c => { c.SendTries = 1; return c; }).ToList();
                        db.SaveChanges();
                    }                      
                }               
            }
        }
    }
}