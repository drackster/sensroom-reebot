﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using System.Threading.Tasks;
using ProjectX.API.Business;

namespace ProjectX.API.Jobs
{
    public class NotificationsJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {

            //LogBC.Log("Executing NotificationsJob...");

            if(!NotificationSender.Instance.IsBussy_iOS)
            {
                NotificationSender.Instance.IsBussy_iOS = true;
                try
                {
                    using(var db = Helpers.DBHelper.GetDatabase())
                    {
                        //MReceiver=2 Significa que solo las notificaciones dirigidas a chicas aplican
                        var notifications = db.Notifications
                            .Include("Asset")
                            .Include("Customer")
                            .Where(x => x.SentOn == null && x.SendTries <= 3
                                && (x.MReceiver == 2 || x.MReceiver == 0))
                            .OrderBy(x => x.CreatedOn).ToList();
                        if (notifications.Count > 0)
                        {

                        }
                        var ToProcess = new List<PushNotificationModel>();
                        foreach (var n in notifications)
                        {
                            var validTokens = n.Asset.DeviceTokens.Where(x => !x.ExpiredOn.HasValue).ToList();
                            if (validTokens.Count > 0)
                            {
                                foreach (var t in validTokens)
                                {
                                    var pn = new PushNotificationModel
                                    {
                                        NotificationId = n.Id,
                                        Message = n.MessageText,
                                        NStatus = NotificationStatus.Pending,
                                        Token = t
                                    };

                                    //Todo templates
                                    pn.Title = ViewModels.Helper.GetMessageTitle((MessageType)n.MType, MessageStatus.Created);
                                    //Si el mensaje es privado, entonces lo dejamos igual
                                    if (n.MType != (int)MessageType.CustomerMessage)
                                    {
                                        //si no, le damos formato
                                        //aqui siempre va el nombre del cliente porque solo las chicas reciben push notifications
                                        pn.Title = ViewModels.Helper.GetMessageTitle((MessageType)n.MType, (MessageStatus)n.MessageStatus);

                                        string customerName = string.Empty;
                                        if (n.Customer != null)
                                            customerName = n.Customer.AspNetUser.UserName;
                                        else if (n.UserRequestLog != null)
                                            customerName = n.UserRequestLog.UserName;
                                        
                                        pn.Message = ViewModels.Helper.GetMessageText((MessageType)n.MType, MessageSender.Asset, customerName, true);
                                    }
                                    ToProcess.Add(pn);
                                }
                            }
                            else//si no tiene dispositivos, entonces marcamos esa notificacion como enviada
                            {
                                n.SendTries = n.SendTries + 1;
                                n.SentOn =DateTime.UtcNow;
                                db.SaveChanges();
                            }
                        }
                        if(ToProcess.Count > 0)
                            NotificationSender.Instance.SendNotifications(ToProcess);
                        else
                            LogBC.Log("No new notifications to send...");

                    }

                    //keep alive
                    System.Net.WebClient c = new System.Net.WebClient();
                    var data = c.DownloadData("http://projectx2000.azurewebsites.net/test.aspx?" + System.Guid.NewGuid().ToString());


                }
                catch (Exception ex)
                {
                    Business.LogBC.LogException(ex);
                }
                finally
                {
                    //Validate(false);
                    NotificationSender.Instance.IsBussy_iOS = false;
                }
            }
            else
            {
                LogBC.Log("---->Notifications Job currently bussy...");
            }
            
        }

        //private bool Validate(bool Open)
        //{
        //    bool valid = false;
        //    using (var db = Helpers.DBHelper.GetDatabase())
        //    {
        //        var config = db.XConfigs.FirstOrDefault();
        //        if (!config.NSJobBussy && Open)
        //        {
        //            config.NSJobBussy = valid = true;
        //            db.SaveChanges();
        //        }
        //        else if (config.NSJobBussy && !Open)
        //        {
        //            config.NSJobBussy = false;
        //            valid = true;
        //            db.SaveChanges();
        //        }
        //    }
        //    return valid;
        //}
    }
}