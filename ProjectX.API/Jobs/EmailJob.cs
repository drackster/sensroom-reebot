﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Quartz;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using System.Threading.Tasks;
using ProjectX.API.Business;

namespace ProjectX.API.Jobs
{
    public class EmailJob : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            if(Validate(true))
            {
                try
                {
                    //Business.EmailBC.ProcessOrderLog();
                }
                catch (Exception ex)
                {
                    Business.LogBC.LogException(ex);
                }
                finally
                {
                    Validate(false);
                }
            }
            else
            {
                LogBC.Log("---->[Email] EmailJob is currently bussy...");
            }
        }

        private bool Validate(bool Open)
        {
            bool valid = false;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var config = db.XConfigs.FirstOrDefault();
                if (!config.EmailJobBussy && Open)
                {
                    config.EmailJobBussy = valid = true;
                    db.SaveChanges();
                }
                else if(config.EmailJobBussy  && !Open)
                {
                    config.EmailJobBussy = false;
                    valid = true;
                    db.SaveChanges();
                }
            }
            return valid;
        }

    }
}