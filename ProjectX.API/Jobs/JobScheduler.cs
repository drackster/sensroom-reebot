﻿using Quartz;
using Quartz.Impl;
using System;
using System.Configuration;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;

namespace ProjectX.API.Jobs
{
    public class JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail nsJob = JobBuilder.Create<NotificationsJob>().Build();
            //IJobDetail emailJob = JobBuilder.Create<EmailJob>().Build();

            IJobDetail PaymentJob = JobBuilder.Create<PaymentJob>().Build();

            IJobDetail offline = JobBuilder.Create<offlineAsset>().Build();

            IJobDetail AlertMessagges = JobBuilder.Create<AlertMessageChat>().Build();

            IJobDetail OrderStanbyCancel = JobBuilder.Create<OrderStanbyCancel>().Build();

            int nsJobTimeOut = 0;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var config = db.XConfigs.FirstOrDefault();
                nsJobTimeOut = config.NSJobSeconds;//CHANGED TO SECONDS
            }
            ITrigger trigger = TriggerBuilder.Create()
                .WithIdentity("nstriggertrigger", "XJobTriggers")
                .StartNow()
                .WithSimpleSchedule(x => x
                    .WithIntervalInSeconds(nsJobTimeOut)
                    //.WithIntervalInHours(nsJobTimeOut)
                    .RepeatForever())
                .Build();

            ITrigger PaymentTrigger = TriggerBuilder.Create()
              .WithIdentity("Paymenttrigger", "XJobTriggers")
              .StartNow()
              .WithSimpleSchedule(x => x
                  .WithIntervalInSeconds(30)
                  //.WithIntervalInHours(nsJobTimeOut)
                  .RepeatForever())
              .Build();

            ITrigger Offlinetrigger = TriggerBuilder.Create()
             .WithIdentity("Offlinetrigger", "XJobTriggers")
             .StartNow()
             .WithSimpleSchedule(x => x
                 .WithIntervalInSeconds(15)
                 //.WithIntervalInHours(nsJobTimeOut)
                 .RepeatForever())
             .Build();

            ITrigger AlertMessageChatTrigger = TriggerBuilder.Create()
                .WithIdentity("AlertMessageChatTrigger", "XJobTriggers")
                .StartNow()
                .WithSimpleSchedule(x => x
                .WithIntervalInSeconds(30)
                .RepeatForever())
                .Build();

            ITrigger OrderStByCancel = TriggerBuilder.Create()
                .WithIdentity("CancelaOrdenStanby", "XJobTriggers")
                .StartNow()
                .WithSimpleSchedule(x => x
                .WithIntervalInSeconds(60)
                .RepeatForever())
                .Build();


            // ITrigger emailTrigger = TriggerBuilder.Create()
            //    .WithIdentity("emailtrigger", "alcioneJobTriggers")
            //    .StartNow()
            //    .WithSimpleSchedule(x => x
            //        .WithIntervalInSeconds(Config.EmailJobSeconds)
            //        .RepeatForever())
            //    .Build();
            scheduler.ScheduleJob(OrderStanbyCancel, OrderStByCancel);
            scheduler.ScheduleJob(AlertMessagges, AlertMessageChatTrigger);
            scheduler.ScheduleJob(nsJob, trigger);
            scheduler.ScheduleJob(PaymentJob, PaymentTrigger);
            scheduler.ScheduleJob(offline, Offlinetrigger);
            //scheduler.ScheduleJob(emailJob, emailTrigger);
        }

        public static void UpdateJob(int CatalogOffSetMinutes)
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            
            // retrieve the trigger
            ITrigger oldTrigger = scheduler.GetTrigger(new TriggerKey("cachedcatalogtrigger", "alcioneJobTriggers"));
            if (oldTrigger != null)
            {
                //obtain the builder that would produce the trigger
                TriggerBuilder tb = oldTrigger.GetTriggerBuilder();

                ITrigger newTrigger = tb
                .WithSimpleSchedule(x => x
                .WithIntervalInMinutes(CatalogOffSetMinutes)
                .RepeatForever())
                .Build();

                scheduler.RescheduleJob(oldTrigger.Key, newTrigger);
            }
        }

        public static void UpdateEmailJob(int EmailJobSeconds)
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();

            // retrieve the trigger
            ITrigger oldTrigger = scheduler.GetTrigger(new TriggerKey("emailtrigger", "alcioneJobTriggers"));
            if (oldTrigger != null)
            {
                //obtain the builder that would produce the trigger
                TriggerBuilder tb = oldTrigger.GetTriggerBuilder();

                ITrigger newTrigger = tb
                .WithSimpleSchedule(x => x
                .WithIntervalInSeconds(EmailJobSeconds)
                .RepeatForever())
                .Build();

                scheduler.RescheduleJob(oldTrigger.Key, newTrigger);
            }
        }

        public static string ManualExec()
        {
            try
            {
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                // retrieve the trigger
                ITrigger oldTrigger = scheduler.GetTrigger(new TriggerKey("cachedcatalogtrigger", "alcioneJobTriggers"));
                if (oldTrigger != null)
                {
                    scheduler.TriggerJob(oldTrigger.JobKey);
                    return "Sincronización Manual Iniciada";
                }
            }
            catch(Exception ex)
            {
                Business.LogBC.LogException(ex);
            }

            return "Sucedió un error al iniciar la sincronización.";
        }

        public static string StopJob()
        {
            try
            {
                IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
                // retrieve the trigger
                ITrigger oldTrigger = scheduler.GetTrigger(new TriggerKey("cachedcatalogtrigger", "alcioneJobTriggers"));
                if (oldTrigger != null)
                {
                    return "Sincronización Manual Iniciada";
                }
            }
            catch (Exception ex)
            {
                Business.LogBC.LogException(ex);
            }

            return "Sucedió un error al iniciar la sincronización.";
        }
    }
}