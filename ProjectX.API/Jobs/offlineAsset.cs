﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Jobs
{
    public class offlineAsset : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            OffLineAsset();
        }

        public void OffLineAsset()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.statusProfile == 2 && x.IsOnline == true && x.OnlineExpires < DateTime.UtcNow).ToList();
                if (asset.Count > 0)
                {
                    for (int i = 0; i < asset.Count; i++)
                    {
                        asset[i].IsOnline = false;
                        db.SaveChanges();
                        MensajeOffline(asset[i].Name);
                    }
                }
            }
        }

        public void OffLineAssetStandby()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.statusProfile == 2 && x.offlineView == true && x.offlineViewExpires < DateTime.UtcNow).ToList();
                if (asset.Count > 0)
                {
                    for (int i = 0; i < asset.Count; i++)
                    {
                        asset[i].offlineView = false;
                        db.SaveChanges();
                        MensajeOfflineView(asset[i].Name);
                    }
                }
            }
        }

        public void MensajeOffline(string name)
        {          
            using (var db = Helpers.DBHelper.GetDatabase())
            {            
                var assetName = db.Assets.Where(x => x.Name == name).FirstOrDefault();
                if (assetName != null)
                {
                    msm sms = new msm();
                    string[] mensaje = { assetName.telefono, "Tu tiempo en línea en la aplicación SensRoom se ha agotado, si deseas estar en línea nuevamente es necesario que lo hagas desde tu perfil. " +" sens-room.com/Account/login"};
                    sms.enviamsm(mensaje);
                }
            }
        }

        public void MensajeOfflineView(string name)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var assetName = db.Assets.Where(x => x.Name == name).FirstOrDefault();
                if (assetName != null)
                {
                    msm sms = new msm();
                    string[] mensaje = { assetName.telefono, "Tu tiempo para recibir invitaciones en modo offline de SensRoom se ha agotado, si deseas seguir visible es necesario que lo configures desde tu perfil" + " sens-room.com/Account/login" };
                    sms.enviamsm(mensaje);
                }
            }
        }
    }
}