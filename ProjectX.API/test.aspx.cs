﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using ProjectX.ViewModels;
using ProjectX.API.Business;
using ProjectX.API.Models;
using ProjectX.API;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Net;

namespace ProjectX.API
{
    public partial class test : System.Web.UI.Page
    {
        public static string[] RandomNames { get; set; }
        public static string[] RandomWords { get; set; }
        protected async void Page_Load(object sender, EventArgs e)
        {
            #region Names
            RandomNames = new string[] {"Ariana",
                                        "Pa",
                                        "Beverley",
                                        "Chelsey",
                                        "Dona",
                                        "Dorris",
                                        "Mendy",
                                        "Fransisca",
                                        "Chanda",
                                        "Ethel",
                                        "Josefa",
                                        "Tessa",
                                        "Kristine",
                                        "Tangela",
                                        "Sha",
                                        "Collette",
                                        "Ngoc",
                                        "Criselda",
                                        "Georgine",
                                        "Larae",
                                        "Genoveva",
                                        "Demetrice",
                                        "Shanita",
                                        "Zoila",
                                        "Melba",
                                        "Belen",
                                        "Hester",
                                        "Laila",
                                        "Marylou",
                                        "Shela",
                                        "Marisa",
                                        "Leesa",
                                        "Sharmaine",
                                        "Sybil",
                                        "Cindy",
                                        "Nakia",
                                        "Jeanmarie",
                                        "Vennie",
                                        "Maxine",
                                        "Jenice",
                                        "Jaunita",
                                        "Evelynn",
                                        "Trang",
                                        "Lynne",
                                        "Raquel",
                                        "Delma",
                                        "Lynnette",
                                        "Claudine",
                                        "Celinda",
                                        "Mozelle","Crystal","Nancy","Samantha" };
            #endregion

            RandomWords = new string[] { "Sexy", "Hot", "Cachonda", "Chica", "Chida", "Petite", "Diosa", "Ardiente", "Deseosa", "Voluptuosa", "Hermosa" }; 
        }
        protected void OxxoPay_Click(object sender, EventArgs e)
        {
            //var udt = ViewModels.Util.GetStartDayFromDate(DateTime.Now, DayOfWeek.Sunday);

            //var cdt = new DateTime(udt.Year, udt.Month, udt.Day, 0, 0, 0);
            //var test = Business.OrderBC.GetSchedule(new NotificationRequestModel()
            //{
            //    SType = MessageSender.Asset,
            //    AssetId = 2,
            //    MType = MessageType.Invitation,
            //    StartDate = cdt
            //});
            //string vics = ViewModels.Util.GetCustomerName("Vics1V2");
            //CustomerPaymentViewModel vicss = new CustomerPaymentViewModel();
            //vicss.NameOnCard = vics;
            //var conekta = new ConektaBC(vicss);
            //var res = conekta.AddCustomer(new CustomerPaymentViewModel()
            //{
            //    ProviderId = "tok_test_visa_4242",
            //    CreditCardNumber = "4242"
            //});

            
           // System.Diagnostics.Debug.WriteLine(res.ProviderId + "   " + res.CreditCardNumber);
            //conekta.CreateOxxoPayCharge(new OrderViewModel() { TotalCost = 500 }, 
            //    DateTime.UtcNow.AddHours(1));
        }
        protected void Generate_Click(object sender, EventArgs e)
        {
            #region Input
            double Latitude = Double.Parse(txtLat.Text);
            double Longitude = Double.Parse(txtLong.Text);
            int assetQty = Int32.Parse(txtQty.Text);
            int photoQty = Int32.Parse(txtImageCount.Text);
            int radius = Int32.Parse(txtRadius.Text);
            int Height = Int32.Parse(txtHeight.Text);
            int Width = Int32.Parse(txtWidth.Text);
            #endregion

            //Catalogs
            var XSettings = Business.CustomerBC.GetXSettings();

            for (int i = 0; i < assetQty; i++)
            {
                int z =DateTime.UtcNow.Millisecond;
                //Random name
                Random random = new Random(z);
                var name = RandomNames[random.Next(0,RandomNames.Length-1)];
                //Random num
                random = new Random(z*2);
                string suffix = random.Next(1, 2017).ToString();
                //Append
                name += suffix;

                var result = CreateAsset(new RegisterViewModel()
                {
                    UserName = name,
                    Password = "123456",
                });

                var model = result.AssetVM;

                //Random Title
                string title = "";
                for (int w = 0; w <= 2; w++)
                {
                    random = new Random((z * 3) + w);
                    string word = RandomWords[random.Next(0, RandomWords.Length - 1)];
                    title += title=="" ? word :" " + word;
                }

                //Random DOB
                random = new Random(z * 4);
                int years = random.Next((1999 - 35), 1999);

                //Random category
                random = new Random(z * 5);
                int c = random.Next(0, XSettings.Categories.Count - 1);
                model.CategoryId = XSettings.Categories[c].Id;
                
                //Random location
                setLocation(z * 6, Latitude, Longitude, radius, ref model);
                
                using (var db = Helpers.DBHelper.GetDatabase())
                {

                    #region AssetInfo
                    var asset = db.Assets.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (asset != null)
                    {
                        //Profile
                        //asset.Age = model.Age;
                        asset.DOB = new DateTime(years, 1, 1);
                        asset.ASex = false;

                        asset.Title = title;
                        asset.Description = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem.";
                        asset.IsOnline = true;
                        asset.OnlineExpires = DateTime.UtcNow.AddMonths(6);
                        asset.CalendarActive = false;
                        asset.IsActive = true;

                        asset.CategoryId = model.CategoryId;
                        asset.APrefId = 2;
                        asset.MaxDistance = 10;

                        asset.Latitude = model.Latitude;
                        asset.Longitude = model.Longitude;
                        asset.LocationUpdate = DateTime.UtcNow;

                        db.SaveChanges();

                        #endregion


                        //Images
                        for (int j = 0; j < photoQty; j++)
                        {
                            //Random image
                            var webClient = new WebClient();
                            string url = "https://unsplash.it/" + Width + "/" + Height + "/?random";
                            byte[] imageBytes = webClient.DownloadData(url);

                            var newImage = db.AssetImages.Add(new AssetImage
                            {
                                Id = System.Guid.NewGuid().ToString(),
                                UserId = model.Id,
                                ImageStream = imageBytes,
                                IndexNum = 0,
                                CreatedOn = DateTime.UtcNow,
                                IsProfile = j == 0
                            });

                            db.SaveChanges();
                        }
                    }
                }


                Response.Write("<br/>"+ name);
            }
        }
        private RegisterViewModel CreateAsset(RegisterViewModel Model)
        {
            UserModel model = new UserModel()
            {
                UserName = Model.UserName,
                Password = Model.Password,
                ConfirmPassword = Model.Password
            };

            AuthRepository _repo = new AuthRepository();
            IdentityResult result = _repo.RegisterUser2(model);

            //bool errrs = false;
            //foreach(var vv in result.Errors)
            //    errrs = true;

            if (result.Succeeded)
            {
                Model.IsActivated = true;
                Model.UserId = Business.UserBC.UserExists(Model.UserName).Id;
                Business.UserBC.UpdateUser(Model.UserName, 2);
                Business.AssetBC.CreateAsset(Model.UserId, Model.UserName);
                Model.AssetVM = Business.AssetBC.GetAsset(Model.UserId);
                return Model;
            }

            return null;

        }
        public void setLocation(int seed, double x0, double y0, int radius, ref AssetViewModel model)
        {
            Random random = new Random(seed);

            // Convert radius from meters to degrees
            double radiusInDegrees = radius / 111000f;

            double u = random.NextDouble();
            double v = random.NextDouble();
            double w = radiusInDegrees * Math.Sqrt(u);
            double t = 2 * Math.PI * v;
            double x = w * Math.Cos(t);
            double y = w * Math.Sin(t);

            // Adjust the x-coordinate for the shrinking of the east-west distances
            double new_x = x / Math.Cos(DegreesToRadians(y0));
            double foundLongitude = new_x + x0;
            double foundLatitude = y + y0;

            Console.WriteLine(foundLongitude + "," + foundLatitude);
            model.Latitude = foundLongitude;
            model.Longitude = foundLatitude;
        }

        #region DegreesToRadians

        /// <summary>
        /// Convert degrees to radians
        /// </summary>
        /// <param name="degrees">An angle in degrees</param>
        /// <returns>The angle expressed in radians</returns>
        public float DegreesToRadians(float degrees)
        {
            const float degToRad = (float)System.Math.PI / 180.0f;
            return degrees * degToRad;
        }

        /// <summary>
        /// Convert radians to degrees
        /// </summary>
        /// <param name="radians">An angle in radians</param>
        /// <returns>The angle expressed in degrees</returns>
        public float RadiansToDegrees(float radians)
        {
            const float radToDeg = 180.0f / (float)System.Math.PI;
            return radians * radToDeg;
        }

        /// <summary>
        /// Convert degrees to radians
        /// </summary>
        /// <param name="degrees">An angle in degrees</param>
        /// <returns>The angle expressed in radians</returns>
        public double DegreesToRadians(double degrees)
        {
            const double degToRad = System.Math.PI / 180.0;
            return degrees * degToRad;
        }

        /// <summary>
        /// Convert radians to degrees
        /// </summary>
        /// <param name="radians">An angle in radians</param>
        /// <returns>The angle expressed in degrees</returns>
        public double RadiansToDegrees(double radians)
        {
            const double radToDeg = 180.0 / System.Math.PI;
            return radians * radToDeg;
        }

        #endregion

    }
}