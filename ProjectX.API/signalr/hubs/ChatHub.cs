﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using ProjectX.API.Business;
using ProjectX.ViewModels;

namespace ProjectX.API.signalr.hubs
{
    public class ChatHub : Hub
    {
      
        public void Send(string name, string message, string grupo)
        {
            try
            {
               string id = grupo.Split(' ')[1];
               int orderid = int.Parse(id);
               string grup = grupo.Replace(" ", "");
               Clients.Group(grup).sendChat(name, message);
              //  Clients.All.sendChat(name, message);

                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    int typeid;

                    if (name == "cliente")
                        typeid = 1;
                    else
                        typeid = 2;


                    var nots = db.Notifications.Where(x => x.OrderId == orderid).FirstOrDefault();
                    var model = new NotificationViewModel
                    {
                        MType = typeid == 2 ? MessageType.AssetMessage : MessageType.CustomerMessage,
                        MReceiver = typeid == 1 ? MessageSender.Customer : MessageSender.Asset,
                        AssetId = nots.AssetId,
                        CustomerId = nots.CustomerId,
                        MessageText = message,
                        OrderId = orderid
                    };
                    MessagingBC.AddNotificationViewModel(model);
                    //db.conektaLogs.Add(new Models.conektaLog { error = name + " /"+ message+" /" + grup, fecha = DateTime.Now });
                    //db.SaveChanges();
                }


            }
            catch (Exception ex)
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var error = db.conektaLogs.Add(new Models.conektaLog { error = ex.Message, fecha = DateTime.Now });
                    db.SaveChanges();
                }
            }
        }

        public void AddGrupo(string grupo)
        {
            string grup = grupo.Replace(" ", "");
            try {
                Groups.Add(Context.ConnectionId, grup);



            }
            catch (Exception ex)
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                     db.conektaLogs.Add(new Models.conektaLog { error = ex.Message, fecha = DateTime.Now });
                    db.SaveChanges();
                }
            }
        }

        public void OnDisconnected(string grupo)
        {
            Groups.Remove(Context.ConnectionId, grupo);
        }
    }
}