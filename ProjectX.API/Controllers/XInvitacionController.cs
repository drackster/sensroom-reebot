﻿using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using RestSharp;
using RestSharp.Authenticators;

namespace ProjectX.API.Controllers
{
    public class XInvitacionController : Controller
    {
        public string ConektaPrivateKey = ConfigurationManager.AppSettings["ConektaKeyPrivate"];
        public string ConektaPublicKey = ConfigurationManager.AppSettings["ConektaKeyPublic"];
        DateTime dt = ViewModels.Util.StripMinSec(DateTime.Now);
        string uId = Helpers.Helper.GetId();


        // GET: XInvitacion
        [HttpGet]
        [Authorize]
        public ActionResult Invitar(int id)
        {        
            InvitacionAsset model = new InvitacionAsset();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                //var MP = db.tMediosDePagoes.ToList();
                var cus = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                var credito = db.tCreditos.Where(x => x.UserId == cus.UserName && x.Credito == true).FirstOrDefault();
                var asste = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                decimal? precio = asste.precio == null ? 0 : asste.precio;
                decimal? anticipo = precio * 0.25m;
                decimal? pendiente = precio * 0.75m;
                if (credito != null) model.Credito = bool.Parse(credito.Credito.ToString());
                else model.Credito = false;
                model.Nombre = asste.Name;
                model.Precio = decimal.Parse(precio.ToString());
                model.Anticipo = decimal.Parse(anticipo.ToString());
                model.Pendiente = decimal.Parse(pendiente.ToString());
                model.Foto = asste.AssetImages.FirstOrDefault();
                model.Ubicacion = "pendiente de ubicacion";
                model.offlineView = (bool)asste.offlineView;
                Session["InvitacionAsset"] = model;
                Session["Asset"] = asste;              
                return View(model);
            }
        }     

       

        [Authorize]
        private void CreaOrden(OrderViewModel model)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var newOrder = db.Orders.Add(new Models.Order
                {
                    CustomerId = model.CustomerId,
                    AssetId = model.AssetId,
                    CategoryTierId = 1,//model.CategoryTier.Id,
                    AdressId = model.AddressId,

                    CLatitude = model.CLatitude,
                    CLongitude = model.CLongitude,
                    ALatitude = model.ALatitude,
                    ALongitude = model.ALongitude,

                    DownPayment = model.DownPayment,
                    TotalCost = model.TotalCost,

                    CreatedOn = DateTime.UtcNow,
                    ExpiresOn = DateTime.UtcNow.AddMinutes(40),
                    ServiceEnd = DateTime.UtcNow.AddDays(1),
                    CashRecollected = 0,

                    OrderStatus = (int)OrderStatus.Pending,
                    RequestType = (int)model.RequestType,
                    RequestedDateTime = null,
                    StartingDistance = 10,
                    CustomerPaymentId = 2,// model.PaymentMethodId,
                    ChargeId = "",
                    PaymentMethod = (int)model.paymentMethod,
                    PaypalTransId = "",
                    OxxoRef = 0000,                  
                    ModeType = model.ModeType
                });
                db.SaveChanges();

                var newNotification = db.Notifications.Add(new Notification
                {
                    //TODO
                    MType = (int)MessageType.Invitation,
                    AssetId = model.AssetId,
                    CustomerId = model.CustomerId,
                    OrderId = newOrder.Id,
                    MessageText = null, //Las invitaciones no llevan, solo los chats
                    MessageStatus = (int)MessageStatus.Created,
                    EventDateTime = model.EventDateTime,
                    CreatedOn = DateTime.UtcNow,
                    MReceiver = (int)MessageSender.None //both
                });
                db.SaveChanges();
            }
        }

        [Authorize]
        private void EnviarMensaje()
        {
            var assetDatos = Session["Asset"] as Asset;
            string telefono = assetDatos.telefono;
            string email = assetDatos.Email;
            string assetName = assetDatos.Name;
            string cuerpo = "¡Felicidades! Tienes una INVITACIÓN en tu portal https://sens-room.com , responde antes de 30 minutos o ésta será cancelada.";
            string[] mensaje = { telefono, cuerpo };
            var sms = new msm();
            //sms.enviamsm(mensaje);
            //var ws = new WhtasApp();
            //ws.Main();
            var fromAddress = new MailAddress("atencion@sens-room.com", "SensRoom");
            var toAddress = new MailAddress(email, assetName);
            const string fromPassword = "Sensroom1";
            const string subject = "SENS-ROOM Invitación";
            const string body = "¡Felicidades! Tienes una INVITACIÓN en tu portal https://sens-room.com , responde antes de 30 minutos o ésta será cancelada.";

            var smtp = new SmtpClient
            {
                Host = "smtpout.secureserver.net",
                Port = 80,
                EnableSsl = false,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }

        public  IRestResponse SendMessageMsm()
        {
            try
            {

                var assetDatos = Session["Asset"] as Asset;
                string telefono = "52"+assetDatos.telefono;
                string nombre = assetDatos.Name;

                var client = new RestClient("https://api.labsmobile.com/json/send");
                client.Authenticator = new HttpBasicAuthenticator("atencion@sens-room.com", ConfigurationManager.AppSettings["tokenSms"].ToString());
                var request = new RestRequest(Method.POST);
                request.AddHeader("Cache-Control", "no-cache");
                request.AddHeader("Content-Type", "application/json");
                request.AddParameter("undefined", "{\"message\":\"SENSROOM. Hola "+ assetDatos.Name +" tienes una invitacion en https://sens-room.com \", \"tpoa\":\"Sender\",\"recipient\":[{\"msisdn\":\""+ telefono + "\"}]}", ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);
                return response;
            }
            catch (Exception)
            {

                throw;
            }

        }

        [HttpPost]
        [Authorize]
        public ActionResult EnviarInvitacionCredito()
        {
            var asset = Session["Asset"] as Asset;
            try
            {
                string Lat1;
                string long1;
                string ciudad;
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                    Lat1 = cus.latitud.ToString();
                    long1 = cus.longitud.ToString();
                    ciudad = cus.Ciudad;
                }
                InvitacionAsset model = Session["InvitacionAsset"] as InvitacionAsset;
                DateTime dt = ViewModels.Util.StripMinSec(DateTime.Now);
                var Mcustomer = Session["Customer"] as Models.Customer;              
                OrderViewModel orden = new OrderViewModel();
                orden.CustomerId = Mcustomer.Id;
                orden.AssetId = asset.Id;
                orden.AddressId = 1;
                orden.Address1 = "";
                orden.CLatitude = double.Parse(Lat1.ToString());
                orden.CLongitude = double.Parse(long1.ToString());
                orden.ciudad = ciudad;
                orden.ALatitude = double.Parse(asset.Latitude.ToString());
                orden.ALongitude = double.Parse(asset.Longitude.ToString());
                orden.DownPayment = 0.0m;// pago.amount;
                orden.paymentMethod = ViewModels.PaymentMethod.Credito;
                orden.TotalCost = (decimal)asset.precio;
                orden.Status = OrderStatus.Pending;
                orden.CreatedOn = DateTime.UtcNow;               
                orden.ModeType = 2; // tipo dos para que la invitacion dure 12 horas   

                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var assetOff = db.Assets.Where(x => x.Id == asset.Id).FirstOrDefault();
                    assetOff.IsOnline = false;
                    db.SaveChanges();
                    var cus = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                    var cred = db.tCreditos.Where(x => x.UserId == cus.UserName && x.Credito == true).FirstOrDefault();
                    cred.Credito = false;
                    db.SaveChanges();
                }
                CreaOrden(orden);
                SendMessageMsm();
                EnviarMensaje();
                return RedirectToAction("Enviada", "Enviada");
            }

             catch (Exception ex)
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var cus = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                    var paypallog = db.conektaLogs.Add(new conektaLog { error = "Credito -- "+ ex.Message.ToString(), fecha = DateTime.Now });
                    var credito = db.tCreditos.Add(new tCredito { UserId = cus.UserName, CreatedOn = DateTime.Now, Credito = true, Monto = 0, NoOrden = "0" });
                    var assetOff = db.Assets.Where(x => x.Id == asset.Id).FirstOrDefault();
                    assetOff.IsOnline = true;
                    ErrorLog e = new ErrorLog();
                    e.Error(ex.Message, "XInvitacion", "EnviarInvitacionCredito");
                    db.SaveChanges();
                    return RedirectToAction("NoEnviada", "Enviada");
                }
            }
        }

        [HttpPost]
        [Authorize]
        public ActionResult EnviarInvitacionPaypal()
        {
            var asset = Session["Asset"] as Asset;
            try
            {
                string Lat1;
                string long1;
                if (Session["lat"].ToString() != "" && Session["lon"].ToString() != "")
                {
                    Lat1 = Session["lat"].ToString();
                    long1 = Session["lon"].ToString();
                }
                else
                {
                    Lat1 = "0";
                    long1 = "0";
                }

                InvitacionAsset model = Session["InvitacionAsset"] as InvitacionAsset;
                string uId = Helpers.Helper.GetId();
                DateTime dt = ViewModels.Util.StripMinSec(DateTime.Now);


                var Mcustomer = Session["Customer"] as Models.Customer;
              

                OrderViewModel orden = new OrderViewModel();
                orden.CustomerId = Mcustomer.Id;
                orden.AssetId = asset.Id;
                orden.AddressId = 1;
                orden.Address1 = "";
                orden.CLatitude = double.Parse(Lat1.ToString());
                orden.CLongitude = double.Parse(long1.ToString());
                orden.ALatitude = double.Parse(asset.Latitude.ToString());
                orden.ALongitude = double.Parse(asset.Longitude.ToString());
                orden.DownPayment = 0.0m;// pago.amount;
                orden.paymentMethod = ViewModels.PaymentMethod.Paypal;
                orden.TotalCost = (decimal)asset.precio;
                orden.Status = OrderStatus.Pending;
                orden.CreatedOn = DateTime.UtcNow;
               
                    orden.ModeType = 2; // tipo dos para que la invitacion dure 12 h is online o offlineview


                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var assetOff = db.Assets.Where(x => x.Id == asset.Id).FirstOrDefault();
                    assetOff.IsOnline = false;
                    var cus = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                    var cred = db.tCreditos.Where(x => x.UserId == cus.UserName).FirstOrDefault();
                    cred.Credito = false;
                    db.SaveChanges();
                }
                CreaOrden(orden);
                EnviarMensaje();
                SendMessageMsm();
                return RedirectToAction("Enviada", "Enviada");
            }

            catch (Exception ex)
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var cus = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                    var paypallog = db.conektaLogs.Add(new conektaLog { error = "PAYPAL -- " + ex.Message.ToString(), fecha = DateTime.Now });
                    var credito = db.tCreditos.Add(new tCredito { UserId = cus.UserName, CreatedOn = DateTime.Now, Credito = true, Monto = 0, NoOrden = "0" });
                    var assetOff = db.Assets.Where(x => x.Id == asset.Id).FirstOrDefault();
                    assetOff.IsOnline = true;
                    ErrorLog e = new ErrorLog();
                    e.Error(ex.Message, "XInvitacion", "EnviarInvitacionPaypaly");
                    db.SaveChanges();
                    return RedirectToAction("NoEnviada", "Enviada");
                }
            }



        }

        [Authorize]
        public ActionResult InvitacionEnviadal()
        {
           
            return View("InvEnviada");
        }

        private bool disponibilidad()
        {
            var asset = Session["Asset"] as Asset;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var assetd = db.Assets.Where(x => x.Id == asset.Id && x.IsOnline == true).FirstOrDefault();

                if (assetd == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }
    }
}