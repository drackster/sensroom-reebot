﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class EnviadaController : Controller
    {
        string uId = Helpers.Helper.GetId();
        // GET: Enviada
        public ActionResult Enviada()
        {
            notifications();
            return View();
        }


        public ActionResult NoDisponible()
        {
            notifications();
            return View();
        }

        public void notifications()
        {
            var today = DateTime.UtcNow.AddHours(-12);
            int[] type = { 1, 7, 8, 9 };

            Session["type"] = 1;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var idM = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                var mensajes = db.Notifications.Where(x => x.CustomerId == idM.Id && x.CreatedOn > today && type.Contains(x.MType)).ToList();

                if (mensajes != null)
                {
                    Session["nots"] = mensajes.Count.ToString();
                }

                var order = db.Orders.Where(x => x.CustomerId == idM.Id && x.CreatedOn >= today && x.OrderStatus == 4).FirstOrDefault();
                if (order != null)
                {
                    Session["orderid"] = order.Id;
                    Session["chatSN"] = true;
                }
            }
            
        }

        public ActionResult NoEnviada()
        {            
            return View("NoEnviada");
        }
    }
}