﻿
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using ProjectX.API.Helpers;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectX.API.Controllers
{
    public class CRegistroController : Controller
    {
        public UserManager<ApplicationUser> UserManager { get; private set; }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        bool sendNot = true;
        public string ConektaPrivateKey = ConfigurationManager.AppSettings["ConektaKeyPrivate"];
        public string ConektaPublicKey = ConfigurationManager.AppSettings["ConektaKeyPublic"];


        RegisterViewModel model;

        public string username;
        // GET: CRegistro
        public ActionResult CRegistro()
        {
            if (Session["CodInit"] != null)
            {
                ViewBag.codigo = Session["CodInit"].ToString();
            }                
            return View();
        }

        public ActionResult TRegistro()
        {

            return View();
        }

        public ActionResult TMembresia()
        {

            return View();
        }

        public int ValidaCodigo(string codigo)
        {
            if (codigo.Trim().ToLower() == "sensroom$$$" || codigo.Trim().ToLower() == "sensroom")
            {
                return 0;
            }

            AspNetUser aspnetUser = null;
            Asset asset = null;
            string cod = codigo.Trim().Substring(codigo.Length - 3, 3);
            string codori = codigo.Replace(cod, "");
            using (var db = DBHelper.GetDatabase())
            {
                aspnetUser = db.AspNetUsers.Where(u => u.UserName == codigo.Trim()).FirstOrDefault();
                if (aspnetUser != null)
                {
                    Session["Codigo"] = codigo;
                    if (aspnetUser.AdminType == 1)
                    {
                        return 0;
                    }
                    return 1;// aspnetUser.AdminType;
                }

                asset = db.Assets.Where(x => x.CusCode == cod && x.Name == codori).FirstOrDefault();

                if (asset != null)
                {
                    Session["Codigo"] = asset.Name;
                    return 2;
                }

                else
                {
                    return 0;
                }
            }
        }

        public RegisterViewModel ValidaAlias(string UserName)
        {
            RegisterViewModel model = new RegisterViewModel();
            UserName.Trim();
            if (string.IsNullOrEmpty(UserName))
            {
                return null;
            }

            else if (UserName.Length > 12)
            {
                return null;
            }


            if (username != UserName)
            {
                username = UserName;
                model.UserName = username;
                var u = Business.UserBC.UserExists(model.UserName);
                if (u == null)
                {
                    model.UserNameValid = true;
                }
                else
                {
                    model.UserNameValid = false;
                    return null;
                }

                if (model.UserNameValid)
                {
                    model.ActivationStatus = 4;
                }
                model.UserName = UserName;
              
                return model;
            }
            else
            { return null; }
        }

        public bool ValidaAlisB(string alias)
        {
            var u = Business.UserBC.UserExists(alias);
            if (u == null)
            {
                //model.UserNameValid = true;
                return true;
            }
            else
            {
                //model.UserNameValid = false;
                return false;                
            }
        }

        public RegisterViewModel Password(string pass)
        {
            if (string.IsNullOrEmpty(pass))
            {
                return null;
            }
            else
            {
               
              
                return model;
            }            
        }

        public ActionResult RegistroT(Registro reg)
        {
            Session["Reg"] = reg;

            if (reg.Codigo != null)
            {
                reg.Type = ValidaCodigo(reg.Codigo);
            }

            if (reg.Type == 0)
            {
                reg.Error = "Código inválido";
                return View("TRegistro", reg);
            }

            if (reg.Alias == null || reg.Alias.ToString().Length < 6)
            {
                reg.Error = "Tu nombre de usuario debe tener mínimo 6 caracteres";
                return View("TRegistro", reg);
            }

            if (reg.Pass1 == null || reg.Pass1.ToString().Length < 6)
            {
                reg.Error = "La contraseña debe tener mínimo 6 caracteres";
                return View("TRegistro", reg);
            }

            if (reg.Pass1 != reg.Pass2)
            {
                reg.Error = "Verifica contraseña";
                return View("TRegistro", reg);
            }

            if (!reg.Tyc)
            {
                reg.Error = "Es necesario aceptar los terminos y condiciones para continuar con el registro";
                return View("TRegistro", reg);
            }

            if (reg.Type == null || reg.Type == 1)
            {
                return View("TMembresia", reg);
            }

            if (reg.Type == 0)
            {
                reg.Error = "Código inválido";
                return View("TRegistro", reg);
            }



            RegisterViewModel model = new RegisterViewModel();
            reg.Codigo = Session["Codigo"].ToString();
            bool whatever;
            string hostcode;

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                whatever = db.AspNetUsers.Any(u => u.UserName == reg.Codigo);
                hostcode = db.AspNetUsers.Where(x => x.UserName == reg.Codigo).Select(x => x.Id).FirstOrDefault();
            }

            if (!whatever)
            {
                model.HostIdValid = false;
                model.Receiver = MessageSender.None;
                model.HostCodeError = "El anfitrión " + model.HostCode + " es inválido.";
            }

            else
            {
                var receiver = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
                //un cliente no puede invitar a una chica
                if (receiver == MessageSender.Customer && model.Sender == MessageSender.Asset)
                {
                    model.HostIdValid = false;
                    model.Receiver = MessageSender.None;
                    model.HostCodeError = "Un cliente no puede invitar a un proveedor.";
                }
                else
                {
                    model.HostIdValid = true;
                    model.Receiver = receiver;
                }
            }

            model.ActivationStatus = 4;
            model.Id = Guid.NewGuid().ToString();
            model.HostId = hostcode;
            model.UserName = reg.Alias;
            model.UserNameValid = true;
            model.Sender = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
            model.Password = reg.Pass1;

            var regi = new UserController();
            var registro = regi.Register(model);
            var contentResult = registro as OkNegotiatedContentResult<UserController>;

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var userMessage = db.AspNetUsers.Where(x => x.Id == hostcode).FirstOrDefault();
                var assetName = db.Assets.Where(x => x.Name == userMessage.UserName).FirstOrDefault();

                var newNotification = db.Notifications.Add(new Notification
                {
                    //TODO
                    MType = 10,
                    AssetId = assetName.Id,// model.AssetId,
                    CustomerId = null,// model.CustomerId,
                    OrderId = null,//newOrder.Id,
                    MessageText = null, //Las invitaciones no llevan, solo los chats
                    MessageStatus = (int)MessageStatus.Created,

                    CreatedOn = DateTime.UtcNow,
                    MReceiver = (int)MessageSender.None //both
                });
                db.SaveChanges();
            }

            CreaCodigoInv(reg.Alias);

            return RedirectToAction("Login", "Account");
        }

        public ActionResult Registro(Registro reg)
        {
            Session["Reg"] = reg;

            if (reg.Codigo != null)
            {
                reg.Type = ValidaCodigo(reg.Codigo);
            }
            else
            {
                Session["Codigo"] = "sensroom";
            }

          

            if (reg.Type == 0)
            {
                reg.Error = "Código inválido";
                return View("CRegistro", reg);
            }

            if (reg.Alias == null || reg.Alias.ToString().Length < 6)
            {
                reg.Error = "Tu nombre de usuario debe tener mínimo 6 caracteres";
                return View("CRegistro", reg);
            }

            if (reg.Pass1 == null || reg.Pass1.ToString().Length < 6)
            {
                reg.Error = "La contraseña debe tener mínimo 6 caracteres";
                return View("CRegistro", reg);
            }           

            if (reg.Pass1 != reg.Pass2)
            {
                reg.Error = "Verifica contraseña";
                return View("CRegistro", reg);
            }

            if (!reg.Tyc)
            {
                reg.Error = "Es necesario aceptar los terminos y condiciones para continuar con el registro";
                return View("CRegistro", reg);
            }

            if (!reg.Ps)
            {
                return View("Membresia", reg);
            }
            
            if (reg.Type == 0)
            {
                reg.Error = "Código inválido";
                return View("CRegistro", reg);
            }

           

            RegisterViewModel model = new RegisterViewModel();
            reg.Codigo = Session["Codigo"].ToString();
            bool whatever;
            string hostcode;
       
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                whatever = db.AspNetUsers.Any(u => u.UserName == reg.Codigo);
                hostcode = db.AspNetUsers.Where(x => x.UserName == reg.Codigo).Select(x => x.Id).FirstOrDefault();
            }

            if (!whatever)
            {
                model.HostIdValid = false;
                model.Receiver = MessageSender.None;
                model.HostCodeError = "El anfitrión " + model.HostCode + " es inválido.";
            }

            else
            {
                var receiver = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
                //un cliente no puede invitar a una chica
                if (receiver == MessageSender.Customer && model.Sender == MessageSender.Asset)
                {
                    model.HostIdValid = false;
                    model.Receiver = MessageSender.None;
                    model.HostCodeError = "Un cliente no puede invitar a un proveedor.";
                }
                else
                {
                    model.HostIdValid = true;
                    model.Receiver = receiver;
                }              
            }

            model.ActivationStatus = 4;           
            model.Id = Guid.NewGuid().ToString();
            model.HostId = hostcode;
            model.UserName = reg.Alias;
            model.UserNameValid = true;
            model.Sender = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
            model.Password = reg.Pass1;

           var regi = new UserController();
           var registro =  regi.Register(model);
           var contentResult = registro as OkNegotiatedContentResult<UserController>;
            FormsAuthentication.SetAuthCookie(model.UserName, true);
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var userMessage = db.AspNetUsers.Where(x => x.Id == hostcode).FirstOrDefault();
                var assetName = db.Assets.Where(x => x.Name == userMessage.UserName).FirstOrDefault();

                var newNotification = db.Notifications.Add(new  Notification
                {
                    //TODO
                    MType = 10,
                    AssetId = assetName.Id,// model.AssetId,
                    CustomerId = null,// model.CustomerId,
                    OrderId = null,//newOrder.Id,
                    MessageText = null, //Las invitaciones no llevan, solo los chats
                    MessageStatus = (int)MessageStatus.Created,
                 
                    CreatedOn = DateTime.UtcNow,
                    MReceiver = (int)MessageSender.None //both
                });
                db.SaveChanges();
            }

            CreaCodigoInv(reg.Alias);

           return RedirectToAction("Index", "AssetW");
        }

        public ActionResult RegistroC(Registro reg)
        {
            if (reg.Type == 0)
            {
                reg.Error = "Código inválido";
                return View("Index", reg);
            }

            if (reg.Pass1.ToString().Length < 6)
            {
                reg.Error = "La contraseña debe tener mínimo 6 caracteres";
                return View("Index", reg);
            }

            if (reg.Alias.ToString().Length < 6)
            {
                reg.Error = "Tu alis debe tener mínimo 6 caracteres";
                return View("Index", reg);
            }

            if (reg.Pass1 != reg.Pass2)
            {
                reg.Error = "Verifica contraseña";
                return View("Index", reg);
            }

            if (!reg.Tyc)
            {
                reg.Error = "Es necesario aceptar los terminos y condiciones para continuer con el registro";
                return View("Index", reg);
            }

            RegisterViewModel model = new RegisterViewModel();
            reg.Codigo = Session["Codigo"].ToString();
            bool whatever;
            string hostcode;
            // long id;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                whatever = db.AspNetUsers.Any(u => u.UserName == reg.Codigo);
                hostcode = db.AspNetUsers.Where(x => x.UserName == reg.Codigo).Select(x => x.Id).FirstOrDefault();
                //id = db.Assets.Where(x => x.UserId == hostcode).Select(x => x.Id).FirstOrDefault();
            }

            if (!whatever)
            {
                model.HostIdValid = false;
                model.Receiver = MessageSender.None;
                model.HostCodeError = "El anfitrión " + model.HostCode + " es inválido.";
            }

            else
            {
                var receiver = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
                //un cliente no puede invitar a una chica
                if (receiver == MessageSender.Customer && model.Sender == MessageSender.Asset)
                {
                    model.HostIdValid = false;
                    model.Receiver = MessageSender.None;
                    model.HostCodeError = "Un cliente no puede invitar a un proveedor.";
                }
                else
                {
                    model.HostIdValid = true;
                    model.Receiver = receiver;
                }
            }

            model.ActivationStatus = 4;
            model.Id = Guid.NewGuid().ToString();
            model.HostId = hostcode;
            model.UserName = reg.Alias;
            model.UserNameValid = true;
            model.Sender = reg.Type == 1 ? MessageSender.Customer : MessageSender.Asset;
            model.Password = reg.Pass1;

            try
            {
                var regi = new UserController();
                var registro = regi.Register(model);
                var contentResult = registro as OkNegotiatedContentResult<UserController>;
            }
            catch (Exception ex)
            {
                reg.Error = ex.Message;
                return View("Index", reg);
            }    
            return RedirectToAction("Login", "Account");     
        }

        [HttpPost]       
        public void CreaCodigoInv(string alias)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var userMessage = db.AspNetUsers.Where(x => x.UserName == alias).FirstOrDefault();
                var assetName = db.Assets.Where(x => x.Name == userMessage.UserName).FirstOrDefault();
                if (assetName != null)
                {
                    string[] array = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "@", "#", "$", "%", "&" };
                    Random rand = new Random();
                    int cod1 = rand.Next(1, array.Length);
                    int cod2 = rand.Next(1, array.Length);
                    int cod3 = rand.Next(1, array.Length);
                    assetName.CusCode = array[cod1]+ array[cod2]+ array[cod3];
                    db.SaveChanges();
                }            
            }
        }

        public void pagopaypal(string lat, string lon)
        {
            Session["lat"] = lat;
            Session["lon"] = lon;
            Registro reg = Session["Reg"] as Registro;
            RegisterViewModel model = new RegisterViewModel();
            string codigo;
            if (reg.Codigo == null)
            {
                codigo = "sensroom";
            }
            else
            {
                codigo = reg.Codigo.Trim();
            }

            bool whatever;
            string hostcode;          
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                whatever = db.AspNetUsers.Any(u => u.UserName == codigo);
                hostcode = db.AspNetUsers.Where(x => x.UserName == codigo).Select(x => x.Id).FirstOrDefault();
            }

            if (!whatever)
            {
                model.HostIdValid = false;
                model.Receiver = MessageSender.None;
                model.HostCodeError = "El anfitrión " + model.HostCode + " es inválido.";
            }
            else
            {
                var receiver = MessageSender.Customer;// en el metodo de membresia solo se puden dar de alta clientes
                                                      //un cliente no puede invitar a una chica
                if (receiver == MessageSender.Customer && model.Sender == MessageSender.Asset)
                {
                    model.HostIdValid = false;
                    model.Receiver = MessageSender.None;
                    model.HostCodeError = "Un cliente no puede invitar a un proveedor.";
                }
                else
                {
                    model.HostIdValid = true;
                    model.Receiver = receiver;
                }
            }

            model.ActivationStatus = 4;
            model.Id = Guid.NewGuid().ToString();
            model.HostId = hostcode;
            model.UserName = reg.Alias;
            model.UserNameValid = true;
            model.Sender = MessageSender.Customer;// en el metodo de membresia solo se puden dar de alta clientes
            model.Password = reg.Pass1;

            // guardar en aspusernet
            var regi = new UserController();
            var registro = regi.Register(model);
            var contentResult = registro as OkNegotiatedContentResult<UserController>;
            FormsAuthentication.SetAuthCookie(model.UserName, true);
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var userMessage = db.AspNetUsers.Where(x => x.Id == hostcode).FirstOrDefault();
                var assetName = db.Assets.Where(x => x.Name == userMessage.UserName).FirstOrDefault();
                if (assetName != null && sendNot)
                {
                    sendNot = false;
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        //TODO
                        MType = 11,// cliente nunevo
                        AssetId = assetName.Id,// model.AssetId,
                        CustomerId = null,// model.CustomerId,
                        OrderId = null,//newOrder.Id,
                        MessageText = null, //Las invitaciones no llevan, solo los chats
                        MessageStatus = (int)MessageStatus.Created,
                        CreatedOn = DateTime.UtcNow,
                        MReceiver = (int)MessageSender.None //both
                    });
                    db.SaveChanges();
                }
            }
        }
        public ActionResult TyC()
        {
            return View("TyC");
        }      
    }
}
   