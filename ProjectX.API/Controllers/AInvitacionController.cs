﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class AInvitacionController : Controller
    {
        // GET: AInvitacion
        public ActionResult Index(int id)
        {

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var invitacion = db.Orders.Where(x => x.Id == id).FirstOrDefault();
                return View(invitacion);
            }                
        }
    }
}