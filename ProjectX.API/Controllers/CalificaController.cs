﻿using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class CalificaController : Controller
    {
        string uId = Helpers.Helper.GetId();
        int admintype = Helpers.Helper.GetAspnetUser().AdminType;
        // GET: Califica
        public ActionResult Index()
        {       
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                if (admintype == 1)
                {
                    var cus = db.Customers.Where(x => x.UserId == uId).Select(x => x.Id).FirstOrDefault();
                    var order = db.Orders.Include("Asset").Where(x => x.CustomerId == cus && x.OrderStatus == 4 && x.Calificacion == null).ToList();
                    List<CalifEvent> eventos = new List<CalifEvent>();
                    foreach (var item in order)
                    {
                        CalifEvent evento = new CalifEvent();
                        evento.AssetId = item.AssetId;
                        evento.AssetName = item.Asset.Name;
                        evento.Date = item.CreatedOn;
                        evento.OrderId = item.Id;
                        eventos.Add(evento);
                    }
                    return View(eventos);
                }
                else
                {                  
                    var asste = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                    var order = db.Orders.Where(x => x.AssetId == asste.Id && x.OrderStatus == 4 && x.CalificacionC == null).ToList();                   
                    List <CalifEvent> eventos = new List<CalifEvent>();
                    foreach (var item in order)
                    {
                        CalifEvent evento = new CalifEvent();
                        evento.CustomerId = item.CustomerId;
                        evento.CustomerName = item.Customer.AspNetUser.UserName;
                        evento.Date = item.CreatedOn;
                        evento.OrderId = item.Id;
                        eventos.Add(evento);
                    }
                    return View(eventos);
                }
            }              
        }

        public ActionResult califica(int val, int orderId)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var order = db.Orders.Where(x => x.Id == orderId).FirstOrDefault();
                if (admintype == 1)
                {                   
                    var asset = db.Assets.Where(x => x.Id == order.AssetId).FirstOrDefault();
                    order.Calificacion = val;
                    if (asset.Calificacion != null)
                    {
                        float Ci = (float)asset.Calificacion;
                        float Cf = val;
                        float res = (Ci + Cf) / 2;
                        asset.Calificacion = res;
                    }
                    else {
                        asset.Calificacion = val;
                    }                   
                }
                else
                {
                    var customer = db.Customers.Where(x => x.Id == order.CustomerId).FirstOrDefault();
                    order.CalificacionC = val;
                    if (customer.Calificacion != null)
                    {
                        float Ci = (float)customer.Calificacion;
                        float Cf = val;
                        float res = (Ci + Cf) / 2;
                        customer.Calificacion = res;
                    }
                    else {
                        customer.Calificacion = val;
                    }
                }
                db.SaveChanges();
            }
                return RedirectToAction("Index", "Califica");
        }       
    }
}