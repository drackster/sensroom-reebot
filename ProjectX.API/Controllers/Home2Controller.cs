﻿using ProjectX.API.Business;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProjectX.API.signalr.hubs;

namespace ProjectX.API.Controllers
{
    public class Home2Controller : Controller
    {
        // GET: Home2

       
        int[] types = new[] { 3, 6 };


        public ActionResult Default()
        {
            var x = new AccountController();
            x.LogOff();
            return null;
        }


        // GET: Chat
        [Authorize]
        public ActionResult Index(string idO)
        {


          //  var dsd = new ChatHub();

           

            Session["chatid"] = idO;
          
           
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                long id = Int32.Parse(idO.ToString());
                var mensajes = db.Notifications.Where(x => x.OrderId == id && types.Contains(x.MType)).ToList();

                /*
                       var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Costo);
                    return View("Index", itemList.ToList());
                 */



                if (mensajes.Count > 0)
                {
                    for (int i = 0; i < mensajes.Count; i++)
                    {
                        mensajes[i].MessageStatus = 3;
                        db.SaveChanges();
                    }
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots = from n in notlist
                               where types.Contains(n.MType)
                               select n;


                    //ViewBag.Chat = nots.ToList();

                    return View("Index", nots.ToList());


                }
                else
                {


                    return RedirectToAction("SalaChat", "SalaChat");
                }

            }
        }



        public ActionResult InsertMessage(int orderid, int typeid, string message)
        {
            if (message != null && message != string.Empty && message != "")
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var nots = db.Notifications.Where(x => x.OrderId == orderid).FirstOrDefault();
                    var model = new NotificationViewModel
                    {
                        MType = typeid == 2 ? MessageType.AssetMessage : MessageType.CustomerMessage,
                        MReceiver = typeid == 1 ? MessageSender.Customer : MessageSender.Asset,
                        AssetId = nots.AssetId,
                        CustomerId = nots.CustomerId,
                        MessageText = message,
                        OrderId = orderid
                    };
                    var n = MessagingBC.AddNotificationViewModel(model);
                    long id = Int32.Parse(orderid.ToString());
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots1 = from x in notlist
                                where types.Contains(x.MType)
                                select x;
                    return View("Chat", nots1.ToList());
                }
            }
            else
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    long id = Int32.Parse(orderid.ToString());
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots1 = from x in notlist
                                where types.Contains(x.MType)
                                select x;
                    return View("Chat", nots1.ToList());
                }
            }
        }
    }
}