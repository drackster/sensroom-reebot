﻿//using System;
//using System.Collections.Generic;
//using System.Configuration;
//using System.IO;
//using System.Linq;
//using System.Web;
//using System.Web.Mvc;
//using Kendo.Mvc.UI;
//using Kendo.Mvc.Extensions;
//using System.Text;
//using Alcione.ViewModels;
//using ProjectX.API.Models;
//using System.Data.Entity.Migrations;
//using System.Web.Configuration;
//using ProjectX.API.Business;

//namespace ProjectX.API.Controllers
//{
//    [Authorize]
//    public class ReportsController : Controller
//    {
//        //1: Porcentaje de Conversión
//        public ActionResult GetActiveConnections([DataSourceRequest] DataSourceRequest request)
//        {
//            var list = Business.ReportBC.GetActiveConnections();
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }
//        //1.5: Porcentaje de Conversión
//        public ActionResult GetActiveUsers(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetActiveUsers(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }
//        //2 Ranking de Categorias
//        public ActionResult GetSessionRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetSessionRank(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        //2 Ranking de Categorias
//        public ActionResult GetCategoryRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetCategoryRank(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        //3 Ranking de Busquedas
//        public ActionResult GetSearchRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetSearchRank(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        //4 Ranking de Productos
//        public ActionResult GetProductRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetProductRank(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        //5: Ranking de acciones
//        public ActionResult GetActionRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetActionRank(StartDate,EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//        //6: Ranking de conversiones
//        public ActionResult GetConversionRank(DateTime StartDate, DateTime EndDate, [DataSourceRequest] DataSourceRequest request)
//        {
//            EndDate = Helpers.Helper.AddHours(EndDate);
//            var list = Business.ReportBC.GetConversionRank(StartDate, EndDate);
//            DataSourceResult result = list.ToDataSourceResult(request);
//            return Json(result, JsonRequestBehavior.AllowGet);
//        }

//    }
//}