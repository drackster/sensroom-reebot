﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProjectX.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ProjectX.API.Models;
using System.Threading.Tasks;

namespace ProjectX.API.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        public UserManager<ApplicationUser> UserManager { get; private set; }
        public UserController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        { }
        public UserController(UserManager<ApplicationUser> userManager)//, RoleManager<ApplicationUser> roleManager)
        {
            UserManager = userManager;
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };
        }

        #region Auth/Activation

        [AllowAnonymous]
        [Route("validatecode")]
        public IHttpActionResult ValidateCode(RegisterViewModel model)
        {
            var u = Business.UserBC.UserExists(model.HostCode);
            if(u == null)
            {
                model.HostIdValid = false;
                model.Receiver = MessageSender.None;
                model.HostCodeError = "El anfitrión " + model.HostCode + " es inválido.";
            }
            else
            {
                var receiver = u.AdminType == 1 ? MessageSender.Customer : MessageSender.Asset;
                //un cliente no puede invitar a una chica
                if(receiver == MessageSender.Customer && model.Sender == MessageSender.Asset)
                {
                    model.HostIdValid = false;
                    model.Receiver = MessageSender.None;
                    model.HostCodeError = "Un cliente no puede invitar a un proveedor.";
                }
                else
                {
                    model.HostIdValid = true;
                    model.Receiver = receiver;
                }
                model.HostId = u.Id;
            }

            return Ok(model);
        }

        [AllowAnonymous]
        [Route("validateusername")]
        public IHttpActionResult ValidateUserName(RegisterViewModel model)
        {
            var u = Business.UserBC.UserExists(model.UserName);
            if(u == null)
            {
                model.UserNameValid = true;
            }
            return Ok(model);
        }
        
        //[AllowAnonymous]
        //[Route("getrequeststatus")]
        //[HttpPost]
        //public IHttpActionResult GetRequestStatus(RegisterViewModel model)
        //{
        //    return Ok(Business.UserBC.GetOrSaveRegisterModel(model));
        //}

        [AllowAnonymous]
        [Route("updaterequeststatus")]
        [HttpPost]
        public IHttpActionResult UpdateRequestStatus(RegisterViewModel model)
        {
            return Ok(Business.UserBC.UpdateRegisterModelStatus(model));
        }

        [AllowAnonymous]
        [Route("getactivationrequest")]
        [HttpGet]
        public IHttpActionResult GetActivationRequest()
        {
            return Ok(Business.UserBC.GetActivationRequest());
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("register")]
        [HttpPost]
        public IHttpActionResult Register(RegisterViewModel Model)
        {

            //Primero generar registro de quien es y qué usuario lo referenció.
            //Con los ultimos cambios, solo validdamos anfitrion y que el usuario no exista
            if (string.IsNullOrEmpty(Model.Id) || string.IsNullOrEmpty(Model.HostId))
            {
                Model.HostCodeError = "Registro Inválido";
                return Ok(Model); //Inválido
            }
            Model = Business.UserBC.SaveRegisterModel(Model);

            UserModel model = new UserModel()
            {
                UserName = Model.UserName,
                Password = Model.Password,
                ConfirmPassword = Model.Password
            };

            AuthRepository _repo = new AuthRepository();
            IdentityResult result = _repo.RegisterUser2(model);

            //bool errrs = false;
            //foreach(var vv in result.Errors)
            //    errrs = true;
            
            if(result.Succeeded)
            {
                Model.IsActivated = true;
                Model.UserId = Business.UserBC.UserExists(Model.UserName).Id;
                int utype = Model.Sender == MessageSender.Customer ? 1 : 2;
                Business.UserBC.UpdateUser(Model.UserName, utype);
                if(utype==2)
                {
                    Business.AssetBC.CreateAsset(Model.UserId, Model.UserName);
                    Model.AssetVM = Business.AssetBC.GetAsset(Model.UserId);
                    //Incluir calendarios
                    Model.AssetVM.Calendars = Business.AssetBC.GetCalendar(-1, false, Model.AssetVM.Id);
                }
                else if(utype==1)
                {
                    Model.UserVM = Business.CustomerBC.CreateCustomer(Model.UserId, Model.UserName);
                    Model.XSettingsVM = Business.CustomerBC.GetXSettings(); //Global Defaults
                    //App.SettingsVM = await App.apiService.GetXSettings();

                }
            }
            //TODO NOTIFICATION

            //IHttpActionResult errorResult = GetErrorResult(result);

            //if (errorResult = null)
            //{
            //    return errorResult;
            //}
            return Ok(Model);
        }

        [AllowAnonymous]
        [Route("login")]
        public IHttpActionResult Login(RegisterViewModel model)
        {
            var userLogin = UserManager.Find(model.UserName, model.Password);
            if(userLogin!=null)
            {
                model.Id = userLogin.Id;
                model.IsActivated = true;
                model.UserId = userLogin.UserName;
                var u = Business.UserBC.UserExists(model.UserName);

                //Customer = 1,
                //Asset = 2,

                if (u.AdminType == 1)
                {
                    model.Sender = MessageSender.Customer;
                    model.UserVM = Business.CustomerBC.GetCustomerViewModel(model.Id);
                }
                else
                {
                    model.Sender = MessageSender.Asset;
                    model.AssetVM = Business.AssetBC.GetAsset(model.Id);
                    //ahora tmb incluimos la lista dde calendarios, desde el login
                    model.AssetVM.Calendars = Business.AssetBC.GetCalendar(-1,false,model.AssetVM.Id);
                    //Ahora, manejar el estado de los devicetokens
                    if(model.DeviceToken!=null)
                    {
                        Business.AssetBC.UpdateDeviceToken(model.DeviceToken,
                            model.AssetVM.Id);
                    }


                }
            }

            return Ok(model);// userInfo);
        }

        [AllowAnonymous]
        [Route("getconfig")]
        [HttpPost]
        public IHttpActionResult GetConfig(ConfigViewModel userConfig)
        {
            //Business.UserBC.logUserAction();
            return Ok(1);// Business.UserBC.GetUserConfig(userConfig));
        }

        #endregion
        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

    }
}
