﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Text;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using ProjectX.API.Business;

namespace ProjectX.API.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "HomePage");
            }
           
            return RedirectToAction("Index", "HomePage");    
        }
        [HttpPost]
        public ActionResult Index(ConfigModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "HomePage");
            }

            if (model.Id > 0)
            {

                TempData["ExecErrors"] = Jobs.JobScheduler.ManualExec();
                //Esperar a que inicie el job
                System.Threading.Thread.Sleep(8000);
            }

            return View();
        }

        }
    }
