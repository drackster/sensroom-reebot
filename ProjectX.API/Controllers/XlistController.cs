﻿using iTextSharp.xmp.impl;
using ProjectX.API.Helpers;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectX.API.Controllers
{

    public class XlistController : Controller
    {
        double latC;
        double lonC;
        double radio = 6378.137f;
        double rad = Math.PI / 180;
        DateTime tiempoActual = DateTime.UtcNow;
        DateTime today = DateTime.UtcNow.AddHours(-24);
        int[] type = { 1, 7, 8, 9 };
        string uId = Helpers.Helper.GetId();
        int edad;
        int pref;
        bool ciudad;
        int distancia;
        Customer prefCus;
        int pag;
        private readonly int _RegistrosPorPagina = 10;
        /// <summary>
        /// index
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int page = 1)
        {

            try
            {

                latC = Session["lat"] == null || Session["lat"] == string.Empty ? 0 : double.Parse(Session["lat"].ToString());
                lonC = Session["lon"] == null || Session["lon"] == string.Empty ? 0 : double.Parse(Session["lon"].ToString());
                nots();

                List<Asset> asset = AssetList(0);
                pag = page;

                if (Session["perfiles"] != null)
                {
                    var cont = Session["perfiles"] as List<PerfilAsset>;
                    if (cont.Count != asset.Count)
                    {
                        Session["perfiles"] = null;
                    }
                }

                if (Session["perfiles"] == null)
                {
                    List<Asset> assetC = CaliculaDistancia(asset);

                    List<PerfilAsset> perfiles = addPerfil(assetC.Skip((pag - 1) * _RegistrosPorPagina)
                                                 .Take(_RegistrosPorPagina).ToList());


                    Session["deatil"] = null;
                    Session["perfiles"] = perfiles;
                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);
                    itemList.ToList()[0].totalAsset = asset.Count;
                    return View("Index", itemList.ToList());
                }
                else
                {
                    List<Asset> assetC = CaliculaDistancia(asset);

                    List<PerfilAsset> perfiles = addPerfil(assetC.Skip((pag - 1) * _RegistrosPorPagina)
                                                 .Take(_RegistrosPorPagina).ToList());

                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);

                    itemList.ToList()[0].totalAsset = asset.Count;
                    return View("Index", itemList.ToList());
                }
            }
            catch (Exception ex)
            {
                ErrorLog e = new ErrorLog();
                e.Error(ex.Message, "Index", "Xlist");
                LogOff();
                return RedirectToAction("Login", "Account");
            }
        }

        private List<Asset> AssetList(int opcion, int pagina = 1)
        {
            List<Asset> asset = new List<Asset>();
            List<Asset> asseton;

            using (var db = DBHelper.GetDatabase())
            {
                prefCus = db.Customers.Where(c => c.UserId == uId).FirstOrDefault();

                edad = prefCus.MaxAge;
                pref = prefCus.CPrefId == null ? 3 : int.Parse(prefCus.CPrefId.ToString());
                decimal costo = prefCus.Balance <= 0 ? 100000 : prefCus.Balance;

                if (opcion == 0)
                {
                    if (pref == 3)
                    {
                        asseton = db.Assets.Where(
                         x =>
                         x.Longitude != null &&
                         x.Latitude != null &&
                         x.precio <= costo &&
                         x.statusProfile == 2 &&
                         x.FBaja == null &&
                         x.AssetImages.Count > 0 && (
                         x.IsOnline == true || x.offlineView == true)).ToList();
                        asset = asseton.ToList();
                    }
                    else
                    {
                        bool sex = pref == 1 ? false : true;
                        asseton = db.Assets.Where(
                        x =>
                        x.ASex == sex &&
                        x.Longitude != null &&
                        x.Latitude != null &&
                        x.precio <= costo &&
                        x.statusProfile == 2 &&
                        x.FBaja == null &&
                        x.AssetImages.Count > 0 && (
                       x.IsOnline == true || x.offlineView == true)).ToList();
                        asset = asseton.ToList();
                    }
                }
                return asset;
            }
        }


        private List<Asset> CaliculaDistancia(List<Asset> asset)
        {
            var resp = new List<Asset>();  
            foreach (var item in asset)
            {             
                var dLat = (rad) * (latC - item.Latitude);
                var dLon = (rad) * (lonC - item.Longitude);
                var a = Math.Sin(double.Parse(dLat.ToString()) / 2) * Math.Sin(double.Parse(dLat.ToString()) / 2) + Math.Cos((rad) * (latC)) * Math.Cos((rad) * (double.Parse(item.Latitude.ToString()))) * Math.Sin(double.Parse(dLon.ToString()) / 2) * Math.Sin(double.Parse(dLon.ToString()) / 2);
                var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
                var d = radio * c;    
                if (latC == 0 || lonC == 0)
                {
                    resp.Add(item);                
                }
                else {
                    if (d < 40)
                    {
                        resp.Add(item);                      
                    }                
                }
            }
            return resp;          
        }

        private List<PerfilAsset> addPerfil(List<Asset> asset)
        {
            List<PerfilAsset> perfiles = new List<PerfilAsset>();            

            foreach (var item in asset)
            {
                var dif = tiempoActual.Subtract(DateTime.Parse(item.DOB.ToString())).TotalDays;
                double años = dif / 365;

                if (años <= edad)
                {
                    PerfilAsset perfil = new PerfilAsset();
                    perfil.ID = Int32.Parse(item.Id.ToString());
                    perfil.Costo = decimal.Parse(item.precio.ToString());
                    perfil.Descripcion = item.Description;
                    perfil.Saludo = item.Title;
                    perfil.Name = item.Name;
                    perfil.Edad = años.ToString("0");
                    //perfil.Distancia = d;//.ToString("0.0") + " Km";
                    perfil.Pprecio = prefCus.Balance < 0 ? 100000 : prefCus.Balance;
                    perfil.Pedad = prefCus.MaxAge;
                    perfil.Psexo = prefCus.CPrefId == null ? 3 : int.Parse(prefCus.CPrefId.ToString());
                    perfil.ciudad = item.ciudad;
                    perfil.estado = item.estado;
                    perfil.calificacion = item.Calificacion == null ? 1 : (float)item.Calificacion;
                    perfil.HoM = item.ASex == false ? "Mujer" : "Hombre";
                    perfil.Offlineview = (bool)item.offlineView;
                    perfil.Foto = addImgae(perfil.ID);
                    perfiles.Add(perfil);

                }
            }
            if (perfiles.Count > 0)
            {
                return perfiles;
            }
            else
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    //  var assetDf = db.Assets.Where(x => x.precio < 2000 && x.statusProfile == 2).Take(10).ToList();
                    var assetDf = db.Assets.ToList();
                    foreach (var item in assetDf)
                    {
                        PerfilAsset perfil = new PerfilAsset();
                        perfil.ID = Int32.Parse(item.Id.ToString());
                        perfil.Costo = decimal.Parse(item.precio.ToString());
                        perfil.Descripcion = item.Description;
                        perfil.Saludo = item.Title;
                        perfil.Name = item.Name;
                        perfil.Edad = "25";
                        perfil.Distancia = 40;//.ToString("0.0") + " Km";
                        perfil.Pprecio = prefCus.Balance < 0 ? 100000 : prefCus.Balance;
                        perfil.Pedad = prefCus.MaxAge;
                        perfil.Psexo = prefCus.CPrefId == null ? 3 : int.Parse(prefCus.CPrefId.ToString());
                        perfil.ciudad = item.ciudad;// "";// assetDf.ciudad;
                        perfil.estado = "";// assetDf.estado;
                        perfil.calificacion = item.Calificacion == null ? 1 : (float)item.Calificacion;
                        perfil.HoM = item.ASex == false ? "Mujer" : "Hombre";
                        perfil.Offlineview = true;
                        perfil.Foto = addImgae(perfil.ID);
                        perfiles.Add(perfil);
                    }

                }
                return perfiles;
            }

        }

        private string addImgae(int id)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var ids = db.AssetImages.Where(x => x.UserId == id).Select(x => x.Id).ToList();
                Random random = new Random();
                int randomNumber = random.Next(0, ids.Count);
                string imid = ids[randomNumber];
                string Foto = db.AssetImages.Where(x => x.Id == imid).FirstOrDefault().urlImage;
                return Foto;
            }
        }

        private void nots()
        {
            using (var db = DBHelper.GetDatabase())
            {
                var customer = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                Session["Customer"] = customer;
                int nots = db.Notifications.Where(n => n.CustomerId == customer.Id && n.MessageStatus == 0 && n.CreatedOn > today && type.Contains(n.MType)).Count();
                var order = db.Orders.Where(x => x.CustomerId == customer.Id && x.CreatedOn >= today && x.OrderStatus == 4).FirstOrDefault();
                if (order != null)
                {
                    Session["orderid"] = order.Id;
                    Session["chatSN"] = true;
                }
                else
                {
                    Session["chatSN"] = false;
                }
                Session["nots"] = nots.ToString();
                Session["chats"] = 0;// chats.ToString();
                Session["type"] = 1;
            }
        }

        /// <summary>
        /// click sobre item
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SelectItem(int id)
        {
            return JavaScript("window.location = 'http://" + Request.Url.Host + ":" + Request.Url.Port + "/ADetail/Index/" + id + "'");
        }

        public class persona
        {
            public string nombre { get; set; }
            public string edad { get; set; }
        }

        [Authorize]
        public ActionResult Filtros(int edad = 99, int pref = 3, decimal precio = 100000)
        {
            var uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                cus.MaxAge = edad;
                cus.CPrefId = pref;
                cus.Balance = decimal.Parse(precio.ToString());
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }

        [Authorize]
        public ActionResult Ordenar(int ordenar = 0)
        {
            List<PerfilAsset> perfiles = new List<PerfilAsset>();
            perfiles = Session["perfiles"] as List<PerfilAsset>;
            if (ordenar == 1)//edad
            {
                var itemList = (from t in perfiles
                                select t).OrderBy(t => t.Edad);
                return View("Index", itemList.ToList());
            }
            else if (ordenar == 2)// costo
            {
                var itemList = (from t in perfiles
                                select t).OrderBy(x => x.Costo);
                return View("Index", itemList.ToList());
            }
            else if (ordenar == 3)// distancia
            {
                var itemList = (from t in perfiles
                                select t).OrderBy(x => x.Distancia);
                return View("Index", itemList.ToList());
            }
            else
            {
                return View("Index", perfiles);
            }
        }


        [Authorize]
        public ActionResult Creditos()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                try
                {
                    var customer = db.CustomerNames.Where(x => x.UserId == uId).FirstOrDefault();
                    var cred = db.tCreditos.Where(x => x.UserId == customer.UserName && x.Credito == true).ToList();
                    if (cred != null)
                    {
                        return View("creditos", cred);
                    }
                    return View("creditos");
                }
                catch
                {
                    LogOff();
                    return RedirectToAction("Login", "Account");
                }

            }
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();

            Session.Abandon();

            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("Login", "Account");
        }
        [Authorize]
        public ActionResult Ayuda()
        {
            return View("Ayuda");
        }

        public JsonResult comentario(string comment, string contact)
        {
            double latC;
            double lonC;
            if (Session["lat"].ToString() != "" && Session["lon"].ToString() != "")
            {
                latC = double.Parse(Session["lat"].ToString());
                lonC = double.Parse(Session["lon"].ToString());
            }
            else
            {
                latC = 0;
                lonC = 0;
            }

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var name = db.AspNetUsers.Where(x => x.Id == uId).FirstOrDefault();

                try
                {
                    db.tComments.Add(new tComment { userName = name.UserName, Comment = comment, latitude = latC, longitude = lonC, CreatedOn = DateTime.Now, Contact = contact });
                    db.SaveChanges();
                    return Json("Mensaje enviado, te contactaremos lo mas pronto posible", JsonRequestBehavior.AllowGet);
                }
                catch
                {
                    return Json("No fue posible enviar el mensaje, por favor manda tu duda a atencion@sens-room.com", JsonRequestBehavior.AllowGet);
                }
            }
        }

        private bool SendMsm(string comentario)
        {
            var msm = new msm();

            string[] mensaje = { "8127518319", "Duda cliente: " + comentario };//[0] teléfono [1]mensaje

            try
            {
                msm.enviamsm(mensaje);
                return true;
            }
            catch
            {
                return false;
            }

        }

        public ActionResult ComoFunciona()
        {
            return View("ComoFunciona");
        }


        public JsonResult UpdateCustomer(string ciudad, string estado, string pais)
        {
            using (var db = DBHelper.GetDatabase())
            {

                Session["perfiles"] = null;
                var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                cus.Ciudad = ciudad;
                cus.Estado = estado;
                cus.Pais = pais;
                cus.IsActive = true;
                cus.ActiveUpdate = DateTime.Now;
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult UpdateLocation(float latC, float lonC)
        {
            Session["perfiles"] = null;
            Session["lat"] = latC;
            Session["lon"] = lonC;
            using (var db = Helpers.DBHelper.GetDatabase())
            { 
                var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                cus.latitud = latC;
                cus.longitud = lonC;
                db.SaveChanges();
            }

                return View("Index");
        }

        public void locationUpdate(float latitud = 0.0f, float longitud = 0.0f, string ciudad = "", string estado = "", string pais = "")
        {
            Session["lat"] = latitud;
            Session["lon"] = longitud;

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                cus.latitud = latitud;
                cus.longitud = longitud;
                cus.Ciudad = ciudad;
                cus.Estado = estado;
                cus.Pais = pais;
                db.SaveChanges();
            }
        }
    }




}