﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using System.Web.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using ProjectX.API.Models;
using ProjectX.API.Business;
using ProjectX.ViewModels;

namespace ProjectX.API.Controllers
{
    //[System.Web.Mvc.RequireHttps]
    //[Authorize]
    [RoutePrefix("api/payment")]
    public class PaymentsController : ApiController
    {
        [HttpGet]
        [Route("getpayments")]
        public IHttpActionResult GetPayments()
        {
            return Ok(Business.CustomerPaymentBC.GetCustomerPaymentList());
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getpaymentslist")]
        public IHttpActionResult GetPaymentsList()
        {
            return Ok(Business.CustomerPaymentBC.GetMediosDePago());
        }

        [HttpPost]
        [Route("updatepayment")]
        public IHttpActionResult UpdatePayment(CustomerPaymentViewModel vm)
        {
            Business.CustomerPaymentBC.UpdateCustomerPayment(vm);
            return Ok();
        }

        [HttpGet]
        [Route("deletepayment/{Id}")]
        public IHttpActionResult DeletePayment(long Id)
        {
            return Ok(Business.CustomerPaymentBC.DeleteCustomerPayment(Id));
        }



        //[Route("deletecards")]
        //[HttpGet]
        //public IHttpActionResult DeleteCards()
        //{
        //    var cardstoDelete = DBHelper.GetDatabase().CustomerPaymentMethods.Where(c => !c.IsDeleted).ToList();
        //    foreach (CustomerPaymentMethod card in cardstoDelete)
        //    {
        //        bool result = Business.CustomerPaymentBC.DeleteCustomerPayment(card.Id);
        //        if (!result)
        //        {
        //            return Ok("Error al remover tarjeta: " + card.CreditCardNumber);
        //        }
        //    }

        //    return Ok(string.Empty);
        //}
    }
}
