﻿using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectX.API.Controllers
{
    public class InboxController : Controller
    {
        string uId = Helpers.Helper.GetId();
        // GET: Inbox
        public ActionResult Inbox()
        {          
            List<Mensajes> message = new List<Mensajes>();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var user = db.AspNetUsers.Where(x => x.Id == uId).FirstOrDefault();
                if (user != null)
                {
                    if (user.AdminType == 2)
                    {
                        int[] type = { 1, 10, 11, 12 };
                        Session["type"] = 2;
                        var today = DateTime.UtcNow.AddDays(-7);
                        var idM = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                        var mensajes = db.Notifications.Where(x => x.AssetId == idM.Id && x.CreatedOn > today && type.Contains(x.MType)).OrderByDescending(x => x.CreatedOn).ToList();
                        if (mensajes != null)
                        {                           
                            Session["nots"] = null;
                            mensajes.ForEach(a =>
                            {
                                a.MessageStatus = 3;
                            });
                            db.SaveChanges();

                            foreach (var item in mensajes)
                            {
                                Mensajes msg = new Mensajes();
                                msg.id = (int)item.Id;
                                msg.AssetId = (int)item.AssetId;
                                msg.CustomerId = item.CustomerId;
                                msg.OrderID = item.OrderId;
                                if (item.MType == 10 || item.MType == 11)
                                {
                                    msg.Mensaje = null;
                                    msg.CustomerName = null;
                                }
                                else if (item.MType == 12)
                                {
                                    msg.Mensaje = item.MessageText;
                                    msg.CustomerName = null;
                                }
                                else
                                {
                                    msg.Mensaje = item.MessageText;
                                    msg.CustomerName = item.Customer.AspNetUser.UserName;
                                }
                                msg.MReceiver = item.MReceiver;
                                msg.MType = item.MType;
                                msg.Fecha = item.CreatedOn;                               
                                msg.Tiempo = msg.Tiempo = calculatiempo(item.CreatedOn);
                                msg.MessageStatus = item.MessageStatus;
                                message.Add(msg);
                            }
                            return View(message);
                        }
                        else
                        {
                            return View(message);
                        }
                       
                    }
                    else
                    {
                        var today =DateTime.UtcNow.AddDays(-7);
                        int[] type = {1, 7, 8, 9 };
                        Session["type"] = 1;
                        var idM = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                        var order = db.Orders.Where(x => x.CustomerId == idM.Id && x.CreatedOn >= today && x.OrderStatus == 4).FirstOrDefault();
                        if (order != null)
                        {                           
                            Session["chatSN"] = true;
                        }
                        var mensajes = db.Notifications.Where(x => x.CustomerId == idM.Id && x.CreatedOn > today && type.Contains(x.MType)).OrderByDescending(x => x.CreatedOn).ToList();
                        if (mensajes != null)
                        {
                            mensajes.ForEach(a =>
                            {
                                a.MessageStatus = 3;
                            });
                            db.SaveChanges();

                            Session["nots"] = null;
                            foreach (var item in mensajes)
                            {      
                                if (item.MType == 7 || item.MType == 8 || item.MType == 1 || item.MType == 9)
                                {
                                    Mensajes msg = new Mensajes();

                                    if (item.MType == 7)
                                        msg.Mensaje = item.Asset.Name + "Aceptó la invitación";

                                    if (item.MType == 8)
                                        msg.Mensaje = item.Asset.Name + "Rechazó la invitación";

                                    if (item.MType == 1)
                                        msg.Mensaje = "Invitacion enviada a " + item.Asset.Name;

                                    if (item.MType == 9)
                                        msg.Mensaje = "La invitación que enviaste a " + item.Asset.Name + " fue cancelada por falta de respuesta. Descuida, ahora tienes un crédito para invitar a quien desees sin pagar ningún anticipo";

                                    msg.id = (int)item.Id;
                                    msg.AssetId = (int)item.AssetId;
                                    msg.CustomerId = (int)item.CustomerId;
                                    msg.MReceiver = item.MReceiver;
                                    msg.MType = item.MType;
                                    msg.Fecha = item.CreatedOn;
                                    msg.Tiempo = calculatiempo(item.CreatedOn);
                                    msg.AssetName = item.Asset.Name;
                                    msg.OrderID = item.OrderId;
                                    message.Add(msg);
                                }
                            }
                        }
                       return View(message);
                    }
                }
                else
                {
                    LogOff();
                    return RedirectToAction("Login", "Account");
                }
            }                
        }
        public string calculatiempo(DateTime cuando)
        {
            var diftiem = DateTime.UtcNow - cuando;
            string dias = diftiem.Days == 0 ? string.Empty : diftiem.Days.ToString();
            string horas = diftiem.Hours.ToString();
            string hace;
            if (dias == string.Empty)
            {
                hace = "Hace " + horas + " horas";
            }
            else
            {
                hace = "Hace " + dias + " dias";
            }
            return hace;
        }

        [HttpPost]
        public ActionResult GoOrderDetail(string id)
        {
            string[] idyTy = { id.Split(',')[0], id.Split(',')[1] };
            if (idyTy[1] == "1")
            {              
                return View("http://" + Request.Url.Host + ":" + Request.Url.Port + "/InvDetail/Index/" + idyTy[0] + "'");
            }
            if (idyTy[1] == "6")
            {
                return View("http://" + Request.Url.Host + ":" + Request.Url.Port + "/Chat/Chat/?idO=" + idyTy[0] + "'");
            }
            return null;
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            Session.Clear();
            return RedirectToAction("Login", "Account");
        }
    }
}