﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ProjectX.API.Helpers;
using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class PassUpdateController : Controller
    {
        // GET: PassUpdate

        private UserManager<IdentityUser> _userManager;

        public ActionResult Index()
        {
            return View();
        }


        public JsonResult update(string name, string tel, string pass, string passConf)
        {
            if (name == "" || tel == "" || pass == "" || passConf == "")
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }

            if (pass != passConf)
            {
                return Json(false, JsonRequestBehavior.AllowGet);
            }          

            using (var db = DBHelper.GetDatabase())
            {
                var ur = db.Assets.Where(x => x.Name == name && x.telefono == tel).FirstOrDefault();
                if (ur != null)
                {
                    db.sp_ActualizaContraseña(name);
                    Updatepass model = new Updatepass
                    {
                         ConfirmPassword = passConf,
                         Password = pass,
                         telefono = tel,
                         UserName = name,
                         userId = ur.UserId
                    };
                    AuthRepository _repo = new AuthRepository();
                    IdentityResult result = _repo.updatepass(model);

                    return Json(true, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(false, JsonRequestBehavior.AllowGet);
                }                
            }
        }
     

        private string encrypt(string value)
        {
            using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
            {
                UTF8Encoding utf = new UTF8Encoding();
                byte[] data = md5.ComputeHash(utf.GetBytes(value));
                return Convert.ToBase64String(data);
            }
        }
    }
}