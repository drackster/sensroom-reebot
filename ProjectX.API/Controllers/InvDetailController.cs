﻿using ProjectX.API.Helpers;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class InvDetailController : Controller
    {
        /// <summary>
        /// detalle de la orden
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        public ActionResult Index(int id)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var nots = db.Notifications.Where(n => n.OrderId == id && n.MessageStatus != 3).FirstOrDefault();
                if (nots != null)
                {
                    nots.MessageStatus = 3;
                    db.SaveChanges();
                }
                var detalle = db.Orders.Where(j => j.Id == id).FirstOrDefault();
                var cus = db.CustomerNames.Where(c => c.id == detalle.CustomerId).FirstOrDefault();              
                Session["idOrder"] = id;
                var detalles = new InvDetail();                
                detalles.CusName = cus.UserName;
                detalles.Anticipo = (float)detalle.TotalCost * 0.25f;
                detalles.Total = (float)detalle.TotalCost;
                detalles.Pendiente = (float)detalle.TotalCost * 0.75f;
                detalles.Calificación = 5;
                detalles.OrderId = detalle.Id;
                detalles.OrderStatus = detalle.OrderStatus;
                detalles.Calificación = (float)cus.Calificacion;
                string kms = CalcularDistancia(detalle.CLatitude, detalle.CLongitude, detalle.ALatitude, detalle.ALongitude);
                detalles.Distancia = "A "+ kms+" km de distancia";
                detalles.Ciudad = cus.Ciudad;
                return View("orderDetail", detalles);
            }
        }

        public string CalcularDistancia(double Lat1, double long1, double Lat2, double long2)
        {
            double latC = Lat1;
            double lonC = long1;
            double latA = Lat2;
            double lonA = long2;
            double radio = 6378.137f;
            double rad = Math.PI / 180;
            var dLat = (rad) * (latC - latA);
            var dLon = (rad) * (lonC - lonA);
            var a = Math.Sin(double.Parse(dLat.ToString()) / 2) * Math.Sin(double.Parse(dLat.ToString()) / 2) 
                + Math.Cos((rad) * (latC)) * Math.Cos((rad) * (latA)) 
                * Math.Sin(double.Parse(dLon.ToString()) / 2) 
                * Math.Sin(double.Parse(dLon.ToString()) / 2);
            var c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
            var d = radio * c;
            return d.ToString("0.0");
        }

        public ActionResult Aceptar()
        {
            int id = int.Parse(Session["idOrder"].ToString());
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var detalle = db.Orders.Where(j => j.Id == id).FirstOrDefault();
                detalle.OrderStatus = 4;
                detalle.UpdatedOn =DateTime.UtcNow;
                db.SaveChanges();               
                return RedirectToAction("Chat","Chat", new { idO = id });                
            }           
        }


        public ActionResult Rechazar(int idorder)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var detalle = db.Orders.Where(j => j.Id == idorder).FirstOrDefault();
                detalle.OrderStatus = 3;
                detalle.UpdatedOn =DateTime.UtcNow;
              
                var CusName = db.CustomerNames.Where(c => c.id == detalle.CustomerId).FirstOrDefault();
                var credito = db.tCreditos.Add(new tCredito
                {
                    CreatedOn =DateTime.UtcNow,
                    Monto = detalle.TotalCost * 0.2m,
                    NoOrden = detalle.Id.ToString(),
                    Credito = true,
                    UserId = CusName.UserName
                });    
                
                db.SaveChanges();
            }
            return null;
        }

        public ActionResult UpdateOrder(UpdateOrder model)
        {
            DateTime dt =DateTime.UtcNow;
            using (var db = DBHelper.GetDatabase())
            {
                var order = db.Orders
                      .Include("Asset")
                      .Include("Customer")
                      .Where(x => x.Id == model.orderid).FirstOrDefault();
                order.OrderStatus = (int)model.orderstatus;
                order.UpdatedOn = dt;
                if (model.orderstatus == 4)// 4 es invitación confirmada
                {
                    #region Notifications
                    //Avisarle al cliente que fue aceptada
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.AcceptedInvitation,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = null,
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    var Credito = db.tCreditos.Where(x => x.NoOrden == order.Id.ToString()).FirstOrDefault();
                    if (Credito != null)
                    {
                        Credito.Credito = false;
                    }

                    //El saludo
                    var greetNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.AssetMessage,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = GetGreetingMessage(),
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    #endregion


                    db.SaveChanges();
                    return RedirectToAction("Chat","Chat", new { idO = model.orderid });
                }
                if (model.orderstatus == 3)// 3 invitacion rechazada
                {
                    Rechazar(model.orderid);
                    #region Notifications
                    //Avisarle al cliente que fue Declinada
                    var newNotification = db.Notifications.Add(new Notification
                    {
                        MType = (int)MessageType.RejectedInvitation,
                        AssetId = order.AssetId,
                        CustomerId = order.CustomerId,
                        OrderId = order.Id,
                        MessageText = null,
                        MessageStatus = (int)MessageStatus.Created,
                        EventDateTime = null,
                        CreatedOn = dt,
                        MReceiver = (int)MessageSender.Customer
                    });

                    #endregion
                }
            }
            return RedirectToAction("Index", "InvDetail", new { id = model.orderid });
        }
        public static string GetGreetingMessage()
        {
            return "Gracias por invitarme, estamos en contacto por este chat mientras nos vemos.";
        }
    }
}