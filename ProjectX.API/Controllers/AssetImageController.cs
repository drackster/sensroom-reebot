﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class AssetImageController : ApiController
    {
        // GET: AssetImage

       
        
        public IHttpActionResult AssetImage(int id)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var detail = db.AssetImages.Where(x => x.UserId == id && !x.IsProfile).FirstOrDefault();
                return Ok(detail);
            }
        }
    }
}