﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Text;
using ProjectX.ViewModels;
using ProjectX.API.Models;

namespace ProjectX.API.Controllers
{
    public class ConektaWebhookController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            Stream req = Request.InputStream;
            req.Seek(0, System.IO.SeekOrigin.Begin);

            string json = new StreamReader(req).ReadToEnd();
            var obj = JObject.Parse(json);
            var data = obj.SelectToken("data");
          
            if (obj.SelectToken("type").ToString() == "charge.paid")
            {
                var resp = data.SelectToken("object");
                Business.OrderBC.UpdateStatusOxxo(obj.SelectToken("data").SelectToken("object").SelectToken("payment_method").SelectToken("reference"));
                //mail.Subject = "Pago comprobado";
                //mail.Body = "Tu pago ha sido confirmado.";
                //client.Send(mail);
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public string OxxoDevPaySignKey
        {
            get
            {
                return @"-----BEGIN RSA PRIVATE KEY-----
MIIEogIBAAKCAQEAvE1QKzmohwb5bIW6u9fzpvGbwNlnjsCbdzzH6hbRNs0RkOmF
bh9YoO0u5FmlFIG2X+JMmo+Apntoq97bOaoRVpFXxt8/cnbcfbdY8h/tmzOb/SbI
/wT/aMMH74hyaLpzKe4ibVd8dEbH9yCCKj3y9ReG2ius+MUujX5g8K+lI0kefU1a
vnw32idd3ZJxSqhTiYA2V+OMxqrlxMoXS7hFqKhAD4BlD53CqMY+UmDnEH5SbEUq
/jWE8Z9/VNF77DGyRWf3zPjkNR5xmGoistBEUP20u9wjDEn34FIuw2Q/gDEjg44U
bPGGLqgBHQrMWpS2q+K2IVsMNbDy0xMUIoJ4BQIDAQABAoIBAF+bNZgs10dvoBac
s5oKZHif9b/t4mmVm5nn720IlsMnTcLw/kFSgxJ6OrPQXEPUND/sy44yTFS7FgMl
zNMzaUg11YZw9WrttLfT7sJIucBybMIU/jqVVATh+VaCReystipU8X/oypTTF1Z/
V/eQqpoMjviY8mMA4skOoLOzyrnl2Ba4yYYiE/tZzHAnPlvcOzWukAzF/o2NMCAs
jouNwp3hmXntHA4YbhuNr1IAiiYkYtAUuF3+Vojoj2xB6PSAoLpJJSgkKlQUSlNm
HkOOzdTXFTGWxwF/SrwVDhglDOAS/bXTzErSycl/PGQ/YwTbNTXmUZX1qnC1A9hy
G7RCmgECgYEA4rGaE2NRdhkuFSpuyz2My9yza9sm/4/WYAl5S6o56fWLMOrfMtO+
w9gy81U25UH7AGxh8GWNU4AB6vLvbrnqFIm8YTCo5FDaiasB3ktj1N8hXKmpWdjl
Zolu5tIjA2agka39/FknRb0yta+JCVqCzKF0SreTNVfxox6cLo0kDoUCgYEA1KUk
QV8IPjsSGs8B5rLV6UpTTEAUt1jaR5LL7H0fzeGQoqNcDiJyuUQWLNNEPZhU3B3h
XRab5RmEV66tNhGQCmdnnPiMBTayKmLKbkJJZXQldmLHqk8z0fYsxnui04GavfkY
VjCGRnkBWuFVbBfRZXVdzEeK8z6YyTJSeYyMu4ECgYBXXi+NjVeV9npmOeZdeilL
YJHd3aaYFC2A8eXHnaGYY8PzZHIFOspo3Gg2k/8GOHmuXhcY+WqQZeNXU62ntQtc
QwFRS9mrtOWk7T1ZGSEup5hKCDF2qTswW4Ncflx0MRdtTet3WDzkidiDYVWlPzbt
+sPBECMyCeTdAy0aW9crqQKBgBrTqrtEjfmr4iDXtNmx/iKsac3Y18Q5xW/6KRfw
wS5sIp8iFN8DoY2V9WY6UQWkTjBm5KOl0o0YpwWY1+lWVeG6Iew/vwp5WAjY5cj7
O988w+0nkSD/VP5MHGH9/xDH7QnCfzuAa7BAW9dnTiuQ5a3zHJ5E+Favsv7PBnCn
NqoBAoGAeR9/Zv5DmGitBCkGZrFwjIBjuZ7h1GF+OO4EVqguxLTYGlfxriXqT/bl
j45UupfGn6u8TeqkVsjYGFX2up5fiiR9BOSt8gZgI6b6tKsd7W8wXSoCfL0t7YYu
qtarIl8gNWGRT9tf51uSYCJLM1WeWr1RW4IwzXLCpQ47iAxdERo=
-----END RSA PRIVATE KEY-----";
            }
        }

    }
}