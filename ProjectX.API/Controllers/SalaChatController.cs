﻿using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class SalaChatController : Controller
    {
        string uId = Helpers.Helper.GetId();
        // GET: SalaChat
        [Authorize]
        public ActionResult SalaChat()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                List<SalaChats> Chats = new List<SalaChats>();
                var aspnetuser = db.AspNetUsers.Where(x => x.Id == uId).FirstOrDefault();

                if (aspnetuser.AdminType == 1)
                {                   
                    DateTime tiempo = DateTime.UtcNow.AddDays(-7);
                    var cus = db.Customers.Where(x => x.UserId == aspnetuser.Id).FirstOrDefault();
                    var chats = db.Orders.Where(x => x.CustomerId == cus.Id && x.OrderStatus == 4 && x.CreatedOn > tiempo).ToList();
                    if (chats != null)
                    {
                        foreach (var item in chats)
                        {
                            Chats.Add(new SalaChats
                            {
                                id = item.Id,
                                nombre = item.Asset.Name,
                                tiempo = item.CreatedOn.Hour
                            });
                        }                        
                    }
                }
                else
                {
                    DateTime tiempo = DateTime.UtcNow.AddDays(-7);
                    var ase = db.Assets.Where(x => x.UserId == aspnetuser.Id).FirstOrDefault();
                    var chats = db.Orders.Where(x => x.AssetId == ase.Id && x.OrderStatus == 4 && x.CreatedOn > tiempo).ToList();
                    if (chats != null)
                    {
                        foreach (var item in chats)
                        {
                            Chats.Add(new SalaChats
                            {
                                id = item.Id,
                                nombre = item.Customer.AspNetUser.UserName,
                                tiempo = item.CreatedOn.Hour - DateTime.UtcNow.Hour
                            });
                        }
                    }
                }
                return View(Chats);
            }             
        }
    }
}