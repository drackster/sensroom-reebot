﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProjectX.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ProjectX.API.Models;
using System.Threading.Tasks;
using System.IO;
using System.Net.Http.Headers;

namespace ProjectX.API.Controllers
{
    [RoutePrefix("api/asset")]
    public class AssetController : ApiController
    {
        #region Asset
        [Authorize]
        [HttpGet]
        [Route("getasset")]
        public IHttpActionResult GetAsset()
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.GetAsset());
        }

        [Authorize]
        [HttpPost]
        [Route("saveasset")]
        public IHttpActionResult SaveAsset(AssetViewModel model)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.SaveAsset(model));
        }
        [Authorize]
        [HttpPost]
        [Route("updateassetlocation")]
        public IHttpActionResult UpdateAssetLocation(AssetLocationModel model)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.UpdateAssetLocation(model));
        }

        [Authorize]
        [HttpGet]
        [Route("assetoffline/{assetID}")]
        public IHttpActionResult assteOffline(long assetID)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.offlineAsset(assetID));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("assetonlineValidate/{assetID}")]
        public IHttpActionResult assetonlineValidate(long assetID)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.onlineAssetValidate(assetID));
        }
        #endregion


        #region Images
        [Authorize]
        [HttpGet]
        [Route("getimagelist")]
        public IHttpActionResult GetImageList()
        {
            return Ok(Business.AssetBC.GetImageList());
        }

        [Authorize]
        [HttpPost]
        [Route("saveimage")]
        public async Task<IHttpActionResult> SaveImage()
        {
            AssetImageViewModel result = null;

            //if (Request.Content.IsFormData())
            //{
            try
            {
                await Request.Content.LoadIntoBufferAsync();
                byte[] bs = await Request.Content.ReadAsByteArrayAsync();
                var img = new Business.AssetBC();
                result = await img.SaveImage(bs);

            }
            catch (Exception ex)
            {

            }
            //}

            return Ok(result);

        }

        //[Authorize]
        //[HttpGet]
        //[Route("getimage/{ImageId}")]
        //public IHttpActionResult GetImage(string ImageId)
        //{
        //    return Ok(Business.AssetBC.GetImage(ImageId));   
        //}
        //TODO
        [AllowAnonymous]
        [HttpGet]
        [Route("getimage/{ImageId}")]
        public HttpResponseMessage GetImage(string ImageId)
        {
            var outputStream = new MemoryStream();
            var img = Business.AssetBC.GetImage(ImageId);
            if (img != null)
            {
                outputStream.WriteAsync(img, 0, img.Length);
                outputStream.Position = 0;
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(outputStream.ToArray())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = ImageId.ToString() + ".png",
                Size = outputStream.Length
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("image/png");
            return result;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getassetcoverimage/{AssetId}")]
        public HttpResponseMessage GetAssetCoverImage(long AssetId)
        {
            var ImageId = Business.AssetBC.GetCoverImage(AssetId).Id;
            var outputStream = new MemoryStream();
            var img = Business.AssetBC.GetImage(ImageId);
            if (img != null)
            {
                outputStream.WriteAsync(img, 0, img.Length);
                outputStream.Position = 0;
            }
            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(outputStream.ToArray())
            };
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("inline")
            {
                FileName = ImageId.ToString() + ".png",
                Size = outputStream.Length
            };
            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("image/png");
            return result;
        }

        [Authorize]
        [HttpGet]
        [Route("deleteimage/{ImageId}")]
        public IHttpActionResult DeleteImage(string ImageId)
        {
            return Ok(Business.AssetBC.DeleteImage(ImageId));
        }



        [Authorize]
        [HttpGet]
        [Route("SetProfileImage/{ImageId}")]
        public IHttpActionResult SetProfileImage(string ImageId)
        {
            return Ok(Business.AssetBC.SetProfileImage(ImageId));
        }

        [Authorize]
        [HttpGet]
        [Route("getcoverimage/{AssetId}")]
        public IHttpActionResult GetCoverImage(long AssetId)
        {
            return Ok(Business.AssetBC.GetCoverImage(AssetId));
        }

        [Authorize]
        [HttpPost]
        [Route("rearangeimages")]
        public IHttpActionResult RearangeImages(List<AssetImageViewModel> model)
        {
            return Ok(Business.AssetBC.RearangeImages(model));
        }

        #endregion

        #region Categories and Services

        [AllowAnonymous]
        [HttpGet]
        [Route("getsettings")]
        public IHttpActionResult GetSettings()
        {
            return Ok(Business.AssetBC.GetSettings());
        }

        [Authorize]
        [HttpPost]
        [Route("savesettings")]
        public IHttpActionResult SaveSettings(ASettingsViewModel model)
        {
            return Ok(Business.AssetBC.SaveSettings(model));
        }

        [Authorize]
        [HttpGet]
        [Route("getcalendartimes")]
        public IHttpActionResult GetCalendarTimes()
        {
            return Ok(Business.AssetBC.GetCalendarTimes());
        }
        #endregion

        #region Calendar

        [Authorize]
        [HttpGet]
        [Route("getcalendar")]
        public IHttpActionResult GetCalendar()
        {
            //Business.UserBC.logUserAction("Favoritos");
            return Ok(Business.AssetBC.GetCalendar());
        }

        [Authorize]
        [HttpPost]
        [Route("savecalendar")]
        public IHttpActionResult SaveCalendar(CalendarViewModel model)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.SaveCalendar(model));
        }
        [Authorize]
        [HttpPost]
        [Route("updatecalendar")]
        public IHttpActionResult UpdateCalendar(CalendarViewModel model)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.UpdateCalendar(model));
        }
        [Authorize]
        [HttpPost]
        [Route("deletecalendar")]
        public IHttpActionResult DeleteCalendar(CalendarViewModel model)
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.DeleteCalendar(model));
        }
        #endregion

        #region Banking
        [Authorize]
        [HttpGet]
        [Route("getbanklist")]
        public IHttpActionResult GetBankList()
        {
            //Business.UserBC.logUserAction();
            return Ok(Business.AssetBC.GetBankList());
        }
        #endregion

        #region Notifications

        [Authorize]
        [HttpPost]
        [Route("getnotifications")]
        public IHttpActionResult GetNotifications(NotificationRequestModel model)
        {
            //Business.UserBC.logUserAction("Favoritos");
            return Ok(Business.MessagingBC.GetNotifications(model));
        }
        [Authorize]
        [HttpPost]
        [Route("getnotificationcount")]
        public IHttpActionResult GetNotificationCount(NotificationRequestModel model)
        {
            //Business.UserBC.logUserAction("Favoritos");
            return Ok(Business.MessagingBC.GetNotifications(model, false, false).Count);
        }
        [Authorize]
        [HttpPost]
        [Route("sendnotification")]
        public IHttpActionResult SendNotification(NotificationViewModel model)
        {
            //Business.UserBC.logUserAction("Favoritos");
            return Ok(Business.MessagingBC.AddNotificationViewModel(model));
        }
        //sendnotificationadmin
        [Authorize]
        [HttpPost]
        [Route("sendnotificationadmin")]
        public IHttpActionResult SendNotificationadmin(NotificationViewModel model)
        {
            //Business.UserBC.logUserAction("Favoritos");
            return Ok(Business.MessagingBC.AddNotificationViewModelAdmin(model));
        }


        [Authorize]
        [HttpPost]
        [Route("updatenotificationstatus")]
        public IHttpActionResult UpdateNotificationStatus(NotificationUpdateModel model)
        {
            return Ok(Business.MessagingBC.UpdateNotificationStatus(model));
        }
        

        #endregion

        #region Customer/Asset
        [AllowAnonymous]
        [HttpPost]
        [Route("getassetlist")]
        public IHttpActionResult GetAssetList(SearchModel model)
        {
             return Ok(Business.AssetBC.GetAssetList(model));
            //return RedirectToRoute("Account", "Login");
            //return Ok();
        }
        #endregion

        #region Reviews

        [Authorize]
        [HttpGet]
        [Route("getassetreviews/{AssetId}")]
        public IHttpActionResult GetAssetReviews(long AssetId)
        {
            return Ok(Business.AssetBC.GetAssetReviews(AssetId));
        }

        [Authorize]
        [HttpPost]
        [Route("addassetreview")]
        public IHttpActionResult AddAssetReview(ReviewViewModel model)
        {
            return Ok(Business.AssetBC.AddAssetReview(model));
        }

        [Authorize]
        [HttpGet]
        [Route("getcustomerreviews/{CustomerId}")]
        public IHttpActionResult GetCustomerReviews(long CustomerId)
        {
            return Ok(Business.AssetBC.GetCustomerReviews(CustomerId));
        }

        [Authorize]
        [HttpPost]
        [Route("addcustomerreview")]
        public IHttpActionResult AddCustomerReview(ReviewViewModel model)
        {
            return Ok(Business.AssetBC.AddCustomerReview(model));
        }

        #endregion

        #region Balance
        [Authorize]
        [HttpGet]
        [Route("getbalance")]
        public IHttpActionResult GetBalance()
        {
            return Ok(Business.AssetBC.GetBalance());
        }

        [Authorize]
        [HttpPost]
        [Route("gettransactions")]
        public IHttpActionResult GetTransactions(TransactionWeek week)
        {
            return Ok(Business.AssetBC.GetTransactions(week));
        }
        #endregion

        #region Push Notifications

        [Authorize]
        [HttpPost]
        [Route("adddevicetoken")]
        public IHttpActionResult AddDeviceToken(DeviceTokenViewModel model)
        {
            return Ok(Business.AssetBC.AddDeviceToken(model));
        }
        
        #endregion
    }
}
