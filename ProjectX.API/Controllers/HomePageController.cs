﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class HomePageController : Controller
    {
        // GET: HomePage
        public ActionResult Index()
        {
            return View("HomePage");
        }

        [HttpPost]
        public ActionResult Ingresar()
        {
            if (Request.Form["codinit"] != string.Empty)
            {
                string codigo = Request.Form["codinit"];
                Session["CodInit"] = codigo;
                return RedirectToAction("CRegistro", "CRegistro");
            }
            else
            {
                return RedirectToAction("CRegistro", "CRegistro");
            }
           

        }
    }
}