﻿using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace ProjectX.API.Controllers
{
    public class ADetailController : Controller
    {
        List<ProjectX.API.Models.AssetImage> Fotos;
        /// <summary>
        /// index vista
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index(int id)
        {
            try
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    if (Session["idDetail"] == null)
                    {
                        Fotos = db.AssetImages.Include("Asset").Where(x => x.UserId == id).ToList();

                        Session["deatil"] = Fotos;
                        Session["idDetail"] = id;

                        if (Fotos[0].Asset.NoVisitas == null)
                        {
                            Fotos[0].Asset.NoVisitas = 1;
                            db.SaveChanges();
                        }
                        else
                        {
                            int visitas = (int)Fotos[0].Asset.NoVisitas;
                            Fotos[0].Asset.NoVisitas = visitas + 1;
                            db.SaveChanges();
                        }

                        return View(Fotos);
                    }
                    else
                    {
                        int idDetail = Int16.Parse(Session["idDetail"].ToString());
                        if (idDetail != id)
                        {
                            Fotos = db.AssetImages.Include("Asset").Where(x => x.UserId == id).ToList();
                            Session["deatil"] = Fotos;
                            Session["idDetail"] = id;
                            if (Fotos[0].Asset.NoVisitas == null)
                            {
                                Fotos[0].Asset.NoVisitas = 1;
                                db.SaveChanges();
                            }
                            else
                            {
                                int visitas = (int)Fotos[0].Asset.NoVisitas;
                                Fotos[0].Asset.NoVisitas = visitas + 1;
                                db.SaveChanges();
                            }
                            return View(Fotos);
                        }
                    }

                    return View(Session["deatil"]);
                }
            }

            catch (Exception ex)
            {
                ErrorLog e = new ErrorLog();
                e.Error(ex.Message, "Index", "ADetail");
                FormsAuthentication.SignOut();
                Session.Clear();
                return RedirectToAction("Login", "Account");
            }
          
        }
        /// <summary>
        /// boton invitar
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Invitar(int id)
        {
            return JavaScript("window.location = 'http://" + Request.Url.Host + ":" + Request.Url.Port + "/XInvitacion/invitar/"+id + "'");
        }
    }
}