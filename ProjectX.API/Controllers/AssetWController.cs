﻿
using ProjectX.API.Helpers;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Twilio;
using Twilio.Rest;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace ProjectX.API.Controllers
{

    public class AssetWController : Controller
    {

        string uId = Helpers.Helper.GetId();
        // GET: AssetW
        /// <summary>
        /// INDEX
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Index()
        {
            PerfilAsset perfil = new PerfilAsset();

            try
            {
                using (var db = DBHelper.GetDatabase())
                {
                    Session["type"] = 2;
                    int[] type = { 1, 10, 11, 12 };
                    var today = DateTime.UtcNow.AddDays(-7);
                    var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                    int nots = db.Notifications.Where(n => n.AssetId == asset.Id && n.MessageStatus == 0 && n.CreatedOn >= today && type.Contains(n.MType)).Count();
                    int chats = db.Notifications.Where(n => n.AssetId == asset.Id && n.MessageStatus == 0 && n.CreatedOn >= today && n.MType == 3).Count();
                    var order = db.Orders.Where(x => x.AssetId == asset.Id && x.CreatedOn >= today && x.OrderStatus == 4).FirstOrDefault();
                    if (order != null)
                    {
                        Session["orderid"] = order.Id;
                        Session["chatSN"] = true;
                    }
                    else
                    {
                        Session["chatSN"] = false;
                    }
                    Session["nots"] = nots.ToString();
                    Session["chats"] = chats.ToString();
                    Session["id"] = asset.Id;
                    var tiempoActual = DateTime.UtcNow;
                    DateTime nac = asset.DOB == null ? DateTime.UtcNow : asset.DOB.Value;
                    //Fecha de nacimiento
                    var dif = tiempoActual.Subtract(nac).TotalDays;
                    double años = dif / 365;

                    perfil.ID = Int32.Parse(asset.Id.ToString());
                    perfil.Costo = asset.precio == null ? 0 : decimal.Parse(asset.precio.ToString());
                    perfil.Descripcion = asset.Description;
                    if (asset.AssetImages.Count > 0)
                    {
                        perfil.Foto = asset.AssetImages.FirstOrDefault().urlImage;
                        perfil.fotos1 = asset.AssetImages.ToList();
                    }
                    perfil.Saludo = asset.Title;
                    perfil.Edad = años.ToString("0");
                    perfil.Estatus = asset.statusProfile == null ? 0 : (int)asset.statusProfile;
                    perfil.Error = null;
                    perfil.online = asset.IsOnline;
                    perfil.Name = asset.Name;
                    perfil.DOB = asset.DOB == null ? DateTime.UtcNow : DateTime.Parse(asset.DOB.ToString());
                    perfil.CLABE = asset.AccountNum2;
                    perfil.ASex = asset.ASex;
                    perfil.HoM = asset.ASex == false ? "Mujer" : "Hombre";
                    perfil.Prefid = asset.APrefId;
                    perfil.visitas = asset.NoVisitas == null ? 0 : (int)asset.NoVisitas;
                    perfil.calificacion = asset.Calificacion == null ? 1 : (float)asset.Calificacion;
                    perfil.Offlineview = (bool)asset.offlineView;
                    perfil.redsocial = asset.RedSocial == null || asset.RedSocial == string.Empty ? "" : asset.RedSocial;
                    perfil.Email = asset.Email == null || asset.Email == string.Empty ? "" : asset.Email;
                    perfil.Disponibilidad = asset.disponibilidad;
                    // perfil.ciudad = asset.ciudad;
                    // perfil.estado = asset.estado;
                    if (asset.telefono != null)
                    {
                        perfil.Telefono = asset.telefono;
                    }

                    if (asset.IsOnline)
                    { Session["online"] = true; }
                    else if (bool.Parse(asset.offlineView.ToString()))
                    {
                        Session["online"] = true;
                    }
                    else { Session["online"] = false; }
                   
                    Session["perfil"] = perfil;
                    return View(perfil);
                }
            }
            catch (Exception ex)
            {
                ErrorLog e = new ErrorLog();
                e.Error(ex.Message, "Index", "AssetW");
                LogOff();
                return RedirectToAction("Login", "Account");

            }



        }
        /// <summary>
        /// PONER EN LINEA A ASSET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult OffOnLine(float latitud = 0.0f, float longitud = 0.0f)
        {
            try
            {
                Session["type"] = 2;
                PerfilAsset perfil = new PerfilAsset();
                using (var db = DBHelper.GetDatabase())
                {
                    var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                    if (asset.IsOnline || bool.Parse(asset.offlineView.ToString()))
                    {
                        asset.IsOnline = false;
                        asset.offlineView = false;
                        //asset.IsOnline = false;  
                    }
                    else
                    {
                       
                        asset.Latitude = latitud;
                        asset.Longitude = longitud;

                        asset.LocationUpdate = DateTime.UtcNow;
                        asset.CalendarActive = true;
                        asset.OnlineExpires = DateTime.UtcNow.AddHours(4);
                        asset.offlineView = true;
                    }
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        perfil.Error = ex.Message;
                        return View("Index", perfil);
                    }
                    var tiempoActual = DateTime.UtcNow;
                    DateTime nac = asset.DOB == null ? DateTime.UtcNow : asset.DOB.Value;
                    var dif = tiempoActual.Subtract(nac).TotalDays;
                    double años = dif / 365;

                    perfil.ID = Int32.Parse(asset.Id.ToString());
                    perfil.Costo = decimal.Parse(asset.precio.ToString());
                    perfil.Descripcion = asset.Description;
                    perfil.Foto = asset.AssetImages.FirstOrDefault().urlImage;
                    perfil.Saludo = asset.Title;
                    perfil.Edad = años.ToString("0");
                    perfil.Estatus = asset.statusProfile == null ? 0 : (int)asset.statusProfile;
                    perfil.Error = null;
                    perfil.online = asset.IsOnline;
                    perfil.Name = asset.Name;
                    perfil.DOB = asset.DOB == null ? DateTime.UtcNow : DateTime.Parse(asset.DOB.ToString());
                    perfil.CLABE = asset.AccountNum2;
                    perfil.ASex = asset.ASex;
                    perfil.Prefid = asset.APrefId;
                    perfil.Telefono = asset.telefono;
                    perfil.visitas = asset.NoVisitas == null ? 0 : (int)asset.NoVisitas;
                    Session["online"] = asset.IsOnline;
                    var assetimage = db.AssetImages.Where(x => x.UserId == asset.Id).ToList();
                    perfil.fotos1 = assetimage;
                    return View("Index", perfil);
                }
            }
            catch(Exception ex)
            {
                ErrorLog e = new ErrorLog();
                e.Error(ex.Message, "OffOnLine", "AssetW");
                LogOff();
                return RedirectToAction("Login", "Account");
            }

        }
        /// <summary>
        /// OBTIENE LOS SETINGS DEL ASSET
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Settings()
        {
            var settings = new SettingsModels();
            int id = int.Parse(Session["id"].ToString());
            using (var db = DBHelper.GetDatabase())
            {
                var asste = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                settings.ASex = asste.ASex;
                settings.Prefid = asste.APrefId;
                settings.Costo = asste.precio;
                settings.Telefono = asste.telefono;
                return View(settings);
            }
        }
        /// <summary>
        /// GUARDA CAMBIOS ASSET SETINGS
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public ActionResult SettingsSave(SettingsModels model)
        {
            int id = int.Parse(Session["id"].ToString());
            using (var db = DBHelper.GetDatabase())
            {
                var asste = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                asste.ASex = model.ASex;
                asste.APrefId = model.Prefid;
                asste.precio = model.Costo;
                asste.MaxDistance = model.Distancia;
                asste.telefono = model.Telefono.Trim();
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
                return View("Settings", model);
            }
        }
        /// <summary>
        /// OBTIENE LAS FOTOS DE LOS ASSETS
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Fotos()
        {
            int id = int.Parse(Session["id"].ToString());
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var assetimage = db.AssetImages.Where(x => x.UserId == id).ToList();
                // var images = db.AssetImages.Where(x => x.UserId == id).ToList();
                return View("Fotos", assetimage);
            }
        }
        /// <summary>
        /// GUARDA FOTOS DE LOS ASSETS
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult FotosSave(AssetImage model)
        {
            int id = int.Parse(Session["id"].ToString());
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var uId = Helpers.Helper.GetId();
                // var assetimage = db.AssetImages;
                AssetImage newfoto = new AssetImage
                {
                    UserId = id,
                    CreatedOn = DateTime.UtcNow,
                    ImageStream = model.ImageStream
                };

                var assetimage = db.AssetImages.Where(x => x.UserId == id).ToList();
                return View("Fotos", assetimage);
            }
        }
        /// <summary>
        /// CARGA FOTOS DESDE LA GALERIA
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult FileUpload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                byte[] array;
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    array = ms.GetBuffer();
                }
                //MemoryStream myMemStream = new MemoryStream(array);
                //Image fullsizeImage = Image.FromStream(myMemStream);

                ////string w = fullsizeImage.Width.ToString();
                ////string h = fullsizeImage.Height.ToString();


                //Image newImage = fullsizeImage.GetThumbnailImage(300, 400, null, IntPtr.Zero);
                //MemoryStream myResult = new MemoryStream();
                //newImage.Save(myResult, System.Drawing.Imaging.ImageFormat.Png);
                //var result = Business.AssetBC.SaveImage(myResult.ToArray());
                var img = new Business.AssetBC();
                var result = img.SaveImage(array);
            }
            return RedirectToAction("Index");
        }
        /// <summary>
        /// OBTIENE LOS DATOS DEL PERFIL
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public ActionResult Perfil()
        {
            var profile = new ProfileModels();
            int id = int.Parse(Session["id"].ToString());
            using (var db = DBHelper.GetDatabase())
            {
                var asste = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                profile.CLABE = asste.AccountNum2;
                profile.Saludo = asste.Title;
                profile.Descripcion = asste.Description;
                profile.DOB = asste.DOB == null ? DateTime.UtcNow : DateTime.Parse(asste.DOB.ToString());
                //var cdt = asste.DOB;
                //var m = Util.GetMonthAsString(cdt.Value.Month).Substring(0, 3);
                //profile.DOB = string.Format("{0:dd}", cdt) + "/" + m + "/" + string.Format("{0:yyyy}", cdt);


                profile.Name = asste.Name;
                return View(profile);
            }


        }
        [HttpPost]
        [Authorize]
        public ActionResult ProfileSave(PerfilAsset model)
        {
            var tiempoActual = DateTime.UtcNow;
            DateTime nac = model.DOB;
            //Fecha de nacimiento
            var dif = tiempoActual.Subtract(nac).TotalDays;

            double años = dif / 365;

            if (años < 18)
            {
                model.Error = "Debe cumplir con la mayoría de edad para poder completar su registro";

                return View("Index", model);
            }

            if (model.Email != null || model.Email != string.Empty)
            {
                try
                {
                    var addr = new System.Net.Mail.MailAddress(model.Email);
                }
                catch
                {
                    model.Error = "Email inválido";
                    return View("Index", model);
                }
            }
            else
            {
                model.Error = "Es necesasrio el Email";
                return View("Index", model);
            }

            if (años > 99)
            {
                model.Error = "Seleccione una fecha de nacimiento correcta";

                return View("Index", model);
            }

            else if (model.Costo == null)
            {
                model.Error = "No has declarado un precio por el servicio";
                return View("Index", model);
            }

            else if (model.Costo < 1000)
            {
                model.Error = "El costo por invitación no puede ser menor a $1000 pesos";
                return View("Index", model);
            }

            else if (model.Disponibilidad == string.Empty || model.Disponibilidad == null)
            {
                model.Error = "Debes ingresar tu disponibilidad";
                return View("Index", model);
            }

            else if (model.Costo > 100000)
            {
                model.Error = "El costo por invitación debe ser menor a $100,000 pesos";
                return View("Index", model);
            }

            else if (model.redsocial == null || model.redsocial == string.Empty)
            {
                model.Error = "Debe ingresar una red social";
                return View("Index", model);
            }

            else if (model.Descripcion == null || model.Descripcion == string.Empty)
            {
                model.Error = "Falata que agregues una descripción";
                return View("Index", model);
            }

            else if (model.Saludo == null || model.Saludo == string.Empty)
            {
                model.Error = "Es necesario que agregues un saludo :)";
                return View("Index", model);
            }

            else if (model.Telefono == null || model.Telefono == string.Empty)
            {
                model.Error = "Es necesario que ingreses un número telefónico para avisarte de actividad en tu cuenta";
                return View("Index", model);
            }

            else if (model.Telefono.Length > 12 || model.Telefono.Length < 8)
            {
                model.Error = "El número de teléfono ingresado es incorrecto";
                return View("Index", model);
            }          

            else { model.Error = null; }

            if (model.CLABE == null || model.CLABE == string.Empty || model.CLABE.Length < 18)
            {
                model.CLABE = "";
            }       

            int id = int.Parse(Session["id"].ToString());
            using (var db = DBHelper.GetDatabase())
            {
                var asste = db.Assets.Where(x => x.Id == id).FirstOrDefault();

                if (asste != null)
                {
                    asste.DOB = model.DOB;
                    asste.Title = model.Saludo;
                    asste.Description = model.Descripcion;
                    asste.AccountNum2 = model.CLABE.Trim();
                    asste.telefono = model.Telefono;
                    asste.ASex = model.ASex;
                    asste.APrefId = model.Prefid;
                    asste.precio = model.Costo;
                    asste.telefono = model.Telefono.Trim();
                    asste.RedSocial = model.redsocial;
                    asste.Email = model.Email;
                    asste.disponibilidad = model.Disponibilidad;


                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        model.Error = ex.Message;
                        return View("Index", model);
                    }

                    model.Error = null;

                    return RedirectToAction("Index");
                }
                else
                {
                    model.Error = "Usurio inexistente";
                    return View("Index");
                }
            }
        }
        /// <summary>
        /// BORRAR FOTOS DEL ASSET
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult DeleteFoto(string id)
        {

            int userid = int.Parse(Session["id"].ToString());
            Business.AssetBC.DeleteImage(id);
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                var assetimage = db.AssetImages.Where(x => x.UserId == userid).ToList();
                // var images = db.AssetImages.Where(x => x.UserId == id).ToList();
                //return JavaScript("window.location = 'http://" + Request.Url.Host + ":" + Request.Url.Port + "/AssetW/Index/" + id + "'");
                return RedirectToAction("Index");
            }
        }

        public ActionResult DePerfil(string id)
        {
            int userid = int.Parse(Session["id"].ToString());

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var assetimage = db.AssetImages.Where(x => x.UserId == userid).ToList();

                for (int i = 0; i < assetimage.Count; i++)
                {
                    if (assetimage[i].Id == id)
                    {
                        assetimage[i].IsProfile = true;
                    }
                    else
                    {
                        assetimage[i].IsProfile = false;
                    }
                    db.SaveChanges();
                }
                return RedirectToAction("Index");
            }
        }
        /// <summary>
        /// MANDAR PERFIL A REVISION
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public ActionResult Revision(Int32 id, float latitud = 0.0f, float longitud = 0.0f)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                PerfilAsset perfil = new PerfilAsset();
                var asset = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                if (asset != null)
                {
                    if (asset.ASex == null)
                    {
                        perfil.Error = "Falata que escojas tu género (Hombre o Mujer)";
                        return View("Index", perfil);
                    }

                    else if (asset.APref == null)
                    {
                        perfil.Error = "Falata que elijas tu preferncia (Hombres, mujeres o ambos)";
                        return View("Index", perfil);
                    }

                    else if (asset.precio == null)
                    {
                        perfil.Error = "No has declarado un precio por el servicio";
                        return View("Index", perfil);
                    }

                    else if (asset.precio < 1000)
                    {
                        perfil.Error = "El por servicio costo no puede ser menor a $1000 pesos";
                        return View("Index", perfil);
                    }

                    else if (asset.precio > 100000)
                    {
                        perfil.Error = "El costo por servicio  debe ser menor a $100,000 pesos";
                        return View("Index", perfil);
                    }

                    else if (asset.disponibilidad == string.Empty || asset.disponibilidad == null)
                    {
                        perfil.Error = "Debes ingresar tu disponibilidad";
                        return View("Index", perfil);
                    }

                    else if (asset.Description == null || asset.Description == string.Empty)
                    {
                        perfil.Error = "Falata que agregues una descripción";
                        return View("Index", perfil);
                    }

                    else if (asset.Title == null || asset.Title == string.Empty)
                    {
                        perfil.Error = "Es necesario que agregues un saludo :)";
                        return View("Index", perfil);
                    }
                    else if (asset.AccountNum2 == null || asset.AccountNum2 == string.Empty)
                    {
                        // perfil.Error = "Es necesario que ingreses un número de cuanta CLABE para el depósito de futuras comisiones";
                        // return View("Index", perfil);
                    }
                    else if (asset.telefono == null || asset.telefono == string.Empty)
                    {
                        perfil.Error = "Es necesario que ingreses un número telefónico para avisarte de actividad en tu cuenta";
                        return View("Index", perfil);
                    }
                    else if (asset.RedSocial == null || asset.RedSocial == string.Empty)
                    {
                        perfil.Error = "Debe ingresar una red social";
                        return View("Index", perfil);
                    }
                    else if (asset.Email == null || asset.Email == string.Empty)
                    {
                        perfil.Error = "Debe ingresar una email";
                        return View("Index", perfil);
                    }



                    else { }

                    var image = db.AssetImages.Where(x => x.UserId == id).Count();

                    if (image > 0)
                    {
                        asset.statusProfile = 1;
                        asset.Latitude = latitud;
                        asset.Longitude = longitud;
                        db.SaveChanges();
                    }
                    else
                    {
                        perfil.Error = "Debes agregar al menos una foto para tu perfil";
                    }

                }
                var tiempoActual = DateTime.UtcNow;
                DateTime nac = asset.DOB.Value;
                //Fecha de nacimiento
                var dif = tiempoActual.Subtract(asset.DOB.Value).TotalDays;

                double años = dif / 365;

                if (años < 18)
                {
                    perfil.Error = "Debe cumplir con la mayoría de edad para poder completar su registro";

                    return View("Index", perfil);
                }

                var assetimage = db.AssetImages.Where(x => x.UserId == asset.Id).ToList();
                PerfilAsset perfil1 = new PerfilAsset()
                {
                    ID = Int32.Parse(asset.Id.ToString()),
                    Costo = decimal.Parse(asset.precio.ToString()),
                    Descripcion = asset.Description,
                    fotos1 = assetimage,
                    Saludo = asset.Title,
                    Edad = años.ToString("0"),
                    Estatus = (int)asset.statusProfile,
                    DOB = asset.DOB == null ? DateTime.UtcNow : DateTime.Parse(asset.DOB.ToString()),
                    CLABE = asset.AccountNum2,
                    Prefid = asset.APrefId,
                    ASex = asset.ASex,
                    Telefono = asset.telefono,

                    Error = null

                };
                return View("Index", perfil1);
            }
        }
        public void mandarMensaje(string telefono)
        {
            //msm message = new msm();
            //string[] datosMensaje = { telefono, "has iniciado sesion en tu cta sens-room", "algo mas" };
            //message.enviamsm(datosMensaje);
        }

        [Authorize]
        public ActionResult Seguridad()
        {
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();

                var alerta = new DatosAlerta();
                alerta.RemitSms = asset.RemitSms;
                alerta.Numero = asset.TelEmergencia;

                return View("Seguridad", alerta);
            }
        }

        [Authorize]
        public ActionResult SaveTelEmergencia(DatosAlerta model)
        {
            Session["type"] = 2;
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                asset.TelEmergencia = model.Numero;
                asset.RemitSms = model.RemitSms;
                try
                {
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }


            }
            return RedirectToAction("Seguridad");
        }

        [Authorize]
        public ActionResult EnviarAlerta(string ubicacion)
        {
            Session["type"] = 2;
            var msm = new msm();
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset.TelEmergencia != null)
                {
                    string[] alerta = { asset.TelEmergencia, "Hola soy " + asset.RemitSms + " y me encuentro en una situación de riesgo esta es mi ubicación." + " " + ubicacion };
                    msm.enviamsm(alerta);
                }
                else
                {
                    ModelState.AddModelError("", "No has declarado un número de teléfono a quien enviar este mensaje");
                }
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        [Authorize]
        public ActionResult EnviarAlertaOK(string ubicacion)
        {
            Session["type"] = 2;
            var msm = new msm();
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset.TelEmergencia != null)
                {
                    string[] alerta = { asset.TelEmergencia, "Hola soy " + asset.RemitSms + ". Para notificar que llegué a la cita y todo está bien." + " " + ubicacion };
                    msm.enviamsm(alerta);
                    ViewBag.Alerta = "Mensaje enviado";
                }
                else
                {


                    ViewBag.Alerta = "Todavia no has guardado el número que recibirá el mensaje";
                }



                // ViewData["mensajealerta"] = "Todavia no has guardado el número que recibirá el mensaje";
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();
            return RedirectToAction("HomePage");
        }

        public ActionResult OnLine(float latitud = 0.0f, float longitud = 0.0f, int minutos = 4, string ciudad = "", string estado = "", string pais = "")
        {

            if (latitud == 0 || longitud == 0)
            {
                ModelState.AddModelError("", "Es necesario el permiso de ubicación para estar en línea.");
                return View("Index", Session["perfil"] as PerfilAsset);
            }

            try
            {
                Session["type"] = 2;
                PerfilAsset perfil = new PerfilAsset();
                using (var db = DBHelper.GetDatabase())
                {
                    var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                               
                        asset.Latitude = latitud;
                        asset.Longitude = longitud;
                        asset.ciudad = ciudad;
                        asset.estado = estado;
                        asset.pais = pais;
                        asset.LocationUpdate = DateTime.UtcNow;
                        asset.CalendarActive = true;
                        asset.OnlineExpires = DateTime.UtcNow.AddYears(minutos);                       
                        asset.offlineView = true;
                    
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch
            {
                LogOff();
                return RedirectToAction("Login", "Account");
            }

        }

        public ActionResult GanaDinero()
        {
            Session["type"] = 2;
            Codigos gana = new Codigos();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();

                if (asset != null)
                {
                    gana.Codigo1 = asset.Name;
                    gana.Codigo2 = asset.Name + asset.CusCode;
                    return View("GanaDinero", gana);
                }
                return View("GanaDinero");
            }
        }

        public ActionResult Eliminar()
        {
            return View("Eliminar");
        }

        [HttpPost]
        public ActionResult DarDeBaja()
        {
            string uId = Helpers.Helper.GetId();

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var aspuser = db.AspNetUsers.Where(x => x.Id == uId).FirstOrDefault();
                aspuser.fBaja = DateTime.Now;
                db.SaveChanges();
            }

            LogOff();
            return RedirectToAction("Login", "Account");
        }

        public JsonResult EnviarAlertaOKAjax(string ubicacion)
        {
            Session["type"] = 2;
            var msm = new msm();
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset.TelEmergencia != null)
                {
                    string[] alerta = { asset.TelEmergencia, "Hola soy " + asset.RemitSms + ". Para notificar que llegué a la cita y todo está bien." + " " + ubicacion };
                    msm.enviamsm(alerta);
                    return Json("Mensaje enviado", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Debes configurar esta seccion en 'Mi seguridad'", JsonRequestBehavior.AllowGet);
                }
            }
        }

        public JsonResult EnviarAlertaAjax(string ubicacion)
        {
            Session["type"] = 2;
            var msm = new msm();
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset.TelEmergencia != null)
                {
                    string[] alerta = { asset.TelEmergencia, "Hola soy " + asset.RemitSms + " y me encuentro en una situación de riesgo esta es mi ubicación." + " " + ubicacion };
                    msm.enviamsm(alerta);
                    return Json("Mensaje enviado", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Debes configurar esta seccion en 'Mi seguridad'", JsonRequestBehavior.AllowGet);
                }
            }
        }


        [Authorize]
        public JsonResult OnLineAjax(float latitud = 0.0f, float longitud = 0.0f, int minutos = 0)
        {
            Session["type"] = 2;
            PerfilAsset perfil = new PerfilAsset();
            using (var db = DBHelper.GetDatabase())
            {
                string uId = Helpers.Helper.GetId();
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                if (asset.IsOnline)
                {
                    asset.IsOnline = false;
                    return Json("fuera de línea", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    asset.IsOnline = true;//
                    asset.Latitude = latitud;
                    asset.Longitude = longitud;
                    asset.LocationUpdate = DateTime.UtcNow;
                    asset.CalendarActive = true;
                    asset.OnlineExpires = DateTime.UtcNow.AddMinutes(minutos);
                    asset.IsOnline = true;
                }
                try
                {
                    db.SaveChanges();
                    return Json("En línea hasta las ..." + (DateTime.Now.TimeOfDay.Minutes + minutos).ToString(), JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    perfil.Error = ex.Message;
                    return Json("Error", JsonRequestBehavior.AllowGet);
                }

            }
        }

        public ActionResult ComoFuncionaA()
        {
            return View("ComoFuncionaA");
        }

        [Authorize]
        public JsonResult OfflineViewActivar(float latitud = 0.0f, float longitud = 0.0f, int minutos = 0, string ciudad = "", string estado = "", string pais = "")
        {
            if (latitud == 0 || longitud == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                asset.Latitude = latitud;
                asset.Longitude = longitud;
                asset.ciudad = ciudad;
                asset.estado = estado;
                asset.pais = pais;
                asset.offlineView = true;
                asset.IsOnline = false;
                asset.offlineViewExpires = DateTime.Now.AddMinutes(minutos);
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult locationUpdate(float latitud = 0.0f, float longitud = 0.0f, string ciudad = "", string estado = "", string pais = "")
        {
            if (latitud == 0 || longitud == 0)
            {
                return Json(false, JsonRequestBehavior.AllowGet);

            }
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                asset.Latitude = latitud;
                asset.Longitude = longitud;
                asset.ciudad = ciudad;
                asset.estado = estado;
                asset.pais = pais;
                asset.LocationUpdate = DateTime.UtcNow;
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult OfflineViewDesactivar()
        {
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                asset.offlineView = false;
                db.SaveChanges();
                return Json(true, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public JsonResult OfflineViewVerificar()
        {
            string uId = Helpers.Helper.GetId();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.UserId == uId).FirstOrDefault();
                return Json(asset.offlineView, JsonRequestBehavior.AllowGet);
            }
        }

       
    }
}