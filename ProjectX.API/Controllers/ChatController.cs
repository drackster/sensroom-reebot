﻿using Newtonsoft.Json;
using ProjectX.API.Business;
using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace ProjectX.API.Controllers
{
    [HandleError]
    public class ChatController : Controller
    {
        int[] types = new[] { 3, 6 };
      

        public ActionResult Default()
        {
            var x = new AccountController();
            x.LogOff();
            return null;
        }


        // GET: Chat
        [Authorize]
        public ActionResult Chat(string idO)
        {
            Session["chatid"] = idO;
            using (var db = Helpers.DBHelper.GetDatabase())
            {  
                long id = Int32.Parse(idO.ToString());                
                var mensajes = db.Notifications.Where(x => x.OrderId == id && types.Contains(x.MType)).ToList();

                if (mensajes.Count > 0 )
                {
                    for (int i = 0; i < mensajes.Count; i++)
                    {
                        if (int.Parse(Session["type"].ToString()) == 1 && mensajes[i].MReceiver == 2)
                        {
                            mensajes[i].MessageStatus = 3;
                            db.SaveChanges();
                        }
                        else if (int.Parse(Session["type"].ToString()) == 2 && mensajes[i].MReceiver == 1)
                        {
                            mensajes[i].MessageStatus = 3;
                            db.SaveChanges();
                        }
                       
                    }
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots = from n in notlist
                               where types.Contains(n.MType)
                               select n;
                    return View("Chat", nots.ToList());
                }
                else
                {                  
                    return RedirectToAction("SalaChat", "SalaChat");
                }
               
            }
        }

        [Authorize]
        public JsonResult ChatAJ(string idO)
        {
            Session["chatid"] = idO;
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                long id = Int32.Parse(idO.ToString());
                var mensajes = db.Notifications.Where(x => x.OrderId == id && types.Contains(x.MType)).ToList();

                if (mensajes.Count > 0)
                {
                    for (int i = 0; i < mensajes.Count; i++)
                    {
                        mensajes[i].MessageStatus = 3;
                        db.SaveChanges();
                    }
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();

                    var mens = new List<MensajesChat>();

                    var nots = from n in notlist
                               where types.Contains(n.MType)
                               select n;

                    foreach (var item in nots)
                    {
                        mens.Add(new MensajesChat()
                        {
                            fecha = item.CreatedOn,
                            mensaje = item.MessageText,
                            type = item.MType
                        });
                       
                    }
                    var json = JsonConvert.SerializeObject(mens);
                 
                    //ViewBag.Chat = nots.ToList();
                    return Json(new { json }, JsonRequestBehavior.AllowGet);
                    //return View("Chat", nots.ToList());


                }
                else
                {

                    return null;
                    //return RedirectToAction("SalaChat", "SalaChat");
                }

            }
        }

        public ActionResult InsertMessage(int orderid, int typeid, string message)
        {
            if (message != null && message != string.Empty && message != "")
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var nots = db.Notifications.Where(x => x.OrderId == orderid).FirstOrDefault();
                    var model = new NotificationViewModel
                    {
                        MType = typeid == 2 ? MessageType.AssetMessage : MessageType.CustomerMessage,
                        MReceiver = typeid == 1 ? MessageSender.Customer : MessageSender.Asset,
                        AssetId = nots.AssetId,
                        CustomerId = nots.CustomerId,
                        MessageText = message,
                        OrderId = orderid
                    };
                    var n = MessagingBC.AddNotificationViewModel(model);
                    long id = Int32.Parse(orderid.ToString());
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots1 = from x in notlist
                                where types.Contains(x.MType)
                                select x;
                    return View("Chat", nots1.ToList());
                }
            }
            else
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    long id = Int32.Parse(orderid.ToString());
                    var notlist = db.Notifications.Where(x => x.OrderId == id).ToList();
                    var nots1 = from x in notlist
                                where types.Contains(x.MType)
                                select x;
                    return View("Chat", nots1.ToList());
                }
            }
        }


      
    }
}