﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using Kendo.Mvc.Extensions;
using System.Text;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using System.Data.Entity.Migrations;
using System.Web.Configuration;
using ProjectX.API.Business;

namespace ProjectX.API.Controllers
{
    [Authorize]
    public class AdminController : Controller
    {
        public ActionResult Configuracion()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            //AlcioneConfigModel model = Business.CatalogBC.GetAlcioneConfigModel();
            return View(1);
        }

        [HttpPost]
        public ActionResult Configuracion(ConfigModel model)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }

            if (ModelState.IsValid && model.Id > 0)
            {
                bool TriggerUpdate = false;
                bool EmailTriggerUpdate = false;

                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    var m = db.XConfigs.Where(x => x.Id == model.Id).FirstOrDefault();
                    if (m != null)
                    {
                        //validar que se vayan a modificar los triggers de quartz
                        TriggerUpdate = m.NSJobSeconds != model.CatalogOffSetMinutes;
                        EmailTriggerUpdate = m.EmailJobSeconds != model.EmailJobSeconds;

                        m.iOSMinVersion = model.iOSMinVersion;
                        m.iOSStoreUrl = model.iOSStoreUrl;
                        m.DroidMinVersion = model.DroidMinVersion;
                        m.DroidStoreUrl = model.DroidStoreUrl;
                        m.EmailJobSeconds = model.EmailJobSeconds;
                        m.ListNumRecords = model.ListNumRecords;
                        m.RefeshCount = model.RefreshCount;
                        m.PageCount = model.PageCount;
                        m.PageSize = model.PageSize;
                        m.NSJobSeconds = model.CatalogOffSetMinutes;
                        m.CartExpireHours = model.CartExpireHours;
                        m.TaxAmount = model.TaxAmount;
                        m.DeliveryNumDays = model.DeliveryNumDays;
                        //No tocar estos campos 
                        //m.CatalogJobBussy = model.CatalogJobBussy;
                        //m.LastUpdate = model.LastUpdate;

                        db.SaveChanges();
                    }
                }

                //Actializar trigger
                if (TriggerUpdate)
                {
                    Jobs.JobScheduler.UpdateJob(model.CatalogOffSetMinutes);
                }
                if (EmailTriggerUpdate)
                {
                    Jobs.JobScheduler.UpdateEmailJob(model.EmailJobSeconds);
                }
            }

            return View(model);
        }

        public ActionResult Usuarios()
        {
            if (!User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Login", "Account");
            }
            return View();
        }

        public ActionResult SetUserAdminType(string UserId, int AdminType, bool IsChecked)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var user = db.AspNetUsers.Where(x => x.Id == UserId).FirstOrDefault();
                if (user != null)
                {
                    if (!IsChecked)
                    {
                        user.AdminType = 0; //normal cuando todo esta apagado
                    }
                    else //kiosk or admin
                    {
                        user.AdminType = AdminType;
                    }
                    db.SaveChanges();
                }
            }
            return Json(new { Error = 0 }, JsonRequestBehavior.AllowGet);
        }

        //public ActionResult UsuariosGrid(string SearchTerm, [DataSourceRequest] DataSourceRequest request)
        //{
        //    try
        //    {
        //        var list = Business.UserBC.SearchUsers(SearchTerm);
        //        DataSourceResult result = list.ToDataSourceResult(request);
        //        return Json(result, JsonRequestBehavior.AllowGet);

        //    }
        //    catch (Exception ex)
        //    {
        //        List<AspNetUser> list = new List<AspNetUser>();
        //        DataSourceResult result = list.ToDataSourceResult(request);
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //public ActionResult Reportes()
        //{
        //    DateTime dt =DateTime.UtcNow.Date;
        //    DateTime start = new DateTime(dt.Year, dt.Month, 1);
        //    DateTime end = dt;
        //    ReportsViewModel model = new ReportsViewModel()
        //    {
        //        StartDate = start,
        //        EndDate = end,
        //        showActiveConnections = false
        //    };

        //    return View(GenerateReport(model));
        //}

        //[HttpPost]
        //public ActionResult Reportes(ReportsViewModel model)
        //{
        //    return View(GenerateReport(model));
        //}

        //private ReportsViewModel GenerateReport(ReportsViewModel model)
        //{

        //    //if (model.showActiveConnections) model.activeConnections = ReportBC.GetActiveUsers();
        //    //if (model.showActionRank) model.userActionRank = ReportBC.GetActionRank(model.StartDate, model.EndDate);
        //    return model;
        //}



    }
}
