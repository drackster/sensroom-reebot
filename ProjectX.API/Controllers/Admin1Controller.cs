﻿using ProjectX.API.Models;
using ProjectX.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class Admin1Controller : Controller
    {
        int cantidad = 0;
        List<PerfilAsset> perfiles = new List<PerfilAsset>();
        // GET: Admin1
        public ActionResult Admin1()
        {
            SelectAsset();
            return View(perfiles);
        }

        public ActionResult Aprobar(int id)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                asset.statusProfile = 2;              
                asset.OnlineExpires = DateTime.UtcNow.AddYears(4);
                asset.offlineView = true;
                db.SaveChanges();
                SelectAsset();
                EnviarMensajeSMS(asset.telefono, asset.Name);
            }
            return View("Admin1", perfiles);
        }

        public ActionResult Rechazar(int id)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                asset.statusProfile = 3;
                db.SaveChanges();
                SelectAsset();
              
            }
            return View("Admin1", perfiles);
        }

        public void SelectAsset()
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {

                var asset = db.Assets.Where(x => x.statusProfile == 1).OrderByDescending(x => x.CreatedOn).Take(10).ToList();
                var tiempoActual = DateTime.UtcNow;
                foreach (var item in asset)
                {
                    try
                    {
                        var dif = tiempoActual.Subtract(DateTime.Parse(item.DOB.ToString())).TotalDays;
                        double años = dif / 365;
                        PerfilAsset perfil = new PerfilAsset();
                        perfil.ID = Int32.Parse(item.Id.ToString());
                        perfil.Costo = decimal.Parse(item.precio.ToString()); ;
                        perfil.Descripcion = item.Description;
                        perfil.Saludo = item.Title;
                        perfil.Name = item.Name;
                        perfil.Edad = años.ToString("0");
                        perfil.Telefono = item.telefono;
                        perfil.Foto = item.AssetImages.FirstOrDefault().urlImage;
                        perfil.redsocial = item.RedSocial;
                        perfiles.Add(perfil);
                    }
                    catch (Exception ex)
                    { }
                }
            }
        }

        private void EnviarMensajeSMS(string tel, string nombre)
        {
            string telefono = tel;
            string cuerpo = "¡Hola " + nombre + "! tú perfil a sido aprobado en SensRoom, da click en https://sens-room.com para entrar a tu perfil y puedas ponerte en línea.";
            string[] mensaje = { telefono, cuerpo };
            var sms = new msm();
            sms.enviamsm(mensaje);
        }

        private void EnviarMensajeSMSF(string tel, string feed)
        {
            string telefono = tel;
            string cuerpo = "¡Hola! Revisamos tus datos en https://sens-room.com y te sugerimos hacer el siguiente ajuste para aprobar tu perfil: " + feed ;
            string[] mensaje = { telefono, cuerpo };
            var sms = new msm();
            sms.enviamsm(mensaje);
        }

        public ActionResult feedback(int id, string mensaje)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var asset = db.Assets.Where(x => x.Id == id).FirstOrDefault();
                asset.statusProfile = 0;
                db.SaveChanges();
                var newNotification = db.Notifications.Add(new Notification
                {
                    //TODO
                    MType = 12,
                    AssetId = id,// model.AssetId,
                    CustomerId = null,// model.CustomerId,
                    OrderId = null,//newOrder.Id,
                    MessageText = mensaje,
                    MessageStatus = (int)MessageStatus.Created,
                    CreatedOn = DateTime.UtcNow,
                    MReceiver = (int)MessageSender.None //both
                });
                db.SaveChanges();
                EnviarMensajeSMSF(asset.telefono, mensaje);

            }
            return RedirectToAction("Admin1", "Admin1");
        }


        protected void Aplication_Error(object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();
            Response.Clear();

            HttpException httpException = exception as HttpException;

            if (httpException != null)
            {
                string action;

                switch (httpException.GetHttpCode())
                {
                    case 404:
                        // page not found
                        action = "HttpError404";
                        break;
                    case 500:
                        // server error
                        action = "HttpError500";
                        break;
                    default:
                        action = "General";
                        break;
                }

                // clear error on server
                Server.ClearError();

                RedirectToAction("Account", "Login");
            }
        }


        public ActionResult page(int pag)
        {
            if (Session["cantidad"] == null)
            {
                Session["cantidad"] = pag;
            }
            else
            {
                int guardado = int.Parse(Session["cantidad"].ToString());
                Session["cantidad"] = guardado + pag;
            }            

            int xxx = int.Parse(Session["cantidadAs"].ToString());

            if (xxx < 0)
            {
                xxx = 0;
            }

            using (var db = Helpers.DBHelper.GetDatabase())
            {
                int assetC = db.Assets.Where(x => x.statusProfile == 1).Count();
                if(xxx > assetC)
                {
                    int sss = 0;
                    sss =  xxx - assetC;
                    xxx = assetC - sss;
                }
                var asset = db.Assets.Where(x => x.statusProfile == 1).OrderByDescending(x => x.CreatedOn).Skip(xxx).Take(10).ToList();
                var tiempoActual = DateTime.UtcNow;
                foreach (var item in asset)
                {
                    try
                    {
                        var dif = tiempoActual.Subtract(DateTime.Parse(item.DOB.ToString())).TotalDays;
                        double años = dif / 365;
                        PerfilAsset perfil = new PerfilAsset();
                        perfil.ID = Int32.Parse(item.Id.ToString());
                        perfil.Costo = decimal.Parse(item.precio.ToString());
                        perfil.Descripcion = item.Description;
                        perfil.Saludo = item.Title;
                        perfil.Name = item.Name;
                        perfil.Edad = años.ToString("0");
                        perfil.Telefono = item.telefono;
                        perfil.Foto = item.AssetImages.FirstOrDefault().urlImage;
                        perfiles.Add(perfil);
                    }
                    catch (Exception ex)
                    { }
                }
            }

            return View("Admin1", perfiles);
        }

        public ActionResult PassUpdate()
        {
            return View("PassUpdate");
        }

        public ActionResult actualiza(string name)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                var sss = db.sp_ActualizaContraseña(name);                
            }
            return View("PassUpdate");
        }

        public ActionResult Reporte()
        {
            return View("Reporte");
        }

        public ActionResult consultar(DateTime? inicial , DateTime? final, int opcion = 0)
        {

            if (inicial == null || final == null)
            {
                inicial = DateTime.Now.AddDays(-7);
                final = DateTime.Now.AddDays(1);
            }           
            var rep = new List<Evento>();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                if (opcion == 0)//todas
                {
                    
                    rep = db.Eventos.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final).OrderByDescending(x => x.CreatedOn).ToList();
                }
                else if (opcion == 1)//aceptada
                {
                    rep = db.Eventos.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final && x.OrderStatus == 4).OrderByDescending(x => x.CreatedOn).ToList();
                }
                else if (opcion == 2)//cancelada
                {
                    rep = db.Eventos.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final && x.OrderStatus == 2).OrderByDescending(x => x.CreatedOn).ToList();
                }
                else // rechazada
                {
                    rep = db.Eventos.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final && x.OrderStatus == 3).OrderByDescending(x => x.CreatedOn).ToList();
                }


                ViewBag.id = "eventos";

                return View("Reporte", rep);
            }            
        }

        public ActionResult Clientes(DateTime? inicial, DateTime? final, int opcion = 0)
        {
            if (inicial == null || final == null)
            {
                inicial = DateTime.Now.AddDays(-7);
                final = DateTime.Now.AddDays(1);
            }           
            var rep = new List<CustomerName>();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                if (opcion == 0)//todas
                {
                    rep = db.CustomerNames.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final).OrderByDescending(x=>x.CreatedOn).ToList();
                }
                ViewBag.id = "Clientes";
                
                return View("Reporte", rep);               
            }
        }

        public ActionResult Assets(DateTime? inicial, DateTime? final, int opcion = 0)
        {
            if (inicial == null || final == null)
            {
                inicial = DateTime.Now.AddDays(-7);
                final = DateTime.Now.AddDays(1);
            }
            var rep = new List<Asset>();
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                if (opcion == 0)//todas
                {
                    rep = db.Assets.Where(x => x.CreatedOn >= inicial && x.CreatedOn <= final).OrderByDescending(x => x.CreatedOn).ToList();
                }
                ViewBag.id = "Assets";

                return View("Reporte", rep);
            }
        }
    }
}