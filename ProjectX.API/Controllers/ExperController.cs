﻿using ProjectX.API.Helpers;
using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class ExperController : Controller
    {
        // GET: Exper
        public ActionResult Index()
        {
            return View();
        }


        [Authorize]
        public void normalizarImg()
        {
            List<AssetImage> arrayl = addImgae(340);
            foreach (var item in arrayl)
            {
                byte[] array = item.ImageStream;
                MemoryStream myMemStream = new MemoryStream(array);
                Image fullsizeImage = Image.FromStream(myMemStream);
                //string w = fullsizeImage.Width.ToString();
                //string h = fullsizeImage.Height.ToString();
                Image newImage = fullsizeImage.GetThumbnailImage(350, 450, null, IntPtr.Zero);
                MemoryStream myResult = new MemoryStream();
                newImage.Save(myResult, System.Drawing.Imaging.ImageFormat.Jpeg);
                //   var result = Business.AssetBC.SaveImage(myResult.ToArray());

                using (var db = DBHelper.GetDatabase())
                {
                    var assimg = db.AssetImages.Where(x => x.Id == item.Id).FirstOrDefault();
                    assimg.ImageStream = myResult.ToArray();
                    db.SaveChanges();
                }
            }

        }

        private List<AssetImage> addImgae(int id)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var ids = db.AssetImages.Where(x => x.UserId == id).ToList();

                return ids;
            }
        }
    }
}