﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProjectX.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ProjectX.API.Models;
using ProjectX.API.Business;
using System.Threading.Tasks;
using System.IO;

namespace ProjectX.API.Controllers
{
    [RoutePrefix("api/customer")]
    public class CustomerController : ApiController
    {
        #region Search Preferences

        [Authorize]
        [HttpGet]
        [Route("getxsettings")]
        public IHttpActionResult GetXSettings()
        {
            return Ok(Business.CustomerBC.GetXSettings());
        }

        [Authorize]
        [HttpPost]
        [Route("savesettings")]
        public IHttpActionResult SaveSettings(ASettingsViewModel model)
        {
            return Ok(Business.AssetBC.SaveSettings(model));
        }

        #endregion

        #region Payments

        [Authorize]
        [HttpPost]
        [Route("savecustomercredit")]
        public IHttpActionResult SaveCustomerCredit(CustomerCredit model)
        {
            return Ok(Business.CustomerBC.SaveCustomerCredit(model));
        }


        [HttpGet]
        [Route("updatecustomercredit/{name}")]
        public IHttpActionResult GetCustomerCredit(string name)
        {
            return Ok(Business.CustomerBC.UpdateCustomerCredit(name));
        }

        //OPENPAY * YA NO SE UTILIZA
        //[Authorize]
        //[HttpPost]
        //[Route("addpayment")]
        //public IHttpActionResult AddPayment(CustomerPaymentViewModel model)
        //{
        //    #region Init
        //    ///Before adding a card, we have to verify that the lava customer has a ProviderId (created by OpenPay)
        //    OpenPayBC openPayBC = new OpenPayBC(); //OpenPayManager
        //    var customer = Helpers.Helper.GetCustomer();
        //    string cname = customer.AspNetUser.UserName.ToLower();
        //    string cemail = cname + "@px.com";
        //    CustomerPaymentMethod x = new CustomerPaymentMethod();
        //    string trimNumber = model.CreditCardType.ToLower() == "amex" ? model.CreditCardNumber.Substring(12, 3) :
        //            model.CreditCardNumber.Substring(12, 4);
        //    #endregion

        //    //Antes de checar si tiene providerId, quiza el cliente ya ha pagado con esta tarjeta
        //    var savedcards = CustomerPaymentBC.GetCustomerPaymentList();

        //    //si ya tiene tarjetas salvadas, re-utilizar las ya grabadas,
        //    //no se pueden reutilizar con la misma terminacion y año de vencimiento
        //    if(savedcards.Count > 0)
        //    {
        //        x = savedcards.Where(z => z.CreditCardNumber == trimNumber
        //                            && z.ExpirationDate.Year == model.ExpirationDate.Year
        //                            && z.ExpirationDate.Month == model.ExpirationDate.Month
        //                            && z.CVC == model.CVC).FirstOrDefault();
        //    }

        //    //Si no tiene tarjetas el cliente o no se encuentra en las que ya tiene salvadas
        //    if (savedcards.Count == 0 || x == null)
        //    {
        //        #region Check/Add Customer ProviderId
        //        //If it doesnt have a providerid, create the openpay customer and add its id as ProviderID in lavaCustomer table
        //        if (customer.ProviderId == null)
        //        {
        //            var presult = CustomerPaymentBC.CreateCustomerOpenPayId(customer);
        //            if (presult.Success)
        //            {
        //                customer.ProviderId = presult.ProviderId;
        //            }
        //            else
        //            {
        //                return Ok(presult);
        //            }
        //        }
        //        #endregion

        //            #region Add New Card
        //            try
        //        {
        //            //Now, with a providerID, Add the new card
        //            var newOpenPayCard = openPayBC.AddCard(customer.ProviderId, cname, //TODO? FACEBOOK NAMES CAN BE DIFFERENT
        //                    model.CreditCardNumber, model.ExpirationDate.Month.ToString(),
        //                    model.ExpirationDate.Year.ToString().Substring(2, 2), model.CVC);
        //            model.ProviderId = newOpenPayCard.Id; //set the card provider id from open pay
        //        }
        //        catch (Openpay.OpenpayException ex)
        //        {
        //            return Ok(new CustomerPaymentViewModel() { Success = false, ProviderId = ex.Description });
        //            //return InternalServerError(new Exception(ex.Description, ex));
        //        }


        //        //TRIM DIGITS
        //        model.CreditCardNumber = trimNumber;

        //        ///If eveything looks ok, Save it to LAVA DB
        //        x = Business.CustomerPaymentBC.CreateCustomerPayment(model);
        //        #endregion
        //    }

        //    //Regresa el VM ya sea con la tarjeta grabada o la recien agregada
        //    return Ok(new CustomerPaymentViewModel()
        //    {
        //        Id = x.Id,
        //        CustomerId = x.CustomerId,
        //        ProviderId = x.ProviderId,
        //        NameOnCard = cname,
        //        CreditCardType = x.CreditCardType,
        //        CreditCardNumber = x.CreditCardNumber,//Save the last 4 digis Only
        //        CVC = x.CVC,
        //        ExpirationDate = x.ExpirationDate,
        //        CreatedOn = x.CreatedOn,
        //        Success = true
        //    });
        //}

        //[Authorize]
        //[Route("createcharge")]
        //[HttpPost]
        //public IHttpActionResult CreateCharge(ChargeViewModel model)
        //{
        //    using (var db = Helpers.DBHelper.GetDatabase())
        //    {
        //        OpenPayBC openPayBC = new OpenPayBC();
        //        var order = db.Orders.Include("Customer").Where(s => s.Id == model.OrderId).FirstOrDefault();
        //        var payment = db.CustomerPaymentMethods.Where(x => x.Id == model.PaymentMethodId).Single();

        //        var charge = openPayBC.CreateCharge(
        //            order.Customer.ProviderId,
        //            payment.ProviderId,
        //            order.TotalCost,
        //            "Servicio Orden" + order.Id.ToString(),
        //            false,
        //            model.DeviceTokenId);

        //        if (charge.ErrorMessage != null)// || order.ChargeRequestId == null)
        //        {
        //            order.ChargeId = charge.Id;
        //            order.OrderStatus = (int)OrderStatus.PreChargeFailure;
        //        }
        //        else
        //        {
        //            order.ChargeId = charge.Id;
        //            order.OrderStatus = (int)OrderStatus.PreChargeSucess;
        //        }

        //        db.SaveChangesAsync();
        //        return Ok(charge.ErrorMessage == null ? string.Empty : charge.ErrorMessage);

        //    }

        //    #region Test
        //    //var service = ReservationBC.GetServiceByRequestToken(RequestToken);

        //    //if(service.CustomerPaymentMethod.CreditCardNumber.EndsWith("2222"))
        //    //{
        //    //    service.StatusId = (int)ServiceStatus.PreChargeFailure;
        //    //    db.SaveChanges();
        //    //    return Ok("Fondos Insuficientes");
        //    //}
        //    //else
        //    //{
        //    //    service.StatusId = (int)ServiceStatus.PreChargeSucess;
        //    //    db.SaveChanges();
        //    //    return Ok(string.Empty);
        //    //}
        //    #endregion

        //}


        #endregion
    }
}
