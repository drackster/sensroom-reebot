﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProjectX.ViewModels;

namespace ProjectX.API.Controllers
{
    [Authorize]
    [RoutePrefix("api/order")]
    public class OrderController : ApiController
    {
        [HttpPost]
        [Route("getorders")]
        public IHttpActionResult GetOrders(OrderSearchModel model)
        {
            return Ok(Business.OrderBC.GetOrders(model));
        }

        [HttpPost]
        [Route("getappointments")]
        public IHttpActionResult GetAppointments(NotificationRequestModel model)
        {
            return Ok(Business.OrderBC.GetSchedule(model));
        }

        //[HttpGet]
        //[Route("getinvitation/{OrderId}")]
        //public IHttpActionResult GetInvitation(long OrderId)
        //{
        //    return Ok(Business.MessagingBC.GetInvitation(OrderId, MessageSender.));
        //}
        [HttpPost]
        [Route("saveorder")]
        public IHttpActionResult SaveOrder(OrderViewModel model)
        {
            return Ok();
            return Ok(Business.OrderBC.SaveOrder(model));
        }

        [HttpPost]
        [Route("saveorderoxxo")]
        public IHttpActionResult SaveOrderOxxo(OrderViewModel model)
        {
            return Ok(Business.OrderBC.SaveOrderOxxo(model));
        }

        [HttpGet]
        [Route("getTDCData/{id}")]
        public IHttpActionResult GetTDCData(int id)
        {
            return Ok(Business.OrderBC.getDatosTDC(id));
        }

        [HttpPost]
        [Route("saveorderandpayment")]
        public IHttpActionResult SaveOrderAndPayment(PaymentChargeModel model)
        {
            return Ok(Business.OrderBC.SaveOrder(model.Order, model.Payment));
        }
        [HttpGet]
        [Route("getorderstatus/{OrderId}")]
        public IHttpActionResult GetOrderStatus(long OrderId)
        {
            return Ok(Business.OrderBC.GetOrderStatus(OrderId));
        }
        [HttpPost]
        [Route("updateorderstatus")]
        public IHttpActionResult UpdateOrderStatus(OrderViewModel model)
        {
            return Ok(Business.OrderBC.UpdateOrderStatus(model));
        }

        [HttpGet]
        [Route("verificaPagoOxxo/{id}")]
        public IHttpActionResult verificaPagoOxxo(string id)
        {
            return Ok(Business.OrderBC.verificaOxxo(id));
        }

        [HttpGet]
        [Route("getcustomercredit/{name}")]
        public IHttpActionResult GetCustomerCredit(string name)
        {
            return Ok(Business.OrderBC.GetCustomerCredit(name));
        }

    }
}
