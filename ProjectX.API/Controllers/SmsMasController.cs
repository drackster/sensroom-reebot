﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class SmsMasController : Controller
    {
       
        // GET: SmsMas
        public ActionResult Index()
        {
            return View("SmsMas");
        }


        public JsonResult comentario(string comment, int group)
        {           
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                msm mensaje = new msm();
                try
                {
                   
                    // [0] telefono [1] mensaje 
                    if (group == 1)
                    {
                        var asset = db.Assets.Where(
                         x=> x.Longitude != null &&
                         x.Latitude != null &&                      
                         x.statusProfile == 2 &&
                         x.FBaja == null &&
                         x.AssetImages.Count > 0 && (
                         x.IsOnline == true || x.offlineView == true)).Select(i => new { i.telefono, i.Name }).ToList();
                        var telList = asset.Distinct();    
                        
                        foreach (var ass in telList)
                        {                          
                           string[] mensajes = { ass.telefono, "SensRoom: Hola " + ass.Name+" " +comment };
                           mensaje.enviamsm(mensajes);
                        }
                    }
                    else if (group == 2)
                    {
                        var asset = db.Assets.Where(x => x.statusProfile == null && x.telefono != null).Select(x => x.telefono).ToList();
                        foreach (var tel in asset)
                        {
                            // string[] mensajes = { tel, comment };
                            // mensaje.enviamsm(mensajes);
                        }
                    }
                    else
                    {
                        //var asset = db.Assets.Where(x => x.telefono != null).Select(x => x.telefono).ToList();
                        //foreach (var tel in asset)
                        //{
                        //    string[] mensajes = { tel, comment };
                        //    mensaje.enviamsm(mensajes);
                        //}
                    }
                    return Json("Mensaje enviado, te contactaremos lo mas pronto posible", JsonRequestBehavior.AllowGet);
                }                  
                
                catch
                {
                    return Json("No fue posible enviar el mensaje, por favor manda tu duda a atencion@sens-room.com", JsonRequestBehavior.AllowGet);
                }

            }
        }



        public JsonResult Clientes(string comment, int group)
        {
            using (var db = Helpers.DBHelper.GetDatabase())
            {
                msm mensaje = new msm();
                try
                {

                    // [0] telefono [1] mensaje 
                    if (group == 1)
                    {
                        var asset = db.Assets.Where(
                         x => x.Longitude != null &&
                         x.Latitude != null &&
                         x.statusProfile == 2 &&
                         x.FBaja == null &&
                         x.AssetImages.Count > 0 && (
                         x.IsOnline == true || x.offlineView == true)).Select(i => new { i.telefono, i.Name }).ToList();
                        var telList = asset.Distinct();

                        foreach (var ass in telList)
                        {
                            string[] mensajes = { ass.telefono, "SensRoom: Hola " + ass.Name + " " + comment };
                           // mensaje.enviamsm(mensajes);
                        }
                    }
                    else if (group == 2)
                    {
                        var asset = db.Assets.Where(x => x.statusProfile == null && x.telefono != null).Select(x => x.telefono).ToList();
                        foreach (var tel in asset)
                        {
                            // string[] mensajes = { tel, comment };
                            // mensaje.enviamsm(mensajes);
                        }
                    }
                    else
                    {
                        //var asset = db.Assets.Where(x => x.telefono != null).Select(x => x.telefono).ToList();
                        //foreach (var tel in asset)
                        //{
                        //    string[] mensajes = { tel, comment };
                        //    mensaje.enviamsm(mensajes);
                        //}
                    }
                    return Json("Mensaje enviado, te contactaremos lo mas pronto posible", JsonRequestBehavior.AllowGet);
                }

                catch
                {
                    return Json("No fue posible enviar el mensaje, por favor manda tu duda a atencion@sens-room.com", JsonRequestBehavior.AllowGet);
                }

            }
        }
    }
}