﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Script.Serialization;
using ProjectX.ViewModels;
using ProjectX.API.Models;
using Microsoft.AspNet.Identity.Owin;
using System.Web.Security;


namespace ProjectX.API.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public UserManager<ApplicationUser> UserManager { get; private set; }
        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;


        public AccountController() : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)//, RoleManager<ApplicationUser> roleManager)
        {
            UserManager = userManager;
            UserManager.UserValidator = new UserValidator<ApplicationUser>(UserManager) { AllowOnlyAlphanumericUserNames = false };
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {

            if (!User.Identity.IsAuthenticated)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            else
            {
                //return RedirectToAction("Login", "Account");

            }
            return View();
        }

        [AllowAnonymous]
        public ActionResult TLogin(string returnUrl)
        {

            if (!User.Identity.IsAuthenticated)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }
            else
            {
                //return RedirectToAction("Login", "Account");

            }
            return View();
        }

        //
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(UserModel model, string returnUrl, string lat, string lon)
        {

            Session["lat"] = lat;
            Session["lon"] = lon;
            //TODO *ESTA CON SANA
            if (model == null || string.IsNullOrEmpty(model.UserName) || string.IsNullOrEmpty(model.Password))
            {
                return View(model);
            }

            var userLogin = UserManager.Find(model.UserName, model.Password);



            if (userLogin != null)
            {
                var u = Business.UserBC.UserExists(model.UserName);

                if (u.fBaja != null)
                {
                    ModelState.AddModelError("", "Usuario con baja en sistema.");
                    return View(model);
                }
                //Customer = 1,
                //Asset = 2,
                Session["type"] = u.AdminType;
                if (u.AdminType == 1)
                {
                    Business.UserBC.Contadordevisitas(u.Id);
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);                 
                    return RedirectToAction("Index", "Xlist");
                }
                else if (u.AdminType == 2)
                {

                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    return RedirectToAction("Index", "AssetW");
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                    return RedirectToAction("Admin1", "Admin1");
                }
            }
            else
            {
                ModelState.AddModelError("", "Usuario o contraseña inválidos.");
                return View(model);
            }
        }

        //private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        //{
        //    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
        //    var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        //    AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        //}

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        // POST: /Account/LogOff
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            //AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "HomePage");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }


        public ActionResult logUser(RegisterViewModel model)
        {
            var userLogin = UserManager.Find(model.UserName, model.Password);
            if (userLogin != null)
            {
                model.Id = userLogin.Id;
                model.IsActivated = true;
                model.UserId = userLogin.UserName;
                var u = Business.UserBC.UserExists(model.UserName);

                //Customer = 1,
                //Asset = 2,

                if (u.AdminType == 1)
                {
                    model.Sender = MessageSender.Customer;
                    model.UserVM = Business.CustomerBC.GetCustomerViewModel(model.Id);
                }
                else
                {
                    model.Sender = MessageSender.Asset;
                    model.AssetVM = Business.AssetBC.GetAsset(model.Id);
                    //ahora tmb incluimos la lista dde calendarios, desde el login
                    model.AssetVM.Calendars = Business.AssetBC.GetCalendar(-1, false, model.AssetVM.Id);
                    //Ahora, manejar el estado de los devicetokens
                    if (model.DeviceToken != null)
                    {
                        Business.AssetBC.UpdateDeviceToken(model.DeviceToken,
                            model.AssetVM.Id);
                    }


                }
            }

            return View(model);// userInfo);
        }

        public ActionResult enter(UserModel model)
        {
            var u = Business.UserBC.UserExists(model.UserName);

            if (u.AdminType == 1)
            {
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                return RedirectToAction("Index", "Xlist");
            }
            else
            {

                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                return RedirectToAction("Index", "AssetW");
            }
        }

        public ActionResult Info()
        {
            return View("Info");
        }
    }
}