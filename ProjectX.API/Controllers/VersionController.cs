﻿using System.Web.Http;
using ProjectX.ViewModels;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using ProjectX.API.Models;

namespace ProjectX.API.Controllers
{
    [RoutePrefix("api/version")]
    public class VersionController : ApiController
    {
        [AllowAnonymous]
        [HttpGet]
        [Route("getversion/{ver}")]
        public IHttpActionResult ValidaVersion(string ver)
        {
            return Ok(Business.VersionBC.GetVersion(ver));       

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getfiltropais/{clave}")]
        public IHttpActionResult ActiveFiltroPais(string clave)
        {
            return Ok(Business.VersionBC.GetFiltroPais(clave));
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("getleyenda/{id}")]
        public IHttpActionResult Leyenda(int id)
        {
            return Ok(Business.VersionBC.GetLeyenda(id));

        }

    }
}