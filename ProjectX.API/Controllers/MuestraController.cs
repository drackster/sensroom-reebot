﻿using ProjectX.API.Helpers;
using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class MuestraController : Controller
    {

        DateTime tiempoActual = DateTime.UtcNow;
        DateTime today = DateTime.UtcNow.AddHours(-24);
        int[] type = { 1, 7, 8, 9 };
        string uId = Helpers.Helper.GetId();
        int edad;

        Customer prefCus;
        // GET: Muestra
        public ActionResult Index()
        {
            try
            {
                List<Asset> asset = AssetList(0);


                if (Session["perfiles"] != null)
                {
                    var cont = Session["perfiles"] as List<PerfilAsset>;
                    if (cont.Count != asset.Count)
                    {
                        Session["perfiles"] = null;
                    }
                }
                if (Session["perfiles"] == null)
                {
                    List<PerfilAsset> perfiles = addPerfil(asset);
                    Session["deatil"] = null;
                    Session["perfiles"] = perfiles;
                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);
                    return View("Index", itemList.ToList());
                }
                else
                {
                    List<PerfilAsset> perfiles = Session["perfiles"] as List<PerfilAsset>;
                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);
                    return View("Index", itemList.ToList());
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }

        private List<Asset> AssetList(int opcion)
        {
            List<Asset> asset = new List<Asset>();
            List<Asset> asseton;

            Random rand = new Random();
            opcion = rand.Next(0, 3);



            using (var db = DBHelper.GetDatabase())
            {
                if (opcion == 0)
                {
                    asseton = db.Assets.Where(
                      x =>
                      x.Longitude != null &&
                      x.Latitude != null &&

                      x.statusProfile == 2 &&
                      x.FBaja == null &&
                      x.AssetImages.Count > 0 && (
                      x.IsOnline == true || x.offlineView == true)).OrderBy(x => x.Id).Take(20).ToList();
                    asset = asseton.ToList();
                }
                else if (opcion == 1)
                {
                    asseton = db.Assets.Where(
                     x =>
                     x.Longitude != null &&
                     x.Latitude != null &&

                     x.statusProfile == 2 &&
                     x.FBaja == null &&
                     x.AssetImages.Count > 0 && (
                     x.IsOnline == true || x.offlineView == true)).OrderByDescending(x => x.Id).Take(20).ToList();
                    asset = asseton.ToList();

                }

                else if (opcion == 2)
                {
                    asseton = db.Assets.Where(
                     x =>
                     x.Longitude != null &&
                     x.Latitude != null &&

                     x.statusProfile == 2 &&
                     x.FBaja == null &&
                     x.AssetImages.Count > 0 && (
                     x.IsOnline == true || x.offlineView == true)).OrderBy(x => x.NoVisitas).Take(20).ToList();
                    asset = asseton.ToList();

                }

                else
                {

                    asseton = db.Assets.Where(
                     x =>
                     x.Longitude != null &&
                     x.Latitude != null &&

                     x.statusProfile == 2 &&
                     x.FBaja == null &&
                     x.AssetImages.Count > 0 && (
                     x.IsOnline == true || x.offlineView == true)).OrderBy(x => x.LocationUpdate).Take(20).ToList();
                    asset = asseton.ToList();


                }


                return asset;
            }
        }

        private List<PerfilAsset> addPerfil(List<Asset> asset)
        {
            List<PerfilAsset> perfiles = new List<PerfilAsset>();
            foreach (var item in asset)
            {
                var dif = tiempoActual.Subtract(DateTime.Parse(item.DOB.ToString())).TotalDays;
                double años = dif / 365;
                PerfilAsset perfil = new PerfilAsset();
                perfil.ID = Int32.Parse(item.Id.ToString());
                perfil.Costo = 0;// item.precio;
                perfil.Descripcion = item.Description;
                perfil.Saludo = item.Title;
                perfil.Name = item.Name;
                perfil.Edad = años.ToString("0");
                //  perfil.Distancia = d;//.ToString("0.0") + " Km";
                // perfil.Pprecio = prefCus.Balance < 0 ? 100000 : prefCus.Balance;
                //  perfil.Pedad = 30;// prefCus.MaxAge;
                //perfil.Psexo = prefCus.CPrefId == null ? 3 : int.Parse(prefCus.CPrefId.ToString());
                perfil.ciudad = item.ciudad;
                perfil.estado = item.estado;
                perfil.calificacion = item.Calificacion == null ? 1 : (float)item.Calificacion;
                perfil.HoM = item.ASex == false ? "Mujer" : "Hombre";
                //  perfil.Offlineview = (bool)item.offlineView;
                perfil.Foto = addImgae(perfil.ID);
                perfiles.Add(perfil);

            }
            if (perfiles.Count > 0)
            {
                return perfiles;
            }
            else
            {
                using (var db = Helpers.DBHelper.GetDatabase())
                {
                    //  var assetDf = db.Assets.Where(x => x.precio < 2000 && x.statusProfile == 2).Take(10).ToList();
                    var assetDf = db.Assets.Where(x => x.Id == 2).ToList();
                    foreach (var item in assetDf)
                    {
                        PerfilAsset perfil = new PerfilAsset();
                        perfil.ID = Int32.Parse(item.Id.ToString());
                        perfil.Costo = decimal.Parse(item.precio.ToString());
                        perfil.Descripcion = item.Description;
                        perfil.Saludo = item.Title;
                        perfil.Name = item.Name;
                        perfil.Edad = "25";
                        //  perfil.Distancia = 40;//.ToString("0.0") + " Km";
                        perfil.Pprecio = prefCus.Balance < 0 ? 100000 : prefCus.Balance;
                        perfil.Pedad = prefCus.MaxAge;
                        perfil.Psexo = prefCus.CPrefId == null ? 3 : int.Parse(prefCus.CPrefId.ToString());
                        perfil.ciudad = item.ciudad;// "";// assetDf.ciudad;
                        perfil.estado = "";// assetDf.estado;
                        perfil.calificacion = item.Calificacion == null ? 1 : (float)item.Calificacion;
                        perfil.HoM = item.ASex == false ? "Mujer" : "Hombre";
                        perfil.Offlineview = true;
                        perfil.Foto = addImgae(perfil.ID);
                        perfiles.Add(perfil);
                    }

                }
                return perfiles;
            }

        }

        private string addImgae(int id)
        {
            using (var db = DBHelper.GetDatabase())
            {
                var ids = db.AssetImages.Where(x => x.UserId == id).Select(x => x.Id).ToList();
                Random random = new Random();
                int randomNumber = random.Next(0, ids.Count);
                string imid = ids[randomNumber];
                string Foto = db.AssetImages.Where(x => x.Id == imid).FirstOrDefault().urlImage;
                return Foto;
            }
        }

        public ActionResult TIndex()
        {
            try
            {
                List<Asset> asset = AssetList(0);

                if (Session["perfiles"] != null)
                {
                    var cont = Session["perfiles"] as List<PerfilAsset>;
                    if (cont.Count != asset.Count)
                    {
                        Session["perfiles"] = null;
                    }
                }
                if (Session["perfiles"] == null)
                {
                    List<PerfilAsset> perfiles = addPerfil(asset);
                    Session["deatil"] = null;
                    Session["perfiles"] = perfiles;
                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);
                    return View("TIndex", itemList.ToList());
                }
                else
                {
                    List<PerfilAsset> perfiles = Session["perfiles"] as List<PerfilAsset>;
                    var itemList = (from t in perfiles
                                    select t).OrderBy(x => x.Distancia);
                    return View("TIndex", itemList.ToList());
                }
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login", "Account");
            }
        }
    }
}