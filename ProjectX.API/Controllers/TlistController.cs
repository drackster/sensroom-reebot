﻿using ProjectX.API.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ProjectX.API.Controllers
{
    public class TlistController : Controller
    {

        string uId = Helper.GetId();
        // GET: Tlist
        [Authorize]
        public ActionResult Index()
        {
            Session["lat"] = string.Empty; Session["lon"] = string.Empty;
           // return RedirectToAction("Index","Xlist");
            return View("Index");
        }

        public ActionResult comenzar()
        {

            using (var db = DBHelper.GetDatabase())
            {
                var cus = db.Customers.Where(x => x.UserId == uId).FirstOrDefault();
                cus.urlReg = "tlist";
                db.SaveChanges();
            }

                return RedirectToAction("Index", "Xlist");
        }
    }
}