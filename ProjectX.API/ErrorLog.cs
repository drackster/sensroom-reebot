﻿using ProjectX.API.Helpers;
using ProjectX.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API
{
    public class ErrorLog
    {
        public void Error(string message, string method, string controller)
        {
            try
            {
                using (var db = DBHelper.GetDatabase())
                {
                    var error = db.ErrorLogs.Add(new Models.ErrorLog { Fecha = DateTime.Now, Controlador = controller, Metodo = method, Mensaje = message });
                    db.SaveChanges();
                }
            }
            catch { }                        
        }
    }
}