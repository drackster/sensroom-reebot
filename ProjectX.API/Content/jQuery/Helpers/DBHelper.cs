﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.API.Models;

namespace ProjectX.API.Helpers
{
    public class DBHelper
    {
        public static ProjectXEntities GetDatabase()
        {
            var db = new ProjectX.API.Models.ProjectXEntities();
            db.Configuration.LazyLoadingEnabled = true;
            return db;
        }
    }
}