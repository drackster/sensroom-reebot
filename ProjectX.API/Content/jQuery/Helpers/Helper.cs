﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Web;
using System.Security.Claims;
using System.IO;
using System.Text;
using ProjectX.API.Models;

namespace ProjectX.API.Helpers
{
    public class Helper
    {
        public static string GetHash(string input)
        {
            HashAlgorithm hashAlgorithm = new SHA256CryptoServiceProvider();

            byte[] byteValue = System.Text.Encoding.UTF8.GetBytes(input);

            byte[] byteHash = hashAlgorithm.ComputeHash(byteValue);

            return Convert.ToBase64String(byteHash);
        }
        public static string GetId()
        {
            var accountId = string.Empty;
            string email = ((System.Security.Claims.ClaimsIdentity)ClaimsPrincipal.Current.Identity).Name;
            using (var db = DBHelper.GetDatabase())
            {
                var u = db.AspNetUsers.Where(x => x.UserName == email).FirstOrDefault();
                if (u != null)
                    accountId = u.Id; 
            }
            return accountId;
            //return Microsoft.AspNet.Identity.IdentityExtensions.GetUserId(ClaimsPrincipal.Current.Identity);
        }
        public static AspNetUser GetAspnetUser()
        {
            var accountId = string.Empty;
            string email = ((System.Security.Claims.ClaimsIdentity)ClaimsPrincipal.Current.Identity).Name;
            using (var db = DBHelper.GetDatabase())
            {
                return db.AspNetUsers.Where(x => x.UserName == email).FirstOrDefault();
            }
        }
        public static string GetEmail()
        {
            return ((System.Security.Claims.ClaimsIdentity)ClaimsPrincipal.Current.Identity).Name;
        }
        public static string GetId(string email)
        {
            string Id = string.Empty;
            using (var db = DBHelper.GetDatabase())
            {
                var u = db.AspNetUsers.Where(x => x.UserName == email).FirstOrDefault();
                if (u != null)
                    Id = u.AXAccountId;
            }
            return Id;
        }
        public static string ToTitleCase(string stringToTitle)
        {
            if (string.IsNullOrEmpty(stringToTitle)) return string.Empty;
            return System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(stringToTitle.ToLower());
        }

        public static long GetCustomerId()
        {
            var cId = GetId();
            using (var db = DBHelper.GetDatabase())
            {
                return db.Customers.Where(x => x.UserId == cId).FirstOrDefault().Id;
            }
        }
        public static Customer GetCustomer()
        {
            var cId = GetId();
            using (var db = DBHelper.GetDatabase())
            {
                return db.Customers.Include("AspNetUser").Where(x => x.UserId == cId).FirstOrDefault();
            }
        }
        /// <summary>
        /// Writes the given object instance to a binary file.
        /// <para>Object type (and all child types) must be decorated with the [Serializable] attribute.</para>
        /// <para>To prevent a variable from being serialized, decorate it with the [NonSerialized] attribute; cannot be applied to properties.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the XML file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the XML file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToBinaryFile<T>(string filePath, T objectToWrite, bool append = false)
        {
            using (Stream stream = File.Open(filePath, append ? FileMode.Append : FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, objectToWrite);
            }
        }

        /// <summary>
        /// Reads an object instance from a binary file.
        /// </summary>
        /// <typeparam name="T">The type of object to read from the XML.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the binary file.</returns>
        public static T ReadFromBinaryFile<T>(string filePath)
        {
            using (Stream stream = File.Open(filePath, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (T)binaryFormatter.Deserialize(stream);
            }
        }



        private static string AesKeyMaterial = "VAkMUiju2rHiRyhJKfo/Jg==";
        private static byte[] iv = Convert.FromBase64String("vaCDYoG9G+4xr15Am15N+w==");

        public static string Encrypting(string text)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = iv;
            aes.Key = Convert.FromBase64String(AesKeyMaterial);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            byte[] src = Encoding.UTF8.GetBytes(text);

            // encryption
            using (ICryptoTransform encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                return Convert.ToBase64String(dest);
            }
        }

        public static string DesEncrypting(string text)
        {
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = iv;
            aes.Key = Convert.FromBase64String(AesKeyMaterial);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            //byte[] src = Encoding.UTF8.GetBytes(text);
            byte[] src = System.Convert.FromBase64String(text);
            // encryption
            using (ICryptoTransform Desencrypt = aes.CreateDecryptor())
            {
                byte[] dest = Desencrypt.TransformFinalBlock(src, 0, src.Length);

                return Encoding.UTF8.GetString(dest);
            }
        }

        public static DateTime AddHours(DateTime EndDate)
        {
            return new DateTime(EndDate.Year, EndDate.Month, EndDate.Day, 23, 59, 59);
        }
    }
}