﻿

$("#acepto-terminos").click(function () {
    if ($(this).is(':checked')) {

        $(this).attr('value', 'true');
    } else {
        $(this).attr('value', 'false');
    }
});



$("#divCodigo").css("display", "none");
$("#txtcodigo").val(null)

$(document).ready(function () {
    $("#txtAlias").change(function () {
        $.ajax({
            type: "GET",
            url: '/CRegistro/ValidaAlisB',
            data: { alias: $('#txtAlias').val() },
            success: function (data) {
                console.log("alias ", data)
                if (data == "True") {
                    $("#mensaje").hide();
                    $("#mensaje2").hide();
                    
                } else {
                    $("#mensaje").html("Alias no disponible");
                    $("#mensaje2").hide();
                    $("#txtAlias").val("");
      
                    
                    $('#acepto-terminos').prop("checked", false);
                    $('#chkcode').prop("checked", false);
                    //agrgar tache y limpiar el campo
                }
            },
        });
    });


    $("#txtcodigo").change(function () {
        $.ajax({
            type: "GET",
            url: '/CRegistro/ValidaCodigo',
            data: { codigo: $('#txtcodigo').val() },

            success: function (data) {

                if (data == 1) {
                    $("#hftype").val(data);
                    $("#msmVar").html("Codigo para obtner una membresia SensRoom con 50% de descuento");
                    $("#divtdc").show();

                } else if (data == 2) {
                    $("#hftype").val(data);
                    $("#msmVar").html("Codigo para ser socio SensRoom");

                    //agrgar tache y limpiar el campo
                }
                else {
                    $("#hftype").val(null);
                    $("#msmVar").html("error en código");
                    $("#acepto-terminos").removeAttr("checked");
                    $("#chkcode").removeAttr("checked");
                }
            },
        });
    });

    $("#chkcode").click(function () {
       
        if ($(this).is(':checked')) {

            $("#divCodigo").css("display", "block");
            $(this).attr('value', 'true');

        }
        else {
            $("#divCodigo").css("display", "none");
            $("#txtcodigo").val(null)
            $(this).attr('value', 'false');

        }

    

        //if ($("#divCodigo").css('display') == 'none') {

        //    $("#divCodigo").css("display", "show");
        //}
        //else {
        //    $("#divCodigo").css("display", "none");
        //    $("#txtcodigo").val(null)
        //}
    });
});