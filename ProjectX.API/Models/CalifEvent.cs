﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class CalifEvent
    {
        public string AssetName { get; set; }
        public long AssetId { get; set; }
        public DateTime Date { get; set; }
        public long OrderId { get; set; }
        public string CustomerName { get; set; }
        public long CustomerId { get; set; }

    }
}