﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class InvDetail
    {
        [Required]
        [Display(Name = "Calificación")]
        public float Calificación { get; set; }

        [Required]
        [Display(Name = "OrderId")]
        public long OrderId { get; set; }

        [Required]
        [Display(Name = "OrderStatus")]
        public int OrderStatus { get; set; }

        [Required]
        [Display(Name = "Total")]
        public float Total { get; set; }

        [Required]
        [Display(Name = "Anticipo")]
        public float Anticipo { get; set; }

        [Required]
        [Display(Name = "Pendiente")]
        public float Pendiente { get; set; }

        [Required]
        [Display(Name = "CusName")]
        public string CusName { get; set; }

        [Required]
        [Display(Name = "Distancia")]
        public string Distancia { get; set; }

        [Required]
        [Display(Name = "Ciudad")]
        public string Ciudad { get; set; }
    }
}