//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectX.API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class CustomerName
    {
        public long id { get; set; }
        public string UserId { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public string UserName { get; set; }
        public Nullable<double> Calificacion { get; set; }
        public string Ciudad { get; set; }
        public Nullable<int> NoVisitas { get; set; }
    }
}
