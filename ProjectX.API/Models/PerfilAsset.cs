﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class PerfilAsset
    {
        [Required]
        [Display(Name = "Saludo")]
        public string Saludo { get; set; }

        [Required]
        [Display(Name = "Descripcion")]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name = "HoM")]
        public string HoM { get; set; }

        [Required(ErrorMessage = "Costo cost is required")]
        [Display(Name = "Costo")]
        public decimal Costo { get; set; }

        [Required]
        [Display(Name = "Edad")]
        public string Edad { get; set; }

        [Required]
        [Display(Name = "Foto")]
        public string Foto { get; set; }

        [Required]
        [Display(Name = "Foto2")]
        public string Foto2 { get; set; }

        [Display(Name = "ID")]
        public Int32 ID { get; set; }

        [Required]
        [Display(Name = "Error")]
        public string Error { get; set; }

        [Required]
        [Display(Name = "Estatus")]
        public int Estatus { get; set; }

        [Required]
        [Display(Name = "online")]
        public bool online { get; set; }

        [Required]
        [Display(Name = "ASex")]
        public bool? ASex { get; set; }

        [Required]
        [Display(Name = "Prefid")]
        public int? Prefid { get; set; }

        [Required]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "DOB")]
        public DateTime DOB { get; set; }

        [Display(Name = "CLABE")]
        public string CLABE { get; set; }

        [Display(Name = "redsocial")]
        public string redsocial { get; set; }

        [Display(Name = "Latitud")]
        public double? Latitud { get; set; }

        [Display(Name = "Longitud")]
        public double? Longitud { get; set; }

        [Display(Name = "Distancia")]
        public double Distancia { get; set; }

        [Required]
        [Display(Name = "fotos1")]
        public List<AssetImage> fotos1 {get; set;}


        [Display(Name = "Psexo")]
        public int Psexo { get; set; }

        [Display(Name = "Pedad")]
        public int Pedad { get; set; }


        [Display(Name = "precio")]
        public decimal Pprecio { get; set; }

        [Display(Name = "visitas")]
        public int visitas { get; set; }

        [Required]
        [Display(Name = "ciudad")]
        public string ciudad { get; set; }

        [Required]
        [Display(Name = "estado")]
        public string estado { get; set; }

        [Required]
        [Display(Name = "calificacion")]
        public float calificacion { get; set; }

        [Required]
        [Display(Name = "updateConexion")]
        public DateTime updateConexion { get; set; }

        [Required]
        [Display(Name = "Offlineview")]
        public bool Offlineview { get; set; }

        [Required]
        [Display(Name = "totalAsset")]
        public int totalAsset { get; set; }

        [Required]
        [RegularExpression(reg_expressions.regEx_email, ErrorMessage = "Caracteres invalidos.")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [RegularExpression(reg_expressions.regEx_email, ErrorMessage = "Caracteres invalidos.")]
        [Display(Name = "Disponibilidad")]
        public string Disponibilidad { get; set; }
    }
    public class reg_expressions
    {
        public const string regEx_texto = @"^[a-zA-Z0-9 .-ñáéíóú:#(),]+$";
        public const string regEx_numeric = @"^[0-9]+(\.[0-9]{1,2})?$";
        public const string regEx_email = @"^.+@.+\..+$";
    }
}