﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class RepClientes
    {
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }
        public string Nombre { get; set; }
        public string Padre { get; set; }
        public DateTime Alta { get; set; }
    }
}