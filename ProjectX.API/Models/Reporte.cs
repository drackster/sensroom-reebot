﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class Reporte
    {
        public DateTime FechaInicial { get; set; }
        public DateTime FechaFinal { get; set; }
        public int OrderId { get; set; }
        public int AssetId { get; set; }
        public string AssetName { get; set; }
        public string AssetNameP { get; set; }
        public int CustomerId { get; set; }
        public string CusName { get; set; }
        public string CusNameP { get; set; }
        public float Amount { get; set; }
        public float Comision { get; set; }
        public int MethodPay { get; set; }
    }
}