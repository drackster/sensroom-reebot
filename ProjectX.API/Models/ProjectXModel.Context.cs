﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectX.API.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class ProjectXEntities : DbContext
    {
        public ProjectXEntities()
            : base("name=ProjectXEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<ABank> ABanks { get; set; }
        public virtual DbSet<ACalendar> ACalendars { get; set; }
        public virtual DbSet<ACalendarDetail> ACalendarDetails { get; set; }
        public virtual DbSet<ACalendarTime> ACalendarTimes { get; set; }
        public virtual DbSet<ACategory> ACategories { get; set; }
        public virtual DbSet<ACategoryTier> ACategoryTiers { get; set; }
        public virtual DbSet<Action> Actions { get; set; }
        public virtual DbSet<ADistance> ADistances { get; set; }
        public virtual DbSet<AHour> AHours { get; set; }
        public virtual DbSet<APref> APrefs { get; set; }
        public virtual DbSet<AService> AServices { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<Asset> Assets { get; set; }
        public virtual DbSet<AssetImage> AssetImages { get; set; }
        public virtual DbSet<AssetReview> AssetReviews { get; set; }
        public virtual DbSet<AssetService> AssetServices { get; set; }
        public virtual DbSet<ATransaction> ATransactions { get; set; }
        public virtual DbSet<CAddress> CAddresses { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<conektaLog> conektaLogs { get; set; }
        public virtual DbSet<CTransaction> CTransactions { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }
        public virtual DbSet<CustomerPaymentMethod> CustomerPaymentMethods { get; set; }
        public virtual DbSet<CustomerReview> CustomerReviews { get; set; }
        public virtual DbSet<DeviceToken> DeviceTokens { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<Evento> Eventos { get; set; }
        public virtual DbSet<LoginUserLog> LoginUserLogs { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderLog> OrderLogs { get; set; }
        public virtual DbSet<orderSB> orderSBs { get; set; }
        public virtual DbSet<ProductActivityLog> ProductActivityLogs { get; set; }
        public virtual DbSet<Receipt> Receipts { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<SearchActivityLog> SearchActivityLogs { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
        public virtual DbSet<tComentario> tComentarios { get; set; }
        public virtual DbSet<tComment> tComments { get; set; }
        public virtual DbSet<tCredito> tCreditos { get; set; }
        public virtual DbSet<tDatosTDCConekta> tDatosTDCConektas { get; set; }
        public virtual DbSet<tModalPopUp> tModalPopUps { get; set; }
        public virtual DbSet<UserActionLog> UserActionLogs { get; set; }
        public virtual DbSet<UserFavourite> UserFavourites { get; set; }
        public virtual DbSet<UserRequestLog> UserRequestLogs { get; set; }
        public virtual DbSet<VersionControlLuxxApp> VersionControlLuxxApps { get; set; }
        public virtual DbSet<XConfig> XConfigs { get; set; }
        public virtual DbSet<AssetRespaldo> AssetRespaldoes { get; set; }
        public virtual DbSet<Invitacione> Invitaciones { get; set; }
        public virtual DbSet<tError> tErrors { get; set; }
        public virtual DbSet<tLeyenda> tLeyendas { get; set; }
        public virtual DbSet<tMediosDePago> tMediosDePagoes { get; set; }
        public virtual DbSet<tRestriccionPai> tRestriccionPais { get; set; }
        public virtual DbSet<tTyC> tTyCs { get; set; }
        public virtual DbSet<CustomerName> CustomerNames { get; set; }
        public virtual DbSet<NotificationAsset> NotificationAssets { get; set; }
        public virtual DbSet<Padre1> Padre1 { get; set; }
        public virtual DbSet<Qi> Qis { get; set; }
        public virtual DbSet<RepF> RepFs { get; set; }
        public virtual DbSet<reporte1> reporte1 { get; set; }
    
        public virtual int sp_alterdiagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_alterdiagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_creatediagram(string diagramname, Nullable<int> owner_id, Nullable<int> version, byte[] definition)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var versionParameter = version.HasValue ?
                new ObjectParameter("version", version) :
                new ObjectParameter("version", typeof(int));
    
            var definitionParameter = definition != null ?
                new ObjectParameter("definition", definition) :
                new ObjectParameter("definition", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_creatediagram", diagramnameParameter, owner_idParameter, versionParameter, definitionParameter);
        }
    
        public virtual int sp_dropdiagram(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_dropdiagram", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagramdefinition_Result> sp_helpdiagramdefinition(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagramdefinition_Result>("sp_helpdiagramdefinition", diagramnameParameter, owner_idParameter);
        }
    
        public virtual ObjectResult<sp_helpdiagrams_Result> sp_helpdiagrams(string diagramname, Nullable<int> owner_id)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_helpdiagrams_Result>("sp_helpdiagrams", diagramnameParameter, owner_idParameter);
        }
    
        public virtual int sp_renamediagram(string diagramname, Nullable<int> owner_id, string new_diagramname)
        {
            var diagramnameParameter = diagramname != null ?
                new ObjectParameter("diagramname", diagramname) :
                new ObjectParameter("diagramname", typeof(string));
    
            var owner_idParameter = owner_id.HasValue ?
                new ObjectParameter("owner_id", owner_id) :
                new ObjectParameter("owner_id", typeof(int));
    
            var new_diagramnameParameter = new_diagramname != null ?
                new ObjectParameter("new_diagramname", new_diagramname) :
                new ObjectParameter("new_diagramname", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_renamediagram", diagramnameParameter, owner_idParameter, new_diagramnameParameter);
        }
    
        public virtual int sp_upgraddiagrams()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_upgraddiagrams");
        }
    
        public virtual int sp_ActualizaContraseña(string name)
        {
            var nameParameter = name != null ?
                new ObjectParameter("name", name) :
                new ObjectParameter("name", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_ActualizaContraseña", nameParameter);
        }
    
        public virtual ObjectResult<sp_AssetOnline_Result> sp_AssetOnline()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_AssetOnline_Result>("sp_AssetOnline");
        }
    
        public virtual ObjectResult<byte[]> sp_GetPhotoPerfil(Nullable<int> id)
        {
            var idParameter = id.HasValue ?
                new ObjectParameter("id", id) :
                new ObjectParameter("id", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<byte[]>("sp_GetPhotoPerfil", idParameter);
        }
    
        public virtual ObjectResult<sp_Repf_Result> sp_Repf(Nullable<System.DateTime> fechaI, Nullable<System.DateTime> fechaF)
        {
            var fechaIParameter = fechaI.HasValue ?
                new ObjectParameter("fechaI", fechaI) :
                new ObjectParameter("fechaI", typeof(System.DateTime));
    
            var fechaFParameter = fechaF.HasValue ?
                new ObjectParameter("fechaF", fechaF) :
                new ObjectParameter("fechaF", typeof(System.DateTime));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Repf_Result>("sp_Repf", fechaIParameter, fechaFParameter);
        }
    }
}
