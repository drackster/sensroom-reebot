﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class Registro
    {

        [Required]
        [Display(Name = "Type")]
        public int? Type { get; set; }

        [Required]
        [Display(Name = "Codigo")]
        public string Codigo { get; set; }

        [Required]
        [Display(Name = "Alias")]
        public string Alias { get; set; }

        [Required]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }

        [Required]
        [Display(Name = "Pass1")]
        public string Pass1 { get; set; }

        [Required]
        [Display(Name = "Pass2")]
        public string Pass2 { get; set; }

       
        [Display(Name = "Error")]
        public string Error { get; set; }

        [Display(Name = "Tyc")]
        public bool Tyc { get; set; }

        [Display(Name = "Ps")]
        public bool Ps { get; set; }
    }
}