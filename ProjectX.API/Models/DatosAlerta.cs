﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class DatosAlerta
    {
        [Required]
        [Display(Name = "Numero")]
        public string Numero { get; set; }

        [Required]
        [Display(Name = "RemitSms")]
        public string RemitSms { get; set; }
    }
}