//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectX.API.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            this.AssetReviews = new HashSet<AssetReview>();
            this.ATransactions = new HashSet<ATransaction>();
            this.CTransactions = new HashSet<CTransaction>();
            this.CustomerReviews = new HashSet<CustomerReview>();
            this.Notifications = new HashSet<Notification>();
            this.Receipts = new HashSet<Receipt>();
        }
    
        public long Id { get; set; }
        public long AssetId { get; set; }
        public long CustomerId { get; set; }
        public double CLatitude { get; set; }
        public double CLongitude { get; set; }
        public double ALatitude { get; set; }
        public double ALongitude { get; set; }
        public int RequestType { get; set; }
        public Nullable<System.DateTime> RequestedDateTime { get; set; }
        public double StartingDistance { get; set; }
        public int OrderStatus { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<System.DateTime> ExpiresOn { get; set; }
        public Nullable<System.DateTime> Arrival { get; set; }
        public Nullable<System.DateTime> ConfirmedOn { get; set; }
        public int CategoryTierId { get; set; }
        public decimal TotalCost { get; set; }
        public Nullable<decimal> DownPayment { get; set; }
        public Nullable<long> AdressId { get; set; }
        public Nullable<long> CustomerPaymentId { get; set; }
        public string ChargeId { get; set; }
        public int PaymentMethod { get; set; }
        public string PaypalTransId { get; set; }
        public Nullable<System.DateTime> AReviewedOn { get; set; }
        public Nullable<System.DateTime> CReviewedOn { get; set; }
        public Nullable<System.DateTime> ADeclinedOn { get; set; }
        public Nullable<System.DateTime> CDeclinedOn { get; set; }
        public Nullable<System.DateTime> CompletedOn { get; set; }
        public Nullable<decimal> CashRecollected { get; set; }
        public System.DateTime ServiceEnd { get; set; }
        public int ComisionStatus { get; set; }
        public int DepositStatus { get; set; }
        public Nullable<long> OxxoRef { get; set; }
        public Nullable<double> Calificacion { get; set; }
        public Nullable<double> CalificacionC { get; set; }
        public Nullable<int> ModeType { get; set; }
        public string ciudad { get; set; }
    
        public virtual Asset Asset { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AssetReview> AssetReviews { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ATransaction> ATransactions { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CTransaction> CTransactions { get; set; }
        public virtual Customer Customer { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CustomerReview> CustomerReviews { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Notification> Notifications { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Receipt> Receipts { get; set; }
    }
}
