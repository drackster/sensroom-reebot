﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ProjectX.ViewModels;
using ProjectX.API.Models;

namespace ProjectX.API.Models
{
    public class PushNotificationModel
    {
        public long NotificationId { get; set; }
        public DeviceToken Token { get; set; }
        public string Title { get; set; }
        public string Message { get; set; }
        public NotificationStatus NStatus { get; set; }
    }
}