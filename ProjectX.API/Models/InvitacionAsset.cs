﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class InvitacionAsset
    {
        [Required]
        [Display(Name = "Nombre")]
        public string Nombre { get; set; }

        [Required]
        [Display(Name = "HoraFecha")]
        public string HoraFecha { get; set; }

        [Required]
        [Display(Name = "Precio")]
        public decimal Precio { get; set; }

        [Required]
        [Display(Name = "Anticipo")]
        public decimal Anticipo { get; set; }

        [Required]
        [Display(Name = "Pendiente")]
        public decimal Pendiente { get; set; }

        [Required]
        [Display(Name = "Ubicacion")]
        public string Ubicacion { get; set; }

        [Required]
        [Display(Name = "Foto")]
        public AssetImage Foto { get; set; }

        [Required]
        [Display(Name = "Credito")]
        public bool Credito { get; set; }

        [Required]
        [Display(Name = "Error")]
        public string Error { get; set; }

        [Required]
        [Display(Name = "offlineView")]
        public bool offlineView { get; set; }
    }
}