﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class ConfigModel
    {
        public int Id { get; set; }

        //Catalogo y Sincronizacion
        
        [Display(Name = "Última Actualización")]
        public DateTime LastUpdate { get; set; }

        [Display(Name = "Estatus Sincronización AX")]
        public bool CatalogJobBussy { get; set; }

        [Required]
        [Display(Name = "Número de Artículos en Caché")]
        public int ProductCount { get; set; }

        [Required]
        [Display(Name = "Número de Artículos en Caché")]
        public int PageSize { get; set; }

        [Required]
        [Display(Name = "Número de Artículos en Caché")]
        public int PageCount { get; set; }

        [Required]
        [Display(Name = "Tiempo de Sincronización Catálogo")]
        public int CatalogOffSetMinutes { get; set; }
    
        public string JobStatusMsg
        {
            get
            {
                if (CatalogJobBussy) return "Sincronizando...";
                if (LastUpdate == null || LastUpdate.Year < 2017) return "Error!";
                return LastUpdate.AddMinutes(CatalogOffSetMinutes).ToString();
            }
        }

        //App Stores
        [Required]
        [Display(Name = "Versión Mínima Requerida iOS")]
        public decimal iOSMinVersion { get; set; }

        [Required]
        [Display(Name = "iOS AppStore Url")]
        public string iOSStoreUrl { get; set; }

        [Required]
        [Display(Name = "Versión Mínima Requerida Android")]
        public decimal DroidMinVersion { get; set; }

        [Required]
        [Display(Name = "Android PlayStore Url")]
        public string DroidStoreUrl { get; set; }

        //Misc

        [Required]
        [Display(Name = "Número de árticulos en lista infinita")]
        public int ListNumRecords { get; set; }

        [Required]
        [Display(Name = "Número de días de entrega")]
        public int DeliveryNumDays { get; set; }


        [Required]
        [Display(Name = "Tiempo de Refresh")]
        public int RefreshCount { get; set; }

        [Required]
        [Display(Name = "Tiempo Envío Emails")]
        public int EmailJobSeconds { get; set; }

        [Required]
        [Display(Name = "Tiempo de Expiración Carrito")]
        public int CartExpireHours { get; set; }

        [Required]
        [Display(Name = "Porcentaje de IVA")]
        public double TaxAmount { get; set; }

        public string SanaTestResult { get; set; }

        public string AXTestResult { get; set; }

    }


}