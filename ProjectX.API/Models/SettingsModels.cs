﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class SettingsModels
    {
        [Required]
        [Display(Name = "ASex")]
        public bool? ASex { get; set; }

        [Required]
        [Display(Name = "Prefid")]
        public int? Prefid { get; set; }

        [Required]
        [Display(Name = "Costo")]
        public decimal? Costo { get; set; }

        
        [Display(Name = "Distancia")]
        public int? Distancia { get; set; }

        [Required]
        [Display(Name = "Telefono")]
        public string Telefono { get; set; }
    }
}