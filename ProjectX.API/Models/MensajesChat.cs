﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class MensajesChat
    {
        public string mensaje { get; set; }
        public int type { get; set; }
        public DateTime fecha { get; set; }
    }
}