﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class Mensajes
    {
        [Required]
        [Display(Name = "id")]
        public int id { get; set; }



        [Required]
        [Display(Name = "AssetId")]
        public int AssetId { get; set; }

        [Required]
        [Display(Name = "CustomerId")]
        public long? CustomerId { get; set; }

        [Required]
        [Display(Name = "OrderID")]
        public long? OrderID { get; set; }

        [Required]
        [Display(Name = "CustomerName")]
        public string CustomerName { get; set; }

        [Required]
        [Display(Name = "AssetName")]
        public string AssetName { get; set; }

        [Required]
        [Display(Name = "Mensaje")]
        public string Mensaje { get; set; }

        [Required]
        [Display(Name = "Fecha")]
        public DateTime Fecha { get; set; }

        [Required]
        [Display(Name = "Tiempo")]
        public string Tiempo { get; set; }

        [Required]
        [Display(Name = "MType")] //tipo de mensaje: chat, invitacion, cancelación, rechazo, etc...
        public int MType { get; set; }

        [Required]
        [Display(Name = "MReceiver")]//tipo de ereceptpr, asset, customer, ...
        public int MReceiver { get; set; }

        [Required]
        [Display(Name = "MessageStatus")]//0 creado, 1 enviado, 2 recibido, 3 leido, ...
        public int MessageStatus { get; set; }


    }
}