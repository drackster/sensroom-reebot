﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ProjectX.API.Models
{
    public class Updatepass
    {
        [Required]
        [Display(Name = "Usuario")]
        public string UserName { get; set; }

        [Required]
        [Display(Name = "userId")]
        public string userId { get; set; }

        [Required]
        [Display(Name = "telefono")]
        public string telefono { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "La {0} debe de ser de mínimo {2} caracteres.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirmar Contraseña")]
        [Compare("Password", ErrorMessage = "Las contraseñas no concuerdan")]
        public string ConfirmPassword { get; set; }
    }
}