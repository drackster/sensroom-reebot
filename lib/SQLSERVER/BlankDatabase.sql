
/****** Object:  Table [dbo].[ABank]    Script Date: 22/06/2018 12:52:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ABank](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IndexNum] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
 CONSTRAINT [PK_ABank] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACalendar]    Script Date: 22/06/2018 12:52:40 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACalendar](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Name] [varchar](15) NOT NULL,
	[Latitude] [float] NOT NULL,
	[Longitude] [float] NOT NULL,
	[From] [bigint] NOT NULL,
	[To] [bigint] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_ACalendar] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACalendarDetail]    Script Date: 22/06/2018 12:52:41 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACalendarDetail](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CalendarId] [bigint] NOT NULL,
	[DayNum] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ACalendarDetail] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACalendarTimes]    Script Date: 22/06/2018 12:52:42 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACalendarTimes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Minutes] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IndexNum] [int] NOT NULL,
 CONSTRAINT [PK_ACalendarTimes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACategory]    Script Date: 22/06/2018 12:52:43 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACategory](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](15) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IndexNum] [int] NOT NULL,
	[cost1] [int] NULL,
	[cost2] [int] NULL,
	[cost3] [int] NULL,
 CONSTRAINT [PK_ACategory] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ACategoryTier]    Script Date: 22/06/2018 12:52:44 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ACategoryTier](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CategoryId] [int] NOT NULL,
	[Name] [varchar](18) NOT NULL,
	[Description] [varchar](255) NULL,
	[Cost] [money] NOT NULL,
	[IndexNum] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[NumHours] [int] NOT NULL,
 CONSTRAINT [PK_ACategoryTier] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Actions]    Script Date: 22/06/2018 12:52:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Actions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Actions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ADistance]    Script Date: 22/06/2018 12:52:45 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ADistance](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](10) NOT NULL,
	[Km] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IndexNum] [int] NOT NULL,
 CONSTRAINT [PK_ADistance] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AHours]    Script Date: 22/06/2018 12:53:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AHours](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Hours] [int] NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IndexNum] [int] NOT NULL,
 CONSTRAINT [PK_AHours] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[APref]    Script Date: 22/06/2018 12:53:07 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[APref](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[AIndexNum] [int] NOT NULL,
	[CIndexNum] [int] NOT NULL,
 CONSTRAINT [PK_APref] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AService]    Script Date: 22/06/2018 12:53:08 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AService](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[IsEnabled] [bit] NOT NULL,
	[IndexNum] [int] NOT NULL,
 CONSTRAINT [PK_AService] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 22/06/2018 12:53:09 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 22/06/2018 12:53:10 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 22/06/2018 12:53:11 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC,
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 22/06/2018 12:53:12 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 22/06/2018 12:53:13 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[Email] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEndDateUtc] [datetime] NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
	[AXAccountId] [varchar](10) NULL,
	[AdminType] [int] NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Asset]    Script Date: 22/06/2018 12:53:14 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Asset](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Name] [varchar](18) NOT NULL,
	[CategoryId] [int] NULL,
	[Title] [varchar](35) NULL,
	[Description] [varchar](500) NULL,
	[Height] [int] NULL,
	[Waist] [int] NULL,
	[BraSize] [int] NULL,
	[BraCup] [varchar](5) NULL,
	[SkinColor] [int] NULL,
	[EyeColor] [int] NULL,
	[HairColor] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[MaxDistance] [int] NULL,
	[AccountNum] [varchar](16) NULL,
	[BankHolderName] [varchar](120) NULL,
	[BankId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[LocationUpdate] [datetime] NULL,
	[ViewCount] [int] NOT NULL,
	[CalendarActive] [bit] NOT NULL,
	[IsOnline] [bit] NOT NULL,
	[DOB] [datetime] NULL,
	[APrefId] [int] NULL,
	[AccountNum2] [varchar](18) NULL,
	[ASex] [bit] NULL,
	[OnlineExpires] [datetime] NULL,
	[Balance] [money] NULL,
	[precio] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Asset] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssetImage]    Script Date: 22/06/2018 12:53:15 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetImage](
	[UserId] [bigint] NOT NULL,
	[ImageStream] [image] NOT NULL,
	[IndexNum] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[Id] [nvarchar](128) NOT NULL,
	[IsProfile] [bit] NOT NULL,
 CONSTRAINT [PK_AssetImage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssetRespaldo]    Script Date: 22/06/2018 12:53:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetRespaldo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[Name] [varchar](18) NOT NULL,
	[CategoryId] [int] NULL,
	[Title] [varchar](35) NULL,
	[Description] [varchar](500) NULL,
	[Height] [int] NULL,
	[Waist] [int] NULL,
	[BraSize] [int] NULL,
	[BraCup] [varchar](5) NULL,
	[SkinColor] [int] NULL,
	[EyeColor] [int] NULL,
	[HairColor] [int] NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[MaxDistance] [int] NULL,
	[AccountNum] [varchar](16) NULL,
	[BankHolderName] [varchar](120) NULL,
	[BankId] [int] NULL,
	[IsActive] [bit] NOT NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[LocationUpdate] [datetime] NULL,
	[ViewCount] [int] NOT NULL,
	[CalendarActive] [bit] NOT NULL,
	[IsOnline] [bit] NOT NULL,
	[DOB] [datetime] NULL,
	[APrefId] [int] NULL,
	[AccountNum2] [varchar](18) NULL,
	[ASex] [bit] NULL,
	[OnlineExpires] [datetime] NULL,
	[Balance] [money] NULL,
	[precio] [decimal](18, 0) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssetReview]    Script Date: 22/06/2018 12:53:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetReview](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetId] [bigint] NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[Rating] [float] NOT NULL,
	[Review] [varchar](500) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[OrderId] [bigint] NOT NULL,
 CONSTRAINT [PK_AssetReview] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssetService]    Script Date: 22/06/2018 12:53:16 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssetService](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[ServiceId] [int] NOT NULL,
 CONSTRAINT [PK_AssetService] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ATransaction]    Script Date: 22/06/2018 12:53:18 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ATransaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetId] [bigint] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_ATransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CAddress]    Script Date: 22/06/2018 12:53:18 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CAddress](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[Address1] [varchar](255) NOT NULL,
	[Address2] [varchar](255) NULL,
	[Address3] [varchar](255) NULL,
	[Comments] [varchar](255) NULL,
 CONSTRAINT [PK_CAddress] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clients]    Script Date: 22/06/2018 12:53:19 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clients](
	[Id] [nvarchar](128) NOT NULL,
	[Secret] [nvarchar](max) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[ApplicationType] [int] NOT NULL,
	[Active] [bit] NOT NULL,
	[RefreshTokenLifeTime] [int] NOT NULL,
	[AllowedOrigin] [nvarchar](100) NULL,
 CONSTRAINT [PK_dbo.Clients] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CTransaction]    Script Date: 22/06/2018 12:53:19 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CTransaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[Amount] [money] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_CTransaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 22/06/2018 12:53:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[Latitude] [float] NULL,
	[Longitude] [float] NULL,
	[LocationUpdate] [datetime] NULL,
	[ACategoryId] [int] NULL,
	[MinAge] [int] NOT NULL,
	[MaxAge] [int] NOT NULL,
	[MinDistance] [int] NOT NULL,
	[MaxDistance] [int] NOT NULL,
	[CPrefId] [int] NULL,
	[ProviderId] [nvarchar](50) NULL,
	[Balance] [money] NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerPaymentMethod]    Script Date: 22/06/2018 12:53:20 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerPaymentMethod](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[ProviderId] [nvarchar](48) NOT NULL,
	[CreditCardNumber] [nvarchar](4) NOT NULL,
	[NameOnCard] [nvarchar](48) NOT NULL,
	[ExpirationDate] [datetime] NOT NULL,
	[CVC] [nvarchar](4) NOT NULL,
	[CreditCardType] [nvarchar](4) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_CustomerPaymentMethod] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CustomerReview]    Script Date: 22/06/2018 12:53:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CustomerReview](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetId] [bigint] NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[Rating] [float] NOT NULL,
	[Review] [varchar](500) NULL,
	[CreatedOn] [datetime] NOT NULL,
	[OrderId] [bigint] NOT NULL,
 CONSTRAINT [PK_CustomerReview] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DeviceToken]    Script Date: 22/06/2018 12:53:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DeviceToken](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[DeviceId] [int] NOT NULL,
	[AssetId] [bigint] NOT NULL,
	[Token] [varchar](256) NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[ExpiredOn] [datetime] NULL,
 CONSTRAINT [PK_DeviceToken] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LoginUserLog]    Script Date: 22/06/2018 12:53:21 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoginUserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[LoginTime] [datetime] NOT NULL,
	[LastActionTime] [datetime] NOT NULL,
 CONSTRAINT [PK_LoginUserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Notification]    Script Date: 22/06/2018 12:53:22 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Notification](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetId] [bigint] NULL,
	[CustomerId] [bigint] NULL,
	[MType] [int] NOT NULL,
	[MReceiver] [int] NOT NULL,
	[MessageText] [nvarchar](250) NULL,
	[MessageStatus] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[OrderId] [bigint] NULL,
	[EventDateTime] [datetime] NULL,
	[SendTries] [int] NOT NULL,
	[SentOn] [datetime] NULL,
	[RequestId] [nvarchar](128) NULL,
 CONSTRAINT [PK_Notification] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 22/06/2018 12:53:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AssetId] [bigint] NOT NULL,
	[CustomerId] [bigint] NOT NULL,
	[CLatitude] [float] NOT NULL,
	[CLongitude] [float] NOT NULL,
	[ALatitude] [float] NOT NULL,
	[ALongitude] [float] NOT NULL,
	[RequestType] [int] NOT NULL,
	[RequestedDateTime] [datetime] NULL,
	[StartingDistance] [float] NOT NULL,
	[OrderStatus] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NULL,
	[ExpiresOn] [datetime] NULL,
	[Arrival] [datetime] NULL,
	[ConfirmedOn] [datetime] NULL,
	[CategoryTierId] [int] NOT NULL,
	[TotalCost] [money] NOT NULL,
	[DownPayment] [money] NULL,
	[AdressId] [bigint] NULL,
	[CustomerPaymentId] [bigint] NULL,
	[ChargeId] [nvarchar](32) NULL,
	[PaymentMethod] [int] NOT NULL,
	[PaypalTransId] [varchar](50) NULL,
	[AReviewedOn] [datetime] NULL,
	[CReviewedOn] [datetime] NULL,
	[ADeclinedOn] [datetime] NULL,
	[CDeclinedOn] [datetime] NULL,
	[CompletedOn] [datetime] NULL,
	[CashRecollected] [money] NULL,
	[ServiceEnd] [datetime] NOT NULL,
	[ComisionStatus] [int] NOT NULL,
	[DepositStatus] [int] NOT NULL,
	[OxxoRef] [bigint] NULL,
 CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderLog]    Script Date: 22/06/2018 12:53:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](256) NOT NULL,
	[OrderId] [nvarchar](20) NOT NULL,
	[Tries] [int] NOT NULL,
	[SentOn] [datetime] NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_OrderLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductActivityLog]    Script Date: 22/06/2018 12:53:23 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductActivityLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ProductId] [varchar](50) NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
	[ViewCount] [int] NOT NULL,
 CONSTRAINT [PK_ProductActivityLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Receipt]    Script Date: 22/06/2018 12:53:24 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipt](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[OrderId] [bigint] NOT NULL,
	[ImageStream] [image] NOT NULL,
	[RStatus] [int] NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_Receipt] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RefreshTokens]    Script Date: 22/06/2018 12:53:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RefreshTokens](
	[Id] [nvarchar](128) NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[ClientId] [nvarchar](50) NOT NULL,
	[IssuedUtc] [datetime] NOT NULL,
	[ExpiresUtc] [datetime] NOT NULL,
	[ProtectedTicket] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.RefreshTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SearchActivityLog]    Script Date: 22/06/2018 12:53:27 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SearchActivityLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[SearchTerm] [varchar](50) NOT NULL,
	[ActivityDate] [datetime] NOT NULL,
	[SearchCount] [int] NOT NULL,
 CONSTRAINT [PK_SearchActivityLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[sysdiagrams]    Script Date: 22/06/2018 12:53:28 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[sysdiagrams](
	[name] [sysname] NOT NULL,
	[principal_id] [int] NOT NULL,
	[diagram_id] [int] IDENTITY(1,1) NOT NULL,
	[version] [int] NULL,
	[definition] [varbinary](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[diagram_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [UK_principal_name] UNIQUE NONCLUSTERED 
(
	[principal_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tCreditos]    Script Date: 22/06/2018 12:53:29 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tCreditos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [varchar](128) NULL,
	[NoOrden] [varchar](50) NULL,
	[Monto] [decimal](18, 0) NULL,
	[Credito] [bit] NULL,
	[CreatedOn] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserActionLog]    Script Date: 22/06/2018 12:53:29 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserActionLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ActionId] [int] NOT NULL,
	[ActionDate] [datetime] NOT NULL,
	[ActionCount] [int] NOT NULL,
 CONSTRAINT [PK_UserActionLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserFavourites]    Script Date: 22/06/2018 12:53:30 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserFavourites](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[ProductId] [varchar](50) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
 CONSTRAINT [PK_UserFavourites] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRequestLog]    Script Date: 22/06/2018 12:53:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRequestLog](
	[Id] [nvarchar](128) NOT NULL,
	[UserId] [nvarchar](128) NOT NULL,
	[UserName] [nvarchar](256) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[SentOn] [datetime] NULL,
	[Status] [smallint] NOT NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_UserRequestLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VersionControlLuxxApp]    Script Date: 22/06/2018 12:53:31 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VersionControlLuxxApp](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sVersion] [varchar](20) NULL,
	[fAlta] [datetime] NULL,
	[fBaja] [datetime] NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[XConfig]    Script Date: 22/06/2018 12:53:32 p. m. ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[XConfig](
	[Id] [int] NOT NULL,
	[MinAge] [int] NOT NULL,
	[MaxAge] [int] NOT NULL,
	[MinDistance] [int] NOT NULL,
	[MaxDistance] [int] NOT NULL,
	[ListNumRecords] [int] NOT NULL,
	[iOSMinVersion] [decimal](7, 5) NOT NULL,
	[iOSStoreUrl] [varchar](255) NOT NULL,
	[DroidMinVersion] [decimal](7, 5) NOT NULL,
	[DroidStoreUrl] [varchar](255) NOT NULL,
	[NSJobBussy] [bit] NOT NULL,
	[EmailJobBussy] [bit] NOT NULL,
	[PageSize] [int] NOT NULL,
	[PageCount] [int] NOT NULL,
	[NSJobSeconds] [int] NOT NULL,
	[EmailJobSeconds] [int] NOT NULL,
	[CartExpireHours] [int] NOT NULL,
	[TaxAmount] [float] NOT NULL,
	[DeliveryNumDays] [int] NOT NULL,
	[RefeshCount] [int] NOT NULL,
	[LastUpdate] [datetime] NOT NULL,
	[ReviewAwaitMinutes] [int] NOT NULL,
	[Comision] [float] NOT NULL,
	[ScheduleFutureDays] [int] NOT NULL,
	[ScheduleFutureMinutes] [int] NOT NULL,
	[ReservationAfterMinutes] [int] NOT NULL,
 CONSTRAINT [PK_XConfig] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ABank] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[ABank] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ACalendarTimes] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ACalendarTimes] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[ACategory] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ACategory] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[ACategoryTier] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[ACategoryTier] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ACategoryTier] ADD  DEFAULT ((0)) FOR [NumHours]
GO
ALTER TABLE [dbo].[ADistance] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[ADistance] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[AHours] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[AHours] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[APref] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[APref] ADD  DEFAULT ((0)) FOR [AIndexNum]
GO
ALTER TABLE [dbo].[APref] ADD  DEFAULT ((0)) FOR [CIndexNum]
GO
ALTER TABLE [dbo].[AService] ADD  DEFAULT ((1)) FOR [IsEnabled]
GO
ALTER TABLE [dbo].[AService] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT ((0)) FOR [AdminType]
GO
ALTER TABLE [dbo].[Asset] ADD  DEFAULT ((0)) FOR [IsActive]
GO
ALTER TABLE [dbo].[Asset] ADD  DEFAULT ((0)) FOR [ViewCount]
GO
ALTER TABLE [dbo].[Asset] ADD  DEFAULT ((0)) FOR [CalendarActive]
GO
ALTER TABLE [dbo].[Asset] ADD  DEFAULT ((0)) FOR [IsOnline]
GO
ALTER TABLE [dbo].[AssetImage] ADD  DEFAULT ((0)) FOR [IndexNum]
GO
ALTER TABLE [dbo].[AssetImage] ADD  DEFAULT ((0)) FOR [IsProfile]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ((18)) FOR [MinAge]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ((50)) FOR [MaxAge]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ((0)) FOR [MinDistance]
GO
ALTER TABLE [dbo].[Customer] ADD  DEFAULT ((10)) FOR [MaxDistance]
GO
ALTER TABLE [dbo].[CustomerPaymentMethod] ADD  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Notification] ADD  DEFAULT ((0)) FOR [SendTries]
GO
ALTER TABLE [dbo].[Order] ADD  DEFAULT ((0)) FOR [DepositStatus]
GO
ALTER TABLE [dbo].[OrderLog] ADD  DEFAULT ((0)) FOR [Tries]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((0)) FOR [NSJobBussy]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((0)) FOR [EmailJobBussy]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((100)) FOR [PageSize]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((0)) FOR [PageCount]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((180)) FOR [NSJobSeconds]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((120)) FOR [EmailJobSeconds]
GO
ALTER TABLE [dbo].[XConfig] ADD  DEFAULT ((24)) FOR [CartExpireHours]
GO
ALTER TABLE [dbo].[ACalendar]  WITH CHECK ADD  CONSTRAINT [FK_ACalendar_Asset] FOREIGN KEY([UserId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[ACalendar] CHECK CONSTRAINT [FK_ACalendar_Asset]
GO
ALTER TABLE [dbo].[ACalendarDetail]  WITH CHECK ADD  CONSTRAINT [FK_ACalendarDetail_ACalendar] FOREIGN KEY([CalendarId])
REFERENCES [dbo].[ACalendar] ([Id])
GO
ALTER TABLE [dbo].[ACalendarDetail] CHECK CONSTRAINT [FK_ACalendarDetail_ACalendar]
GO
ALTER TABLE [dbo].[ACategoryTier]  WITH CHECK ADD  CONSTRAINT [FK_ACategoryTier_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ACategory] ([Id])
GO
ALTER TABLE [dbo].[ACategoryTier] CHECK CONSTRAINT [FK_ACategoryTier_Category]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_APref] FOREIGN KEY([APrefId])
REFERENCES [dbo].[APref] ([Id])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_APref]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_AspNetUsers]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Bank] FOREIGN KEY([BankId])
REFERENCES [dbo].[ABank] ([Id])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_Bank]
GO
ALTER TABLE [dbo].[Asset]  WITH CHECK ADD  CONSTRAINT [FK_Asset_Category] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[ACategory] ([Id])
GO
ALTER TABLE [dbo].[Asset] CHECK CONSTRAINT [FK_Asset_Category]
GO
ALTER TABLE [dbo].[AssetImage]  WITH CHECK ADD  CONSTRAINT [FK_AssetImage_Asset] FOREIGN KEY([UserId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[AssetImage] CHECK CONSTRAINT [FK_AssetImage_Asset]
GO
ALTER TABLE [dbo].[AssetReview]  WITH CHECK ADD  CONSTRAINT [FK_AssetReview_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[AssetReview] CHECK CONSTRAINT [FK_AssetReview_Asset]
GO
ALTER TABLE [dbo].[AssetReview]  WITH CHECK ADD  CONSTRAINT [FK_AssetReview_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[AssetReview] CHECK CONSTRAINT [FK_AssetReview_Customer]
GO
ALTER TABLE [dbo].[AssetReview]  WITH CHECK ADD  CONSTRAINT [FK_AssetReview_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[AssetReview] CHECK CONSTRAINT [FK_AssetReview_Order]
GO
ALTER TABLE [dbo].[AssetService]  WITH CHECK ADD  CONSTRAINT [FK_AssetService_Asset] FOREIGN KEY([UserId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[AssetService] CHECK CONSTRAINT [FK_AssetService_Asset]
GO
ALTER TABLE [dbo].[AssetService]  WITH CHECK ADD  CONSTRAINT [FK_AssetService_ServiceId] FOREIGN KEY([ServiceId])
REFERENCES [dbo].[AService] ([Id])
GO
ALTER TABLE [dbo].[AssetService] CHECK CONSTRAINT [FK_AssetService_ServiceId]
GO
ALTER TABLE [dbo].[ATransaction]  WITH CHECK ADD  CONSTRAINT [FK_ATransaction_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[ATransaction] CHECK CONSTRAINT [FK_ATransaction_Order]
GO
ALTER TABLE [dbo].[CAddress]  WITH CHECK ADD  CONSTRAINT [FK_CAddress_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CAddress] CHECK CONSTRAINT [FK_CAddress_Customer]
GO
ALTER TABLE [dbo].[CTransaction]  WITH CHECK ADD  CONSTRAINT [FK_CTransaction_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[CTransaction] CHECK CONSTRAINT [FK_CTransaction_Order]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_ACategory] FOREIGN KEY([ACategoryId])
REFERENCES [dbo].[ACategory] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_ACategory]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_AspNetUsers]
GO
ALTER TABLE [dbo].[Customer]  WITH CHECK ADD  CONSTRAINT [FK_Customer_CPref] FOREIGN KEY([CPrefId])
REFERENCES [dbo].[APref] ([Id])
GO
ALTER TABLE [dbo].[Customer] CHECK CONSTRAINT [FK_Customer_CPref]
GO
ALTER TABLE [dbo].[CustomerPaymentMethod]  WITH CHECK ADD  CONSTRAINT [FK_CustomerPaymentMethod_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CustomerPaymentMethod] CHECK CONSTRAINT [FK_CustomerPaymentMethod_Customer]
GO
ALTER TABLE [dbo].[CustomerReview]  WITH CHECK ADD  CONSTRAINT [FK_CustomerReview_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[CustomerReview] CHECK CONSTRAINT [FK_CustomerReview_Asset]
GO
ALTER TABLE [dbo].[CustomerReview]  WITH CHECK ADD  CONSTRAINT [FK_CustomerReview_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[CustomerReview] CHECK CONSTRAINT [FK_CustomerReview_Customer]
GO
ALTER TABLE [dbo].[CustomerReview]  WITH CHECK ADD  CONSTRAINT [FK_CustomerReview_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[CustomerReview] CHECK CONSTRAINT [FK_CustomerReview_Order]
GO
ALTER TABLE [dbo].[DeviceToken]  WITH CHECK ADD  CONSTRAINT [FK_DeviceToken_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[DeviceToken] CHECK CONSTRAINT [FK_DeviceToken_Asset]
GO
ALTER TABLE [dbo].[LoginUserLog]  WITH CHECK ADD  CONSTRAINT [FK_LoginUserLog_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[LoginUserLog] CHECK CONSTRAINT [FK_LoginUserLog_AspNetUsers]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Assets] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_Assets]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Customers] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_Customers]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_Orders]
GO
ALTER TABLE [dbo].[Notification]  WITH CHECK ADD  CONSTRAINT [FK_Notification_UserRequestLog] FOREIGN KEY([RequestId])
REFERENCES [dbo].[UserRequestLog] ([Id])
GO
ALTER TABLE [dbo].[Notification] CHECK CONSTRAINT [FK_Notification_UserRequestLog]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Address] FOREIGN KEY([AdressId])
REFERENCES [dbo].[CAddress] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Address]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Asset] FOREIGN KEY([AssetId])
REFERENCES [dbo].[Asset] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Asset]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_CategoryTier] FOREIGN KEY([CategoryTierId])
REFERENCES [dbo].[ACategoryTier] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_CategoryTier]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_Customer] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customer] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_Customer]
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_PaymentId] FOREIGN KEY([CustomerPaymentId])
REFERENCES [dbo].[CustomerPaymentMethod] ([Id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_PaymentId]
GO
ALTER TABLE [dbo].[Receipt]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_Order] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Order] ([Id])
GO
ALTER TABLE [dbo].[Receipt] CHECK CONSTRAINT [FK_Receipt_Order]
GO
ALTER TABLE [dbo].[UserActionLog]  WITH CHECK ADD  CONSTRAINT [FK_UserActionLog_Actions] FOREIGN KEY([ActionId])
REFERENCES [dbo].[Actions] ([Id])
GO
ALTER TABLE [dbo].[UserActionLog] CHECK CONSTRAINT [FK_UserActionLog_Actions]
GO
ALTER TABLE [dbo].[UserRequestLog]  WITH CHECK ADD  CONSTRAINT [FK_UserRequestLog_AspNetUsers] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[UserRequestLog] CHECK CONSTRAINT [FK_UserRequestLog_AspNetUsers]
GO

INSERT [dbo].[XConfig] ([Id], [MinAge], [MaxAge], [MinDistance], [MaxDistance], [ListNumRecords], [iOSMinVersion], [iOSStoreUrl], [DroidMinVersion], [DroidStoreUrl], [NSJobBussy], [EmailJobBussy], [PageSize], [PageCount], [NSJobSeconds], [EmailJobSeconds], [CartExpireHours], [TaxAmount], [DeliveryNumDays], [RefeshCount], [LastUpdate], [ReviewAwaitMinutes], [Comision], [ScheduleFutureDays], [ScheduleFutureMinutes], [ReservationAfterMinutes]) VALUES (1, 18, 50, 1, 10, 15, CAST(1.00000 AS Decimal(7, 5)), N'apple.com', CAST(1.00000 AS Decimal(7, 5)), N'google.com', 0, 0, 100, 1, 15, 100, 45, 16, 1, 0, CAST(N'2017-01-01T00:00:00.000' AS DateTime), 1, 0.2, 13, 120, 30)
GO
INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName], [Discriminator], [AXAccountId], [AdminType]) VALUES (N'cab8edfb-c2c6-4d43-afdf-b708af4109bd', NULL, 0, N'AAgHn2wUzwyR9wjdvrr8ipW2DEoBj37NHRUCb6VAAFDS54tUw80IaPRqcT23bvdDqg==', N'6e2184cf-07e3-48a5-9b7f-ee82209a2601', NULL, 0, 0, NULL, 0, 0, N'luxxvip', N'IdentityUser', NULL, 2)
GO
INSERT INTO ACategory ([Name],[IsEnabled],[IndexNum],[cost1],[cost2],[cost3])
SELECT 'Cat1',1,0,0,0,0
GO
INSERT [dbo].[Asset] ([UserId], [Name], [CategoryId], [Title], [Description], [Height], [Waist], [BraSize], [BraCup], [SkinColor], [EyeColor], [HairColor], [CreatedOn], [UpdatedOn], [MaxDistance], [AccountNum], [BankHolderName], [BankId], [IsActive], [Latitude], [Longitude], [LocationUpdate], [ViewCount], [CalendarActive], [IsOnline], [DOB], [APrefId], [AccountNum2], [ASex], [OnlineExpires], [Balance], [precio]) 
VALUES (N'cab8edfb-c2c6-4d43-afdf-b708af4109bd', N'luxxvip', 1, N'luxxvip', N'luxxvip', NULL, NULL, NULL, NULL, NULL, NULL, NULL, CAST(N'2018-05-30T23:29:36.107' AS DateTime), NULL, 1, N'', NULL, NULL, 1, 25.6418961, -100.3222841, CAST(N'2018-06-19T22:04:26.983' AS DateTime), 0, 0, 1, CAST(N'2000-04-01T00:00:00.000' AS DateTime), NULL, NULL, 0, CAST(N'2018-06-20T06:04:13.640' AS DateTime), NULL, CAST(1000 AS Decimal(18, 0)))
GO



SET IDENTITY_INSERT [dbo].[APref] ON 
GO
INSERT [dbo].[APref] ([Id], [Name], [IsEnabled], [AIndexNum], [CIndexNum]) VALUES (1, N'Mujeres', 1, 2, 1)
GO
INSERT [dbo].[APref] ([Id], [Name], [IsEnabled], [AIndexNum], [CIndexNum]) VALUES (2, N'Hombres', 1, 1, 2)
GO
INSERT [dbo].[APref] ([Id], [Name], [IsEnabled], [AIndexNum], [CIndexNum]) VALUES (3, N'Mujeres y Hombres', 1, 3, 3)
GO
SET IDENTITY_INSERT [dbo].[APref] OFF
GO



SET IDENTITY_INSERT [dbo].[AHours] ON 
GO
INSERT [dbo].[AHours] ([Id], [Name], [Hours], [IsEnabled], [IndexNum]) VALUES (1, N'1 hora', 1, 1, 1)
GO
INSERT [dbo].[AHours] ([Id], [Name], [Hours], [IsEnabled], [IndexNum]) VALUES (2, N'2 horas', 2, 1, 2)
GO
INSERT [dbo].[AHours] ([Id], [Name], [Hours], [IsEnabled], [IndexNum]) VALUES (3, N'4 horas', 4, 1, 3)
GO
INSERT [dbo].[AHours] ([Id], [Name], [Hours], [IsEnabled], [IndexNum]) VALUES (4, N'8 horas', 8, 1, 4)
GO
SET IDENTITY_INSERT [dbo].[AHours] OFF
GO

SET IDENTITY_INSERT [dbo].[ACalendarTimes] ON 
GO
INSERT [dbo].[ACalendarTimes] ([Id], [Name], [Minutes], [IsEnabled], [IndexNum]) VALUES (1, N'1 minuto', 1, 1, 1)
GO
INSERT [dbo].[ACalendarTimes] ([Id], [Name], [Minutes], [IsEnabled], [IndexNum]) VALUES (2, N'2 horas', 120, 1, 2)
GO
INSERT [dbo].[ACalendarTimes] ([Id], [Name], [Minutes], [IsEnabled], [IndexNum]) VALUES (3, N'4 horas', 240, 1, 3)
GO
INSERT [dbo].[ACalendarTimes] ([Id], [Name], [Minutes], [IsEnabled], [IndexNum]) VALUES (4, N'8 horas', 480, 1, 4)
GO
SET IDENTITY_INSERT [dbo].[ACalendarTimes] OFF
GO
