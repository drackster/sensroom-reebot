﻿using System;
using System.Collections.Generic;

namespace ProjectX.ViewModels
{
    public class XSettingsViewModel
    {
        public List<ACategoryViewModel> Categories { get; set; }
        public List<DistanceViewModel> Distances { get; set; }
        public List<APrefViewModel> Preferences { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public DateTime LastUpdate { get; set; }

        public int ScheduleFutureDays { get; set; }
        public int ScheduleFutureMinutes { get; set; }
        public int ReservationAfterMinutes { get; set; }
    }
}
