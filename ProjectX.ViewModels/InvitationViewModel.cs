﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class InvitationViewModel
    {
        public long CustomerId { get; set; }
        public long OrderId { get; set; }
        public OrderType oType { get; set; }
        public OrderStatus oStatus { get; set; }
        public DepositStatus dStatus { get; set; }
        public string StatusName { get; set; }
        public string CustomerName { get; set; }
        public double GlobalRating { get; set; }
        public DateTime CreatedOn { get; set; }
        public string RequestedTime1 { get; set; }
        //public string RequestedTime2 { get; set; }
        public DateTime? EventDateTime { get; set; }
        public DateTime? EventEnd { get; set; }
        public DateTime ExpiresOn { get; set; }
        public string TierName { get; set; }
        public decimal Cost { get; set; }
        public decimal? DownPayment { get; set; }
        public decimal Precio { get; set; }

        public long? AddressId { get; set; }
        public string Address { get; set; }
        public string Comments { get; set; }

        public string ReviewerName { get; set; }
        public string Review { get; set; }
        public int ReviewCount { get; set; }
        public DateTime ReviewDate { get; set; } 
        public double Rating { get; set; }


        public string DistanceFrom { get; set; }
        public double CLatitude { get; set; }
        public double CLongitude { get; set; }
        public string AssetCoverImageId { get; set; }

        public ChargeViewModel ChargeModel { get; set; }
        public PaymentMethod paymentMethod { get; set; }
        public string TransactionId { get; set; }
        public long? OxxoRef { get; set; }
    }
}
