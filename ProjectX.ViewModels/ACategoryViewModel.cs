﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ACategoryViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<ACategoryTierViewModel> Tiers { get; set; }
        public bool IsSelected { get; set; }
        public bool IsChanged { get; set; }
        public int Cost1 { get; set; }
        public int Cost2 { get; set; }
        public int Cost3 { get; set; }
        public decimal precio { get; set; }
       
    }
}
