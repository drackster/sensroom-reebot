﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class MessageViewModel
    {
        public long Id { get; set; }
        public MessageSender SenderType { get; set; }
        public int SenderId { get; set; }
        public string SenderName { get; set; }
        public string SenderImage { get; set; }
        public MessageStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public string MessageText { get; set; }
        
    }
}
