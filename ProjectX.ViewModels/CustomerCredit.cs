﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class CustomerCredit
    {
        [PrimaryKey]
        public string UserId { get; set; }
        public string NoOrden { get; set; }
        public decimal Monto { get; set; }
        public bool Credito { get; set; }
        public DateTime? CreatedOn { get; set; }
    }
}
