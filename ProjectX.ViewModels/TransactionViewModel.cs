﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class TransactionViewModel
    {
        public DateTime Week { get; set; }
    }

    public class TransactionWeek
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string WeekDate
        {
            get
            {
                var f = Util.GetMonthAsString(From.Month).Substring(0, 3);
                var t = Util.GetMonthAsString(To.Month).Substring(0, 3);

                return f + "/" + string.Format("{0:dd/yy}", From) + " - " +
                       t + "/" + string.Format("{0:dd/yy}", To);
            }
        }
    }
}
