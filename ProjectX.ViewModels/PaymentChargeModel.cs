﻿using System;

namespace ProjectX.ViewModels
{
    public class PaymentChargeModel
    {
        public CustomerPaymentViewModel Payment { get;set; }
        public OrderViewModel Order { get; set; }
    }
}
