﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class AssetImageViewModel
    {
        [PrimaryKey]
        public string Id { get; set; }
        //public byte[] ImgSource { get; set; }
        public int IndexNum { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool IsProfile { get; set; }
        public bool ShowEmpty { get { return !IsProfile; } }
        //public double ImageHeight { get; set; }
        public bool HasChanges { get; set; }
        public bool IsDeleted { get; set; }
    }
}
