﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace ProjectX.ViewModels
{
    public class CalendarDetailViewModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public long CalendarId { get; set; }
        public DayOfWeek Day { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
        public bool IsChanged { get; set; }
    }
}
