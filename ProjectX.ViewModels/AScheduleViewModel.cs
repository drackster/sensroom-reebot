﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class AScheduleViewModel
    {
        public AScheduleViewModel()
        {
            Invitations = new List<InvitationViewModel>();
            Space = new List<ScheduleSpace>();
            //NonWorkingSpace = new List<ScheduleSpace>();
        }
        public List<ScheduleSpace> Space { get; set; }
        public List<InvitationViewModel> Invitations { get; set; }
        public int WorkDayStart { get; set; }
        public int WorkDayEnd { get; set; }
        //public DateTime? MinDay { get; set; }
        //public DateTime? MaxDay { get; set; }
    }
}
