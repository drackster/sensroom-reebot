﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class DeviceTokenViewModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public long AssetId { get; set; }
        public DeviceType DeviceId { get; set; }
        public string Token { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime? ExpiredOn { get; set; }
        //public bool IsRegistered { get; set; }
    }
}
