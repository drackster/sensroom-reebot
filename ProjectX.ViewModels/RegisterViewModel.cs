﻿using System;
using SQLite;

namespace ProjectX.ViewModels
{
    public class RegisterViewModel
    {
        [PrimaryKey]
        public string Id { get; set; }
        public string HostId { get; set; }
        public string HostCode { get; set; }
        public bool HostIdValid { get; set; }
        public string HostCodeError { get; set; }
        public string UserName { get; set; }
        public bool UserNameValid { get; set; }
        public string Password { get; set; }
        public bool IsActivated { get; set; }
        public string UserId { get; set; }
        public MessageSender Sender { get; set; }
        public MessageSender Receiver { get; set; }
        public bool AccountSetup { get; set; }
        public short ActivationStatus { get; set; }
        public string ActivationStatusStr { get; set; }
        //0 = Nada/Reset
        //1 = Anfitrion
        //2 = Usuario
        //3 = Solicitado
        //4 = Autorizado
        //5 = Rechazado
        //6 = Cancelado por el usuario

        public DateTime CreateOn { get; set; }
        public DateTime UpdatedOn { get; set; }
        public DateTime ExpiresOn { get; set; }
        public bool Permissions { get; set; }

        [Ignore]
        public UserViewModel UserVM { get; set; }

        [Ignore]
        public AssetViewModel AssetVM { get; set; }

        [Ignore]
        public XSettingsViewModel XSettingsVM { get; set; }


        //Esta propiedad es necesarias para cambios de usuario
        //Por ejemplo, si hacen logout, y luego login con otro usuario
        //tenemos que actualizar el usuario al que pertenece ese token (que no ha cambiado)
        //Lo utilizaremos en el login
        [Ignore]
        public DeviceTokenViewModel DeviceToken { get; set; }
    }
}
