﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class NotificationRequestModel
    {
        public MessageType MType { get; set; }
        public MessageSender SType { get; set; }
        public long? AssetId { get; set; }
        public long? CustomerId { get; set; }
        public long? OrderId { get; set; }
        public long LastId { get; set; }
        public bool IsChat { get; set; }
        public DateTime? StartDate { get; set; }
    }
}
