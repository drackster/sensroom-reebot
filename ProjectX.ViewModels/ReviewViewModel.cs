﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ReviewViewModel
    {
        public long Id { get; set; }
        public long OrderId { get; set; }
        public bool UserDeclined { get; set; }
        public long ReviewedId { get; set; }
        public long ReviewerId { get; set; }
        public string ReviewerName { get; set; }
        public string Review { get; set; }
        public double Rating { get; set; }
        public DateTime CreatedOn { get; set; }
        public bool ShowDetail { get; set; }
    }
}
