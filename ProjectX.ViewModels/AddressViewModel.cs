﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class AddressViewModel
    {
        public long Id { get; set; }
        public string PlaceId { get; set; }
        public string CountryCode { get; set; }
        public string FeatureName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string Locality { get; set; }
        public string PostalCode { get; set; }
        public string SubLocality { get; set; }
        public string SubThoroughfare { get; set; }
        public string Thoroughfare { get; set; }

        //public string A1
        //{
        //    get
        //    {
        //        var a1 = string.Empty;
        //        a1 += Thoroughfare == null ? "" : Thoroughfare + " ";
        //        a1 += SubThoroughfare == null ? "" : SubThoroughfare + " ";
        //        return a1;
        //    }
        //}
        //public string A2
        //{
        //    get
        //    {
        //        var a2 = string.Empty;
        //        a2 += Locality == null ? "" : Locality + " ";
        //        a2 += SubLocality == null ? "" : SubLocality + " ";
        //        return a2;
        //    }
        //}
        //public string A3
        //{
        //    get
        //    {
        //        var a3 = string.Empty;
        //        a3 += PostalCode == null ? "" : PostalCode + " ";
        //        a3 += CountryCode == null ? "" : CountryCode + " ";
        //        return a3;
        //    }
        //}
        //public string A
        //{
        //    get
        //    {
        //        string a = string.Empty;
        //        a += A1 == null ? "" : A1;
        //        a += A2 == null ? "" : A2;
        //        a += A3 == null ? "" : A3;
        //        return a;
        //    }
        //}
        //public void SetA()
        //{
        //    var A1 = string.Empty;
        //    var A2 = string.Empty;
        //    var A3 = string.Empty;
        //    A1 += Thoroughfare == null ? "" : Thoroughfare + " ";
        //    A1 += SubThoroughfare == null ? "" : SubThoroughfare + " ";

        //    A2 += Locality == null ? "" : Locality + " ";
        //    A2 += SubLocality == null ? "" : SubLocality + " ";

        //    A3 += PostalCode == null ? "" : PostalCode + " ";
        //    A3 += CountryCode == null ? "" : CountryCode + " ";

        //    A = string.Empty;
        //    A += A1 == null ? "" : A1;
        //    A += A2 == null ? "" : A2;
        //    A += A3 == null ? "" : A3;            
        //}
        public string A { get; set; }
        public string A1 { get; set; }
        public string A2 { get; set; }
        public string A3 { get; set; }

        public string A12
        {
            get
            {
                string a12 = string.Empty;
                a12 += A1 == null ? "" : A1;
                a12 += A2 == null ? "" : A2;
                return a12;
            }
        }
        public string A23
        {
            get
            {
                string a23 = string.Empty;
                a23 += A2 == null ? "" : A2;
                a23 += A3 == null ? "" : A3;
                return a23;
            }
        }
        public void Parse()
        {
            A1 = string.Empty;
            A1 += Thoroughfare == null ? "" : Thoroughfare + " ";
            A1 += SubThoroughfare == null ? "" : SubThoroughfare + " ";

            A2 = string.Empty;
            A2 += Locality == null ? "" : Locality + " ";
            A2 += SubLocality == null ? "" : SubLocality + " ";

            A3 = string.Empty;
            A3 += PostalCode == null ? "" : PostalCode + " ";
            A3 += CountryCode == null ? "" : CountryCode + " ";
        }
    }
}
