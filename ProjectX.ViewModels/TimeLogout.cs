﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class TimeLogout
    {
        public DateTime TimeDisappear { get; set; }
        public DateTime TimeAppear { get; set; }
    }
}
