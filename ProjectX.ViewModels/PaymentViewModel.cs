﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class PaymentViewModel
    {
        public int Id { get; set; }
        public PaymentMethod Method { get; set; }
        public string Name { get; set; }
        public string PaymentMethodName
        {
            get
            {
                return Enum.GetName(Method.GetType(), Method);
            }
        }
        public bool IsSelected { get; set; }
        public bool ShowName
        {
            get
            {
                return string.IsNullOrEmpty(Name) ? false : true;
            }
        }

    }
    
}
