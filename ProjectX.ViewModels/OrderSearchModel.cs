﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class OrderSearchModel
    {
        public OrderStatus orderStatus { get; set; }
        public MessageSender MSender { get; set; }
        public ReviewStatus reviewStatus { get; set; }
        public int PageIndex { get; set; }
        public bool CalendarOnly { get; set; }
        public TransactionWeek week { get; set; }
    }
}
