﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ASettingsViewModel
    {
        public List<ACategoryViewModel> Categories { get; set; }
        public List<AServiceViewModel> Services { get; set; }
        public List<DistanceViewModel> Distances { get; set; }
        public List<ACategoryViewModel> Precio { get; set; }
        //Esta lista se movió a AssetViewModel
        //Este VM no debe de contener informaciOn personal de una chica
        //public List<CalendarViewModel> Calendars { get; set; }
        public List<APrefViewModel> Preferences { get; set; }
        //public List<CalendarTimeViewModel> CalendarTimes { get; set; }
    }
}
