﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum DepositStatus
    {
        None = 0,
        Pending = 1,
        Payed = 2,
        Canceled = 3,
        Expired = 4
    }
}
