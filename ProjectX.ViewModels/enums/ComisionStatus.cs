﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum ComisionStatus
    {
        None = 0,
        Complete = 1,
        Refunded = 2,
        Cancelled = 3
    }
}
