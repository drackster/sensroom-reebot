﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels.enums
{
    public enum ModeType
    {
        // tipo de invitación, cuando el asset está en modo offlineView o en modo online
        onlilneMode = 1,
        offlineMode = 2
    }
}
