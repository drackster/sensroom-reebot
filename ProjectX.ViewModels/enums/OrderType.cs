﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum OrderType
    {
        NotDefined=0,
        ASAP = 1,
        Calendar = 2
    }
}
