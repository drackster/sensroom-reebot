﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum OrderStatus
    {
        All = 0,
        Pending = 1,
        Canceled = 2,
        Rejected = 3,
        Confirmed = 4,
        InTransit = 5,
        Closed = 6,
        Expired = 7,
        PreCharging = 21, //SE ESTA INTENTANDO REALIZAR UN PRECARGO DE TARJETA ...(NO APLICA PARA PAYPAL U OTROS METODOS DE PAGO)
        PreChargeSucess = 22, //SE REALIZO EL PRECARGO CON EXITO
        PreChargeFailure = 23, //HUBO UN PROBLEMA CON EL PRECARGO
    }
}
