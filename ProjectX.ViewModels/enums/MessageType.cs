﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum MessageType
    {
        AuthRequest = 0,
        Invitation = 1,
        Deposit = 2,
        CustomerMessage = 3,
        Rating = 4,
        All = 5,
        AssetMessage = 6,
        AcceptedInvitation=7,
        RejectedInvitation=8,
        CanceledInvitation=9

    }
}
