﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum PaymentMethod
    {
        OpenPay=1, //TDC
        Paypal=2, //paypal
        Paynet=3, //oxxo
        Credito =4 // credito por cancelación
    }
}
