﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum MessageStatus
    {
        Created = 0,
        Sent = 1,
        Received = 2,
        Read = 3
    }
}
