﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum ReviewStatus
    {
        All = 0,
        Pending=1,
        Reviewed = 2,
        Canceled=3
    }
}
