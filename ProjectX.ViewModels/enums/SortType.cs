﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public enum SortType
    {
        Distance = 1,
        Rating = 2,
        Price = 3,
        JoinDate = 4
    }
}
