﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ScheduleSpace
    {
        public ScheduleSpaceType SType { get; set; }
        public long? OrderId { get; set; }
        public int day { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string LabelMsg { get; set; }
        public OrderType OType { get; set; }
    }

    public enum ScheduleSpaceType
    {
        Invitation = 1,
        FreeSpace = 2,
        SleepSpace = 3
    }
}
