﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using SQLite;

namespace ProjectX.ViewModels
{
    public class UserToken
    {
        public UserToken()
        {
            Authenticated = false;
            UserName = Password = string.Empty;
        }

        [PrimaryKey]
        [JsonProperty(PropertyName = "userName")]
        public string UserName { get; set; }

        [JsonProperty(PropertyName = "access_token")]
        public string AccessToken { get; set; }

        [JsonProperty(PropertyName = "token_type")]
        public string TokenType { get; set; }

        [JsonProperty(PropertyName = ".issued")]
        public DateTime Issued { get; set; }

        [JsonProperty(PropertyName = ".expires")]
        public DateTime Expires { get; set; }

        [JsonProperty(PropertyName = "expires_in")]
        public int ExpiresIn { get; set; }
        public bool Authenticated { get; set; }
        public string Password { get; set; }

        [JsonProperty(PropertyName = "internal_id")]
        public long InternalUserId { get; set; }
    }
}
