﻿using System;
using System.Text.RegularExpressions;

namespace ProjectX.ViewModels
{
    public class Util
    {
        public static string GetOrderStatus(OrderStatus status)
        {
            switch (status)
            {
                case OrderStatus.Pending: return "Pendiente";
                case OrderStatus.Canceled: return "Cancelado";
                case OrderStatus.Confirmed: return "Confirmado";
                case OrderStatus.InTransit: return "En Camino";
                case OrderStatus.Rejected: return "Rechazado";
                case OrderStatus.Closed: return "Cerrado";
            }
            return "Pendiente";
        }
        public static string GetCustomerName(string customerName)
        {
            return  Regex.Replace(customerName, @"[0-9\-]", string.Empty).Trim();
        }

        #region Date and Time Helper Methods
        public static string ConvertToFriendlyDT(DateTime DT, bool FriendlyDay, bool FriendlyMonth,
                                            bool IncludeYear, bool IncludeTime)
        {
            string result = string.Empty;
            result += FriendlyDay ? Util.GetDayAsString(DT.DayOfWeek).Substring(0, 3) + " " + string.Format("{0:dd}", DT) : string.Format("{0:dd}", DT);
            result += "/";
            result += FriendlyMonth ? Util.GetMonthAsString(DT.Month).Substring(0, 3) : string.Format("{0:MM}", DT);

            if (IncludeYear)
                result += "/" + string.Format("{0:yy}", DT);
            if (IncludeTime)
                result += " " + string.Format("{0:HH:mm}", DT);

            return result;
        }
        public static DateTime StripMinSec(DateTime dt)
        {
            if (dt.Second > 0)
                dt = dt.AddSeconds(dt.Second * -1);
            if (dt.Millisecond > 0)
                dt = dt.AddMilliseconds(dt.Millisecond * -1);
            return dt;
        }

        public static string[] Months = new string[]{"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio",
            "Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
        public static DateTime GetCurrentDate(int OpenHour)
        {
            //TEST, uncomment before rollout
            //DateTime dt =DateTime.UtcNow;
            //return new DateTime(2017, 4, 2).Date;
            //dt = dt.AddDays(-(dt - dt2).Days);
            //return dt;
            //Good
            DateTime dt =DateTime.UtcNow;
            if (dt.Hour < OpenHour)
                return dt.AddDays(-1);
            return dt.Date;
        }
        public static bool TimeIsGreaterThan(DateTime CurrentTime, int Hour, int Minute)
        {
            return CurrentTime.Date + new TimeSpan(Hour, Minute, 0) > CurrentTime ? true : false;
        }
        public static int GetWeekNumber(DateTime fromDate, DayOfWeek WeekStartDay)
        {
            // Get jan 1st of the year
            DateTime startOfYear = fromDate.AddDays(-fromDate.Day + 1).AddMonths(-fromDate.Month + 1);
            // Get dec 31st of the year
            DateTime endOfYear = startOfYear.AddYears(1).AddDays(-1);
            // ISO 8601 weeks start with Monday 
            // The first week of a year includes the first Thursday 
            // DayOfWeek returns 0 for sunday up to 6 for saterday
            int[] iso8601Correction = { 6, 7, 8, 9, 10, 4, 5 };
            int nds = fromDate.Subtract(startOfYear).Days + iso8601Correction[(int)startOfYear.DayOfWeek];
            int wk = nds / 7;
            switch (wk)
            {
                case 0:
                    // Return weeknumber of dec 31st of the previous year
                    return GetWeekNumber(startOfYear.AddDays(-1), WeekStartDay);
                case 53:
                    // If dec 31st falls before thursday it is week 01 of next year
                    if (endOfYear.DayOfWeek < DayOfWeek.Thursday)
                        return 1;
                    else
                        return wk;
                default: return wk;
            }
        }
        public static string GetFirstDayOfMonth(int Month, int Year)
        {
            string m = Month < 10 ? "0" + Month.ToString() : Month.ToString();
            return Year.ToString() + m + "01";
        }
        public static string GetLastDayOfMonth(int Month, int Year)
        {
            int DaysInMonth = System.DateTime.DaysInMonth(Year, Month);
            string m = Month < 10 ? "0" + Month.ToString() : Month.ToString();
            return Year.ToString() + m + (DaysInMonth < 10 ? "0" + DaysInMonth.ToString() : DaysInMonth.ToString());
        }
        public static DateTime GetPrevYearWeekDate(int WeekNumber, int Year, DayOfWeek WeekStartDay, DayOfWeek WeekDay)
        {
            DateTime WeekStart = GetStartDayFromWeekNumber(WeekNumber, Year, WeekStartDay);
            return WeekStart.AddDays((double)WeekDay - 1);
        }
        public static DateTime GetPrevMonthWeekDate(DateTime CurrentDate)
        {
            int Year, Month, Day;
            Year = CurrentDate.Year;
            if (CurrentDate.Month == 1)
            {
                Month = 12;
                Year--;
            }
            else
            {
                Month = CurrentDate.Month - 1;
            }
            Day = CurrentDate.Day;

            if (Month < 12 && System.DateTime.DaysInMonth(Year, Month) >= System.DateTime.DaysInMonth(Year, Month + 1))
                return new DateTime(Year, Month, Day);
            return new DateTime(Year, Month, System.DateTime.DaysInMonth(Year, Month));

        }
        public static DateTime GetStartDayFromDate(DateTime WeekDate, DayOfWeek WeekStartDay)
        {
            //int WeekNumber = GetWeekNumber(WeekDate, WeekStartDay);
            if (WeekDate.DayOfWeek == WeekStartDay) return WeekDate;
            var d = WeekDate;
            while (d.DayOfWeek != WeekStartDay)
                d = d.AddDays(-1);
            return d;
            //return GetStartDay(WeekNumber, WeekDate.Year, WeekStartDay);
        }
        public static DateTime GetStartDayFromWeekNumber(int WeekNumber, int Year, DayOfWeek WeekStartDay)
        {
            return GetStartDay(WeekNumber, Year, WeekStartDay);
        }
        public static DateTime GetStartDay(int WeekNumber, int Year, DayOfWeek WeekStartDay)
        {
            //if (WeekNumber == 0 || WeekNumber > 54)
            //    throw new ArgumentException("Week number must be between 1 and 54! (Yes, 54... Year 2000 had Jan. 1 on a Saturday plus 53 Sundays.)");

            //// January 1 -- first week.
            //DateTime firstDayInWeek = new DateTime(Year, 1, 1);
            //if (WeekNumber == 1) return firstDayInWeek;
            //// Get second week, starting on the following Sunday.      
            //do
            //{
            //    firstDayInWeek = firstDayInWeek.AddDays(1);
            //} while (firstDayInWeek.DayOfWeek != WeekStartDay);

            //if (WeekNumber == 2) return firstDayInWeek;
            //// Now get the Sunday of whichever week we're looking for.
            //return firstDayInWeek.AddDays((WeekNumber - 1) * 7);


            DateTime dt = new DateTime(Year, 1, 1).Date;




            while (dt.Year == Year)
            {
                if (GetWeekNumber(dt, WeekStartDay) == WeekNumber)
                    break;
                dt = dt.AddDays(1);
            }
            return dt;

        }
        public static DayOfWeek Day(Int32 DayString)
        {
            DayOfWeek DayOfWeekCon = DayOfWeek.Monday;
            switch (DayString)
            {
                case 1:
                    DayOfWeekCon = DayOfWeek.Monday;
                    break;
                case 2:
                    DayOfWeekCon = DayOfWeek.Tuesday;
                    break;
                case 3:
                    DayOfWeekCon = DayOfWeek.Wednesday;
                    break;
                case 4:
                    DayOfWeekCon = DayOfWeek.Thursday;
                    break;
                case 5:
                    DayOfWeekCon = DayOfWeek.Friday;
                    break;
                case 6:
                    DayOfWeekCon = DayOfWeek.Saturday;
                    break;
                case 7:
                    DayOfWeekCon = DayOfWeek.Sunday;
                    break;
            }

            return DayOfWeekCon;
        }
        public static int DayNumber(DayOfWeek Day)
        {
            int DayOfWeekNum = 0;
            switch (Day)
            {
                case DayOfWeek.Monday:
                    DayOfWeekNum = 1;
                    break;
                case DayOfWeek.Tuesday:
                    DayOfWeekNum = 2;
                    break;
                case DayOfWeek.Wednesday:
                    DayOfWeekNum = 3;
                    break;
                case DayOfWeek.Thursday:
                    DayOfWeekNum = 4;
                    break;
                case DayOfWeek.Friday:
                    DayOfWeekNum = 5;
                    break;
                case DayOfWeek.Saturday:
                    DayOfWeekNum = 6;
                    break;
                case DayOfWeek.Sunday:
                    DayOfWeekNum = 7;
                    break;
            }

            return DayOfWeekNum;
        }
        public static string GetDayAsString(DayOfWeek Day)
        {
            string strDay = string.Empty;
            switch (Day)
            {
                case DayOfWeek.Monday: strDay = "Lunes"; break;
                case DayOfWeek.Tuesday: strDay = "Martes"; break;
                case DayOfWeek.Wednesday: strDay = "Miércoles"; break;
                case DayOfWeek.Thursday: strDay = "Jueves"; break;
                case DayOfWeek.Friday: strDay = "Viernes"; break;
                case DayOfWeek.Saturday: strDay = "Sábado"; break;
                case DayOfWeek.Sunday: strDay = "Domingo"; break;
            }
            return strDay;
        }
        public static string GetMonthAsString(int Month)
        {
            return Months[Month - 1];
        }
        public static int GetMininumDays(int CurrentWeekNum)
        {
            DateTime cdt = GetCurrentDate(12);//TODO
            int wk = Util.GetWeekNumber(cdt, DayOfWeek.Monday);//Day
            if (wk == CurrentWeekNum)
                return DayNumber(cdt.DayOfWeek);
            return 0;
        }
        public static string GetLastUpdatedStr(DateTime UpdatedOn)
        {
            DateTime cd =DateTime.UtcNow;
            if (UpdatedOn.Date == cd.Date)
            {
                if (UpdatedOn.Hour == cd.Hour)
                {
                    if (UpdatedOn.Minute == cd.Minute)
                    {
                        int s = cd.Second - UpdatedOn.Second;
                        var sec = Math.Abs(s);
                        if (sec == 0) return "Ahora";
                        return "hace " + sec.ToString() + " segundo" + (s > 1 ? "s" : "");
                    }
                    int m = cd.Minute - UpdatedOn.Minute;
                    var min = Math.Abs(m);
                    if (min == 0) return "Ahora";
                    return "hace " + min.ToString() + " minuto" + (m > 1 ? "s" : "");
                }
                else
                {

                    int h = cd.Hour - UpdatedOn.Hour;
                    if (h == 1)
                    {
                        TimeSpan ts = cd.Subtract(UpdatedOn);
                        int m = ts.Minutes;
                        return "hace " + Math.Abs(m).ToString() + " minuto" + (m > 1 ? "s" : "");

                    }
                    return "hace " + Math.Abs(h).ToString() + " hora" + (h > 1 ? "s" : "");
                }
            }
            else
            {   //TODOF
                TimeSpan ts = cd.Subtract(UpdatedOn);
                if (ts.Days == 0)
                {
                    int hhh = (24 - ts.Hours);
                    return "hace " + hhh.ToString() + " hora" + (hhh > 1 ? "s" : "");
                }
                if (ts.Days == 1) return "hace " + ts.Hours + " horas";
                return "hace " + ts.Days + " días";
            }
        }

        public static int GetAge(DateTime dateOfBirth)
        {
            var today = DateTime.Today;

            var a = (today.Year * 100 + today.Month) * 100 + today.Day;
            var b = (dateOfBirth.Year * 100 + dateOfBirth.Month) * 100 + dateOfBirth.Day;

            return (a - b) / 10000;
        }
        #endregion
    }
}
