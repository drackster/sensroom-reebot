﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class Helper
    {
        public static List<CalendarDetailViewModel> GetWeekDays()
        {
            return new List<CalendarDetailViewModel>
            {
                new CalendarDetailViewModel { Id=0, Day = DayOfWeek.Sunday, Name = "Domingo" },
                new CalendarDetailViewModel { Id=1, Day = DayOfWeek.Monday, Name = "Lunes" },
                new CalendarDetailViewModel { Id=2, Day = DayOfWeek.Tuesday, Name = "Martes" },
                new CalendarDetailViewModel { Id=3, Day = DayOfWeek.Wednesday, Name = "Miércoles" },
                new CalendarDetailViewModel { Id=4, Day = DayOfWeek.Thursday, Name = "Jueves" },
                new CalendarDetailViewModel { Id=5, Day = DayOfWeek.Friday, Name = "Viernes" },
                new CalendarDetailViewModel { Id=6, Day = DayOfWeek.Saturday, Name = "Sábado" }

            };
        }
        public static string GetDays(List<CalendarDetailViewModel> Detail)
        {
            if (Detail.Count == 1)//solo un día
                return Detail[0].Name;

            //var days = Helper.GetWeekDays();

            //Validar que los días esten seguidos
            bool stringed = true;
            int cId = (int)Detail[0].Day;
            for (int i = 1; i < Detail.Count; i++)
            {
                int nId = (int)Detail[i].Day;
                if ((cId + 1) != nId)
                {
                    stringed = false;
                    break;
                }
                cId = nId;
            }
            if (!stringed) //salteados
            {
                string result = string.Empty;
                foreach (var d in Detail)
                    result += d.Name.Substring(0, 2) + ",";
                return result.Substring(0, result.Length - 1);
            }
            else//el primero y el ultimo
            {
                return Detail[0].Name + "-" + Detail[Detail.Count - 1].Name;
            }
        }

        public static string GetRequestText(string name, short status)
        {
            if (status == 4) return "Has autorizado a " + name;
            if (status == 5) return "Has rechazado a " + name;
            return name + " necesita tu autorización";
        }
        public static string GetMessageTitle(MessageType type, MessageStatus status)
        {
            string result = status == MessageStatus.Created ? "Nueva " : "";

            switch(type)
            {
                case MessageType.AuthRequest: result += "Solicitud"; break;
                //case MessageType.CustomerMessage: result += "Mensaje privado"; break;
                //case MessageType.AssetMessage: result += "Mensaje privado"; break;
                case MessageType.CustomerMessage: result += "Mensaje privado"; result = "Mensaje privado"; break;
                case MessageType.AssetMessage: result += "Mensaje privado"; result = "Mensaje privado"; break;
                case MessageType.Deposit: result += "Depósito"; break;
                case MessageType.Invitation: result += "Invitación pendiente de aceptar"; break;
                case MessageType.Rating: result += "Valoración"; break;
                case MessageType.AcceptedInvitation: result = "Invitación Aceptada"; break;
                case MessageType.RejectedInvitation: result = "Invitación Rechazada"; break;
                case MessageType.CanceledInvitation: result = "Invitación Cancelada"; break;
                default: return string.Empty;
            }
            return result;
        }
        public static string GetMessageText(MessageType type, MessageSender sender, 
                                    string name, bool isPush=false)
        {

            string newStr = isPush ? " Nueva" : "";
            string result = string.Empty;

            if (sender == MessageSender.Asset)
            {
                switch (type)
                {
                    case MessageType.AuthRequest: result = "Recibiste una" + newStr + " solicitud de " + name; break;
                    case MessageType.Deposit: result += "Recibiste un Depósito bancario"; break;
                    case MessageType.Invitation: result += "Recibiste una" + newStr + " invitación de " + name; break;
                    case MessageType.Rating: result += name + " te ha calificado"; break;
                    case MessageType.CanceledInvitation: result += name + " ha cancelado la invitación"; break;
                    default: return string.Empty;
                }
            }
            else if(sender == MessageSender.Customer)
            {
                switch (type)
                {
                    case MessageType.AuthRequest: result = "Tienes una " + newStr + " solicitud de " + name; break;
                    case MessageType.Invitation: result += "Enviaste una invitación a " + name; break;
                    case MessageType.AcceptedInvitation: result += name + " ha aceptado tu invitación" ; break;
                    case MessageType.RejectedInvitation: result +="A "+ name + " No le será posible asistir a la cita, pero no te preocupes, puedes invitar a alguien mas cuando lo desees sin tener que pagar el anticipo"; break;
                    default: return string.Empty;
                }
            }
            return result;
        }
        public static string GetMessageSenderName(MessageSender Sender, string CName, string AName, bool IsChat, MessageType MType)
        {

            if (IsChat)
                return MType != MessageType.CustomerMessage ? AName : CName;
            return Sender == MessageSender.Customer ? AName : CName;
            //switch (type)
            //{
            //    //case MessageType.Invitation: return CName;
            //    //case MessageType.Deposit: return CName;
            //    //case MessageType.CustomerMessage: return CName;
            //    //case MessageType.AssetMessage: return AName;
            //    case MessageType.Rating: return CName;
            //    default: return Receiver == MessageSender.Asset ? AName : CName;

            //}
            //          if (type == MessageType.Invitation)
            //              return CName;
            ////          if (type == MessageType.CustomerMessage)
            //              return CName;

            //                                        || y.MType == (int)MessageType.Invitation ?
        }
        public static string OrderStatusConverter(OrderStatus status)
        {
        //            public static string PendingConfirmation { get { return "EN ESPERA"; } }
        //public static string ConfirmInvitation { get { return "ACEPTAR INVITACION"; } }
        //public static string ConfirmedInvitation { get { return "ACEPTADA"; } }
        //public static string CanceledInvitation { get { return "CANCELADA *"; } }
        //public static string RejectedInvitation { get { return "RECHAZADA"; } }
        //public static string ExpiredInvitation { get { return "EXPIRADA"; } }

            switch(status)
            {
                case OrderStatus.Canceled: return "CANCELADA";
                case OrderStatus.Rejected: return "RECHAZADA";
                case OrderStatus.Expired: return "EXPIRADA";
                case OrderStatus.Closed: return "CERRADA";
                case OrderStatus.Confirmed: return "CONFIRMADA";
                case OrderStatus.Pending:return "EN ESPERA";
                default: return "N/D";
            }
        }

        public static AssetViewModel CloneAsset(AssetViewModel model)
        {
            AssetViewModel asset = new AssetViewModel();

            asset.DOB = model.DOB;
            asset.IsMan = model.IsMan;

            asset.Title = model.Title;
            asset.Description = model.Description;
            asset.IsOnline = model.IsOnline;
            asset.CalendarActive = model.CalendarActive;
            asset.IsActive = !model.IsInvalid;
            asset.CategoryId = model.CategoryId;
            asset.Precio = model.Precio;
            asset.PreferenceId = model.PreferenceId;
            asset.MaxDistance = model.MaxDistance;

            asset.BankId = model.BankId;
            asset.AccountType = model.AccountType;
            asset.AccountNum = model.AccountNum;
            asset.AccountNum2 = model.AccountNum2;
            asset.BankHolderName = model.BankHolderName;

            asset.Latitude = model.Latitude;
            asset.Longitude = model.Longitude;
            asset.LocationUpdate = model.LocationUpdate;
            asset.OnlineExpires = model.OnlineExpires;

            return asset;
        }
    }
}
