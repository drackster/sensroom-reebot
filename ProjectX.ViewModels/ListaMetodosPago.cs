﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{

    public class ListaMetodos
    {
        public List<MetodosPagoVM> ListaMetodosPago { get; set; }
    }


    public class MetodosPagoVM
    {
        public int id { get; set; }
        public string Nombre { get; set; }
        public bool  status { get; set; }
    }
}
