﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class NotificationUpdateModel
    {
        public long NotificationId { get; set; }
        public MessageStatus MsgStatus { get; set; }
    }
}
