﻿using System;
using System.Collections.Generic;
using SQLite;

namespace ProjectX.ViewModels
{
    public class CalendarViewModel
    {
        public CalendarViewModel()
        {
            Detail = new List<CalendarDetailViewModel>();
        }

        [PrimaryKey]
        public long Id { get; set; }
        public string Name { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public TimeSpan From { get; set; }
        public TimeSpan To { get; set; }

        [Ignore]
        public List<CalendarDetailViewModel> Detail { get; set; }
        public string Days { get; set; }
        public string Times
        {
            get
            {
                return From.ToString("hh\\:mm") + "-" + To.ToString("hh\\:mm");
            }
        }
        
    }
}
