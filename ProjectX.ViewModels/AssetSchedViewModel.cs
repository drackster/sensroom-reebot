﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class AssetSchedViewModel
    {
        public string Name { get; set; }
        public DateTime Available { get; set; }
        public DayOfWeek StartDay { get; set; }
        public string StartDayStr { get; set; }
        public string StartTime { get; set; }
        public DayOfWeek EndDay { get; set; }
        public string EndDayStr { get; set; }
        public string EndTime { get; set; }
       
    }
}
