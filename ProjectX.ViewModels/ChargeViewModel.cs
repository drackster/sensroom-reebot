﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ChargeViewModel
    {
        public string ChargeId { get; set; }
        public OrderStatus ChargeStatus { get; set; }
        public string OrderId { get; set; }

        public long? Reference { get; set; }
        public string MerchantId { get; set; }
        public string ProviderId { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        //public long OrderId { get; set; }
        //public long PaymentMethodId { get; set; }
        //public string DeviceTokenId { get; set; }
    }
}
