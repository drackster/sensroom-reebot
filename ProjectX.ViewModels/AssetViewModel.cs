﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;
namespace ProjectX.ViewModels
{
    public class AssetViewModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public int ItemIndex { get; set; }//La posicion fisica en la lista (visual)
        public string Name { get; set; }
        public DateTime? DOB { get; set; }
        public bool? IsMan { get; set; } //0 = Woman, 1 = Man
        public DateTime JoinDate { get; set; }
        public int Age
        {
            get
            {
                if(DOB.HasValue)
                return Util.GetAge(DOB.Value);
                return 0;
            }
        }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool IsOnline { get; set; }
        public bool MainLoaded { get; set; }
        
        public bool IsActive { get; set; }
        //Validations
        public bool IsInvalid
        {
            get { return ProfileInvalid || PhotosInvalid || PreferencesInvalid; }
        }
        public bool ProfileInvalid
        {
            get { return string.IsNullOrEmpty(Title) || string.IsNullOrEmpty(Description) || Age < 18 ? true : false; }
        }
        public bool PhotosInvalid
        {
            get { return Images == null || Images.Count == 0 ? true : false; }
        }
        public bool PreferencesInvalid
        {
            get { return CategoryId.HasValue && PreferenceId.HasValue && IsMan.HasValue ? false : true; }
        }
        public bool CalendarActive { get; set; }
        public int? CategoryId { get; set; }
        public string CategoryName { get; set; }
        public decimal Precio { get; set; } 
        public int? PreferenceId { get; set; }
        public string PreferenceName { get; set; }

        [Ignore]
        public List<ACategoryTierViewModel> Tiers { get; set; }
        public int MaxDistance { get; set; }

        //Images
        [Ignore]
        public List<AssetImageViewModel> Images { get; set; }

        public string CoverImage
        {
            get
            {
                if (Images == null)
                {
                    return null;
                }
                    

                var cover = Images.Where(x => x.IsProfile).FirstOrDefault();

                if (cover != null)
                {
                    return cover.Id;
                }

                //  return Images[0].Id;
                return "bdc0dba5-9e19-48c9-a91d-8aa40eac2538";
              //  return Images != null && Images.Count > 0 ? Images[0].Id : null;
            }
        }

        [Ignore]
        public List<AServiceViewModel> Services { get; set; }

        //[Ignore]
        //public List<AssetSchedViewModel> Scheds { get; set; }

        [Ignore]
        public List<CalendarViewModel> Calendars { get; set; }
        //Evals

        [Ignore]
        public ReviewViewModel LastReview { get; set; }
        public int ReviewCount { get; set; }
        public double GlobalRating { get; set; }
        public bool HasReviews { get { return ReviewCount > 0; } }

        //Geolocation
        public string DistanceFrom { get; set; } //kms o mts
        public double Distance { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public DateTime? LocationUpdate { get; set; }

        #region BankInfo
        public int? BankId { get; set; }
        public int? AccountType { get; set; }
        public string BankName { get; set; }
        public string AccountNum { get; set; }
        public string AccountNum2 { get; set; }
        public string BankHolderName { get; set; }

        #endregion
        public DateTime? OnlineExpires { get; set; }
        //todo Height, Waist, Bracup, etc

    }
}
