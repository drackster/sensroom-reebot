﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class XSortByViewModel
    {
        public SortType sortType { get; set; }
        public string Name { get; set; }
        public bool IsSelected { get; set; }
    }
}
