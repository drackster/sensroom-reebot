﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class ACategoryTierViewModel
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public string Name { get; set; }
        public int NumHours { get; set; }
        public string Description { get; set; }
        public decimal Cost { get; set; }
        public decimal reserva { get; set; }
        public bool IsSelected { get; set; }
        public bool IsChanged { get; set; }
        public decimal precio { get; set; }
    }
}
