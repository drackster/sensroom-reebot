﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class OrderStatusViewModel
    {
        public OrderStatus orderStatus { get; set; }
        public DepositStatus depositStatus { get; set; }
    }
}
