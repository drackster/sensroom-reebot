﻿using System;

namespace ProjectX.ViewModels
{
    public class ConfigViewModel
    {
        public bool NeedsUpdate { get; set; }
        public string UpdateMsg { get; set; }
        public int ListNumRecords { get; set; }
        public decimal CurrentVersion { get; set; }
        public decimal NewVersion { get; set; }
        public string OS { get; set; }
        public string AppStoreUrl { get; set; }
        public DateTime LastUpdate { get; set; }
        public int CartExpireHours { get; set; }
        public double TaxAmount { get; set; }
        public int DeliveryNumDays { get; set; }

    }
}