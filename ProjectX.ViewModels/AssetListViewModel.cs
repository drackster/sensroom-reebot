﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProjectX.ViewModels
{
    public class AssetListViewModel
    {
        public ObservableCollection<AssetViewModel> Assets { get; set; }
        public int AssetCount { get { return Assets == null ? 0 : Assets.Count; } }
        public int TotalCount { get; set; }
        

    }
}
