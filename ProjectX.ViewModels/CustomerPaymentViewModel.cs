﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class CustomerPaymentViewModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string ProviderId { get; set; }
        public string CreditCardNumber { get; set; }
        public string NameOnCard { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public DateTime ExpirationDate { get; set; }
        public string CVC { get; set; }
        public string CreditCardType { get; set; }
        public bool IsSelected { get; set; }
        public bool Success { get; set; }
        public DateTime CreatedOn { get; set; }

    }
}
