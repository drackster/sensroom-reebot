﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class SearchModel
    {
        [PrimaryKey]
        public string Id { get; set; }
        public int? CategoryId { get; set; }
        public int? PreferenceId { get; set; }
        public int MinAge { get; set; }
        public int MaxAge { get; set; }
        public int MaxDistanceRadius { get; set; }//Meters
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public DateTime? SelectedDateTime { get; set; }
        public OrderType orderType { get; set; }
        public SortType sortType { get; set; }
        public int PageIndex { get; set; }
        public int Hours { get; set; }
        public decimal Precio { get; set; }
    }
}
