﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{

    public class OrderListViewModel
    {
        public List<OrderViewModel> Orders { get; set; }
        public double NewRating { get; set; }
    }
    public class OrderViewModel
    {
        public long Id { get; set; }
        public long AssetId { get; set; }
        public AssetViewModel Asset { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        //public UserViewModel Customer { get; set; }
        public ACategoryTierViewModel CategoryTier { get; set; }

        public OrderStatus Status { get; set; }
        public string StatusName { get; set; }
        public decimal? DownPayment { get; set; }
        public bool HasDownPayment { get { return DownPayment.HasValue; } }
        public decimal TotalCost { get; set; }
        public decimal Cost { get { return DownPayment.HasValue ? DownPayment.Value : TotalCost; } }
        public decimal TotalDeposit { get; set; }
        public decimal TotalComision { get; set; }
        public decimal Precio { get; set; }
        public bool HasCashRecollected { get { return CashRecollected > 0; } }
        public decimal CashRecollected { get; set; }
        public ComisionStatus comisionStatus { get; set; }
        public decimal Tax { get { return Cost * 0.16M; } }
        public OrderType RequestType { get; set; }
        public DateTime? EventDateTime { get; set; }
        public DateTime? EventEnd { get; set; }
        public int ModeType { get; set; }

        public double CLatitude { get; set; }
        public double CLongitude { get; set; }
        public double ALatitude { get; set; }
        public double ALongitude { get; set; }
        public long AddressId { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string Comments { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? ConfirmedOn { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public DateTime? Arrival { get; set; }
        public long? PaymentMethodId { get; set; }//En caso de pagos con tarjeta, Id tarjeta
        public PaymentMethod paymentMethod { get; set; }
        public string PaypalTransId { get; set; }
        //public string ChargeId { get; set; }
        //public string ProviderId { get; set; }
        public DateTime? AReviewedOn { get; set; }
        public DateTime? CReviewedOn { get; set; }
        public DateTime? ADeclinedOn { get; set; }
        public DateTime? CDeclinedOn { get; set; }
        public double CustomerRating { get; set; }
        public bool HasRating { get; set; }
        public int Week { get; set; }
        public DateTime ServiceEnd { get; set; }
        public string ciudad { get; set; }

    }
}
