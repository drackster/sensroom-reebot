﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite;

namespace ProjectX.ViewModels
{
    public class UserViewModel
    {
        [PrimaryKey]
        public long Id { get; set; }
        public string UserName { get; set; }
        public bool IsAuthenticated { get; set; }

        [Ignore]
        public SearchModel SearchFilters { get; set; }//filtros
       
    }
}
