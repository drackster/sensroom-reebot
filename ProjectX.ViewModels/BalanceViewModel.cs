﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class BalanceViewModel
    {
        public TransactionWeek Week { get; set; }
        public decimal TotalCost { get; set; }
        public decimal TotalEarnings { get; set; }
        public decimal TotalComission { get; set; }
        public decimal TotalDeposit { get; set; }
        public int Transactions { get; set; }

    }
}
