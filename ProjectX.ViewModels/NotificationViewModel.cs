﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class NotificationViewModel
    {
        public long Id { get; set; }
        public MessageType MType { get; set; }
        public MessageSender MReceiver { get; set; }
        public string MTypeStr { get; set; }
        public string MessageText { get; set; }
        public MessageStatus Status { get; set; }
        public int MessageStatusId { get; set; }
        public long? AssetId { get; set; }
        public long? CustomerId { get; set; }
        public string SenderName { get; set; }
        public string TierName { get; set; }
        public DateTime? EventDateTime { get; set; }
        public long? OrderId { get; set; }
        public string RequestId { get; set; }
        public short RequestActivationStatus { get; set; }
        public string BGColor
        {
            get
            {
                switch (Status)
                {
                    case MessageStatus.Read: return "#ffffff";
                    default: return "#D3D3D3";
                }
            }
        }
        public DateTime CreatedOn { get; set; }
        public InvitationViewModel invitation { get; set; }
        //public RegisterViewModel request { get; set; }
        public bool ShowRatingIcon
        {
            get { return MType == MessageType.Rating; }
        }
        public bool ShowMessageIcon
        {
            get { return MType == MessageType.CustomerMessage || MType == MessageType.AssetMessage; }
        }
        public bool ShowRequestIcon
        {
            get { return MType == MessageType.AuthRequest; }
        }
        public bool ShowInviteIcon
        {
            get { return MType == MessageType.Invitation; }
        }
        public bool ShowOkIcon
        {
            get { return MType == MessageType.AcceptedInvitation; }
        }
        public bool ShowCanceledIcon
        {
            get { return MType == MessageType.RejectedInvitation || 
                    MType == MessageType.CanceledInvitation; }
        }

    }
}
