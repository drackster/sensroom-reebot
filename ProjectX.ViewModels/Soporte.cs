﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectX.ViewModels
{
    public class Soporte
    {
        public string Usuario { get; set; }
        public string Contacto { get; set; }
        public string Comentario { get; set; }
        public string Respuesta { get; set; }
    }
}
